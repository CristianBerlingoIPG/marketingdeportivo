import os
import sys
import shutil
import logging
import argparse
import itertools
import configparser

from datetime import datetime, timedelta
from typing import Union, List, Tuple

import utils_cris as uc
from utils_cris import LoggingType as lt
from drawer_detected_object import DrawerDetectedObject
from logo_detection_persistence import LogoDetectionPersistence as ldp, ImplementationIntervalsValidatonRule as iivr

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil
from DAL.model.interval import Interval
from DAL.model.interval import ItemRealTime
from DAL.model.report.stats_process_detections_to_intervals import StatsProcessDetectionsToIntervals

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=15)
parser.add_argument('-pov',
                    '--path_output_video',
                    help='Ruta de salida de los videos generados.'
                         'Por defecto /home/enzoscocco/Documents/object_detection/data_detection/intervals',
                    default='/home/enzoscocco/Documents/object_detection/data_detection/intervals')
parser.add_argument('-fr',
                    '--frame_rate',
                    help='Tasa de frames de los videos generados para cada intervalo. Por defecto 5',
                    type=int,
                    default=5)
parser.add_argument('-csi',
                    '--capture_servers_ips',
                    help='Servers de captura de los que se encargara el script. Por default se toman todos los servers.'
                         'Se puede especificar servers concretos pasando sus IPs separadas por coma y sin espacio, por'
                         'ejemplo 10.228.8.138,10.228.8.129,10.228.69.54',
                    default='')
parser.add_argument('-aocr',
                    '--apply_ocr',
                    help='Boolean que indica si se debe aplicar el filtro OCR o no. Por defecto True',
                    type=lambda b: b.lower() in ['1', 'true'],
                    default=True)
parser.add_argument('-fn',
                    '--first_newest',
                    help='Boolean que indica se se empieza a procesar desde los archivos mas recientes al contrario del'
                         'comportamiento normal que es procesar desde los archivos mas antiguos.'
                         'Por defecto False',
                    type=lambda b: b.lower() in ['1', 'true'],
                    default=False)
parser.add_argument('-pod',
                    '--path_output_detection',
                    help='Ruta base donde se guardaran las imagenes de las detecciones que se traigan de los '
                         'servidores. Esta ruta es una ruta base, se seguira considerando las carpetas relacionadas '
                         'co el vehiculo y la fecha. '
                         'Por defecto /home/enzoscocco/Documents/object_detection/data_detection',
                    default='/home/enzoscocco/Documents/object_detection/data_detection')

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
path_output_video = args.path_output_video
frame_rate = args.frame_rate
capture_servers_ips = args.capture_servers_ips.split(',') if args.capture_servers_ips != '' else []
apply_ocr = args.apply_ocr
first_newest = args.first_newest
path_output_detection = args.path_output_detection


def list_interval_by_validation_rule_to_by_ocr(
        libvr: Union[List[Interval], List[Tuple[Interval, List[ItemRealTime]]]]
) -> List[Tuple[Interval, List[ItemRealTime], int]]:
    # interval                                                         -> intervals_by_recognition
    # Union[List[Interval], List[Tuple[Interval, List[ItemRealTime]]]] -> List[Tuple[Interval, List[ItemRealTime], int]]
    locr = []
    for item in libvr:
        if type(item) == Interval:
            locr.append((item, [], 0))
        if type(item) == tuple and len(item) == 2:
            if type(item[0]) == Interval and type(item[1]) == list:
                locr.append((item[0], item[1], 0))

    return locr


########################################################################################################################
# logging configuration
datetime_log = datetime.now()
template_general_log_path = 'logs/detections_to_intervals/{}/{}/info/detections_to_intervals__{}.log'
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################

try:
    configs = configparser.ConfigParser()
    configs.read(config_path)

    config_data_detection = configs['data_detection']
    database_config = configs['Database']
    database_local_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    template_foldersname_frames_detected_datetime = \
        config_data_detection['template_foldersname_frames_detected_datetime']
    template_foldersname_frames_detected = config_data_detection['template_foldersname_frames_detected']
    path_config_csv = config_data_detection['path_config_csv']
    key_configuration_file = config_data_detection['key_configuration_file']
    csv_pattern = config_data_detection['csv_pattern']

    driver = database_config['driver']
    server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None
    schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
    table_history = database_config['table_history'] if 'table_history' in database_config else None
    sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

    # capture_servers = ssh_scp_configs['capture_servers'].split(',')
    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

    larg = []
    [larg.extend(a) for a in args._get_kwargs()]
    logging_message_configs = '\nParameters:' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}'.format(*larg)

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              server,
                                              database,
                                              uid,
                                              pwd)

    logging_message_configs += '\nConfigurations detection:' \
                               '\n\tcsv_pattern: {}'.format(csv_pattern)

    logging_message_configs += '\nConfigurations database:' \
                               '\n\tDriver: {}' \
                               '\n\tServer: {}' \
                               '\n\tDatabase: {}' \
                               '\n\tUID: {}' \
                               '\n\tSchema history: {}' \
                               '\n\tTable history: {}' \
                               '\n\tConnection string: {}'.format(driver,
                                                                  server,
                                                                  database,
                                                                  uid,
                                                                  schema_history,
                                                                  schema_history,
                                                                  cssl.replace(pwd, '*******'))

    logging_message_configs += '\nConfigurations SSH_SCP' \
                               '\n\tusername: {}\n'.format(username)

    uc.print_and_logging(logging_message_configs)

    dal = DAL(connection_string=cssl)

    server_host_ip = uc.get_ip()
    server_process = dal.get_server_by_ip(server_host_ip)
    country = dal.get_country_by_id(server_process.country_id)
    client = dal.get_clients()[0]
    # solo por que puedo asegurar que esta regla aplica a todos las detecciones del historico
    intervals_validation_rule = dal.get_intervals_validation_rule_by_id(1)
    capture_servers = list(dal.get_servers(country.id).values())
    if len(capture_servers_ips) > 0:
        capture_servers = [cs for cs in capture_servers if cs.ip in capture_servers_ips]

    uc.print_and_logging('\nServidorers de captura que seran procesados '
                         'segun el valor del parametro --capture_servers_ips:'
                         '\n\t{}'.format('\n\t'.join([str(cs) for cs in capture_servers])))

    vehicles = dal.get_vehicles_associated_to_source_signals(verbose=True)
    logos = dal.get_logos_by_client(client.id)

    dal.clear_csv_being_processing_by_server(server_process.id)
    while True:
        for server in capture_servers:
            try:
                uc.print_and_logging('\n\nServer: {}'.format(server))

                ssh = SshUtil(server.ip, username, password)

                csv_start_time = datetime.now().replace(minute=0, second=0, microsecond=0)

                csv_already_processed = [hcp.path for hcp
                                         in dal.get_history_csv_already_processed_by_server_id(server.id)]

                csvs_item_real_time = sorted(
                    [
                        f for f
                        in ssh.get_find(server.data_detection_path, csv_pattern)
                        if f not in csv_already_processed
                        and ldp.get_datetime_from_filename_csv(os.path.basename(f)) != csv_start_time
                    ],
                    key=lambda _csv: ldp.get_datetime_from_filename_csv(os.path.basename(_csv)),
                    reverse=first_newest
                )

                qty_csv = len(csvs_item_real_time)
                for n_csv, _csv in enumerate(csvs_item_real_time):

                    csv_date_emission = ldp.get_datetime_from_filename_csv(os.path.basename(_csv))
                    csv_directory = os.path.dirname(_csv)
                    st_process_file = uc.print_start_time(
                        '\n\n\nInicio de procesamiento del archivo [{}/{}] {} {}'.format(n_csv + 1,
                                                                                         qty_csv,
                                                                                         _csv,
                                                                                         '{}'),
                        to_log=True
                    )

                    is_csv_already_processed_or_being_processing = \
                        dal.is_csv_already_processed_or_being_processing(_csv, verbose=True)
                    if not is_csv_already_processed_or_being_processing:

                        csv_vehicle_id = ldp.get_vehicle_id_from_filename_csv(_csv)
                        csv_source_signal_id = ldp.get_source_signal_id_from_filename_csv(_csv)
                        try:
                            capture_server = [sv for sv in capture_servers
                                              if sv.is_the_owner_of_the_source_signal(csv_source_signal_id)][0]
                        except:
                            uc.print_and_logging('El source_signal_id {} no esta relacionado con ningun server de '
                                                 'captura el archivo no sera procesado'
                                                 .format(csv_source_signal_id))
                            # error_csv_paths.append(path_file_raw_data)

                            continue

                        vehicle = [v for v in vehicles if v.id == csv_vehicle_id]
                        if len(vehicle) > 0:
                            vehicle = vehicle[0]
                        else:
                            uc.print_and_logging('el vehicle_id {} no esta relacionado con ningun vehiculo, '
                                                 'el archivo no sera procesado'.format(csv_vehicle_id))
                            # error_csv_paths.append(path_file_raw_data)

                            continue

                        ################################################################################################
                        # Actualizando estado de CSV como en procesamiento por parte del server intermedio
                        ################################################################################################
                        try:
                            uc.print_and_logging('Actualizando el CSV ({}) que esta siendo procesado '
                                                 'por el server intermedio'.format(_csv))
                            dal.update_csv_being_processing_by_server(_csv, server_process.id, verbose=True)
                        except Exception as ex_update_csv_being_processing_by_server:
                            uc.print_and_logging('Ocurrio una excepcion durante actualizacion del CSV '
                                                 'procesado actualmente:'
                                                 '\n{}'.format(ex_update_csv_being_processing_by_server),
                                                 logging_type=lt.EXCEPTION)
                            uc.print_and_logging('Al no poder marcar como en proceso por el server actual el CSV '
                                                 '{} no sera procesado'.format(_csv))

                            continue

                        st_reading_csv = uc.print_start_time('Inicio de descarga y lectura de archivo CSV {}'
                                                             .format(_csv),
                                                             to_log=True)
                        path_output_scp_file_csv = os.path.join(path_output_detection, _csv[_csv.find('/vh') + 1:])
                        ssh.get_file_scp(
                            _csv,
                            path_output_scp_file_csv,
                            create_path_output=True
                        )

                        ################################################################################################
                        # Leyendo items desde el csv
                        ################################################################################################
                        items_real_time = ldp.get_items_real_from_csv(path_output_scp_file_csv)
                        et_reading_csv = uc.print_end_elapsed_time(st_reading_csv)

                        qty_items_real_time = len(items_real_time)
                        uc.print_and_logging('\tCantidad de items a procesar: {}'.format(qty_items_real_time))

                        csv_source_signal = capture_server.get_source_signal_by_id(int(csv_source_signal_id))

                        stat_process = StatsProcessDetectionsToIntervals(
                            date=st_process_file,
                            country_id=country.id,
                            country=country.name,
                            server_process_id=server_process.id,
                            server_process=server_process.hostname,
                            csv=_csv,
                            server_detection_id=server.id,
                            server_detection=server.hostname,
                            vehicle_id=vehicle.id,
                            vehicle=vehicle.name,
                            source_signal_id=csv_source_signal.id,
                            date_emission=csv_date_emission,
                            time_reading_csv=et_reading_csv,
                            items_per_csv=qty_items_real_time,
                            interval_validation_rule_id=intervals_validation_rule.id)

                        ################################################################################################
                        # Obteniendo los intervalos validos por regla de negocio
                        ################################################################################################
                        st_elapsed_time_validation_rule = uc.print_start_time(
                            '\tInicio de de obtencion de intervalos al ejecutar la regla de validacion {}',
                            to_log=True)
                        intervals = intervals_validation_rule.get_valid_intervals(items_real_time,
                                                                                  [csv_source_signal],
                                                                                  logos,
                                                                                  path_output_scp_file_csv,
                                                                                  return_items_real_time=True,
                                                                                  server_host_ip=server_host_ip)

                        path_output_scp_images = os.path.dirname(path_output_scp_file_csv)
                        for _interval in intervals:
                            if type(_interval) == tuple:
                                if len(_interval) == 2:
                                    if type(_interval[1]) == list:
                                        for _irt in _interval[1]:
                                            _irt.path = os.path.join(path_output_scp_images,
                                                                     os.path.basename(_irt.path))

                        et_validation_rule = uc.print_end_elapsed_time(st_elapsed_time_validation_rule,
                                                                       to_log=True)
                        qty_intervals_by_validation_rule = len(intervals)
                        qty_items_for_intervals_by_validation_rule = sum([len(irt_vr) for _, irt_vr in intervals])

                        stat_process.avg_detection_score_items_validation_rule, \
                            stat_process.detections_below_avg_score_validation_rule = \
                            Interval.get_avg_detection_score_items_and_detections_below(
                                list(itertools.chain(*[irt_vr for _, irt_vr in intervals]))
                            )

                        stat_process.intervals_by_validation_rule = qty_intervals_by_validation_rule
                        stat_process.items_for_intervals_by_validation_rule = qty_items_for_intervals_by_validation_rule
                        stat_process.time_validation_rule = et_validation_rule

                        uc.print_and_logging('\tCantidad de intervalos encontrados segun regla de negocio: {}'
                                             .format(qty_intervals_by_validation_rule))
                        uc.print_and_logging('\tCantidad de items relacionados a los intervalos encontrados segun '
                                             'regla de negocio: {}'
                                             .format(qty_items_for_intervals_by_validation_rule)
                                             )

                        if qty_intervals_by_validation_rule > 0:
                            st_elapsed_time_download_images = uc.print_start_time(
                                '\tInicio de descarga de imagenes de los intervalos: {}',
                                to_log=True)
                            try:

                                ########################################################################################
                                # Obtencion de imagenes para filtro OCR
                                ########################################################################################
                                dal.get_files_scp_recursive(password,
                                                            '{}:{}'.format(capture_server.ip,
                                                                           csv_directory),
                                                            path_output_scp_images[:-3],
                                                            create_path_output=True)

                            except Exception as ex_get_files_scp_recursive:
                                stat_process.successful_process = 0
                                stat_process.type_of_error = str(type(ex_get_files_scp_recursive)).replace("'", '')
                                stat_process.error_description = str(ex_get_files_scp_recursive.args).replace("'", '')

                                stat_process.time_total_process = datetime.now() - st_process_file

                                dal.insert_stat_process_detections(stat_process, verbose=True)

                                continue

                            et_download_images = uc.print_end_elapsed_time(
                                st_elapsed_time_download_images,
                                message='\tFinalizacion de descarga de imagenes {} tiempo transcurrido {}',
                                to_log=True
                            )

                            stat_process.time_download_images = et_download_images

                            qty_images_downloaded = len([file for file
                                                         in os.listdir(path_output_scp_images)
                                                         if file.endswith('.jpg')]) \
                                if os.path.isdir(path_output_scp_images) else 0

                            if qty_images_downloaded == 0:
                                uc.print_and_logging('No se ha descargado ninguna imagen')
                                ssh = SshUtil(capture_server.ip, username, password)
                                try:
                                    images_remote_server = [file for file
                                                            in ssh.get_ls(csv_directory)
                                                            if file.endswith('.jpg')
                                                            ]
                                except Exception as ex_get_ls:
                                    stat_process.successful_process = 0
                                    stat_process.type_of_error = str(type(ex_get_ls)).replace("'", '')
                                    stat_process.error_description = str(ex_get_ls.args)[1:-2].replace("'", '')

                                    dal.insert_stat_process_detections(stat_process, verbose=True)

                                    continue

                                if len(images_remote_server) == 0:
                                    uc.print_and_logging('No existen imagenes de detecciones en la carpeta {} '
                                                         'del server {} '
                                                         'ip {}'.format(csv_directory,
                                                                        capture_server.hostname,
                                                                        capture_server.ip))

                            else:
                                st_elapsed_time_ocr = uc.print_start_time('Inicio de ejecucion de filtro OCR {}',
                                                                          to_log=True)

                                ########################################################################################
                                # Obtencion de intervalos por OCR
                                ########################################################################################
                                intervals_by_recognition = iivr.static_execute_filter_ocr(intervals,
                                                                                          logos,
                                                                                          process_all=True) \
                                    if apply_ocr else list_interval_by_validation_rule_to_by_ocr(intervals)

                                qty_intervals_by_ocr = len(intervals_by_recognition)
                                uc.print_and_logging(
                                    '\t[apply_ocr: {}] '
                                    'Cantidad de intervalos que pasaron filtro OCR: {}'.format(apply_ocr,
                                                                                               qty_intervals_by_ocr)
                                )
                                et_ocr = uc.print_end_elapsed_time(
                                    st_elapsed_time_ocr,
                                    message='\tFinalizacion de ejecucion de filtro OCR {}  tiempo transcurrido {}',
                                    to_log=True
                                )

                                qty_items_for_intervals_by_ocr = 0
                                qty_processed_items_by_ocr = 0
                                for _, irt_ocr, qty_items_processed_ocr in intervals_by_recognition:
                                    qty_items_for_intervals_by_ocr += len(irt_ocr)
                                    qty_processed_items_by_ocr += qty_items_processed_ocr

                                stat_process.intervals_by_ocr = qty_intervals_by_ocr
                                stat_process.items_for_intervals_by_ocr = qty_items_for_intervals_by_ocr
                                stat_process.processed_items_by_ocr = qty_processed_items_by_ocr
                                stat_process.time_ocr = et_ocr

                                stat_process.avg_detection_score_items_ocr, \
                                    stat_process.detections_below_avg_score_ocr = \
                                    Interval.get_avg_detection_score_items_and_detections_below(
                                        list(itertools.chain(*[irt_ocr for _, irt_ocr, __ in intervals_by_recognition]))
                                    )

                                st_generate_video = uc.print_start_time(
                                    'Inicio de generacion de videos: {}',
                                    to_log=True)

                                for n_interval_ocr, (interval_ocr, irt_for_interval_ocr, _) \
                                        in enumerate(intervals_by_recognition):

                                    ####################################################################################
                                    # Creacion de las imagenes con los objetos detectados dibujados
                                    ####################################################################################
                                    paths_images_with_detected_objects = DrawerDetectedObject \
                                        .static_write_images_with_detected_objects(irt_for_interval_ocr,
                                                                                   logos)
                                    ####################################################################################
                                    # Ejecucion de FFMPEG para generar el video
                                    ####################################################################################
                                    try:
                                        path_input = os.path.dirname(irt_for_interval_ocr[0].path)
                                        real_path_output_video = os.path.join(
                                            path_output_video,
                                            'generation_{}/emission_{}'.format(
                                                datetime.now().strftime('%Y%m%d'),
                                                irt_for_interval_ocr[0].date.strftime('%Y%m%d'))
                                        )
                                        uc.create_folders(os.path.join(real_path_output_video,
                                                                       interval_ocr.get_video_name()),
                                                          path_base='/')
                                        uc.print_and_logging('[{}/{}] generate_video: path_input={}, path_output={}, '
                                                             'frame_rate={}'
                                                             .format(n_interval_ocr + 1,
                                                                     qty_intervals_by_ocr,
                                                                     path_input,
                                                                     real_path_output_video,
                                                                     frame_rate))
                                        interval_ocr.path_video = interval_ocr.generate_video(path_input,
                                                                                              real_path_output_video,
                                                                                              frame_rate=frame_rate)
                                    except ValueError as ffmpeg_exc:
                                        # error_csv = True
                                        uc.print_and_logging('Ocurrio una excepcion '
                                                             'durante la generacion del video del intervalo:\n',
                                                             logging_type=lt.EXCEPTION)

                                        stat_process.successful_process = 0
                                        stat_process.type_of_error = str(type(ffmpeg_exc)).replace("'", '')
                                        stat_process.error_description = str(ffmpeg_exc.args)[1:-2].replace("'", '')
                                        stat_process.time_total_process = datetime.now() - st_process_file

                                        dal.insert_stat_process_detections(stat_process, verbose=True)

                                        continue

                                ########################################################################################

                                et_generate_video = uc.print_end_elapsed_time(
                                    st_generate_video,
                                    message='Finalizacion de    la generación de videos {} tiempo transcurrido {}',
                                    to_log=True
                                )

                                stat_process.time_generate_video = et_generate_video

                                if qty_intervals_by_ocr > 0:
                                    st_elapsed_time_insert_intervals = uc.print_start_time(
                                        '\tInicio de insercion de intervalos en la base de datos: {}',
                                        to_log=True)
                                    try:
                                        dal.insert_intervals([ibc for ibc, _, __ in intervals_by_recognition],
                                                             verbose=True)
                                    except Exception as ex_insert_intervals:
                                        uc.print_and_logging('Ocurrio una excepcion durante la insercion de los '
                                                             'intervalos del csv {}'
                                                             '\ndel server de captura {} ip{}:'
                                                             '\n{}'.format(_csv,
                                                                           capture_server.hostname,
                                                                           capture_server.ip,
                                                                           ex_insert_intervals),
                                                             logging_type=lt.EXCEPTION)

                                        stat_process.successful_process = 0
                                        stat_process.type_of_error = str(type(ex_insert_intervals)).replace("'", '')
                                        stat_process.error_description = str(ex_insert_intervals.args)[1:-2]\
                                            .replace("'", '')
                                        stat_process.time_total_process = datetime.now() - st_process_file

                                        dal.insert_stat_process_detections(stat_process, verbose=True)

                                        continue

                                    et_insert_intervals = uc.print_end_elapsed_time(
                                        st_elapsed_time_insert_intervals,
                                        to_log=True
                                    )

                                else:
                                    et_insert_intervals = timedelta()

                                stat_process.time_insert_intervals = et_insert_intervals

                                try:
                                    st_insert_history_csv_processed = datetime.now()
                                    dal.insert_history_csv_processed(path=_csv,
                                                                     client_id=client.id,
                                                                     vehicle_id=csv_vehicle_id,
                                                                     source_signal_id=csv_source_signal_id,
                                                                     server_id=capture_server.id,
                                                                     process_interval=1,
                                                                     process_ocr=1,
                                                                     process_video_intervals=1,
                                                                     verbose=True)

                                    stat_process.time_insert_history_csv_processed = \
                                        datetime.now() - st_insert_history_csv_processed

                                    try:
                                        ################################################################################
                                        # Borrado de archivos ya procesados en server de detección
                                        ################################################################################

                                        if ssh.file_exists(csv_directory):
                                            uc.print_and_logging('Eliminando archivos del server de deteccion {} IP {}'
                                                                 .format(capture_server.hostname, capture_server.ip))
                                            st_delete_remote_files = uc.print_start_time(
                                                'Iniciando borrado de archivos del server de '
                                                'deteccion {} IP {}'.format(capture_server.hostname,
                                                                            capture_server.ip))
                                            ssh.rm_folder(csv_directory, verbose=True)

                                            stat_process.time_delete_remote_files = uc.print_end_elapsed_time(
                                                st_delete_remote_files
                                            )

                                    except Exception as ex_delete_files_capture_server:
                                        uc.print_and_logging(
                                            'Ocurrio una excepcion durante el borrado '
                                            'de archivos en el server de deteccion {} '
                                            'IP {}:\n{}'.format(capture_server.hostname,
                                                                capture_server.ip,
                                                                ex_delete_files_capture_server),
                                            logging_type=lt.EXCEPTION
                                        )

                                        stat_process.type_of_error = str(type(ex_delete_files_capture_server))\
                                            .replace("'", '')
                                        stat_process.error_description = str(ex_delete_files_capture_server.args)[1:-2]\
                                            .replace("'", '')

                                        # No realizo la inserción en este momento por que no es error critico que
                                        # rompa el flujo del sistema
                                        # dal.insert_stat_process_detections(stat_process, verbose=True)

                                    ####################################################################################
                                    # Borrado de archivos ya procesados en server intermedio
                                    ####################################################################################

                                    st_delete_local_files = datetime.now()
                                    if os.path.isdir(csv_directory):
                                        shutil.rmtree(csv_directory, ignore_errors=True)

                                    stat_process.time_delete_local_files = datetime.now() - st_delete_local_files
                                    stat_process.successful_process = 1

                                except Exception as ex_insert_history_csv_processed:
                                    uc.print_and_logging(
                                        'Ocurrio una excepcion durante la insercion del '
                                        'history_csv_processed del server de captura {} ip {}:'
                                        '\n{}'.format(capture_server.hostname,
                                                      capture_server.ip,
                                                      ex_insert_history_csv_processed),
                                        logging_type=lt.EXCEPTION)

                                    stat_process.successful_process = 0
                                    stat_process.type_of_error = str(type(ex_insert_history_csv_processed))\
                                        .replace("'", '')
                                    stat_process.error_description = str(ex_insert_history_csv_processed.args)[1:-2]\
                                        .replace("'", '')

                        else:
                            try:
                                uc.print_and_logging('El archivo no tenia intervalos validos por regla de negocio '
                                                     'que procesar')
                                uc.print_and_logging('Inserta registro de en HistoryCSVProcessed')
                                st_insert_history_csv_processed = datetime.now()
                                dal.insert_history_csv_processed(path=_csv,
                                                                 client_id=client.id,
                                                                 vehicle_id=csv_vehicle_id,
                                                                 source_signal_id=csv_source_signal_id,
                                                                 server_id=capture_server.id,
                                                                 process_interval=1,
                                                                 process_ocr=1,
                                                                 process_video_intervals=1,
                                                                 verbose=True)

                            except Exception as ex_insert_history_csv_processed:
                                uc.print_and_logging(
                                    'Ocurrio una excepcion durante la insercion del '
                                    'history_csv_processed del server de captura {} ip {}:'
                                    '\n{}'.format(capture_server.hostname,
                                                  capture_server.ip,
                                                  ex_insert_history_csv_processed),
                                    logging_type=lt.EXCEPTION)

                                stat_process.successful_process = 0
                                stat_process.type_of_error = str(type(ex_insert_history_csv_processed)) \
                                    .replace("'", '')
                                stat_process.error_description = str(ex_insert_history_csv_processed.args)[1:-2] \
                                    .replace("'", '')

                        try:
                            stat_process.time_total_process = datetime.now() - st_process_file
                            dal.insert_stat_process_detections(stat_process, verbose=True)
                        except Exception as ex_insert_stat_process:
                            uc.print_and_logging('Ocurrio una excepcion durante la insercion del '
                                                 'stat_process_detections del '
                                                 'server de captura {} ip {}:'
                                                 '\n{}'.format(capture_server.hostname,
                                                               capture_server.ip,
                                                               ex_insert_stat_process),
                                                 logging_type=lt.EXCEPTION)

                    else:
                        uc.print_and_logging('El archivo {} ya se encuentra procesado'.format(_csv))

            except KeyboardInterrupt as ki:
                uc.print_and_logging('Saliendo por KeyboardInterrupt')
                sys.exit()

            except Exception as e:
                uc.print_and_logging('Ocurrio una exception con el server {}:\n{}'.format(server, e),
                                     logging_type=lt.EXCEPTION)

except Exception as e:
    print('Exc:{}'.format(e))

