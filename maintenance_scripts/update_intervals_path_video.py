import os
import glob

from DAL.dal import DAL

cssl = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER=10.240.14.213,59964;' \
       'DATABASE=MarketingDeportivo;UID=BUEMBW-LNXAPP06;PWD=Mematas01'

sql_query_intervals_to_update = "SELECT i.* " \
                                "\n\tFROM Detection.Interval i" \
                                "\n\tWHERE CHARINDEX('generation_', i.PathVideo) = 0" \
                                "\n\t\t  AND i.PathVideo != ''" \
                                "\n\tORDER BY i.ID DESC"

sql_template_update_path_video = "UPDATE Detection.Interval" \
                                 "\n\tSET PathVideo = '{}'" \
                                 "\n\tWHERE ID = {}"

dal = DAL(cssl)
sql_results = dal.engine.execute(sql_query_intervals_to_update)
intervals = dal.sql_results_to_intervals(sql_results)
base_pattern = '/home/enzoscocco/Documents/object_detection/data_detection/intervals/*/*/'
qty_intervals = len(intervals)
n_updated = 0
intervals_with_errors = []

print('{} intervalos para actualizar'.format(qty_intervals))
for n_interval, interval in enumerate(intervals):
    try:
        print('\tactualizando el path_video del intervalo [{}/{}] id {}'.format(n_interval + 1,
                                                                                qty_intervals,
                                                                                interval.id))
        new_path_video = glob.glob(base_pattern + os.path.basename(interval.path_video))[0]
        sql_query_update_path_video = sql_template_update_path_video.format(new_path_video,
                                                                            interval.id)
        dal.engine.execute(sql_query_update_path_video)
        n_updated += 1
    except Exception as e:
        intervals_with_errors.append(interval)
        print('\nSucedio un error con el intervalo [{}/{}] id {} \n\tpath_video: {}\n\texcepcion: {}\n'
              .format(n_interval,
                      qty_intervals,
                      interval.id,
                      interval.path_video,
                      e))

print('Actualizados {} intervalos'.format(n_updated))
print('{} intervalos con errores'.format(len(intervals_with_errors)))
if len(intervals_with_errors) > 0:
    print('\t{}'.format('\n\t'.join([interval_with_error.path_video for interval_with_error in intervals_with_errors])))

# Excepcion con este '/home/enzoscocco/Documents/object_detection/data_detection/intervals/generation_20190424/
# emission_20190423/id1350_rid1_lid1_vid7_sid27_st20190423h02m15s25_et20190423h02m15s27_sirt3540660_eirt3540697.mp4'
