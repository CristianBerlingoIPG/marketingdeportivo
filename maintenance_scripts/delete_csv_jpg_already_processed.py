import os
import sys
import time
import math
import shutil
import logging
import argparse
import configparser

from datetime import datetime, timedelta

sys.path.extend(['/home/enzoscocco/Marketing_Deportivo/models/research/object_detection'])

import utils_cris as uc

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.model.server import ServerType
from DAL.ssh.ssh_util import SshUtil
from utils_cris import LoggingType as lt
from logo_detection_persistence import LogoDetectionPersistence as ldp

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='../etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=60)
parser.add_argument('-csi',
                    '--capture_servers_ips',
                    help='Servers de captura de los que se encargara el script. Por default se toman todos los servers.'
                         'Se puede especificar servers concretos pasando sus IPs separadas por coma y sin espacio, por'
                         'ejemplo 10.228.8.138,10.228.8.129,10.228.69.54',
                    default='')
parser.add_argument('-s',
                    '--since',
                    help='Desde que fecha de emision se debe borrar sus carpetas. El formato a seguir es YYYY-MM-dd. '
                         'Por defecto desde hace 10 dias',
                    type=lambda d: datetime.strptime(d, '%Y-%m-%d'),
                    default=datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=10))
parser.add_argument('-u',
                    '--until',
                    help='Hasta que fecha de emision se debe borrar sus carpetas. El formato a seguir es YYYY-MM-dd. '
                         'Por defecto hasta hace 2 dias',
                    type=lambda d: datetime.strptime(d, '%Y-%m-%d'),
                    default=datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=2))

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
capture_servers_ips = args.capture_servers_ips.split(',') if args.capture_servers_ips != '' else []
since = args.since
until = args.until
# since = datetime.strptime(args.since, '%Y-%m-%d') if args.since is not None \
#     else datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=10)

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
template_general_log_path = 'logs/delete_csv_jpg_already_processed/{}/{}/info/delete_csv_jpg_already_processed__{}.log'
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################

PATH_FILE_FOLDERS_TO_DELETE = '/home/enzoscocco/Documents/object_detection/data_detection'
FILE_NAME_FOLDERS_TO_DELETE = 'folders_to_delete_{}_de_{}_{}.txt'
MAX_FOLDERS_PER_FILE = 100

try:
    configs = configparser.ConfigParser()
    configs.read(config_path)

    config_data_detection = configs['data_detection']

    database_config = configs['Database']
    database_local_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    base_path_output = config_data_detection['base_path_output']
    template_foldersname_frames_detected_datetime = \
        config_data_detection['template_foldersname_frames_detected_datetime']
    template_foldersname_frames_detected = config_data_detection['template_foldersname_frames_detected']
    path_config_csv = config_data_detection['path_config_csv']
    key_configuration_file = config_data_detection['key_configuration_file']
    csv_pattern = config_data_detection['csv_pattern']

    driver = database_config['driver']
    server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None
    schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
    table_history = database_config['table_history'] if 'table_history' in database_config else None
    sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

    # capture_servers = ssh_scp_configs['capture_servers'].split(',')
    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              server,
                                              database,
                                              uid,
                                              pwd)

    larg = []
    [larg.extend(a) for a in args._get_kwargs()]
    logging_message_configs = '\nParameters:' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}'.format(*larg)

    logging_message_configs += '\nConfigurations detection:' \
                               '\n\tbase_path_output: {}' \
                               '\n\tcsv_pattern: {}'.format(base_path_output,
                                                            csv_pattern)

    logging_message_configs += '\nConfigurations database:' \
                               '\n\tDriver: {}' \
                               '\n\tServer: {}' \
                               '\n\tDatabase: {}' \
                               '\n\tUID: {}' \
                               '\n\tSchema history: {}' \
                               '\n\tTable history: {}' \
                               '\n\tConnection string: {}'.format(driver,
                                                                  server,
                                                                  database,
                                                                  uid,
                                                                  schema_history,
                                                                  schema_history,
                                                                  cssl.replace(pwd, '*******'))

    logging_message_configs += '\nConfigurations SSH_SCP' \
                               '\n\tusername: {}\n'.format(username)

    uc.print_and_logging(logging_message_configs)

    dal = DAL(connection_string=cssl)

    server_host_ip = uc.get_ip()
    server_process = dal.get_server_by_ip(server_host_ip)

    while True:
        try:
            detection_servers = dal.get_servers_by_type_and_country_id(ServerType.DETECTION, server_process.country_id)
        except Exception as ex_get_servers_by_type_and_country_id:
            uc.print_and_logging('Ocurrio una excepcion durante la obtencion de los servers de deteccion:'
                                 '\n{}'.format(ex_get_servers_by_type_and_country_id),
                                 logging_type=lt.EXCEPTION)
        if len(capture_servers_ips) > 0:
            detection_servers = [cs for cs in detection_servers if cs.ip in capture_servers_ips]

        start_time = datetime.now()
        sleep_until = start_time + timedelta(minutes=waiting_time_iteraction)

        for detection_server in detection_servers:
            uc.print_and_logging('\n\n\nEliminando archivos del server {} IP {}'.format(detection_server.hostname,
                                                                                        detection_server.ip))
            try:
                hcp_to_delete = dal.get_history_csv_already_processed_by_server_id(detection_server.id,
                                                                                   verbose=True)
            except Exception as ex_get_history_csv_already_processed_by_server_id:
                uc.print_and_logging('Ocurrio una excepcion al intentar obtener el historial de CSV ya procesados para '
                                     'el server:\n{}'.format(ex_get_history_csv_already_processed_by_server_id),
                                     logging_type=lt.EXCEPTION)
                continue

            folders_to_delete = [
                os.path.dirname(hcp.path)
                for hcp in hcp_to_delete
                if since <= ldp.get_datetime_from_filename_csv(os.path.basename(hcp.path)) <= until
            ]

            qty_folders_to_delete = len(folders_to_delete)

            folders_to_delete_split = [
                folders_to_delete[start_index * MAX_FOLDERS_PER_FILE
                                  : (start_index * MAX_FOLDERS_PER_FILE) + MAX_FOLDERS_PER_FILE]
                for start_index
                in range(math.ceil(qty_folders_to_delete / MAX_FOLDERS_PER_FILE))
            ]

            uc.print_and_logging('Cantidad de carpetas que vaciar: {}'.format(qty_folders_to_delete))
            qty_files_ftd = len(folders_to_delete_split)
            ssh = SshUtil(detection_server.ip, username, password)
            for n_slice, _slice_to_delete in enumerate(folders_to_delete_split):
                uc.print_and_logging('Eliminando desde la carpeta {} hasta la {} '
                                     'de las {} totales'.format(n_slice * MAX_FOLDERS_PER_FILE + 1,
                                                                n_slice * MAX_FOLDERS_PER_FILE + len(_slice_to_delete),
                                                                qty_folders_to_delete)
                                     )
                path_file_folders_to_delete = os.path.join(
                    PATH_FILE_FOLDERS_TO_DELETE,
                    FILE_NAME_FOLDERS_TO_DELETE.format(
                        n_slice + 1,
                        qty_files_ftd,
                        start_time.strftime('%Y%m%d_h%Hm%M')
                    )
                )
                uc.print_and_logging('Creando en el server {} ip {} el '
                                     'archivo {} de las carpetas a borrar'.format(detection_server.id,
                                                                                  detection_server.ip,
                                                                                  path_file_folders_to_delete))
                try:
                    ssh.write_to_file('\n'.join(f for f in _slice_to_delete), path_file_folders_to_delete, append=False)
                except Exception as ex_ssh_write_to_file:
                    uc.print_and_logging('Ocurrio una excepcion durante la escritura del archivo con las carpetas a '
                                         'borrar en el server de deteccion:\n{}'.format(ex_ssh_write_to_file),
                                         logging_type=lt.EXCEPTION)
                    continue

                command_delete_folders = 'cat {} | xargs rm -r'.format(path_file_folders_to_delete)
                uc.print_and_logging('Ejecutando comando en server de deteccion para borrar las carpetas contenidas en '
                                     'el archivo de texto:\n\t{}'.format(command_delete_folders))
                try:
                    ssh.execute_command([command_delete_folders])
                except Exception as ex_delete_folders:
                    uc.print_and_logging('Ocurrio una excepcion durante el borrado de carpetas en el server de '
                                         'deteccion:\n{}'.format(ex_ssh_write_to_file),
                                         logging_type=lt.EXCEPTION)
                    continue

                uc.print_and_logging('Ejecutando borrado del archivo de texto ya procesado')
                try:
                    ssh.rm_file(path_file_folders_to_delete)
                except Exception as ex_rm_file:
                    uc.print_and_logging('Ocurrio una excepcion durante el borrado del archivo {} en el server de '
                                         'deteccion:\n{}'.format(path_file_folders_to_delete,
                                                                 ex_ssh_write_to_file),
                                         logging_type=lt.EXCEPTION)

            uc.print_and_logging('Terminado borrado de archivos en servidor de '
                                 'deteccion {} ip {}'.format(detection_server.id,
                                                             detection_server.ip))

            uc.print_and_logging('Comienzo de borrado de archivos en server intermedio')
            for index_hcp, folder in enumerate(folders_to_delete):
                try:
                    if os.path.isdir(folder):
                        shutil.rmtree(folder, ignore_errors=True)
                except Exception as ex_rm_folder:
                    uc.print_and_logging('Ocurrio una excepcion mientras se intentaba borrar la carpeta {} [{}/{}]:'
                                         '\n{}'.format(folder,
                                                       index_hcp + 1,
                                                       qty_folders_to_delete,
                                                       ex_rm_folder),
                                         logging_type=lt.EXCEPTION)

        seconds_to_sleep = (sleep_until - datetime.now()).total_seconds()
        if seconds_to_sleep > 0:
            uc.print_and_logging('En sleep hasta: {}\n'.format(sleep_until.strftime('%H:%M:%S')))
            time.sleep(seconds_to_sleep)


except KeyboardInterrupt as ki:
    uc.print_and_logging('Saliendo por KeyboardInterrupt')
    sys.exit()
except Exception as e:
    uc.print_and_logging('Ocurrio una excepcion que no fue captura en los excepts anteriores y '
                         'llego hasta el superior:\n{}'.format(e),
                         logging_type=lt.EXCEPTION)

