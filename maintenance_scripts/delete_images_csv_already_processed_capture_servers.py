import os

from DAL.dal import DAL

cssl = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER=10.240.14.213,59964;' \
       'DATABASE=MarketingDeportivo;UID=BUEMBW-LNXAPP06;PWD=Mematas01'

dal = DAL(cssl)
csv_processed = [csv for csv in dal.get_history_csv_processed() if csv.process_ocr == 1]

qty_deleted_files = 0
qty_folders = len(csv_processed)
print('Cantidad de archivos ya procesados: {}'.format(qty_folders))

ssh = SshUtil(server_owner.ip, username, password)

for index, csv in enumerate(csv_processed):
    try:
        folder = os.path.dirname(csv.path)
        images_and_zip_to_delete = [file for file in os.listdir(folder) if '.jpg' in file or '.zip' in file]
        print('Borrando las imagenes y zip de la carpeta [{}/{}] {}'.format(index, qty_folders, folder))
        for image_zip in images_and_zip_to_delete:
            os.remove(os.path.join(folder, image_zip))
            qty_deleted_files += 1
    except Exception as e:
        print('Ocurrio una excepcion con {}:\n{}\n'.format(folder, e))

print('Se borraron {} archivos'.format(qty_deleted_files))
print('Termino')
