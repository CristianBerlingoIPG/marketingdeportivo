import os

from DAL.dal import DAL

cs = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER=10.240.14.213,59964;' \
     'DATABASE=MarketingDeportivo;UID=BUEMBW-LNXAPP06;PWD=Mematas01'

dal = DAL(connection_string=cs)

PATH = '/home/enzoscocco/Videos/intervalos_negativos'

negative_intervals = [ni for ni in os.listdir(PATH) if '.mp4' in ni]
qty_negative_intervals = len(negative_intervals)

print('Cantidad de intervalos negativos: {}'.format(qty_negative_intervals))

for index, ni in enumerate(negative_intervals):
    print('Actualizando el intervalo [{}/{}] {}'.format(index + 1, qty_negative_intervals, ni))
    try:
        dal.update_interval_reviewed_and_operator_filter_by_video_name(1, 0, ni)
    except Exception as e:
        print('Sucedio un error:\n{}'.format(e))
