import os
import sys
import shutil
import logging
import argparse
import configparser

from datetime import datetime, timedelta
from zipfile import ZipFile, BadZipFile, LargeZipFile

import utils_cris as uc
from utils_cris import LoggingType as lt
from drawer_detected_object import DrawerDetectedObject
from logo_detection_persistence import LogoDetectionPersistence as ldp, ImplementationIntervalsValidatonRule as iivr

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil
from DAL.model.report.stats_process_detections_to_intervals import StatsProcessDetectionsToIntervals

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='../etl_item_real_time.ini')
parser.add_argument('-csi',
                    '--capture_servers_ips',
                    help='Servers de captura de los que se encargara el script. Por default se toman todos los servers.'
                         'Se puede especificar servers concretos pasando sus IPs separadas por coma y sin espacio, por'
                         'ejemplo 10.228.8.138,10.228.8.129,10.228.69.54',
                    default='')

args = parser.parse_args()
config_path = args.config_path
capture_servers_ips = args.capture_servers_ips.split(',') if args.capture_servers_ips != '' else []

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
template_general_log_path = 'logs/detections_to_intervals/{}/{}/info/detections_to_intervals__{}.log'
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################

try:
    configs = configparser.ConfigParser()
    configs.read(config_path)

    config_data_detection = configs['data_detection']
    database_config = configs['Database']
    database_local_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    base_path_output = config_data_detection['base_path_output']
    template_foldersname_frames_detected_datetime = \
        config_data_detection['template_foldersname_frames_detected_datetime']
    template_foldersname_frames_detected = config_data_detection['template_foldersname_frames_detected']
    path_config_csv = config_data_detection['path_config_csv']
    key_configuration_file = config_data_detection['key_configuration_file']
    csv_pattern = config_data_detection['csv_pattern']

    driver = database_config['driver']
    server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None
    schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
    table_history = database_config['table_history'] if 'table_history' in database_config else None
    sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

    # capture_servers = ssh_scp_configs['capture_servers'].split(',')
    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

    larg = []
    [larg.extend(a) for a in args._get_kwargs()]
    logging_message_configs = '\nParameters:' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}'.format(*larg)

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              server,
                                              database,
                                              uid,
                                              pwd)

    logging_message_configs += '\nConfigurations detection:' \
                               '\n\tbase_path_output: {}' \
                               '\n\tcsv_pattern: {}'.format(base_path_output,
                                                            csv_pattern)

    logging_message_configs += '\nConfigurations database:' \
                               '\n\tDriver: {}' \
                               '\n\tServer: {}' \
                               '\n\tDatabase: {}' \
                               '\n\tUID: {}' \
                               '\n\tSchema history: {}' \
                               '\n\tTable history: {}' \
                               '\n\tConnection string: {}'.format(driver,
                                                                  server,
                                                                  database,
                                                                  uid,
                                                                  schema_history,
                                                                  schema_history,
                                                                  cssl.replace(pwd, '*******'))

    logging_message_configs += '\nConfigurations SSH_SCP' \
                               '\n\tusername: {}\n'.format(username)

    uc.print_and_logging(logging_message_configs)

    dal = DAL(connection_string=cssl)

    server_host_ip = uc.get_ip()
    server_process = dal.get_server_by_ip(server_host_ip)
    country = dal.get_country_by_id(server_process.country_id)
    client = dal.get_clients()[0]
    # solo por que puedo asegurar que esta regla aplica a todos las detecciones del historico
    intervals_validation_rule = dal.get_intervals_validation_rule_by_id(1)
    capture_servers = dal.get_servers(country.id).values()
    if len(capture_servers_ips) > 0:
        capture_servers = [cs for cs in capture_servers if cs.ip in capture_servers_ips]

    uc.print_and_logging('\nServidorers de captura que seran procesados '
                         'segun el valor del parametro --capture_servers_ips:'
                         '\n\t{}'.format('\n\t'.join([str(cs) for cs in capture_servers])))

    vehicles = dal.get_vehicles_associated_to_source_signals(verbose=True)
    logos = dal.get_logos_by_client(client.id)

    dal.clear_csv_being_processing_by_server(server_process.id)
    while True:
        for server in capture_servers:
            try:
                uc.print_and_logging('\n\nServer: {}'.format(server))

                ssh = SshUtil(server.ip, username, password)

                csv_start_time = datetime.now().replace(minute=0, second=0, microsecond=0)

                csvs_item_real_time = sorted(
                    [
                        f for f
                        in ssh.get_find(base_path_output, csv_pattern)
                        if ldp.get_datetime_from_filename_csv(os.path.basename(f)) != csv_start_time
                    ],
                    key=lambda _csv: ldp.get_datetime_from_filename_csv(os.path.basename(_csv))
                )

                qty_csv = len(csvs_item_real_time)
                step_slice = 50

                pfrd_to_delete = [
                    csvs_item_real_time[_slice * step_slice: _slice * step_slice + step_slice]
                    for _slice
                    in range(0, 7)
                ]

                tmp_qry_select_interval = "SELECT * " \
                                          "\n\tFROM Detection.Interval" \
                                          "\n\tWHERE PathFileRawData IN ({})"
                # utilizar OUTPUT no funcniono en pyodbc, posiblemente por el autocommit
                # tmp_qry_delete_interval = "DECLARE @interval_ids_deleted TABLE (" \
                #                           "\n\tID INT" \
                #                           "\n)" \
                #                           "\nDELETE FROM Detection.Interval " \
                #                           "\n\tOUTPUT deleted.ID" \
                #                           "\n\t\tINTO @interval_ids_deleted" \
                #                           "\n\tWHERE PathFileRawData IN ({})" \
                #                           "\nSELECT COUNT(1) Deleted FROM @interval_ids_deleted"
                # tmp_update_hcp = "DECLARE @interval_ids_updated TABLE (" \
                #                  "\n\tID INT" \
                #                  "\n)" \
                #                  "\nUPDATE Detection.HistoryCSVProcessed" \
                #                  "\n\tSET ProcessInterval = 0," \
                #                  "\n\t\tProcessOCR = 0," \
                #                  "\n\t\tProcessVideoIntervals = 0" \
                #                  "\n\tOUTPUT inserted.id" \
                #                  "\n\t\tINTO @interval_ids_updated" \
                #                  "\n\tWHERE [Path] IN ({})" \
                #                  "\nSELECT COUNT(1) FROM @interval_ids_updated"

                tmp_qry_delete_interval = "DELETE FROM Detection.Interval " \
                                          "\n\tWHERE PathFileRawData IN ({})"
                tmp_update_hcp = "UPDATE Detection.HistoryCSVProcessed" \
                                 "\n\tSET ProcessInterval = 0," \
                                 "\n\t\tProcessOCR = 0," \
                                 "\n\t\tProcessVideoIntervals = 0" \
                                 "\n\tWHERE [Path] IN ({})"

                for n_slice, _slice in enumerate(pfrd_to_delete):
                    try:
                        uc.print_and_logging('\nEjecutando proceso para los HCP de la parte {}/{}'
                                             .format(n_slice + 1, len(pfrd_to_delete)))

                        qry_delete_interval = tmp_qry_delete_interval.format(
                            "'" + "',\n\t\t\t\t\t\t\t  '".join(pfrd_to_delete[n_slice]) + "'")
                        dal.engine.execute(qry_delete_interval)

                        # uc.print_and_logging('Cantidad de intervalos borrados {}'.format(qty_deleted_intervals))

                        qry_update_hcp = tmp_update_hcp.format(
                            "'" + "',\n\t\t\t\t\t\t\t  '".join(pfrd_to_delete[n_slice]) + "'"
                        )

                        dal.engine.execute(qry_update_hcp)

                        # uc.print_and_logging('Cantidad de HCP actualizados')

                    except KeyboardInterrupt as ki:
                        uc.print_and_logging('Saliendo por KeyboardInterrupt')
                        sys.exit()

                    except Exception as slice_exc:
                        uc.print_and_logging('Ocurrio una excepcion durante el procesamiento del '
                                             'proceso para los HCP de la parte {}/{}'
                                             .format(n_slice + 1, len(pfrd_to_delete)))

            except KeyboardInterrupt as ki:
                uc.print_and_logging('Saliendo por KeyboardInterrupt')
                sys.exit()

            except Exception as e:
                uc.print_and_logging('Ocurrio una exception con el server {}:\n{}'.format(server, e),
                                     logging_type=lt.EXCEPTION)

except Exception as e:
    print('Exc:{}'.format(e))
