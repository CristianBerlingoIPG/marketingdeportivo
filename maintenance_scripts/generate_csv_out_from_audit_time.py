import os
import csv

from datetime import timedelta

from DAL.dal import DAL
from DAL.model.server import ServerType
from logo_detection_persistence import LogoDetectionPersistence as ldp

dal = DAL('DRIVER={ODBC Driver 17 for SQL Server};SERVER=10.240.14.213,59964;DATABASE=MarketingDeportivo;'
          'UID=BUEMBW-LNXAPP06;PWD=Mematas01')

vehicles = {}
[vehicles.update({v.id: v}) for v in dal.get_vehicles()]
detection_servers = dal.get_servers_by_type_and_country_id(ServerType.DETECTION, 2)

sql_query = 'SELECT sv.ID ServerID, sv.HostName, hcp.Path [PathFileRawData]' \
            '\n\tFROM Detection.HistoryCSVProcessed hcp' \
            '\n\t\tINNER JOIN Detection.[Server] sv' \
            '\n\t\t\tON hcp.ServerID = sv.ID' \
            '\n\tWHERE hcp.ServerID = {}' \
            '\n\tORDER BY hcp.Path'

csv_header = ['ServerID', 'HostName', 'PathFileRawData']
for detection_server in detection_servers:
    to_csv = []
    sql_results = dal.engine.execute(sql_query.format(detection_server.id)).fetchall()
    qty_sql_results = len(sql_results)
    for index, r in enumerate(sql_results):
        basename = os.path.basename(r.PathFileRawData)
        try:
            csv_datetime = ldp.get_datetime_from_filename_csv(basename)
            vehicle_id = ldp.get_vehicle_id_from_filename_csv(basename)
            if vehicle_id in vehicles.keys():
                vehicle = vehicles[vehicle_id]
                audit_start_time = csv_datetime.replace(hour=vehicle.audit_start_time.hour,
                                                        minute=vehicle.audit_start_time.minute,
                                                        second=vehicle.audit_start_time.second)
                audit_end_time = csv_datetime.replace(hour=vehicle.audit_end_time.hour,
                                                      minute=vehicle.audit_end_time.minute,
                                                      second=vehicle.audit_end_time.second)
                if vehicle.audit_end_time < vehicle.audit_start_time:
                    audit_end_time = audit_end_time + timedelta(days=1)

                if not (audit_start_time <= csv_datetime <= audit_end_time):
                    to_csv.append(r)

        except Exception as e:
            print('Sucedio una excepcion con el registro [{}/{}]:\n{}'.format(index, qty_sql_results, e))

    with open('{}_files_to_delete.csv'.format(detection_server.hostname), mode='w') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(csv_header)
        writer.writerows(to_csv)
