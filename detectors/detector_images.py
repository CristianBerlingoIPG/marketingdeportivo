import sys

sys.path.append("..")

import logging

logging.getLogger("tensorflow").setLevel(logging.WARNING)

import os
import cv2
import ast
import argparse
import numpy as np
import tensorflow as tf

from utils import label_map_util
# from utils import visualization_utils as vis_util

import utils_cris as uc
from utils_cris import LoggingType as lt

from datetime import datetime
from streams_data import streams_data
from logo_detection_persistence import LogoDetectionPersistence

parser = argparse.ArgumentParser()
parser.add_argument('--model_path',
                    help='Ruta del modelo',
                    required=True)
parser.add_argument('--device',
                    help='Dispositivo sobre cual ejecutar el procesamiento grafico. '
                         'Ejemplo: /device:GPU:0 para utilizar el primer dispositivo gpu.'
                         '         /cpu:0 para utilizar el CPU.'
                         'Mas informacion: https://www.tensorflow.org/guide/using_gpu.'
                         'Default: /device:GPU:0',
                    default='/device:GPU:0')
parser.add_argument('-plm',
                    '--path_labelmap',
                    help='Path del labelmap',
                    required=True)

args = parser.parse_args()

larg = []
[larg.extend(a) for a in args._get_kwargs()]
logging_message_configs = '\nParameters:' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}\n'.format(*larg)

model_path = args.model_path
device = args.device
path_labelmap = args.path_labelmap

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
ip = uc.get_ip()
template_general_log_path = './logs/detector/detection_multiple_stream__ip{}__{}.log'.format(ip, '{}')
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('.log', '_ERROR.log')
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################

label_map = label_map_util.load_labelmap(path_labelmap)
num_clases = len(label_map.item)
categories = label_map_util.convert_label_map_to_categories(label_map,
                                                            max_num_classes=num_clases,
                                                            use_display_name=True)

logging_message_configs += '\nnum_clases: {}' \
                           '\ncategories: \n\t{}\n'.format(num_clases,
                                                           '\n\t'.join([str(category) for category in categories]))

category_index = label_map_util.create_category_index(categories)

with tf.device(device):
    detection_graph = tf.Graph()
# config = tf.ConfigProto(
#     device_count={'GPU': 0},
#     log_device_placement=True
# )
config = tf.ConfigProto(
    log_device_placement=True
)
# config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.5

with detection_graph.as_default():
    od_graph_def = tf.GraphDef()

    with tf.gfile.GFile(model_path, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    # sess = tf.Session(graph=detection_graph)
    sess = tf.Session(graph=detection_graph, config=config)

image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

num_detections = detection_graph.get_tensor_by_name('num_detections:0')

