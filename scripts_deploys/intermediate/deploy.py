import os
import shutil
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument('-pos',
                    '--path_os_services',
                    help='Ruta de los servicios del OS donde se copiaran los servicios del proyecto. '
                         'Por default /lib/systemd/system.',
                    default='/lib/systemd/system')
parser.add_argument('-ppsi',
                    '--path_project_services',
                    help='Ruta de los servicios del proyecto que deben ser copiados al OS. '
                         'Por default: /home/enzoscocco/Marketing_Deportivo/models/research/'
                         'object_detection/services/intermediate',
                    default='/home/enzoscocco/Marketing_Deportivo/models/research/'
                            'object_detection/services')
parser.add_argument('-pli',
                    '--path_launchers',
                    help='Ruta de los lanzadores que son referenciados por los servicios. '
                         'Por default /home/enzoscocco/Marketing_Deportivo/models/'
                         'research/object_detection/launchers.',
                    default='/home/enzoscocco/Marketing_Deportivo/models/research/'
                            'object_detection/launchers/intermediate')
parser.add_argument('-csi',
                    '--capture_servers_ips',
                    help='Servers de captura de los que se encargara el script. Por default se toman todos los servers.'
                         'Se puede especificar servers concretos pasando sus IPs separadas por coma y sin espacio, por'
                         'ejemplo 10.228.8.138,10.228.8.129,10.228.69.54',
                    default='')

args = parser.parse_args()
path_os_services = args.path_os_services
path_project_services = args.path_project_services
path_launchers = args.path_launchers
capture_servers_ips = args.capture_servers_ips.split(',') if args.capture_servers_ips != '' else []

path_project_services_intermediate = os.path.join(path_project_services, 'intermediate')
path_project_services_shared = os.path.join(path_project_services, 'shared')

services_intermediate = [
    os.path.join(path_project_services_intermediate, s)
    for s in os.listdir(path_project_services_intermediate)
]

services_shared = [
    os.path.join(path_project_services_shared, s)
    for s in os.listdir(path_project_services_shared)
]

service_intermediate_to_initialize = [*services_intermediate, *services_shared]

########################################################################################################################
print('Copiando {} servicios \n\tdesde {} \n\ty {} \n\ta {}'.format(len(service_intermediate_to_initialize),
                                                                    path_project_services_intermediate,
                                                                    path_project_services_shared,
                                                                    path_os_services))
for ss in service_intermediate_to_initialize:
    print('\tcopy {}'.format(ss))
    try:
        shutil.copyfile(ss, os.path.join(path_os_services, os.path.basename(ss)))
    except Exception as e:
        print(e)

########################################################################################################################

path_launchers_intermediate = os.path.join(path_launchers, 'intermediate')
path_launchers_shared = os.path.join(path_launchers, 'shared')

launchers_intermediate = [os.path.join(path_launchers_intermediate, l) for l in os.listdir(path_launchers_intermediate)]
launchers_shared = [os.path.join(path_launchers_shared, l) for l in os.listdir(path_launchers_shared)]
launchers = [*launchers_intermediate, *launchers_shared]


########################################################################################################################
print('\nCambiando los permisos de los {} launchers sh de la carpeta {} a ejecutables'.format(len(launchers),
                                                                                              path_launchers))
for launcher in launchers:
    print('\tchmod {} 0o777'.format(launcher))
    os.chmod(os.path.join(path_launchers, launcher), 0o777)


########################################################################################################################
print('\nInicializando servicios:')
template_systemctl_start = 'systemctl start {}'
for sei in service_intermediate_to_initialize:
    command_systemctl_start = template_systemctl_start.format(os.path.basename(sei))
    print('\t{}'.format(command_systemctl_start))
    systemctl_start_process = subprocess.run([command_systemctl_start],
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE,
                                             shell=True)
    if systemctl_start_process.returncode != 0:
        message_error = 'Ocurrio un error durante la ejecucion de {}:\n{}'.format(
            command_systemctl_start,
            systemctl_start_process.stderr.decode('utf-8'))
        raise ValueError(message_error)
    else:
        print(systemctl_start_process.stdout.decode('utf-8'))

########################################################################################################################
print('\nConfigurando servicios para su ejecucion desde el booteo del OS')
template_systemctl_enable = 'systemctl enable {}'
for sei in service_intermediate_to_initialize:
    command_systemctl_enable = template_systemctl_enable.format(os.path.basename(sei))
    print('\t{}'.format(command_systemctl_enable))
    systemctl_enable_process = subprocess.run([command_systemctl_enable],
                                              stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE,
                                              shell=True)

    if systemctl_enable_process.returncode != 0:
        message_error = 'Ocurrio un error durante la ejecucion de {}:\n{}'.format(
            command_systemctl_enable,
            systemctl_enable_process.stderr.decode('utf-8'))
        raise ValueError(message_error)
    else:
        print(systemctl_enable_process.stdout.decode('utf-8'))


########################################################################################################################
command_systemctl_daemon_reload = 'systemctl daemon-reload'
print('\nRecargando demonio de systemd: {}'.format(command_systemctl_daemon_reload))
systemctl_daemon_reload_process = subprocess.run([command_systemctl_daemon_reload],
                                                 stdout=subprocess.PIPE,
                                                 stderr=subprocess.PIPE,
                                                 shell=True)

if systemctl_daemon_reload_process.returncode != 0:
    message_error = 'Ocurrio un error durante la ejecucion de {}:\n{}'.format(
        command_systemctl_daemon_reload,
        systemctl_daemon_reload_process.stderr.decode('utf-8'))
    raise ValueError(message_error)
else:
    print(systemctl_daemon_reload_process.stdout.decode('utf-8'))

print('Proceso terminado')
