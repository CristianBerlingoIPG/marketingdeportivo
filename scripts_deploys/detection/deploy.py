import os
import shutil
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument('-pos',
                    '--path_os_services',
                    help='Ruta de los servicios del OS donde se copiaran los servicios del proyecto. '
                         'Por default /lib/systemd/system.',
                    default='/lib/systemd/system')
parser.add_argument('-pps',
                    '--path_project_services',
                    help='Ruta de los servicios del proyecto que deben ser copiados al OS. '
                         'Por default: /home/enzoscocco/Marketing_Deportivo/models/research/object_detection/services',
                    default='/home/enzoscocco/Marketing_Deportivo/models/research/object_detection/services/detection')
parser.add_argument('-pl',
                    '--path_launchers',
                    help='Ruta de los lanzadores que son referenciados por los servicios. '
                         'Por default '
                         '/home/enzoscocco/Marketing_Deportivo/models/research/object_detection/launchers/detection.',
                    default='/home/enzoscocco/Marketing_Deportivo/models/research/object_detection/launchers/detection')
parser.add_argument('-pwd',
                    '--password',
                    help='Password del usuario root',
                    type=str,
                    required=True)

args = parser.parse_args()
path_os_services = args.path_os_services
path_project_services = args.path_project_services
path_launchers = args.path_launchers
password = args.password

larg = []
[larg.extend(a) for a in args._get_kwargs()]
logging_message_configs = '\nParameters:' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}\n'.format(*larg)

print(logging_message_configs)

# services_capture_server = ['detection_multiple_stream.service',
#                            'http_server_mkd_data_detection.service',
#                            'http_server_mkd_logs.service']
# services_capture_server = ['http_server_mkd_logs2.service']

services_detection_server = os.listdir(path_project_services)

print('Copiando {} servicios desde la carpeta {} a la carpeta {}'.format(len(services_detection_server),
                                                                         path_project_services,
                                                                         path_os_services))
for scs in services_detection_server:
    print('\tcopy {}'.format(scs))
    try:
        # [Errno 13] Permission denied: '/lib/systemd/system/detection_multiple_stream.service'
        # shutil.copy(os.path.join(path_project_services, scs), os.path.join(path_os_services, scs))
        shutil.copyfile(os.path.join(path_project_services, scs), os.path.join(path_os_services, scs))
    except Exception as e:
        print(e)

launchers = os.listdir(path_launchers)
print('\nCambiando los permisos de los {} launchers sh de la carpeta {} a ejecutables'.format(len(launchers),
                                                                                              path_launchers))
for launcher in launchers:
    print('\tchmod {} 0o777'.format(launcher))
    os.chmod(os.path.join(path_launchers, launcher), 0o777)


print('\nInicializando servicios:')
echo_password = 'echo "{}"'.format(password)
template_systemctl_start = 'systemctl start {}'
for scs in services_detection_server:
    command_systemctl_start = template_systemctl_start.format(scs)
    print('\t{}'.format(command_systemctl_start))
    systemctl_start_process = subprocess.run(['{} | sudo -S {}'.format(echo_password, command_systemctl_start)],
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE,
                                             shell=True)

    if systemctl_start_process.returncode != 0:
        message_error = 'Ocurrio un error durante la ejecucion de {}:\n{}'.format(
            command_systemctl_start,
            systemctl_start_process.stderr.decode('utf-8'))
        raise ValueError(message_error)
    else:
        print(systemctl_start_process.stdout.decode('utf-8'))


print('\nConfigurando servicios para su ejecucion desde el booteo del OS')
template_systemctl_enable = 'systemctl enable {}'
for scs in services_detection_server:
    command_systemctl_enable = template_systemctl_enable.format(scs)
    print('\t{}'.format(command_systemctl_enable))
    systemctl_enable_process = subprocess.run(['{} | sudo -S {}'.format(echo_password, command_systemctl_enable)],
                                              stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE,
                                              shell=True)

    if systemctl_enable_process.returncode != 0:
        message_error = 'Ocurrio un error durante la ejecucion de {}:\n{}'.format(
            command_systemctl_enable,
            systemctl_enable_process.stderr.decode('utf-8'))
        raise ValueError(message_error)
    else:
        print(systemctl_enable_process.stdout.decode('utf-8'))


command_systemctl_daemon_reload = '{} | sudo -S systemctl daemon-reload'.format(echo_password)
print('\nRecargando demonio de systemd: {}'.format(command_systemctl_daemon_reload))
systemctl_daemon_reload_process = subprocess.run([command_systemctl_daemon_reload],
                                                 stdout=subprocess.PIPE,
                                                 stderr=subprocess.PIPE,
                                                 shell=True)

if systemctl_daemon_reload_process.returncode != 0:
    message_error = 'Ocurrio un error durante la ejecucion de {}:\n{}'.format(
        command_systemctl_daemon_reload,
        systemctl_daemon_reload_process.stderr.decode('utf-8'))
    raise ValueError(message_error)
else:
    print(systemctl_daemon_reload_process.stdout.decode('utf-8'))
