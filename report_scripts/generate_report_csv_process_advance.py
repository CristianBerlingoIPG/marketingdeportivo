import os
import re
import sys
import time
import logging
import argparse
import configparser

from datetime import datetime, timedelta

sys.path.append('/home/enzoscocco/Marketing_Deportivo/models/research/object_detection')
print("sys.path.append('/home/enzoscocco/Marketing_Deportivo/models/research/object_detection')")

import utils_cris as uc
from utils_cris import LoggingType as lt
from logo_detection_persistence import LogoDetectionPersistence as ldp

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil
from DAL.model.server import ServerType
from DAL.model.report.csv_process_advance import CSVProcessAdvance

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='../etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 60.',
                    default=60)
parser.add_argument('-cty',
                    '--country_id',
                    type=int,
                    help='Id del pais')

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
country_id = args.country_id

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
template_general_log_path = 'logs/generate_report_csv_process_advance/{}/{}/info/' \
                            'generate_report_csv_process_advance__{}.log'
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################

try:
    configs = configparser.ConfigParser()
    configs.read(config_path)
    config_data_detection = configs['data_detection']
    database_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    base_path_output = config_data_detection['base_path_output']
    csv_pattern = config_data_detection['csv_pattern']

    driver = database_config['driver']
    sql_server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None
    schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
    table_history = database_config['table_history'] if 'table_history' in database_config else None
    sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

    # capture_servers = ssh_scp_configs['capture_servers'].split(',')
    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    sql_server = sql_server if instance is None else '{},{}'.format(sql_server,
                                                                    sqlserverport.lookup(sql_server, instance))

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              sql_server,
                                              database,
                                              uid,
                                              pwd)

    logging_message_configs = '\nConfigurations detection:' \
                              '\n\tbase_path_output: {}' \
                              '\n\tcsv_pattern: {}'.format(base_path_output,
                                                           csv_pattern)

    logging_message_configs += '\nConfigurations database:' \
                               '\n\tDriver: {}' \
                               '\n\tServer: {}' \
                               '\n\tDatabase: {}' \
                               '\n\tUID: {}' \
                               '\n\tSchema history: {}' \
                               '\n\tTable history: {}' \
                               '\n\tConnection string: {}'.format(driver,
                                                                  sql_server,
                                                                  database,
                                                                  uid,
                                                                  schema_history,
                                                                  schema_history,
                                                                  cssl.replace(pwd, '*******'))

    logging_message_configs += '\nConfigurations SSH_SCP' \
                               '\n\tusername: {}\n'.format(username)

    dal = DAL(connection_string=cssl)
    country = dal.get_country_by_id(country_id)

    while True:
        start_time = datetime.now()
        sleep_until = start_time + timedelta(minutes=waiting_time_iteraction)
        try:
            detection_servers = dal.get_servers_by_type_and_country_id(ServerType.DETECTION, country_id)
        except Exception as ex_get_servers_by_type_and_country_id:
            uc.print_and_logging('Ocurrio una excepcion durante la obtención de los servers de '
                                 'deteccion:\n{}'.format(ex_get_servers_by_type_and_country_id),
                                 logging_type=lt.EXCEPTION)
            detection_servers = []
        for detection_server in detection_servers:
            try:

                datetime_log = uc.restart_daily_logging_file(datetime_log,
                                                             template_general_log_path,
                                                             initial_message=logging_message_configs)

                uc.print_and_logging('\n\nServer: {}'.format(detection_server))
                ssh = SshUtil(detection_server.ip, username, password)

                history_csv_processed = dal.get_history_csv_processed()

                uc.print_and_logging('Ejecutando busqueda de archivos CSV.')
                csvs_uploaded = [hcp.path for hcp in history_csv_processed]

                csvs_item_real_time = sorted(
                    ssh.get_find(base_path_output,
                                 csv_pattern)
                )

                uc.print_and_logging('Se encontraron {} CSV  en el server {} ip {}'.format(len(csvs_item_real_time),
                                                                                           detection_server.id,
                                                                                           detection_server.ip))

                csv_start_time = start_time.replace(minute=0, second=0, microsecond=0)

                vehicles = dal.get_vehicles_by_server_id(detection_server.id)
                vehicles_ids = [v.id for v in vehicles]
                uc.print_and_logging('Vehiculos relacionados al server {} id {}:'
                                     '\n\t{}'.format(detection_server.hostname,
                                                     detection_server.id,
                                                     '\n\t'.join(['id {}  {}'.format(v.id, v.name) for v in vehicles])))
                csvs_item_real_time_by_vehicle = {}
                [csvs_item_real_time_by_vehicle.update({v.id: {'vehicle': v, 'list_csv': []}}) for v in vehicles]

                for _csv in csvs_item_real_time:
                    # para evitar contabilizar como CSV listos para subir aquellos que todavia estan siendo escritos
                    file_name_csv = os.path.basename(_csv)
                    if ldp.get_datetime_from_filename_csv(file_name_csv) != csv_start_time:
                        _csv_vehicle_id = ldp.get_vehicle_id_from_filename_csv(file_name_csv)
                        if _csv_vehicle_id in vehicles_ids:
                            if _csv not in csvs_item_real_time_by_vehicle[_csv_vehicle_id]['list_csv']:
                                csvs_item_real_time_by_vehicle[_csv_vehicle_id]['list_csv'].append(_csv)

                reports = []
                for vehicle_id, vehicle_list_csv in csvs_item_real_time_by_vehicle.items():
                    vehicle = csvs_item_real_time_by_vehicle[vehicle_id]['vehicle']
                    # csvs_items_real_time_for_vehicle = [
                    #     csv_np for csv_np in csvs_item_real_time
                    #     if ldp.get_datetime_from_filename_csv(os.path.basename(csv_np)) != csv_start_time
                    #        and ldp.get_vehicle_id_from_filename_csv(os.path.basename(csv_np)) == vehicle.id
                    # ]

                    csv_items_real_time_for_vehicle = vehicle_list_csv['list_csv']

                    csv_not_uploaded_vehicle = [
                        csv_np for csv_np in csv_items_real_time_for_vehicle
                        if csv_np not in csvs_uploaded
                    ]

                    qty_csv_not_uploaded_for_vehicle = len(csv_not_uploaded_vehicle)

                    qty_csv_uploaded_for_vehicle = 0
                    csv_uploaded_and_processed_vehicle = []
                    csv_uploaded_and_not_processed_vehicle = []
                    for hcp in history_csv_processed:
                        if hcp.vehicle_id == vehicle.id:
                            qty_csv_uploaded_for_vehicle += 1
                            if hcp.process_ocr == 1:
                                csv_uploaded_and_processed_vehicle.append(hcp)
                            else:
                                csv_uploaded_and_not_processed_vehicle.append(hcp)

                    qty_csv_uploaded_and_processed_vehicle = len(csv_uploaded_and_processed_vehicle)
                    qty_csv_uploaded_and_not_processed_vehicle = len(csv_uploaded_and_not_processed_vehicle)

                    # porcentaje de archivos subidos sobre el total para el vehiculo
                    uploaded_percentage = \
                        qty_csv_uploaded_for_vehicle / (
                                    qty_csv_not_uploaded_for_vehicle + qty_csv_uploaded_for_vehicle) * 100 \
                            if (qty_csv_not_uploaded_for_vehicle + qty_csv_uploaded_for_vehicle) != 0 else 0

                    # porcentaje de archivos subidos y procesados sobre el total de archivos subidos para el vehiculo
                    processed_percentage_over_uploaded_csv = \
                        qty_csv_uploaded_and_processed_vehicle / qty_csv_uploaded_for_vehicle * 100 \
                            if qty_csv_uploaded_for_vehicle != 0 else 0

                    # porcentaje de archivos procesados sobre el total (subidos o sin subir) para el vehiculo
                    processed_percentage_over_total_csv = \
                        qty_csv_uploaded_and_processed_vehicle / \
                        (qty_csv_not_uploaded_for_vehicle + qty_csv_uploaded_for_vehicle) * 100 \
                            if (qty_csv_not_uploaded_for_vehicle + qty_csv_uploaded_for_vehicle) != 0 else 0

                    report_csv_process_advance = \
                        CSVProcessAdvance(date=start_time,
                                          country_id=country_id,
                                          country=country.name,
                                          server_id=detection_server.id,
                                          server=detection_server.hostname,
                                          vehicle_id=vehicle.id,
                                          vehicle=vehicle.name,
                                          csv_not_uploaded=qty_csv_not_uploaded_for_vehicle,
                                          uploaded_percentage=uploaded_percentage,
                                          csv_uploaded_and_not_processed=qty_csv_uploaded_and_not_processed_vehicle,
                                          csv_uploaded_and_processed=qty_csv_uploaded_and_processed_vehicle,
                                          processed_percentage_over_uploaded_csv=processed_percentage_over_uploaded_csv,
                                          processed_percentage_over_total_csv=processed_percentage_over_total_csv,
                                          used_storage=0,
                                          available_storage=0)

                    # uc.print_and_logging('Insertando en la BD el registro de ReportCSVProcessAdvance:\n\t{}'
                    #                      .format(report_csv_process_advance))
                    reports.append(report_csv_process_advance)

                uc.print_and_logging('Insertando {} registros de ReportCSVProcessAdvance con '
                                     '\n\tDate {} '
                                     '\n\tvehicle ids {}'.format(len(reports),
                                                                 start_time,
                                                                 ','.join([str(v.id) for v in vehicles])))

                df_result = ssh.execute_df(mounted_on='/home')[0]
                for r in reports:
                    r.used_storage = re.sub(r'\D', '', df_result.used)
                    r.available_storage = re.sub(r'\D', '', df_result.avail)
                dal.insert_report_csv_process_advance(reports)

            except KeyboardInterrupt as ki:
                uc.print_and_logging('Saliendo por KeyboardInterrupt')
                sys.exit()
            except Exception as e:
                uc.print_and_logging('Ocurrio una exception con el server {}:\n{}'.format(detection_server, e),
                                     logging_type=lt.EXCEPTION)

        seconds_to_sleep = (sleep_until - datetime.now()).total_seconds()
        if seconds_to_sleep > 0:
            uc.print_and_logging('En sleep hasta: {}\n'.format(sleep_until.strftime('%H:%M:%S')))
            time.sleep(seconds_to_sleep)

except KeyboardInterrupt as ki:
    uc.print_and_logging('Saliendo por KeyboardInterrupt')
    sys.exit()
except Exception as e:
    uc.print_and_logging('Ocurrio una excepcion que no fue captura en los excepts anteriores y '
                         'llego hasta el superior:\n{}'.format(e),
                         logging_type=lt.EXCEPTION)
