import cv2

from sources.source import Source
from logo_detection_persistence import LogoDetectionPersistence


class VideoSource(Source):
    # {
    #     'stream': '/dev/video0',
    #     'vehicle_id': 27,
    #     'source_signal_id': 37
    # }
    def __init__(self, stream_path, vehicle_id, source_signal_id, detection_persistence: LogoDetectionPersistence):
        self._stream_path = stream_path
        self._stream = cv2.VideoCapture(stream_path)
        self._vehicle_id = vehicle_id
        self._source_signal_id = source_signal_id
        self._detection_persistence = detection_persistence

    @property
    def detection_persistence(self):
        return self._detection_persistence

    @property
    def vehicle_id(self):
        return self._vehicle_id

    def is_opened(self):
        self._stream.isOpened()

    def read(self):
        self._stream.read()
