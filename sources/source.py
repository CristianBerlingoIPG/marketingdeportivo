from abc import ABCMeta, abstractmethod


class Source(metaclass=ABCMeta):

    @property
    def detection_persistence(self):
        raise NotImplementedError

    @property
    def vehicle_id(self) -> int:
        raise NotImplementedError

    @abstractmethod
    def is_opened(self) -> bool:
        pass

    @abstractmethod
    def read(self):
        pass
