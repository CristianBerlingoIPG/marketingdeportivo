from sources.source import Source
from logo_detection_persistence import LogoDetectionPersistence


class FileSource(Source):

    def __init__(self, vehicle_id, detection_persistence: LogoDetectionPersistence):
        self._vehicle_id = vehicle_id
        self._detection_persistence = detection_persistence

    @property
    def vehicle_id(self):
        return self.vehicle_id

    @property
    def detection_persistence(self):
        return self._detection_persistence

    def get_current_folder(self):
        pass

    def is_opened(self):
        return True

    def read(self):
        path_files = self._detection_persistence.get_foldersname_frames_detected()
