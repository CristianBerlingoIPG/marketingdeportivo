from sources.video_source import VideoSource
from sources.file_source import FileSource

class FactorySource:

    class SourceType:
        VIDEO = 'VIDEO'
        FILE = 'FILE'

    @classmethod
    def create_source(cls, source_type: str):
        source = None

        if source == cls.SourceType.VIDEO:
            source = VideoSource('/dev/video0', 27, 37, 'detection')

        if source == cls.SourceType.FILE:
            source = FileSource()
