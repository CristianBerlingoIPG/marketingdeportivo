# server	        stream	    vehicle_id	vehicle	source_signal_id
# BUEMBW-LNXAPP01	/dev/video0	    1	    C5N             21
# BUEMBW-LNXAPP01	/dev/video1	    2	    DEPORTV	        22
# BUEMBW-LNXAPP01	/dev/video2	    3	    ESPN	        23
# BUEMBW-LNXAPP01	/dev/video3	    4	    ESPN 2	        24
# BUEMBW-LNXAPP02	/dev/video0	    5	    ESPN 3	        25
# BUEMBW-LNXAPP02	/dev/video1	    6	    Fox Sports	    26
# BUEMBW-LNXAPP02	/dev/video2	    7	    Fox Sports Premium	27
# BUEMBW-LNXAPP02	/dev/video3	    8	    Fox Sports2     28
# BUEMBW-LNXAPP03	/dev/video0	    9	    Fox Sports3 	29
# BUEMBW-LNXAPP03	/dev/video1	    10	    TNT Sports	    30
# BUEMBW-LNXAPP03	/dev/video2	    11	    TN	            31
# BUEMBW-LNXAPP03	/dev/video3	    12	    TyC Sports	    32
# BUEMBW-LNXAPP04	/dev/video0	    13	    America	        33
# BUEMBW-LNXAPP04	/dev/video1	    14	    El nueve	    34
# BUEMBW-LNXAPP04	/dev/video2	    15	    El trece	    35
# BUEMBW-LNXAPP04	/dev/video3	    16	    Telefe	        36
# BUEMBW-LNXAPP05	/dev/video0	    17	    TV Pública	    37
# GYQMKD01	        /dev/video0	    21	    Gama TV	        41
# GYQMKD01	        /dev/video1	    22	    Teleamazonas	42
# GYQMKD01	        /dev/video2	    23	    RTS	            43
# GYQMKD01	        /dev/video3	    24	    EcuadorTV	    44
# GYQMKD02	        /dev/video0	    25	    Gol TV	        45
# GYQMKD02	        /dev/video1	    26	    Ecuavisa	    46
# GYQMKD02	        /dev/video2	    27	    TC Television	47
# GYQMKD02	        /dev/video3	    28	    Canal 1	        48
# GYQMKD02	        /dev/video4	    29	    Ecuavisa	    49

servers = {
    'BUEMBW-LNXAPP01': [
        {
            'stream': '/dev/video0',
            'vehicle_id': 1,
            'vehicle': 'Gamma TV',
            'source_signal_id': 21
        },
        {
            'stream': '/dev/video1',
            'vehicle_id': 2,
            'vehicle': 'Teleamazonas',
            'source_signal_id': 22
        },
        {
            'stream': '/dev/video2',
            'vehicle_id': 3,
            'vehicle': 'RTS',
            'source_signal_id': 23
        },
        {
            'stream': '/dev/video3',
            'vehicle_id': 4,
            'vehicle': 'EcuadorTV',
            'source_signal_id': 24
        }
    ],
    'BUEMBW-LNXAPP02': [
        {
            'stream': '/dev/video0',
            'vehicle_id': 5,
            'vehicle': 'ESPN 3',
            'source_signal_id': 25
        },
        {
            'stream': '/dev/video1',
            'vehicle_id': 6,
            'vehicle': 'Fox Sports',
            'source_signal_id': 26
        },
        {
            'stream': '/dev/video2',
            'vehicle_id': 7,
            'vehicle': 'Fox Sports Premium',
            'source_signal_id': 27
        },
        {
            'stream': '/dev/video3',
            'vehicle_id': 8,
            'vehicle': 'Fox Sports2',
            'source_signal_id': 28
        }
    ],
    'BUEMBW-LNXAPP03': [
        {
            'stream': '/dev/video0',
            'vehicle_id': 9,
            'vehicle': 'Fox Sports3',
            'source_signal_id': 29
        },
        {
            'stream': '/dev/video1',
            'vehicle_id': 10,
            'vehicle': 'TNT Sports',
            'source_signal_id': 30
        },
        {
            'stream': '/dev/video2',
            'vehicle_id': 11,
            'vehicle': 'TN',
            'source_signal_id': 31
        },
        {
            'stream': '/dev/video3',
            'vehicle_id': 12,
            'vehicle': 'TyC Sports',
            'source_signal_id': 32
        }
    ],
    'BUEMBW-LNXAPP04': [
        {
            'stream': '/dev/video0',
            'vehicle_id': 13,
            'vehicle': 'America',
            'source_signal_id': 33
        },
        {
            'stream': '/dev/video1',
            'vehicle_id': 14,
            'vehicle': 'El nueve',
            'source_signal_id': 34
        },
        {
            'stream': '/dev/video2',
            'vehicle_id': 15,
            'vehicle': 'El trece',
            'source_signal_id': 35
        },
        {
            'stream': '/dev/video3',
            'vehicle_id': 16,
            'vehicle': 'Telefe',
            'source_signal_id': 36
        }
    ],
    'BUEMBW-LNXAPP05': [
        {
            'stream': '/dev/video0',
            'vehicle_id': 17,
            'vehicle': 'TV Pública',
            'source_signal_id': 37
        }],
    'GYQMKD01': [
        {
            'stream': '/dev/video0',
            'vehicle_id': 21,
            'vehicle': 'Gamma TV',
            'source_signal_id': 41
        },
        {
            'stream': '/dev/video1',
            'vehicle_id': 22,
            'vehicle': 'Teleamazonas',
            'source_signal_id': 42
        },
        {
            'stream': '/dev/video2',
            'vehicle_id': 23,
            'vehicle': 'RTS',
            'source_signal_id': 43
        },
        {
            'stream': '/dev/video3',
            'vehicle_id': 24,
            'vehicle': 'EcuadorTV',
            'source_signal_id': 44
        }
    ],
    'GYQMKD02': [
        {
            'stream': '/dev/video0',
            'vehicle_id': 25,
            'vehicle': 'Gol TV',
            'source_signal_id': 45
        },
        {
            'stream': '/dev/video1',
            'vehicle_id': 26,
            'vehicle': 'TC Television',
            'source_signal_id': 46
        },
        {
            'stream': '/dev/video2',
            'vehicle_id': 27,
            'vehicle': 'Canal 1',
            'source_signal_id': 47
        },
        {
            'stream': '/dev/video3',
            'vehicle_id': 28,
            'vehicle': 'Televicentro',
            'source_signal_id': 48
        },
        # prueba con cinco canales
        {
            'stream': '/dev/video4',
            'vehicle_id': 29,
            'vehicle': 'Ecuavisa',
            'source_signal_id': 49
        }
    ]
}
