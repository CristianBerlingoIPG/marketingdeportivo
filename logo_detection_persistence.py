import os
import cv2
import csv
import operator
import requests
import sqlalchemy
import pytesseract
import PIL.Image as Image

from typing import List, Tuple
from urllib.parse import quote_plus
from datetime import datetime, timedelta

import utils_cris as uc

from DAL.model.logo import Logo
from DAL.model.server import Server
from DAL.model.interval import Interval
from DAL.model.source_signal import SourceSignal
from DAL.model.item_real_time import ItemRealTime
from DAL.model.intervals_validation_rule import IntervalsValidationRule


class LogoDetectionPersistence:
    CHARACTERS_TO_DELETE = '!"#$%&/()=?¡@¨*[];:´+{},.~^`·<>|¬°½\\'
    TEMPLATE_FILE_NAME = '{}__{}__{}__.csv'
    FORMAT_DATE_FILE = '%Y%m%d%H'
    TEMPLATE_LINE_DETECTION = '{},{},{},{},{},{},{},{},{},{},{}\n'
    TEMPLATE_FRAME_DETECTED = '{}n{}__{}__{}.jpg'
    TEMPLATE_FOLDERSNAME_FRAMES_DETECTED_DATETIME = '%Y/%m/%d/%H'
    TEMPLATE_FOLDERSNAME_FRAMES_DETECTED = 'vh{}/{}'
    FORMAT_DATE_FRAME = '%Y%m%d_h%Hm%Ms%Sf%f'
    HEADER_CSV = 'VehicleID,SourceSignalID,LogoID,Date,Path,Score,BoxYMin,BoxXMin,BoxYMax,BoxXMax,PathFileRawData,' \
                 'RecognizedText\n'

    def __init__(self, vehicle_id, source_signal_id, output, logger=None):
        self.vehicle_id = vehicle_id
        self.source_signal_id = source_signal_id
        self.file_name = self.generate_file_name_by_convention(datetime.now())
        self.output = output
        self.logger = logger
        self.writer = None
        self.open_writer(datetime.now())
        self.fix_values_line = LogoDetectionPersistence \
            .TEMPLATE_LINE_DETECTION.format(self.vehicle_id, self.source_signal_id, '{}', '{}', '{}',
                                            '{}', '{}', '{}', '{}', '{}', '{}')
        self.prefix_frames_detected = LogoDetectionPersistence \
            .TEMPLATE_FRAME_DETECTED.format('{}', '{}', self.source_signal_id, self.vehicle_id)
        self.last_date_frame_detected = None
        self.n_frames_in_second = 1

    def open_writer(self, _datetime, newline='\r\n'):
        if os.path.exists(os.path.join(self.output, self.file_name)):
            self.writer = open(os.path.join(self.get_foldersname_frames_detected(_datetime), self.file_name),
                               'a',
                               newline=newline)
        else:
            self.writer = open(os.path.join(self.get_foldersname_frames_detected(_datetime), self.file_name),
                               'w',
                               newline=newline)
            self.writer.write(LogoDetectionPersistence.HEADER_CSV)

    def close_writer(self):
        if self.writer is not None:
            self.writer.close()

    def generate_file_name_by_convention(self, _datetime):
        dt_string = _datetime.strftime(LogoDetectionPersistence.FORMAT_DATE_FILE)
        normalized_stream_name = str(self.source_signal_id)
        for character in LogoDetectionPersistence.CHARACTERS_TO_DELETE:
            normalized_stream_name = normalized_stream_name.replace(character, '')

        return LogoDetectionPersistence.TEMPLATE_FILE_NAME.format(dt_string, normalized_stream_name, self.vehicle_id)

    def restart_writer(self, _datetime):
        self.close_writer()
        self.file_name = self.generate_file_name_by_convention(_datetime)
        self.open_writer(_datetime)

    def write_item_real_time(self, _datetime, detectedlogos_pathframes):
        date_detection = _datetime.strftime('%Y-%m-%d %H%M%S.%f')[:-3]
        for detected_logo, path_frame in detectedlogos_pathframes:
            self.writer.write(self.fix_values_line.format(detected_logo, date_detection, path_frame))

    def get_filename_detected_frame(self, _datetime):
        if _datetime == self.last_date_frame_detected:
            self.n_frames_in_second += 1
        else:
            self.last_date_frame_detected = _datetime
            self.n_frames_in_second = 1
        filename_detected_frame = self.prefix_frames_detected.format(
            _datetime.strftime(LogoDetectionPersistence.FORMAT_DATE_FRAME), self.n_frames_in_second)

        return filename_detected_frame

    def get_foldersname_frames_detected(self, _datetime):
        foldersname = self.TEMPLATE_FOLDERSNAME_FRAMES_DETECTED \
            .format(self.vehicle_id,
                    _datetime.strftime(LogoDetectionPersistence.TEMPLATE_FOLDERSNAME_FRAMES_DETECTED_DATETIME))
        folders = foldersname.split('/')
        _path_output = self.output
        for folder in folders:
            _path_output = os.path.join(_path_output, folder)
            if not os.path.isdir(_path_output):
                os.mkdir(_path_output)

        return _path_output

    def get_path_csv(self, _datetime: datetime = None):
        _datetime = _datetime if _datetime is not None else datetime.now()
        return os.path.join(self.get_foldersname_frames_detected(_datetime), self.file_name)

    def write_detected_frame(self, _cv2, frame, path_frame=None):
        if path_frame is None:
            dt = datetime.now()
            path_output = self.get_foldersname_frames_detected(dt)
            filename_frame = self.get_filename_detected_frame(dt)
            path_frame = os.path.join(path_output, filename_frame)
        _cv2.imwrite(path_frame, frame)

        return path_frame

    @staticmethod
    def get_string_items_real_time(items_real_time):
        string_items_real_time = ''
        for irt in items_real_time:
            string_items_real_time += irt.to_csv() + '\n'

        return string_items_real_time

    def write_data_detection(self, _cv2, frame, boxes, classes, scores, _datetime, min_score_thresh,
                             color_rgb2bgr=True) -> Tuple[any, List[ItemRealTime]]:
        # dt_write_data_detection = datetime.now()
        items_real_time = []
        path_frame_detected = os.path.abspath(os.path.join(self.get_foldersname_frames_detected(_datetime),
                                                           self.get_filename_detected_frame(_datetime)))

        path_file_raw_data = os.path.join(os.path.dirname(path_frame_detected),
                                          self.generate_file_name_by_convention(_datetime=_datetime))

        # dt_zip = datetime.now()
        for index in range(0, len(scores)):
            if scores[index] > min_score_thresh:
                items_real_time.append(ItemRealTime(self.vehicle_id,
                                                    self.source_signal_id,
                                                    classes[index],
                                                    _datetime,
                                                    path_frame_detected,
                                                    scores[index],
                                                    boxes[index][0],
                                                    boxes[index][1],
                                                    boxes[index][2],
                                                    boxes[index][3],
                                                    path_file_raw_data))

        # uc.print_end_elapsed_time(dt_zip, message='items_real_time_zip Endtime {}  elapsed time {}')

        if len(items_real_time) > 0:
            if color_rgb2bgr:
                frame = _cv2.cvtColor(frame, _cv2.COLOR_RGB2BGR)
            self.write_detected_frame(_cv2, frame, path_frame=path_frame_detected)
            if self.file_name[:len(_datetime.strftime(LogoDetectionPersistence.FORMAT_DATE_FILE))] \
                    != _datetime.strftime(LogoDetectionPersistence.FORMAT_DATE_FILE):
                self.restart_writer(_datetime)
            self.writer.write(self.get_string_items_real_time(items_real_time))

        return frame, items_real_time

    @staticmethod
    def split_csv_items_real_time(csv_path, path_base='/home/enzoscocco/Desktop/ItemsHistoric'):
        with open(csv_path, newline='') as csvfile:
            header = csvfile.readline()
            previoulsy_file = csvfile.readline().split(',')[11]
            path_output = os.path.join(path_base, os.path.basename(previoulsy_file))
            print('Creando archivo {}'.format(path_output))
            csvfile_copy = open(path_output, 'w')
            csvfile.seek(0)
            csvfile.readline()
            for index, row in enumerate(csvfile):
                try:
                    current_file = row.split(',')[11]
                    if previoulsy_file == current_file:
                        csvfile_copy.write(row)
                    else:
                        csvfile_copy.close()
                        path_output = os.path.join(path_base, os.path.basename(current_file))
                        print('Creando archivo {}'.format(path_output))
                        csvfile_copy = open(path_output, 'w')
                        csvfile_copy.write(header)
                        csvfile_copy.write(row)
                        previoulsy_file = current_file
                except Exception as e:
                    print(e)

        csvfile_copy.close()

    @staticmethod
    def delete_errors_text_recognized_csv(csv_path):
        csv_path_fixed = csv_path.replace('.csv', '_fix.csv')
        with open(csv_path_fixed, 'w') as csvfile_fixed:
            with open(csv_path, 'rb') as csvfile:
                header = csvfile.readline()
                csvfile_fixed.write(header.decode('utf-8'))
                # origin_line = csvfile..readline()
                while True:
                    line = csvfile.readline()
                    try:
                        fixed_line = b','.join(line.split(b',')[:12]) + b',\r\n'
                        csvfile_fixed.write(fixed_line.decode('utf-8'))
                    except Exception as e:
                        print(e)
                    if not line:
                        break


    @staticmethod
    def get_items_real_from_csv_sort_by_datetime(csv_path):
        items_real_time = {}
        irt_key_dates = []
        irt_key_paths = {}
        errors = []
        with open(csv_path, newline='') as csvfile:
            try:
                csvfile.readline()
                rows = csv.reader(csvfile, delimiter=',', quotechar='|')
                for index, row in enumerate(rows):
                    try:
                        csv_date = LogoDetectionPersistence.get_datetime_from_filename_csv(
                            os.path.basename(row[11])).strftime('%Y%m%d')
                        path_file_raw_data = row[11]

                        if csv_date not in irt_key_dates:
                            items_real_time.update({csv_date: {}})
                            irt_key_dates.append(csv_date)
                            irt_key_paths.update({csv_date: []})
                        if path_file_raw_data not in irt_key_paths[csv_date]:
                            items_real_time[csv_date].update({path_file_raw_data: []})
                            irt_key_paths[csv_date].append(path_file_raw_data)

                        items_real_time[csv_date][path_file_raw_data].append(
                            ItemRealTime(id=row[0],
                                         vehicle_id=row[1],
                                         source_signal_id=row[2],
                                         logo_id=row[3],
                                         date=row[4][:26],
                                         path=row[5],
                                         score=row[6],
                                         box_y_min=row[7],
                                         box_x_min=row[8],
                                         box_y_max=row[9],
                                         box_x_max=row[10],
                                         path_file_raw_data=row[11],
                                         recognized_text=row[12]))
                    except Exception as e:
                        errors.append(row)
                        uc.print_and_logging('Sucedio un error con la fila {} '
                                             'al intentar extraer la fecha de su path_file_raw_data:'
                                             '\n{}'.format(index, e))
            except Exception as er:
                print('Ocurrio una excepcion durante reader:\n\t{}'.format(er))
                # errors = []
            #     for index, r in enumerate(rows):
            #         try:
            #             dt = LogoDetectionPersistence.get_datetime_from_filename_csv(os.path.basename(r[11]))
            #             r.append(dt.strftime('%Y%m%d'))
            #         except Exception as e:
            #             errors.append(r)
            #             uc.print_and_logging('Sucedio un error con la fila {} '
            #                                  'al intentar extraer la fecha de su path_file_raw_data:'
            #                                  '\n{}'.format(index, e))
            #     uc.print('Cantidad de filas que producieron errorres :{}'.format(len(errors)))
            #     if len(errors) > 0:
            #         uc.print('Eliminando las filas con errores de la lista de items a procesar')
            #         for error in errors:
            #             rows.remove(error)
            #     # rows.sort(key=operator.itemgetter(14, 11, 0))
            # except Exception as er:
            #     print('Ocurrio una excepcion durante reader:\n\t{}'.format(er))

        return items_real_time

    @staticmethod
    def get_items_real_from_csv(csv_path):
        items_real_time = []
        errors = []
        with open(csv_path, newline='') as csvfile:
            try:
                csvfile.readline()
                rows = csv.reader(csvfile, delimiter=',', quotechar='|')
                for index, row in enumerate(rows):
                    try:

                        items_real_time.append(
                            ItemRealTime(vehicle_id=row[0],
                                         source_signal_id=row[1],
                                         logo_id=row[2],
                                         date=row[3][:26],
                                         path=row[4],
                                         score=row[5],
                                         box_y_min=row[6],
                                         box_x_min=row[7],
                                         box_y_max=row[8],
                                         box_x_max=row[9],
                                         path_file_raw_data=row[10],
                                         recognized_text=row[11])
                        )
                    except Exception as e:
                        errors.append(row)
                        uc.print_and_logging('Sucedio un error con la fila {} '
                                             'al intentar extraer la fecha de su path_file_raw_data:'
                                             '\n{}'.format(index, e))
            except Exception as er:
                print('Ocurrio una excepcion durante reader:\n\t{}'.format(er))

        return items_real_time

    @staticmethod
    def get_datetime_from_filename_csv(filename) -> datetime:
        try:
            until_char = len(datetime.now().strftime(LogoDetectionPersistence.FORMAT_DATE_FILE))
            datetime_from_filename_csv = datetime.strptime(filename[:until_char],
                                                           LogoDetectionPersistence.FORMAT_DATE_FILE)
        except Exception as get_datetime_from_filename_csv_exception:
            raise ValueError('[get_datetime_from_filename_csv] Sucedio un error:{}'
                             '\nfilename: {}'
                             '\nLogoDetectionPersistence.FORMAT_DATE_FILE: {}'
                             .format(get_datetime_from_filename_csv_exception,
                                     filename,
                                     LogoDetectionPersistence.FORMAT_DATE_FILE))
        return datetime_from_filename_csv

    @staticmethod
    def get_vehicle_id_from_filename_csv(filename):
        return int(filename.split('__')[2])

    @staticmethod
    def get_source_signal_id_from_filename_csv(filename):
        return int(filename.split('__')[1])


class ImplementationIntervalsValidatonRule:
    intervals_validation_rules: IntervalsValidationRule
    ENGINE_CONNECTION_STRING = 'mssql+pyodbc:///?odbc_connect={}'
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3
    QUERY_UPDATE_RECOGNIZED_TEXT_ITEMS_REAL_TIME = "UPDATE [{}].[ItemRealTime]" \
                                                   "\n\tSET RecognizedText = '{}'" \
                                                   "\n\tWHERE ID = {}"

    def __init__(self, client_id, country_id, connection_string=None,
                 execute_since=(datetime.now() - timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0),
                 execute_until=datetime.now().replace(hour=0, minute=0, second=0, microsecond=0),
                 schema='Detection', table_intervals='Interval',
                 store_procedure_intervals_validation_rule='GetIntervalsValidationRuleByCountryAndClient',
                 schema_source_signal='Detection', table_source_signal='SourceSignal',
                 store_procedure_items_real_time='GetItemsRealTime', store_procedure_logos='GetLogosByClient',
                 schema_history='Detection', table_history='HistoryUpdates', engine=None):
        self.client_id = client_id
        self.country_id = country_id
        self.connection_string = connection_string
        self.execute_since = execute_since
        self.execute_until = execute_until
        if self.execute_until <= self.execute_since:
            raise ValueError('el parametro execute_until no puede ser igual o menor a execute_until')
        self.schema = schema
        self.table_intervals = table_intervals
        self.store_procedure_intervals_validation_rule = store_procedure_intervals_validation_rule
        self.store_procedure_items_real_time = store_procedure_items_real_time
        self.store_procedure_logos = store_procedure_logos
        self.schema_source_signal = schema_source_signal
        self.table_source_signal = table_source_signal
        self.engine_connection_string = ImplementationIntervalsValidatonRule \
            .ENGINE_CONNECTION_STRING.format(quote_plus(self.connection_string))
        self.engine = sqlalchemy.create_engine(self.engine_connection_string) \
            if self.engine_connection_string is not None else engine
        self.schema_history = schema_history
        self.table_history = table_history
        self.intervals_validation_rule: IntervalsValidationRule = None
        self.load_intervals_validation_rule()
        self.source_signals: List[SourceSignal] = []
        self.load_source_signals()
        self.items_real_time: List[ItemRealTime] = []
        self.load_items_real_time()
        self.logos: List[Logo] = []
        self.load_logos()

    def load_intervals_validation_rule(self):
        sql_str_execute_since = self.execute_since.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_command = "EXECUTE [{}].[{}] '{}', {}, {}".format(self.schema,
                                                              self.store_procedure_intervals_validation_rule,
                                                              sql_str_execute_since,
                                                              self.client_id,
                                                              self.country_id)

        # self.intervals_validation_rule = self.raw_sql_intervals_validation_rule_to_object(
        #     self.engine.execute(sql_command).first())

        ivr = self.engine.execute(sql_command).first()
        if ivr is not None:
            self.intervals_validation_rule = IntervalsValidationRule(ivr.ID,
                                                                     ivr.ClientID,
                                                                     ivr.CountryID,
                                                                     ivr.Since,
                                                                     ivr.MinimumExpositionTime,
                                                                     ivr.MinimumPercentFrames,
                                                                     ivr.MinimumPercentMatchDetection,
                                                                     ivr.Created)
            print('\n#################################################################################################')
            print('intervals_validation_rule')
            print(self.intervals_validation_rule)
            print('#################################################################################################\n')
        else:
            raise ValueError('No existe ninguna regla de validación de intervalos para estos parametros')

    def load_source_signals(self):
        sql_command = 'SELECT * FROM [{}].[{}]'.format(self.schema_source_signal, self.table_source_signal)

        self.source_signals = [SourceSignal(ss.ID,
                                            ss.Name,
                                            ss.Observations,
                                            ss.Active,
                                            ss.Path,
                                            ss.FPS,
                                            ss.Width,
                                            ss.Height,
                                            ss.ServerID)
                               for ss in self.engine.execute(sql_command).fetchall()]

    def load_items_real_time(self):
        sql_str_execute_since = self.execute_since.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_str_execute_until = self.execute_until.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_command = "EXECUTE [{}].[{}] '{}', '{}', {}, {}".format(self.schema,
                                                                    self.store_procedure_items_real_time,
                                                                    sql_str_execute_since,
                                                                    sql_str_execute_until,
                                                                    self.client_id,
                                                                    self.country_id)

        # def __init__(self, vehicle_id, source_signal_id, logo_id, date, path, score,
        #              box_y_min, box_x_min, box_y_max, box_x_max, path_file_raw_data, id=0):

        self.items_real_time = [ItemRealTime(irt.VehicleID,
                                             irt.SourceSignalID,
                                             irt.LogoID,
                                             irt.Date,
                                             irt.Path,
                                             irt.Score,
                                             irt.BoxYMin,
                                             irt.BoxXMin,
                                             irt.BoxYMax,
                                             irt.BoxXMax,
                                             irt.PathFileRawData,
                                             id=irt.ID) for irt in self.engine.execute(sql_command).fetchall()]

    def get_items_real_time(self, interval_validation_rule: IntervalsValidationRule):
        sql_str_execute_since = self.execute_since.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_str_execute_until = self.execute_until.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_command = "EXECUTE [{}].[{}] '{}', '{}', {}, {}".format(self.schema,
                                                                    self.store_procedure_items_real_time,
                                                                    sql_str_execute_since,
                                                                    sql_str_execute_until,
                                                                    interval_validation_rule.client_id,
                                                                    interval_validation_rule.country_id)

        items_real_time = [ItemRealTime(irt.VehicleID,
                                        irt.SourceSignalID,
                                        irt.LogoID,
                                        irt.Date,
                                        irt.Path,
                                        irt.Score,
                                        irt.BoxYMin,
                                        irt.BoxXMin,
                                        irt.BoxYMax,
                                        irt.BoxXMax,
                                        irt.PathFileRawData,
                                        id=irt.ID) for irt in self.engine.execute(sql_command).fetchall()]

        return items_real_time

    def load_logos(self):
        sql_command = 'EXECUTE [{}].[{}] {}'.format(self.schema, self.store_procedure_logos, self.client_id)

        self.logos = [Logo(l.Name,
                           l.Observations,
                           l.Path,
                           l.ProductID,
                           l.Active,
                           l.DisplayName,
                           id=l.ID) for l in self.engine.execute(sql_command).fetchall()]

    def get_logos(self, client_id):
        sql_command = 'EXECUTE [{}].[{}] {}'.format(self.schema, self.store_procedure_logos, client_id)

        self.logos = [Logo(l.Name,
                           l.Observations,
                           l.Path,
                           l.ProductID,
                           l.Active,
                           l.DisplayName,
                           id=l.ID) for l in self.engine.execute(sql_command).fetchall()]

    def get_valid_intervals(self, path_file_raw_data, vehicle_id=None, source_signal_id=None):
        sources_signals = self.source_signals if source_signal_id is None \
            else [ss for ss in self.source_signals if ss.id == source_signal_id]
        if source_signal_id is not None:
            items_real_time = [irt1 for irt1 in self.items_real_time
                               if int(irt1.source_signal_id) == int(source_signal_id)]
        else:
            items_real_time = self.items_real_time

        if vehicle_id is not None:
            items_real_time = [irt2 for irt2 in items_real_time if int(irt2.vehicle_id) == int(vehicle_id)]

        valid_intervals = self.intervals_validation_rule.get_valid_intervals(items_real_time,
                                                                             sources_signals,
                                                                             self.logos,
                                                                             path_file_raw_data)

        return valid_intervals

    def write_valid_intervals_to_sql(self, intervals=None):
        sql_command_insert = 'INSERT INTO [{}].[{}] VALUES ({})'.format(self.schema, self.table_intervals, '{}')
        valid_intervals = intervals if intervals is not None else self.get_valid_intervals()
        for interval in valid_intervals:
            print(sql_command_insert.format(interval.to_values_insert_sql(with_id=False)))
            self.engine.execute(sql_command_insert.format(interval.to_values_insert_sql(with_id=False)))

    @staticmethod
    def get_valids_values_logo_namepercent_match_ocr(logo_name, min_quantity_chars_by_value=3):
        valid_values = []
        for i in range(0, len(logo_name) - (min_quantity_chars_by_value - 1)):
            valid_values.append(logo_name[i:i + min_quantity_chars_by_value].lower())

        return valid_values

    @staticmethod
    def get_image_black_and_white(path_image, bilateral_filter=False):
        image = cv2.imread(path_image)
        image_grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Simple Threshold
        # image_black_and_white = cv2.threshold(image_grayscale, thresh, 255, cv2.THRESH_BINARY)[1]

        # Adaptative Threshold
        image_black_and_white = cv2.adaptiveThreshold(image_grayscale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                                      cv2.THRESH_BINARY, 31, 2)

        if bilateral_filter:
            image_black_and_white = cv2.bilateralFilter(image_black_and_white, 9, 75, 75)

        return image_black_and_white

    @staticmethod
    def get_image_detected_object(item_real_time: ItemRealTime, image):
        image_width, image_height = image.size
        box = (float(item_real_time.box_x_min) * image_width,
               float(item_real_time.box_y_min) * image_height,
               float(item_real_time.box_x_max) * image_width,
               float(item_real_time.box_y_max) * image_height)

        image_detected_object = image.crop(box)

        return image_detected_object

    # @staticmethod
    # def get_text_from_image(item_real_time: ItemRealTime, process_all=False, save_black_and_white_image=False,
    #                         save_detected_object_image=False, include_text_in_file_name=False, verbose=False):
    #
    #     sufix_bw = '_BW'
    #     suffix_do = '_DO'
    #     file_extension = item_real_time.path.split('.')[-1]
    #     template_file_name = '.'.join(item_real_time.path.split('.')[:-1]) + '{}_{}_.' + file_extension
    #
    #     if item_real_time.recognized_text == ItemRealTime.DEFAULT_VALUE_RECOGNIZED_TEXT or process_all:
    #         cv2_img_bw = ImplementationIntervalsValidatonRule.get_image_black_and_white(item_real_time.path)
    #         # image = Image.fromarray(cv2.cvtColor(cv2_img_bw, cv2.COLOR_BGR2RGB))
    #
    #         image = Image.fromarray(cv2_img_bw)
    #
    #         image_detected_object = ImplementationIntervalsValidatonRule.get_image_detected_object(item_real_time,
    #                                                                                                image)
    #
    #         recognized_text = pytesseract.image_to_string(image_detected_object)\
    #             .replace('\n', ' ')\
    #             .replace('/', '')\
    #             .replace('\0', '')
    #
    #         if verbose:
    #             uc.print_and_logging('image {} recognized text: {}'.format(item_real_time.path, recognized_text))
    #
    #         if save_black_and_white_image:
    #             image.save(
    #                 template_file_name.format(
    #                     sufix_bw,
    #                     '' if not include_text_in_file_name else '|{}|'.format(recognized_text)
    #                 )
    #             )
    #
    #         if save_detected_object_image:
    #             image_detected_object.save(
    #                 template_file_name.format(
    #                     suffix_do,
    #                     '' if not include_text_in_file_name else '|{}|'.format(recognized_text)
    #                 )
    #             )
    #
    #     else:
    #         recognized_text = item_real_time.recognized_text
    #
    #     return recognized_text

    @staticmethod
    def get_text_from_image(item_real_time: ItemRealTime, process_all=False, save_black_and_white_image=False,
                            save_detected_object_image=False, include_text_in_file_name=False, object_detected=None,
                            verbose=False):

        sufix_bw = '_BW'
        suffix_do = '_DO'
        file_extension = item_real_time.path.split('.')[-1]
        template_file_name = '.'.join(item_real_time.path.split('.')[:-1]) + '{}_{}_.' + file_extension
        image_path = item_real_time.path if object_detected is None else object_detected

        if item_real_time.recognized_text == ItemRealTime.DEFAULT_VALUE_RECOGNIZED_TEXT or process_all:
            cv2_img_bw = ImplementationIntervalsValidatonRule.get_image_black_and_white(image_path)
            # image = Image.fromarray(cv2.cvtColor(cv2_img_bw, cv2.COLOR_BGR2RGB))

            image = Image.fromarray(cv2_img_bw)

            if object_detected is None:
                image_detected_object = ImplementationIntervalsValidatonRule.get_image_detected_object(item_real_time,
                                                                                                       image)
            else:
                image_detected_object = image

            recognized_text = pytesseract.image_to_string(image_detected_object) \
                .replace('\n', ' ') \
                .replace('/', '') \
                .replace('\0', '')

            if verbose:
                uc.print_and_logging('image {} recognized text: {}'.format(item_real_time.path, recognized_text))

            if save_black_and_white_image:
                image.save(
                    template_file_name.format(
                        sufix_bw,
                        '' if not include_text_in_file_name else '|{}|'.format(recognized_text)
                    )
                )

            if save_detected_object_image:
                image_detected_object.save(
                    template_file_name.format(
                        suffix_do,
                        '' if not include_text_in_file_name else '|{}|'.format(recognized_text)
                    )
                )

        else:
            recognized_text = item_real_time.recognized_text

        return recognized_text

    @staticmethod
    def update_recognized_text(items_real_time: List[ItemRealTime], break_with_valid_text=False,
                               valid_values=None, verbose=False, process_all=False) \
            -> Tuple[List[ItemRealTime], ItemRealTime, float, int]:
        _break = False
        n_items_real_time = len(items_real_time)
        n_mod = int(n_items_real_time / 10) if n_items_real_time > 100 else 10
        frame_match_ocr = None
        percent_match_ocr = 0.0
        qty_items_processed = 0
        for index, item_real_time in enumerate(items_real_time):
            qty_items_processed = index + 1
            if verbose:
                if n_mod > 1:
                    if index % n_mod == 0:
                        uc.print_and_logging('Actualizados los textos reconocidos de [{}/{}] items'
                                             .format(index, n_items_real_time))
            try:
                item_real_time.recognized_text = ImplementationIntervalsValidatonRule.get_text_from_image(
                    item_real_time, process_all=process_all)
            except Exception as e:
                uc.print_and_logging('Error con el item [{}/{}] path {}:\n{}'.format(index,
                                                                                     n_items_real_time,
                                                                                     item_real_time.path,
                                                                                     e))
            if break_with_valid_text:
                if valid_values is not None:
                    # _break = self.is_valid_recognized_text(item_real_time.recognized_text, valid_values)
                    percent_match_ocr, value_coincident \
                        = ImplementationIntervalsValidatonRule.get_percent_match_recognition_and_value_coincident(
                        item_real_time.recognized_text,
                        valid_values
                    )
                    if percent_match_ocr > 0.0:
                        frame_match_ocr = item_real_time
                        message_logging_break = 'Salio al reconocer {}' \
                                                '\n\tcomo un match para el valor valido: {} ' \
                                                '\n\tteniendo una coincidencia con el display_name del logo de: {}' \
                                                '\n\tpara el item id:{} path: {}'.format(item_real_time.recognized_text,
                                                                                         value_coincident,
                                                                                         percent_match_ocr,
                                                                                         item_real_time.id,
                                                                                         item_real_time.path)
                        uc.print_and_logging(message_logging_break)
                        break

        return items_real_time, frame_match_ocr, percent_match_ocr, qty_items_processed

    @staticmethod
    def get_percent_match_recognition_and_value_coincident(recognized_text, valid_values):
        percent_match_recognition = 0.0
        value_coincident = None
        for percent_match_ocr, values in reversed(list(valid_values.items())):
            for vv in values:
                if vv.lower() in recognized_text.lower():
                    value_coincident = vv
                    percent_match_recognition = percent_match_ocr
                    break
            if percent_match_recognition != 0.0:
                break

        return percent_match_recognition, value_coincident

    @staticmethod
    def have_valid_recognized_text(items_real_time: List[ItemRealTime], valid_values: List[str]):
        recognized_texts = [item.recognized_text.lower() for item in items_real_time if item.recognized_text != '']
        have_valid_recognized_text = False
        for valid_value in valid_values:
            for recognized_text in recognized_texts:
                if valid_value in recognized_text:
                    have_valid_recognized_text = True

        return have_valid_recognized_text

    def update_recognized_text_items_real_time(self, items_real_time: List[ItemRealTime]):
        template_sql_command = self.QUERY_UPDATE_RECOGNIZED_TEXT_ITEMS_REAL_TIME.format(self.schema,
                                                                                        '{}',
                                                                                        '{}')

        items_real_time_with_recognized_text = [irt for irt in items_real_time
                                                if irt.recognized_text != ItemRealTime.DEFAULT_VALUE_RECOGNIZED_TEXT]
        for irt_wrt in items_real_time_with_recognized_text:
            sql_command = template_sql_command.format(irt_wrt.recognized_text.replace("'", ''),
                                                      irt_wrt.id)
            self.engine.execute(sql_command)

    @staticmethod
    def download_file(url, output_file, auth=()):
        print('descargando archivo {}  desde la url {}'.format(url.split('/')[-1], url))
        # NOTE the stream=True parameter below
        with requests.get(url, stream=True, auth=auth) as r:
            r.raise_for_status()
            with open(output_file, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)
                        # f.flush()
        return output_file

    @staticmethod
    def download_files_by_url(urls_outputs: List[tuple], auth=()):
        for url, output in urls_outputs:
            ImplementationIntervalsValidatonRule.download_file(url, output, auth=auth)

    def execute_filter_ocr(self, intervals: List[Interval], remote_server: Server = None, user=None, password=None):
        valids_intervals_by_recognized_text = []
        valid_values = {}
        auth = () if user is None or password is None else (user, password)
        # for logo in self.logos:
        #     valid_values[str(logo.id)] = self.get_valids_values_logo_name(logo.display_name)

        for logo in self.logos:
            min_quantity_chars_by_value = 3 if len(logo.display_name) > 3 else len(logo.display_name)
            valid_values[str(logo.id)] = logo.get_valids_values_for_name(min_quantity_chars_by_value)

        for interval in intervals:
            items_real_time = interval.get_items_real_time_for_interval(engine=self.engine)
            if remote_server is not None:
                irt_urls_output = [(irt.get_url(remote_server.ip), irt.path) for irt in items_real_time]
                self.download_files_by_url(irt_urls_output, auth=auth)

            uc.print_and_logging('Actualizando texto reconocido para los items [{}] del intervalo\n{}\n'
                                 .format(len(items_real_time), interval))
            irts_updated_recognized_text, frame_match_ocr, percent_match_ocr, qty_items_processed = \
                self.update_recognized_text(
                    items_real_time,
                    break_with_valid_text=True,
                    valid_values=valid_values[str(interval.logo_id)])

            if frame_match_ocr is not None:
                uc.print_and_logging('Actualizando valor frame_match_ocr_id[{}] '
                                     'y percent_match_ocr[{}] del intervalo'.format(frame_match_ocr.id,
                                                                                    percent_match_ocr))
                interval.frame_match_ocr_id = frame_match_ocr.id
                interval.percent_match_ocr = percent_match_ocr
                valids_intervals_by_recognized_text.append(interval)
                uc.print_and_logging('Actualizando la lista de items con sus textos reconocidos')
                # self.update_recognized_text_items_real_time(items_real_time)
                self.update_recognized_text_items_real_time(
                    [irt_wrt for irt_wrt in irts_updated_recognized_text
                     if irt_wrt.recognized_text != ItemRealTime.DEFAULT_VALUE_RECOGNIZED_TEXT]
                )

        return valids_intervals_by_recognized_text

    @staticmethod
    def static_execute_filter_ocr(intervals_with_items: List[Tuple[Interval, List[ItemRealTime]]], logos,
                                  remote_server: Server = None, user=None, password=None, process_all=False) \
            -> List[Tuple[Interval, List[ItemRealTime], int]]:
        valids_intervals_by_recognized_text = []
        valid_values = {}
        auth = () if user is None or password is None else (user, password)

        for logo in logos:
            min_quantity_chars_by_value = 3 if len(logo.display_name) > 3 else len(logo.display_name)
            valid_values[str(logo.id)] = logo.get_valids_values_for_name(min_quantity_chars_by_value)

        qty_intervals_with_items = len(intervals_with_items)
        for index, (interval, items_real_time) in enumerate(intervals_with_items):
            if remote_server is not None:
                irt_urls_output = [(irt.get_url(remote_server.ip), irt.path) for irt in items_real_time]
                ImplementationIntervalsValidatonRule.download_files_by_url(irt_urls_output, auth=auth)

            uc.print_and_logging('Actualizando texto reconocido para los items [{}] del intervalo [{}/{}]'
                                 .format(len(items_real_time),
                                         index + 1,
                                         qty_intervals_with_items))
            irts_updated_recognized_text, frame_match_ocr, percent_match_ocr, qty_items_processed = \
                ImplementationIntervalsValidatonRule.update_recognized_text(
                    items_real_time,
                    break_with_valid_text=True,
                    valid_values=valid_values[str(interval.logo_id)],
                    process_all=process_all
                )

            if frame_match_ocr is not None:
                uc.print_and_logging('Actualizando valor frame_match_ocr_id[{}] '
                                     'y percent_match_ocr[{}] del intervalo'.format(frame_match_ocr.id,
                                                                                    percent_match_ocr))
                interval.frame_match_ocr_id = frame_match_ocr.id
                interval.recognized_text = frame_match_ocr.recognized_text
                interval.percent_match_ocr = percent_match_ocr
                valids_intervals_by_recognized_text.append((interval, items_real_time, qty_items_processed))

        return valids_intervals_by_recognized_text

