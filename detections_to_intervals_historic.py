import os
import sys
import csv
import shutil
import logging
import argparse
import configparser

from datetime import datetime, timedelta
from zipfile import ZipFile, BadZipFile, LargeZipFile

import utils_cris as uc
from utils_cris import LoggingType as lt
from logo_detection_persistence import LogoDetectionPersistence as ldp, ImplementationIntervalsValidatonRule as iivr

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil, ZipValuesIfExists
from DAL.model.history_csv_processed import HistoryCSVProcessed

from drawer_detected_object import DrawerDetectedObject

# sys.path.append('/home/enzoscocco/projects/ETL_Excel_Generic')

# from LoaderExcel.LoaderExcel import LoaderExcel

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=15)
parser.add_argument('-cty',
                    '--country_id',
                    type=int,
                    help='Id del pais')
parser.add_argument('-pov',
                    '--path_output_video',
                    help='Ruta de salida de los videos generados.'
                         'Por defecto /home/enzoscocco/Documents/object_detection/data_detection/intervals',
                    default='/home/enzoscocco/Documents/object_detection/data_detection/intervals')
parser.add_argument('-fr',
                    '--frame_rate',
                    help='Tasa de frames de los videos generados para cada intervalo. Por defecto 5',
                    type=int,
                    default=5)
parser.add_argument('-ph',
                    '--path_historic',
                    default='/home/enzoscocco/Desktop/ItemsHistoric/abril')

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
country_id = args.country_id
path_output_video = args.path_output_video
frame_rate = args.frame_rate
path_historic = args.path_historic

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
template_general_log_path = 'logs/detections_to_intervals/{}/{}/info/detections_to_intervals__{}.log'
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################

try:
    configs = configparser.ConfigParser()
    configs.read(config_path)

    config_data_detection = configs['data_detection']
    database_config = configs['Database']
    database_local_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    base_path_output = config_data_detection['base_path_output']
    template_foldersname_frames_detected_datetime = \
        config_data_detection['template_foldersname_frames_detected_datetime']
    template_foldersname_frames_detected = config_data_detection['template_foldersname_frames_detected']
    path_config_csv = config_data_detection['path_config_csv']
    key_configuration_file = config_data_detection['key_configuration_file']
    csv_pattern = config_data_detection['csv_pattern']

    driver = database_config['driver']
    server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None
    schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
    table_history = database_config['table_history'] if 'table_history' in database_config else None
    sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

    # capture_servers = ssh_scp_configs['capture_servers'].split(',')
    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

    server_host_ip = uc.get_ip()

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              server,
                                              database,
                                              uid,
                                              pwd)

    logging_message_configs = '\nConfigurations detection:' \
                              '\n\tbase_path_output: {}' \
                              '\n\tcsv_pattern: {}'.format(base_path_output,
                                                           csv_pattern)

    logging_message_configs += '\nConfigurations database:' \
                               '\n\tDriver: {}' \
                               '\n\tServer: {}' \
                               '\n\tDatabase: {}' \
                               '\n\tUID: {}' \
                               '\n\tSchema history: {}' \
                               '\n\tTable history: {}' \
                               '\n\tConnection string: {}'.format(driver,
                                                                  server,
                                                                  database,
                                                                  uid,
                                                                  schema_history,
                                                                  schema_history,
                                                                  cssl.replace(pwd, '*******'))

    logging_message_configs += '\nConfigurations SSH_SCP' \
                               '\n\tusername: {}\n'.format(username)

    uc.print_and_logging(logging_message_configs)

    dal = DAL(connection_string=cssl)

    # csv_historic = ['/home/enzoscocco/ItemsPendingProcessing.part.0{}.csv'.format(n)
    #                 for n in range(0, 8)]

    csv_historic = [os.path.join(path_historic, file) for file in os.listdir(path_historic) if '.csv' in file]

    header_report = ['HistoricCSV', 'ElapsedTimeReadingHistoricCSV', 'DatetimeProcess', 'Server', 'Vehicle',
                     'SourceSignalID', 'DateEmission',
                     'PathFileRawData', 'QuantityItemsForCSV', 'IntervalValidationRuleID',
                     'QuantityIntervalsByValidationRule', 'QuantityItemsForIntervalsByValidationRule',
                     'QuantityIntervalsByOCR', 'QuantityItemsForIntervalsByOCR',
                     'ElapsedTimeValidationRule', 'ElapsedTimeDownloadImages', 'ElapsedTimeOCR',
                     'ElapsedTimeInsertIntervals', 'ElapsedTimeGenerateVideo', 'ElapsedTimeTotalProcess']

    path_report_file = '/home/enzoscocco/Desktop/report_performance_detections_to_intervals_historic_{}.csv'\
        .format(datetime_log.strftime('%Y%m%d_h%Hm%M'))
    report_file = open(path_report_file, 'w')
    report_writer = csv.writer(report_file)
    report_writer.writerow(header_report)

    capture_servers = dal.get_servers(country_id).values()
    vehicles = dal.get_vehicles()
    client = dal.get_clients()[0]
    # solo por que puedo asegurar que esta regla aplica a todos las detecciones del historico
    intervals_validation_rule = dal.get_intervals_validation_rule_by_id(1)
    logos = dal.get_logos_by_client(client.id)

    error_csv_paths = []
    qty_csv_historic = len(csv_historic)

    for n_csv, _csv in enumerate(csv_historic):
        # for vehicle in vehicles:
        try:

            ############################################################################################################
            #           Equivalente a etl_item_real_time
            ############################################################################################################

            st_process_file_historic = uc.print_start_time(
                '\n\n\nInicio de procesamiento del archivo [{}/{}] {} {}'.format(n_csv + 1,
                                                                                 qty_csv_historic,
                                                                                 _csv,
                                                                                 '{}'),
                to_log=True
            )
            st_reading_csv = uc.print_start_time('Inicio de lectura de csv: {}', to_log=True)
            csvs_item_real_time = ldp.get_items_real_from_csv_sort_by_datetime(_csv)
            et_reading_csv = uc.print_end_elapsed_time(st_reading_csv, to_log=True)

            ############################################################################################################
            # evitar procesar archivos archivos anteriores a abril
            ############################################################################################################

            dates_to_delete = [k for k in csvs_item_real_time.keys() if k.startswith('201903')]
            for date_to_delete in dates_to_delete:
                csvs_item_real_time.pop(date_to_delete)

            for csv_date, dict_path_file_raw_data in sorted(csvs_item_real_time.items()):
                qty_path_file_raw_data = len(dict_path_file_raw_data.keys())
                uc.print_and_logging('Procesando los {} archivos del {}'.format(qty_path_file_raw_data, csv_date))
                for index_pfr, (path_file_raw_data, items_real_time) in enumerate(
                        sorted(dict_path_file_raw_data.items())):

                    try:
                        ################################################################################################
                        #           Generacion de valores por defecto para cada fila del reporte a nivel de CSV
                        ################################################################################################
                        row_report = [_csv]
                        row_report.append('')  # ElapsedTimeReadingHistoricCSV
                        row_report.append('')  # DatetimeProcess
                        row_report.append('')  # Server
                        row_report.append('')  # Vehicle
                        row_report.append('')  # SourceSignalID
                        row_report.append('')  # DateEmission
                        row_report.append('')  # PathFileRawData
                        row_report.append('')  # QuantityItemsForCSV
                        row_report.append('')  # IntervalValidationRuleID
                        row_report.append('')  # QuantityIntervalsByValidationRule
                        row_report.append('')  # QuantityItemsForIntervalsByValidationRule
                        row_report.append('')  # QuantityIntervalsByOCR
                        row_report.append('')  # QuantityItemsForIntervalsByOCR
                        row_report.append('')  # ElapsedTimeValidationRule
                        row_report.append('')  # ElapsedTimeDownloadImages
                        row_report.append('')  # ElapsedTimeOCR
                        row_report.append('')  # ElapsedTimeInsertIntervals
                        row_report.append('')  # ElapsedTimeGenerateVideo
                        row_report.append('')  # ElapsedTimeGenerateVideo

                        is_csv_already_processed = dal.is_csv_already_processed(path_file_raw_data)
                        if not is_csv_already_processed:

                            row_report[1] = et_reading_csv  # ElapsedTimeReadingHistoricCSV
                            row_report[2] = str(datetime.now())  # DatetimeProcess
                            st_elapsed_time_total_process = \
                                uc.print_start_time('\tProcesando el archivo [{}/{}]: {}'
                                                    .format(index_pfr,
                                                            qty_path_file_raw_data,
                                                            path_file_raw_data)
                                                    )  # ElapsedTimeTotalProcess

                            qty_items_real_time = len(items_real_time)
                            uc.print_and_logging('\tCantidad de items a procesar: {}'.format(qty_items_real_time))
                            items_real_time.sort(key=lambda item: item.id)
                            csv_source_signal_id = ldp.get_source_signal_id_from_filename_csv(path_file_raw_data)
                            try:
                                capture_server = [sv for sv in capture_servers
                                                  if sv.is_the_owner_of_the_source_signal(csv_source_signal_id)][0]
                            except:
                                uc.print_and_logging('El source_signal_id {} no esta relacionado con ningun server de '
                                                     'captura el archivo no sera procesado'
                                                     .format(csv_source_signal_id))
                                error_csv_paths.append(path_file_raw_data)
                                continue

                            csv_vehicle_id = ldp.get_vehicle_id_from_filename_csv(path_file_raw_data)
                            vehicle = [v for v in vehicles if v.id == csv_vehicle_id]
                            if len(vehicle) > 0:
                                vehicle = vehicle[0]
                            else:
                                uc.print_and_logging('el vehicle_id {} no esta relacionado con ningun vehiculo, '
                                                     'el archivo no sera procesado'.format(csv_vehicle_id))
                                error_csv_paths.append(path_file_raw_data)
                                continue

                            csv_source_signal = capture_server.get_source_signal_by_id(int(csv_source_signal_id))

                            row_report[3] = capture_server.hostname
                            row_report[4] = vehicle.id
                            row_report[5] = csv_source_signal.id
                            row_report[6] = ldp.get_datetime_from_filename_csv(
                                os.path.basename(path_file_raw_data)
                            )  # DateEmission
                            row_report[7] = path_file_raw_data
                            row_report[8] = qty_items_real_time
                            row_report[9] = intervals_validation_rule.id

                            ############################################################################################
                            #                    Equivalente a etl_intervals
                            ############################################################################################

                            st_elapsed_time_validation_rule = uc.print_start_time(
                                '\tInicio de de obtencion de intervalos al ejecutar la regla de validacion {}',
                                to_log=True)
                            intervals = intervals_validation_rule.get_valid_intervals(items_real_time,
                                                                                      [csv_source_signal],
                                                                                      logos,
                                                                                      path_file_raw_data,
                                                                                      return_items_real_time=True,
                                                                                      server_host_ip=server_host_ip)
                            et_elapsed_time_validation_rule = uc.print_end_elapsed_time(st_elapsed_time_validation_rule,
                                                                                        to_log=True)
                            qty_intervals_by_validation_rule = len(intervals)
                            qty_items_for_intervals_by_validation_rule = sum([len(irt_vr) for _, irt_vr in intervals])

                            row_report[10] = qty_intervals_by_validation_rule
                            row_report[11] = qty_items_for_intervals_by_validation_rule

                            uc.print_and_logging('\tCantidad de intervalos encontrados segun regla de negocio: {}'
                                                 .format(qty_intervals_by_validation_rule))
                            if qty_intervals_by_validation_rule > 0:
                                st_elapsed_time_download_images = uc.print_start_time(
                                    '\tInicio de descarga de imagenes de los intervalos: {}',
                                    to_log=True)

                                dal.get_files_scp_recursive(password,
                                                            '{}:{}'.format(capture_server.ip,
                                                                           os.path.dirname(path_file_raw_data)),
                                                            os.path.dirname(path_file_raw_data)[:-3],
                                                            create_path_output=True)

                                et_elapsed_time_download_images = uc.print_end_elapsed_time(
                                    st_elapsed_time_download_images,
                                    message='\tFinalizacion de descarga de imagenes {} tiempo transcurrido {}',
                                    to_log=True
                                )

                                qty_images_downloaded = len([file for file
                                                             in os.listdir(os.path.dirname(path_file_raw_data))
                                                             if file.endswith('.jpg')]) \
                                    if os.path.isdir(os.path.dirname(path_file_raw_data)) else 0
                                if qty_images_downloaded == 0:
                                    uc.print_and_logging('No se ha descargado ninguna imagen')
                                    ssh = SshUtil(capture_server.ip, username, password)
                                    images_remote_server = [file for file
                                                            in ssh.get_ls(os.path.dirname(path_file_raw_data))
                                                            if file.endswith('.jpg')
                                                            ]
                                    if len(images_remote_server) == 0:
                                        uc.print_and_logging('No existen imagenes de detecciones en la carpeta {} '
                                                             'del server {} '
                                                             'ip {}'.format(os.path.dirname(path_file_raw_data),
                                                                            capture_server.hostname,
                                                                            capture_server.ip))

                                else:
                                    st_elapsed_time_ocr = uc.print_start_time('Inicio de ejecucion de filtro OCR {}',
                                                                              to_log=True)

                                    # Obtencion de intervalos por OCR
                                    intervals_by_recognition = iivr.static_execute_filter_ocr(intervals,
                                                                                              logos,
                                                                                              process_all=True)

                                    qty_intervals_by_ocr = len(intervals_by_recognition)
                                    uc.print_and_logging('\tCantidad de intervalos que pasaron filtro OCR: {}'
                                                         .format(qty_intervals_by_ocr))
                                    et_elapsed_time_ocr = uc.print_end_elapsed_time(
                                        st_elapsed_time_ocr,
                                        message='\tFinalizacion de ejecucion de filtro OCR {}  tiempo transcurrido {}',
                                        to_log=True
                                    )

                                    qty_items_for_intervals_by_ocr = sum([len(irt_ocr) for _, irt_ocr
                                                                          in intervals_by_recognition])

                                    row_report[12] = qty_intervals_by_ocr  # QuantityIntervalsByOCR
                                    row_report[13] = qty_items_for_intervals_by_ocr  # QuantityItemsForIntervalsByOCR
                                    row_report[14] = et_elapsed_time_validation_rule  # ElapsedTimeValidationRule
                                    row_report[15] = et_elapsed_time_download_images  # ElapsedTimeDownloadImages
                                    row_report[16] = et_elapsed_time_ocr  # ElapsedTimeOCR

                                    ########################################################################################
                                    #           Equivalente a etl_video_intervals
                                    ########################################################################################

                                    st_elapsed_time_generate_video = uc.print_start_time(
                                        'Inicio de generacion de videos: {}',
                                        to_log=True)
                                    for n_interval_ocr, (interval_ocr, irt_for_interval_ocr) \
                                            in enumerate(intervals_by_recognition):

                                        ####################################################################################
                                        # Precaución por si alguna imagen no fue descargada anteriormente
                                        ####################################################################################
                                        irts_without_downloaded_images = []
                                        [irts_without_downloaded_images.append(wodi.path)
                                         for wodi in irt_for_interval_ocr if not os.path.isfile(wodi.path)]

                                        if len(irts_without_downloaded_images) > 0:
                                            zip_list_names = '\n'.join([os.path.basename(frame_path) for frame_path
                                                                        in irts_without_downloaded_images])
                                            zip_path_input = os.path.dirname(irts_without_downloaded_images[0])
                                            # server_interval = dal.get_server_by_interval_id(interval.id)
                                            ssh = SshUtil(capture_server.ip, username, password)
                                            path_file_list_names = os.path.join(zip_path_input,
                                                                                interval_ocr.get_file_name_list_frames())

                                            uc.print_and_logging('escribiendo archivo con la lista de los frames faltantes '
                                                                 'para el intervalo {}'
                                                                 '\n\tzip_list_names: {}'
                                                                 '\n\tpath_file: {}'.format(interval_ocr.id,
                                                                                            zip_list_names,
                                                                                            path_file_list_names))
                                            ssh.write_to_file(text=zip_list_names,
                                                              path_file=path_file_list_names,
                                                              append=False)

                                            zip_path_output = 'missing_frames_interval_{}.zip'.format(interval_ocr.id)
                                            uc.print_and_logging('creando zip:'
                                                                 '\n\tzip_path_input: {}'
                                                                 '\n\tzip_list_names: {}'
                                                                 '\n\tzip_path_output: {}'.format(zip_path_input,
                                                                                                  zip_list_names,
                                                                                                  zip_path_output))
                                            zip_path_file = ssh.zip_from_file_list(path_input=zip_path_input,
                                                                                   name_list=path_file_list_names,
                                                                                   path_output=zip_path_output)

                                            uc.print_and_logging('Descargando zip {} en carpeta equivalente'
                                                                 .format(zip_path_file))
                                            ssh.get_file_scp(path_remote_file=zip_path_file,
                                                             path_output=zip_path_file,
                                                             create_path_output=True)

                                            ssh.rm_file(zip_path_file)

                                            try:
                                                uc.print_and_logging('Extrayendo imagenes en {}'
                                                                     .format(os.path.dirname(zip_path_file)))
                                                zip_ref = ZipFile(zip_path_file, 'r')
                                                zip_ref.extractall(os.path.dirname(zip_path_file))
                                                zip_ref.close()
                                            except (BadZipFile, LargeZipFile) as zip_exception:
                                                error_csv = True
                                                raise ValueError('Ocurrio una excepcion durante la lectura del zip:\n{}'
                                                                 .format(zip_exception))

                                        ####################################################################################
                                        # Creacion de las imagenes con los objetos detectados dibujados
                                        ####################################################################################
                                        paths_images_with_detected_objects = DrawerDetectedObject\
                                            .static_write_images_with_detected_objects(irt_for_interval_ocr,
                                                                                       logos)

                                        ####################################################################################
                                        # Ejecucion de FFMPEG para generar el video
                                        ####################################################################################
                                        try:
                                            path_input = os.path.dirname(irt_for_interval_ocr[0].path)
                                            real_path_output_video = os.path.join(
                                                path_output_video,
                                                'generation_{}/emission_{}'.format(
                                                    datetime.now().strftime('%Y%m%d'),
                                                    irt_for_interval_ocr[0].date.strftime('%Y%m%d'))
                                            )
                                            uc.create_folders(os.path.join(real_path_output_video,
                                                                           interval_ocr.get_video_name()),
                                                              path_base='/')
                                            uc.print_and_logging('generate_video: path_input={}, path_output={}, '
                                                                 'frame_rate={}'
                                                                 .format(path_input,
                                                                         real_path_output_video,
                                                                         frame_rate))
                                            interval_ocr.path_video = interval_ocr.generate_video(path_input,
                                                                                                  real_path_output_video,
                                                                                                  frame_rate=frame_rate)
                                        except ValueError as ffmpeg_exc:
                                            error_csv = True
                                            uc.print_and_logging('Ocurrio una excepcion '
                                                                 'durante la generacion del video del intervalo:\n',
                                                                 logging_type=lt.EXCEPTION)

                                    ########################################################################################

                                    et_elapsed_time_generate_video = uc.print_end_elapsed_time(
                                        st_elapsed_time_generate_video,
                                        message='Finalizacion de la generación de videos {} tiempo transcurrido {}',
                                        to_log=True
                                    )

                                    if qty_intervals_by_ocr > 0:
                                        st_elapsed_time_insert_intervals = uc.print_start_time(
                                            '\tInicio de insercion de intervalos en la base de datos: {}',
                                            to_log=True)
                                        dal.insert_intervals([ibc for ibc, _ in intervals_by_recognition],
                                                             verbose=True)
                                        et_elapsed_time_insert_intervals = uc.print_end_elapsed_time(
                                            st_elapsed_time_insert_intervals,
                                            to_log=True
                                        )

                                    else:
                                        et_elapsed_time_insert_intervals = timedelta()

                                    et_elapsed_time_total_process = uc.print_end_elapsed_time(
                                        st_elapsed_time_total_process,
                                        message='\tFinalizacion de procesamiento del archivo {}: {}'.format(index_pfr,
                                                                                                            '{}'),
                                        to_log=True
                                    )

                                    dal.insert_history_csv_processed(path=path_file_raw_data,
                                                                     client_id=client.id,
                                                                     vehicle_id=csv_vehicle_id,
                                                                     source_signal_id=csv_source_signal_id,
                                                                     server_id=capture_server.id,
                                                                     process_interval=1,
                                                                     process_ocr=1,
                                                                     process_video_intervals=1,
                                                                     verbose=True)

                                    if os.path.isdir(os.path.dirname(path_file_raw_data)):
                                        shutil.rmtree(os.path.dirname(path_file_raw_data), ignore_errors=True)

                                    row_report[17] = et_elapsed_time_insert_intervals  # ElapsedTimeInsertIntervals
                                    row_report[18] = et_elapsed_time_generate_video  # ElapsedTimeGenerateVideo
                                    row_report[19] = et_elapsed_time_total_process  # ElapsedTimeTotalProcess

                                    report_writer.writerow(row_report)
                                    report_file.close()  # para poder tener aceso a los datos parciales
                                    if os.path.isfile(path_report_file):
                                        report_file = open(path_report_file, 'a')
                                        report_writer = csv.writer(report_file)
                                    else:
                                        report_file = open(path_report_file, 'w')
                                        report_writer = csv.writer(report_file)
                                        report_writer.writerow(header_report)

                        else:
                            uc.print_and_logging('El archivo {} ya se encuentra procesado'.format(path_file_raw_data))

                    except Exception as ex_path_file_raw_data:
                        uc.print_and_logging('Ocurrio una excepcion con el procesamiento de path_file_raw_data {}:'
                                             '\n{}'.format(path_file_raw_data, ex_path_file_raw_data))

        except KeyboardInterrupt as ki:
            uc.print_and_logging('Saliendo por KeyboardInterrupt')
            report_file.close()
            sys.exit()
        except Exception as e:
            uc.print_and_logging('Ocurrio una exception con el server {}'.format(server),
                                 logging_type=lt.EXCEPTION)

    et_process_file_historic = uc.print_end_elapsed_time(st_process_file_historic,
                                                         message='El procesamiento del archivo historico CSV '
                                                                 'termino {} Tiempo transcurrido {}',
                                                         to_log=True)

except KeyboardInterrupt as ki:
    uc.print_and_logging('Saliendo por KeyboardInterrupt')
    report_file.close()
    sys.exit()
except Exception as e:
    uc.print_and_logging('Ocurrio una excepcion que no fue captura en los excepts anteriores y llego hasta el superior',
                         logging_type=lt.EXCEPTION)
