import argparse

from datetime import datetime

from training import Training

# -tn 20190128_summit_ipgmediabrands
# -xd images/20190128_summit_ipgmediabrands_test images/20190128_summit_ipgmediabrands_train
# -ts 3000
parser = argparse.ArgumentParser()
parser.add_argument('-tn',
                    '--training_name',
                    required=True,
                    help='Nombre del entranamiento que sera utilizado en todas las rutas, '
                         'por ejemplo en data/<training_name>/test.record, '
                         'inference_graphs/<training_name>/frozen_inference_graph.pb, '
                         'trainings/<training_name>/ssd_mobilenet_v1_pets.config, etc.')
parser.add_argument('-xd',
                    '--xml_directories',
                    help='Directorios que contienen los xml a copnvertir. Ejemplo: xml/_test xml/_train',
                    nargs='+',
                    required=True)
parser.add_argument('-ts',
                    '--training_num_steps',
                    help='Cantidad de steps de duracion del entrenamiento. Default 10000 steps.',
                    default=10000,
                    type=int)


args = parser.parse_args()
training_name = args.training_name
xml_directories = args.xml_directories
training_num_steps = args.training_num_steps

larg = []
[larg.extend(a) for a in args._get_kwargs()]
print('\nArgumentos:'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}\n'.format(*larg))

INIT_SEPARATOR = '\n\n\n#################################################################################################'
END_SEPARATOR = '#################################################################################################\n\n\n'


# python xml_to_csv.py --xml_directories images/20190122_summit_ipgmediabrands_test \
#                                        images/20190122_summit_ipgmediabrands_train \
#                      --path_output data/20190122_summit_ipgmediabrands
training = Training(training_name, xml_directories, training_num_steps)
training.xml_directories_to_csv()
print(INIT_SEPARATOR)
print('{} xml_directories_to_csv successfully'.format(datetime.now()))
print(END_SEPARATOR)

# python generic_generate_tfrecord.py
#       --csv_input data/20190122_summit_ipgmediabrands/20190122_summit_ipgmediabrands_test_labels.csv
#       --image_dir images/20190122_summit_ipgmediabrands_test
#       --output_path data/20190122_summit_ipgmediabrands/test.record
#       --path_labelmap data/20190122_summit_ipgmediabrands/label_map.pbtx
training.generate_tfrecords()
print(INIT_SEPARATOR)
print('{} generate_tfrecords successfully'.format(datetime.now()))
print(END_SEPARATOR)

# python train.py --logtostderr
#                 --train_dir=trainings/20190122_summit_ipgmediabrands/
#                 --pipeline_config_path=trainings/20190122_summit_ipgmediabrands/ssd_mobilenet_v1_pets.config
training.train()
print(INIT_SEPARATOR)
print('{} train successfully'.format(datetime.now()))
print(END_SEPARATOR)


# python original_export_inference_graph.py \
#     --input_type image_tensor \
#     --pipeline_config_path trainings/20190122_summit_ipgmediabrands/ssd_mobilenet_v1_pets.config \
#     --trained_checkpoint_prefix trainings/20190122_summit_ipgmediabrands/model.ckpt-8967 \
#     --output_directory inference_graphs/20190122_summit_ipgmediabrands
training.export_inference_graph()
print(INIT_SEPARATOR)
print('{} export_inference_graph successfully'.format(datetime.now()))
print(END_SEPARATOR)

print(INIT_SEPARATOR)
print('{} Proceso terminado'.format(datetime.now()))
print(END_SEPARATOR)
input('Presione una tecla para terminar\n')
