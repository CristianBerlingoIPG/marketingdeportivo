"""
Usage:
  # From tensorflow/models/
  # Create train data:
  python generate_tfrecord.py --csv_input=images/train_labels.csv --image_dir=images/train --output_path=train.record

  # Create test data:
  python generate_tfrecord.py --csv_input=images/test_labels.csv  --image_dir=images/test --output_path=test.record
"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import os
import io
import sys
import pandas as pd
import tensorflow as tf

from PIL import Image

sys.path.append("..")
from object_detection.utils import dataset_util
# from utils import dataset_util
from collections import namedtuple, OrderedDict

flags = tf.app.flags
flags.DEFINE_string('csv_input', '', 'Path to the CSV input')
flags.DEFINE_string('image_dir', '', 'Path to the image directory')
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
flags.DEFINE_string('path_labelmap', '', 'Path to labelmap')
FLAGS = flags.FLAGS


# TO-DO replace this with label map
# def class_text_to_int(row_label):
#     if row_label == 'directv':
#         return 1
#     elif row_label == 'Qatar_airways_logo':
#         return 2
#     else:
#         return None

# volver generico class_to_int
def class_text_to_int(row_label, labelmap):
    return get_int_label(row_label, labelmap)


def get_int_label(label, labelmap):
    return int([lm for lm in labelmap if lm['name'] == label][0]['id'])


def rawitem_to_parseditem(raw_item):
    attributes_pairs = [i.strip().split(':') for i in raw_item.strip().split('\n') if '{' not in i and '}' not in i]
    item = {k.strip().replace('"', ''): v.strip().replace('"', '') for k, v in attributes_pairs}

    return item


# hecho pensando un labelmap con esta estructura
# item {
#   id: 1
#   name: "directv"
#   display_name: "directv"
# }
#
# item {
#   id: 2
#   name: "Qatar_airways_logo"
#   display_name: "Qatar_airways_logo"
# }
def parse_text_labelmap_to_dict(text_labelmap):
    raw_items = [ri.strip() for ri in text_labelmap.split('item') if ri != '']
    parsed_items = [rawitem_to_parseditem(raw_item) for raw_item in raw_items]

    return parsed_items


def split(df, group):
    data = namedtuple('data', ['filename', 'object'])
    gb = df.groupby(group)
    return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]


def create_tf_example(group, path, labelmap):
    print('group.filename', group.filename)
    with tf.gfile.GFile(os.path.join(path, '{}'.format(group.filename)), 'rb') as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = Image.open(encoded_jpg_io)
    width, height = image.size

    filename = group.filename.encode('utf8')
    image_format = b'jpg'
    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []

    for index, row in group.object.iterrows():
        xmins.append(row['xmin'] / width)
        xmaxs.append(row['xmax'] / width)
        ymins.append(row['ymin'] / height)
        ymaxs.append(row['ymax'] / height)
        classes_text.append(row['class'].encode('utf8'))
        classes.append(class_text_to_int(row['class'], labelmap))

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


def main(_):
    with open(FLAGS.path_labelmap) as f:
        labelmap = parse_text_labelmap_to_dict(f.read())
    writer = tf.python_io.TFRecordWriter(FLAGS.output_path)
    path = os.path.join(os.getcwd(), FLAGS.image_dir)
    print('path', path)
    examples = pd.read_csv(FLAGS.csv_input)
    grouped = split(examples, 'filename')
    for group in grouped:
        tf_example = create_tf_example(group, path, labelmap)
        writer.write(tf_example.SerializeToString())

    writer.close()
    output_path = os.path.join(os.getcwd(), FLAGS.output_path)
    print('Successfully created the TFRecords: {}'.format(output_path))


if __name__ == '__main__':
    print('empezo')
    tf.app.run()
