# https://adamspannbauer.github.io/2018/03/02/app-icon-dominant-colors/
import cv2
import numpy as np
import argparse
# from utils.colorutils import get_dominant_color
from get_dominant_color import get_dominant_color, get_all_dominant_color

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i",
                "--imagePath",
                required=True,
                help="Path to image to find dominant color of")
ap.add_argument("-k",
                "--clusters",
                default=3,
                type=int,
                help="Number of clusters to use in kmeans when finding dominant color")
args = vars(ap.parse_args())

# read in image of interest
bgr_image = cv2.imread(args['imagePath'])
# convert to HSV; this is a better representation of how we see color
hsv_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2HSV)

# extract dominant color
# (aka the centroid of the most popular k means cluster)
# dom_color = get_dominant_color(hsv_image, k=args['clusters'])
dominant_colors_and_percentage = get_all_dominant_color(hsv_image, k=args['clusters'])

# create a square showing dominant color of equal size to input image
height, difference = divmod(bgr_image.shape[0], len(dominant_colors_and_percentage))
width = 100
depth = bgr_image.shape[2]
dominant_colors_hsv = []
for index, dominant_color in enumerate([dc['color'] for dc in dominant_colors_and_percentage]):
    if index == 0:
        dominant_colors_hsv.append(np.full((height + difference, width, depth),
                                           dominant_color,
                                           dtype='uint8'))
    else:
        dominant_colors_hsv.append(np.full((height, width, depth),
                                           dominant_color,
                                           dtype='uint8'))

joined_dominant_color_hsv = np.concatenate(dominant_colors_hsv)
# convert to bgr color space for display
dom_color_bgr = cv2.cvtColor(joined_dominant_color_hsv, cv2.COLOR_HSV2BGR)

# concat input image and dom color square side by side for display
output_image = np.hstack((bgr_image, dom_color_bgr))

# show results to screen
cv2.imshow('Image Dominant Color', output_image)
cv2.waitKey(0)
