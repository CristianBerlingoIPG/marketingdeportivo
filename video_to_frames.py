import os
import cv2
import logging
import argparse
import mimetypes

from datetime import datetime

import utils_cris as uc

parser = argparse.ArgumentParser(description='Script para extraer frames de video')
parser.add_argument('-pv',
                    '--path_video',
                    type=str,
                    help='Ruta del video ruta o de la carpeta que contiene los videos. En caso de pasar la ruta de una '
                         'carpeta se procesaran todos los videos que esten en esta',
                    required=True)
parser.add_argument('--path_output',
                    type=str,
                    help='Ruta destino de los frames. Por defecto se creara una carpeta con el mismo nombre del video '
                         'con el sufijo "_frames".',
                    default=None)
parser.add_argument('--one_of_each',
                    type=int,
                    help='Parametro que permite establecer cada cuantos frames se debe guardar uno.'
                         'Por defecto se guardan todos los frames.',
                    default=1)
parser.add_argument('-of',
                    '--one_folder',
                    help='Carpeta donde se guardaran todas las imagenes sin considerar la logica por default de '
                         'separacion de carpetas.',
                    default=None)

SUFFIX_PATH_OUTPUT = '_frames'

args = parser.parse_args()
path_video = args.path_video
path_output = args.path_output if args.path_output is not None \
    else os.path.abspath(path_video).split('.')[0] + SUFFIX_PATH_OUTPUT
one_of_each = args.one_of_each
one_folder = args.one_folder

if not os.path.exists(path_output):
    os.mkdir(path_output)

logging.basicConfig(filename=path_output + '/' + '{:%Y-%m-%d}'.format(datetime.now()) + '.log',
                    level=logging.DEBUG,
                    format='%(asctime)s - [%(filename)s:%(lineno)d - %(exc_info)s - %(stack_info)s] - %(message)s',
                    filemode='w')

video_files = [
    os.path.join(path_video, v) for v in os.listdir(path_video)
    if os.path.isfile(os.path.join(path_video, v))
       and mimetypes.guess_type(v)[0] is not None
       and 'video' in mimetypes.guess_type(v)[0]
] if os.path.isdir(path_video) else [path_video]

for video_file in video_files:
    print('\nProcesando video: {}'.format(video_file))
    video_name, video_extension = os.path.splitext(os.path.basename(video_file))
    video = cv2.VideoCapture(video_file)
    total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    nframe = 0
    nframe_saved = 0
    nframes_per_folder = 200
    template_mkdir = '{}/{}/frames_from_{}_to_{}'.format(path_output,
                                                         '.'.join(os.path.basename(video_file).split('.')[:-1]),
                                                         '{}',
                                                         '{}')

    template_frame_name = '{}/{}_{}.jpg'.format('{}', video_name, '{}')
    while video.isOpened() and nframe <= total_frames:
        try:
            ret, frame = video.read()
            if nframe_saved % nframes_per_folder == 0:
                path_mkdir = template_mkdir.format(nframe_saved,
                                                   (nframes_per_folder * ((nframe_saved // nframes_per_folder) + 1))-1)
                # if not os.path.isdir(path_mkdir):
                #     os.mkdir(path_mkdir)
            if nframe % one_of_each == 0 and frame is not None:
                file_frame_name = template_frame_name.format(path_mkdir, nframe)
                logging.info('Guardando frame numero ' + str(nframe) + ' en ' + file_frame_name)
                if one_folder is not None:
                    file_frame_name = os.path.join(one_folder, os.path.basename(file_frame_name))
                uc.create_folders(file_frame_name, path_base='/')
                cv2.imwrite(file_frame_name, frame)
                nframe_saved += 1
            nframe += 1
        except Exception as e:
            logging.exception('Ocurrio una excepcion\n')
            print('Ocurrio una excepcion')
            if e is not None:
                print(e)
    print('Cantidad de frames:', nframe)
