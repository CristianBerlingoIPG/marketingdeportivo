-- Tiempo capturado por vehiculo
SELECT vh.ID, vh.[Name] Vehicle,
       COUNT(irt.ID) [Detected frames], 
       COUNT(irt.ID) / 15 Seconds, 
       COUNT(irt.ID) / (15*60) [Minutes],
       COUNT(irt.ID) / CONVERT(DECIMAL(7,2), (15*60*60)) [Hours]
    FROM Detection.ItemRealTime irt
        INNER JOIN Detection.SourceSignal ss
            ON irt.SourceSignalID = ss.ID
        INNER JOIN Media.Vehicle vh
            ON irt.VehicleID = vh.ID
    GROUP BY vh.ID, vh.Name
    ORDER BY vh.ID




-- Tiempo capturado por vehiculo, mes y dia
SELECT vh.ID VehicleID, vh.[Name] Vehicle, DATEPART(MONTH, irt.[Date]) [Month], DATEPART(DAY, irt.[Date]) [Day],
       COUNT(irt.ID) [Detected frames], 
       COUNT(irt.ID) / 15 Seconds, 
       COUNT(irt.ID) / (15*60) [Minutes],
       COUNT(irt.ID) / CONVERT(DECIMAL(7,2), (15*60*60)) [Hours]
    FROM Detection.ItemRealTime irt
        INNER JOIN Detection.SourceSignal ss
            ON irt.SourceSignalID = ss.ID
        INNER JOIN Media.Vehicle vh
            ON irt.VehicleID = vh.ID
    GROUP BY vh.ID, vh.[Name], DATEPART(MONTH, irt.[Date]), DATEPART(DAY, irt.[Date])
    ORDER BY vh.ID, vh.[Name], DATEPART(MONTH, irt.[Date]), DATEPART(DAY, irt.[Date])




-- Tiempo capturado por vehiculo, dia y hora
SELECT vh.ID VehicleID, vh.[Name] Vehicle, DATEPART(MONTH, irt.[Date]) [Month], DATEPART(DAY, irt.[Date]) [Day],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 0 THEN 1 ELSE 0 END) / (15*60) [00],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 1 THEN 1 ELSE 0 END) / (15*60) [01],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 2 THEN 1 ELSE 0 END) / (15*60) [02],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 3 THEN 1 ELSE 0 END) / (15*60) [03],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 4 THEN 1 ELSE 0 END) / (15*60) [04],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 5 THEN 1 ELSE 0 END) / (15*60) [05],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 6 THEN 1 ELSE 0 END) / (15*60) [06],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 7 THEN 1 ELSE 0 END) / (15*60) [07],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 8 THEN 1 ELSE 0 END) / (15*60) [08],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 9 THEN 1 ELSE 0 END) / (15*60) [09],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 10 THEN 1 ELSE 0 END) / (15*60) [10],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 11 THEN 1 ELSE 0 END) / (15*60) [11],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 12 THEN 1 ELSE 0 END) / (15*60) [12],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 13 THEN 1 ELSE 0 END) / (15*60) [13],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 14 THEN 1 ELSE 0 END) / (15*60) [14],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 15 THEN 1 ELSE 0 END) / (15*60) [15],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 16 THEN 1 ELSE 0 END) / (15*60) [16],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 17 THEN 1 ELSE 0 END) / (15*60) [17],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 18 THEN 1 ELSE 0 END) / (15*60) [18],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 19 THEN 1 ELSE 0 END) / (15*60) [19],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 20 THEN 1 ELSE 0 END) / (15*60) [20],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 21 THEN 1 ELSE 0 END) / (15*60) [21],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 22 THEN 1 ELSE 0 END) / (15*60) [22],
       SUM(CASE WHEN DATEPART(HOUR, irt.[Date]) = 23 THEN 1 ELSE 0 END) / (15*60) [23],
       SUM(1) / (15*60) [Daily Total Minutes]
    FROM Detection.ItemRealTime irt
        INNER JOIN Detection.SourceSignal ss
            ON irt.SourceSignalID = ss.ID
        INNER JOIN Detection.[Server] sv
            ON ss.ServerID = sv.ID
        INNER JOIN Media.Vehicle vh
            ON irt.VehicleID = vh.ID
    GROUP BY vh.ID, vh.Name, DATEPART(MONTH, irt.[Date]), DATEPART(DAY, irt.[Date])
    ORDER BY vh.ID, vh.Name, DATEPART(MONTH, irt.[Date]), DATEPART(DAY, irt.[Date])



-- Intervalos actuales con el frame que logro pasar el OCR
-- ID 419 intervalo detectado de la prueba de rugby
-- ID 421 intervalo detectado de la prueba de rugby
SELECT ie.*
    FROM [Detection].[IntervalExpanded] ie
    WHERE ie.ID > 902
    ORDER BY ie.PercentMatchOCR DESC, ID DESC


SELECT * 
	FROM [Detection].HistoryCSVProcessed 
	WHERE ClientID = 2 
		  AND ProcessInterval = 0
		  AND ID % 3 = 2


-- Consulta para saber cuantos archivos CSV que contienen las detecciones realizadas quedan por procesar
SELECT COUNT(ID) --COUNT(1) [Cantidad de CSVs sin procesar]
	FROM [Detection].HistoryCSVProcessed 
	WHERE ClientID = 2 
		  AND ProcessInterval = 0

SELECT * FROM Detection.[Server]

SELECT TOP 1 * FROM Detection.ItemRealTime WHERE RecognizedText LIKE '%SIN%' --ID = 1820731

SELECT lg.*
    FROM Detection.Logo lg


SELECT COUNT(1) FROM Detection.Interval WHERE PathVideo = ''


SELECT i.*, irt.RecognizedText, irt.[Path]
    FROM Detection.Interval i
        INNER JOIN Detection.ItemRealTime irt
            ON i.FrameMatchOCRID = irt.ID
    -- WHERE i.ID = 797
    WHERE i.PathVideo = ''
    ORDER BY i.PercentMatchOCR DESC


ALTER TABLE Detection.Interval ADD FrameMatchOCRID BIGINT DEFAULT 0
ALTER TABLE Detection.Interval ADD PercentMatchOCR DECIMAL (5, 2) NOT NULL DEFAULT 0

ALTER TABLE Detection.Interval DROP DF__Interval__Percen__3E1D39E1
ALTER TABLE Detection.Interval DROP COLUMN PercentMatchOCR


SELECT * FROM Detection.Interval WHERE PathVideo = '/home/enzoscocco/Documents/object_detection/data_detection/intervals/id687_rid1_lid1_vid6_sid26_st20190410h22m03s40_et20190410h22m03s45_sirt1791807_eirt1791838.mp4'
DELETE FROM Detection.Interval WHERE ID = 783
INSERT INTO Detection.Interval 
    VALUES (1,1,6,26,'2019-04-10 22:03:40.923','2019-04-10 22:03:45.923',1791807,1791838,0.00000,0.00000,0.00000,
            '/home/enzoscocco/Documents/object_detection/data_detection/intervals/id687_rid1_lid1_vid6_sid26_st20190410h22m03s40_et20190410h22m03s45_sirt1791807_eirt1791838.mp4',
            0.0,0.0)

--ID	IntervalsValidationRuleID	LogoID	VehicleID	SourceSignalID	Starttime	Endtime	StartItemRealTimeID	EndItemRealTimeID	GRPS	GrossInvestment	NetInvestment	PathVideo	FrameMatchOCRID	PercentMatchOCR

EXEC [Detection].[GetItemsRealTimeForIntervalWithIndistinctLogo] 1793259, 1797446, '2019-04-10 23:48:07.037', '2019-04-10 23:49:20.503', 6, 26



ALTER PROCEDURE [Detection].[GetItemsRealTimeForIntervalWithIndistinctLogo]
	@start_item_real_time_id INT,
	@end_item_real_time_id INT,
    @start_time VARCHAR(23),
    @end_time VARCHAR (23),
	@vehicle_id INT,
	@source_signal_id INT
AS
	SELECT *
		FROM Detection.ItemRealTime irt
		WHERE irt.ID >= @start_item_real_time_id
			  AND irt.ID <= @end_item_real_time_id
              AND irt.[Date] >= @start_time
              AND irt.[Date] <= @end_time
			  AND irt.VehicleID = @vehicle_id
			  AND irt.SourceSignalID = @source_signal_id

GO

SELECT * FROM Detection.[Server]

CREATE PROCEDURE [Detection].[GetItemsRealTimeForIntervalWithLogo]
	@start_item_real_time_id INT,
	@end_item_real_time_id INT,
	@logo_id INT,
	@vehicle_id INT,
	@source_signal_id INT
AS
SELECT irt.ID, irt.VehicleID, irt.SourceSignalID, 
	   irt.[Date], irt.[Path], irt.Score,
	   irt.BoxYMin, irt.BoxXMin, irt.BoxYMax, irt.BoxXMax,
	   irt.PathFileRawData,
	   lg.ID [LogoID], lg.Name LogoName, lg.Observations LogoObservations,
	   lg.[Path] LogoPath, lg.ProductID LogoProductID,
	   lg.Active LogoActive, lg.DisplayName
	FROM Detection.ItemRealTime irt
		INNER JOIN Detection.Logo lg
			ON irt.LogoID = lg.ID
	WHERE irt.ID >= @start_item_real_time_id
			AND irt.ID <= @end_item_real_time_id
			AND irt.LogoID = @logo_id
			AND irt.VehicleID = @vehicle_id
			AND irt.SourceSignalID = @source_signal_id
GO




SELECT * FROM Media.Vehicle

UPDATE [Detection].[Interval]
	SET PathVideo = ''
    WHERE ID = 817

SELECT * FROM Detection.Interval WHERE ID = 1465


