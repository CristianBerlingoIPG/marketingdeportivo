import sys
sys.path.append("..")

import os
import cv2
import ast
import argparse
import numpy as np
import tensorflow as tf

from utils import label_map_util
from utils import visualization_utils as vis_util

from logo_detection_persistence import LogoDetectionPersistence

parser = argparse.ArgumentParser()
parser.add_argument('--stream_path',
                    help='Ruta del stream',
                    required=True)
parser.add_argument('--model_path',
                    help='Ruta del modelo',
                    required=True)
parser.add_argument('--device',
                    help='Dispositivo sobre cual ejecutar el procesamiento grafico. '
                         'Ejemplo: /device:GPU:0 para utilizar el primer dispositivo gpu.'
                         '         /cpu:0 para utilizar el CPU.'
                         'Mas informacion: https://www.tensorflow.org/guide/using_gpu.'
                         'Default: /device:GPU:0',
                    default='/device:GPU:0')
parser.add_argument('--path_output_file',
                    help='Path donde se guardara el video con los objetos detectados.'
                         'Si no se configura un path no se guarda el archivo de salida.',
                    default=None)
parser.add_argument('--show_input',
                    help='Boolean que define si se mostrara en una ventana el input.'
                         'Default: False.',
                    default='False')
parser.add_argument('--show_output',
                    help='Boolean que define si se mostrara en una ventana el output.'
                         'Default: False.',
                    default='False'),
parser.add_argument('-mst',
                    '--min_score_thresh',
                    help='Decimal que establece el porcentaje minimo de coincidencia para que una deteccion sea '
                         'mostrada. Default: 0.5',
                    default=0.5,
                    type=float)
parser.add_argument('-plm',
                    '--path_labelmap',
                    help='Path del labelmap',
                    required=True)

MAX_FRAMES_NONE = 200

args = parser.parse_args()

larg = []
[larg.extend(a) for a in args._get_kwargs()]
print('\nParameters:'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}\n'.format(*larg))

stream_path = args.stream_path
model_path = args.model_path
device = args.device
path_output_file = args.path_output_file
show_input = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_input == 'True' else False
show_output = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_output == 'True' else False
min_score_thresh = args.min_score_thresh
path_labelmap = args.path_labelmap


# NUM_CLASSES = 2
# PATH_TO_LABELS = os.path.join('data/ipgmediabrands', 'label_map.pbtxt')
# label_map = label_map_util.load_labelmap('data/20190201_summit_ipgmediabrands/label_map.pbtxt')
label_map = label_map_util.load_labelmap(path_labelmap)
num_clases = len(label_map.item)
categories = label_map_util.convert_label_map_to_categories(label_map,
                                                            max_num_classes=num_clases,
                                                            use_display_name=True)

category_index = label_map_util.create_category_index(categories)

with tf.device(device):
    detection_graph = tf.Graph()
# config = tf.ConfigProto(
#     device_count={'GPU': 0},
#     log_device_placement=True
# )
config = tf.ConfigProto(
    log_device_placement=True
)
# config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.5

with detection_graph.as_default():
    od_graph_def = tf.GraphDef()

    with tf.gfile.GFile(model_path, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    # sess = tf.Session(graph=detection_graph)
    sess = tf.Session(graph=detection_graph, config=config)

image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

num_detections = detection_graph.get_tensor_by_name('num_detections:0')

video = cv2.VideoCapture(stream_path)
nframe = 0
nframe_none = 0
# ldp = LogoDetectionPersistence(1, 2, )
while video.isOpened():
    try:
        frame = video.read()[1]
        nframe += 1
        if frame is not None:
            nframe_none = 0
            color_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame_expanded = np.expand_dims(color_frame, axis=0)
            (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: frame_expanded})
            has_boxes = vis_util.visualize_boxes_and_labels_on_image_array(
                color_frame,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=8,
                min_score_thresh=min_score_thresh)[1]
                # frame_greyscale = frame

            frame = cv2.cvtColor(color_frame, cv2.COLOR_RGB2BGR)
            if len(has_boxes) > 0:
                cv2.imwrite('images/test_stream/{}.jpg'.format(nframe), frame)

            cv2.imshow(stream_path, frame)

            if cv2.waitKey(1) == ord('q'):
                break
        else:
            nframe_none += 1
    except Exception as e:
        if e is not None:
            print(e)
    if not video.isOpened() or nframe_none >= MAX_FRAMES_NONE:
        sys.exit()

print('Termino')
