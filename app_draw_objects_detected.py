import argparse

from datetime import datetime

parser = argparse.ArgumentParser()

parser.add_argument('-p',
                    '--path_output_images',
                    help='Ruta de la carpeta con las imagenes.',
                    required=True)
parser.add_argument('-cl',
                    '--client_id',
                    help='Id del cliente',
                    required=True)
parser.add_argument('-cty',
                    '--country_id',
                    help='Id del pais',
                    required=True)
parser.add_argument('-s',
                    '--since',
                    help='Desde que fecha se tomaran ItemRealTime(s) de los cuales se procesaran sus imagenes',
                    required=True)
parser.add_argument('-u',
                    '--until',
                    help='Hasta que fecha se tomaran ItemRealTime(s) de los cuales se procesaran sus imagenes.'
                         'El formato debe ser 2019-02-21 20:31:52. Por defecto se tomara la fecha y hora actual.',
                    default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

args = parser.parse_args()
path_output_images = args.path_output_images
client_id = args.client_id
country_id = args.country_id
since = args.since
until = args.until



# vis_util.visualize_boxes_and_labels_on_image_array(
#                     frame,
#                     np.squeeze(boxes),
#                     np.squeeze(classes).astype(np.int32),
#                     np.squeeze(scores),
#                     category_index,
#                     use_normalized_coordinates=True,
#                     line_thickness=8,
#                     min_score_thresh=minimum_percent_match_detection)[1]
