import os
import shutil
import argparse
import xml.etree.ElementTree as et

parser = argparse.ArgumentParser()
parser.add_argument('-px',
                    '--path_xml',
                    help='Ruta donde estan los XML y sus imagenes que deben ser movidos',
                    required=True)
parser.add_argument('-po',
                    '--path_output',
                    help='Ruta destino de los archivos.',
                    required=True)
parser.add_argument('-ixwo',
                    '--include_xml_without_objects',
                    type=bool,
                    help='Boolean que indica si se debe incluir entre los XML movidos aquellos que no tengan objetos '
                         'definidos.',
                    default=False)
parser.add_argument('-dof',
                    '--delete_old_files',
                    type=bool,
                    help='Boolean que indica si se deben borrar los archivos viejos (XML, Imagenes) deespues de '
                         'copiados. Por defecto en False',
                    default=False)

args = parser.parse_args()
path_xml = args.path_xml
path_output = args.path_output
include_xml_without_objects = args.include_xml_without_objects
delete_old_files = args.delete_old_files


def xml_has_objects_and_get_image_associated(xml_filepath):
    tree = et.parse(xml_filepath)
    root = tree.getroot()
    objects = root.findall('./object')
    xml_image_path = root.find('./path').text

    return len(objects) > 0 and os.path.isfile(xml_image_path), xml_filepath, xml_image_path


files = os.listdir(path_xml)
xml_files = [xml_has_objects_and_get_image_associated(os.path.join(path_xml, xml)) for xml in files
             if '.xml' in xml.lower()]

if not include_xml_without_objects:
    xml_files = [xml_with_objects for xml_with_objects in xml_files if xml_with_objects[0]]

if not os.path.exists(path_output):
    os.mkdir(path_output)

if delete_old_files:
    for has_objects, xml_file, xml_filename_image in xml_files:
        os.rename(os.path.abspath(xml_file), os.path.join(path_output, xml_file))
        os.rename(os.path.abspath(xml_filename_image), os.path.join(path_output, xml_filename_image))
else:
    for has_objects, xml_file, xml_filename_image in xml_files:
        shutil.copy(os.path.abspath(xml_file), os.path.join(path_output, os.path.basename(xml_file)))
        shutil.copy(os.path.abspath(xml_filename_image),
                    os.path.join(path_output, os.path.basename(xml_filename_image)))

print('Termino. Se movieron:', len(xml_files))
