class CSVProcessAdvance:
    # Date
    # CountryID
    # Country
    # ServerID
    # Server
    # VehicleID
    # Vehicle
    # CSVNotUploaded
    # UploadedPercentage
    # CSVUploadedAndNotProcessed CSVNotProcessed
    # CSVUploadedAndProcessed CSVProcessed
    # ProcessedPercentageOverUploadedCSV ProcessedPercentage
    # ProcessedPercentageOverTotalCSV
    SQL_DECIMAL_PRECISION_PERCENTAGES = 3
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3
    TEMPLATE__REPR__ = '{} date:{}, country_id:{}, country:{}, server_id:{}, server:{}, vehicle_id:{}, vehicle:{}, ' \
                       'csv_not_uploaded:{}, uploaded_percentage:{}, csv_uploaded_and_not_processed:{}, ' \
                       'csv_uploaded_and_processed:{}, processed_percentage_over_uploaded_csv:{}, ' \
                       'processed_percentage_over_total_csv:{}, used_storage:{}, available_storage:{}'
    TEMPLATE_TO_VALUES_INSERT_SQL = "'{}',{},'{}',{},'{}',{},'{}'," \
                                    "{},{},{}," \
                                    "{},{}," \
                                    "{},{},{}"

    def __init__(self, date, country_id, country, server_id, server, vehicle_id, vehicle, csv_not_uploaded,
                 uploaded_percentage, csv_uploaded_and_not_processed, csv_uploaded_and_processed,
                 processed_percentage_over_uploaded_csv, processed_percentage_over_total_csv, used_storage, available_storage):
        self.date = date
        self.country_id = country_id
        self.country = country
        self.server_id = server_id
        self.server = server
        self.vehicle_id = vehicle_id
        self.vehicle = vehicle
        self.csv_not_uploaded = csv_not_uploaded
        self.uploaded_percentage = round(float(uploaded_percentage),
                                         CSVProcessAdvance.SQL_DECIMAL_PRECISION_PERCENTAGES)
        self.csv_uploaded_and_not_processed = csv_uploaded_and_not_processed
        self.csv_uploaded_and_processed = csv_uploaded_and_processed
        self.processed_percentage_over_uploaded_csv = round(float(processed_percentage_over_uploaded_csv),
                                                            CSVProcessAdvance.SQL_DECIMAL_PRECISION_PERCENTAGES)
        self.processed_percentage_over_total_csv = round(float(processed_percentage_over_total_csv),
                                                         CSVProcessAdvance.SQL_DECIMAL_PRECISION_PERCENTAGES)
        self.used_storage = used_storage
        self.available_storage = available_storage

    def __repr__(self):
        return self.TEMPLATE__REPR__.format(self.__class__, self.date, self.country_id, self.country, self.server_id,
                                            self.server, self.vehicle_id, self.vehicle, self.csv_not_uploaded,
                                            self.uploaded_percentage, self.csv_uploaded_and_not_processed,
                                            self.csv_uploaded_and_processed,
                                            self.processed_percentage_over_uploaded_csv,
                                            self.processed_percentage_over_total_csv,
                                            self.used_storage, self.available_storage)

    def to_values_insert_sql(self):

        return self.TEMPLATE_TO_VALUES_INSERT_SQL.format(
            self.date.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL], self.country_id, self.country,
            self.server_id, self.server, self.vehicle_id, self.vehicle, self.csv_not_uploaded, self.uploaded_percentage,
            self.csv_uploaded_and_not_processed, self.csv_uploaded_and_processed,
            self.processed_percentage_over_uploaded_csv, self.processed_percentage_over_total_csv,
            self.used_storage, self.available_storage)
