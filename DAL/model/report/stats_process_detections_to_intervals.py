from datetime import datetime, timedelta


class StatsProcessDetectionsToIntervals:

    SQL_DECIMAL_PRECISION_PERCENTAGES = 3
    SQL_DECIMAL_PRECISION = 2
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3
    TEMPLATE__REPR__ = '{} date:{}, country_id:{}, country:{}, server_process_id:{}, server_process:{}, ' \
                       'csv:{}, server_detection_id:{}, server_detection:{}, vehicle_id:{}, ' \
                       'vehicle:{}, source_signal_id:{}, date_emission:{}, ' \
                       'time_reading_csv:{}, items_per_csv:{},interval_validation_rule_id:{},' \
                       'intervals_by_validation_rule:{}, items_for_intervals_by_validation_rule:{}, ' \
                       'intervals_by_ocr:{}, items_for_intervals_by_ocr:{}, ' \
                       'processed_items_by_ocr:{}, time_validation_rule:{}, ' \
                       'time_download_images:{}, time_ocr:{},' \
                       'time_generate_video:{}, time_insert_intervals:{},' \
                       'time_insert_history_csv_processed:{}, time_delete_local_files:{},' \
                       'time_delete_remote_files:{}, time_total_process:{},' \
                       'successful_process:{}, type_of_error:{}, error_description:{}, ' \
                       'avg_detection_score_items_validation_rule:{}, detections_below_avg_score_validation_rule:{},' \
                       'avg_detection_score_items_ocr:{}, detections_below_avg_score_ocr:{}'
    TEMPLATE_TO_VALUES_INSERT_SQL = "'{}',{},'{}',{},'{}'," \
                                    "'{}',{},'{}',{}," \
                                    "'{}',{},'{}'," \
                                    "'{}',{},{}," \
                                    "{},{}," \
                                    "{},{},{}," \
                                    "'{}','{}'," \
                                    "'{}','{}'," \
                                    "'{}','{}'," \
                                    "'{}','{}','{}'," \
                                    "{},'{}','{}'," \
                                    "{},{}," \
                                    "{},{}"

    def __init__(self, date: datetime, country_id: int, country: str, server_process_id: int, server_process: str,
                 csv: str, server_detection_id: int, server_detection: str, vehicle_id: int,
                 vehicle: str, source_signal_id: int, date_emission: datetime,
                 time_reading_csv: timedelta = timedelta(), items_per_csv: int = 0,
                 interval_validation_rule_id: int = 0,
                 intervals_by_validation_rule: int = 0, items_for_intervals_by_validation_rule: int = 0,
                 intervals_by_ocr: int = 0, items_for_intervals_by_ocr: int = 0,
                 processed_items_by_ocr: int = 0, time_validation_rule: timedelta = timedelta(),
                 time_download_images: timedelta = timedelta(), time_ocr: timedelta = timedelta(),
                 time_generate_video: timedelta = timedelta(), time_insert_intervals: timedelta = timedelta(),
                 time_insert_history_csv_processed: timedelta = timedelta(),
                 time_delete_local_files: timedelta = timedelta(), time_delete_remote_files: timedelta = timedelta(),
                 time_total_process: timedelta = timedelta(), successful_process: int = 1,
                 type_of_error: str = '', error_description: str = '',
                 avg_detection_score_items_validation_rule: float = 0,
                 detections_below_avg_score_validation_rule: int = 0,
                 avg_detection_score_items_ocr: float = 0, detections_below_avg_score_ocr: int = 0):
        self.date = date
        self.country_id = country_id
        self.country = country
        self.server_process_id = server_process_id
        self.server_process = server_process
        self.csv = csv
        self.server_detection_id = server_detection_id
        self.server_detection = server_detection
        self.vehicle_id = vehicle_id
        self.vehicle = vehicle
        self.source_signal_id = source_signal_id
        self.date_emission = date_emission
        self.time_reading_csv = time_reading_csv
        self.items_per_csv = items_per_csv
        self.interval_validation_rule_id = interval_validation_rule_id
        self.intervals_by_validation_rule = intervals_by_validation_rule
        self.items_for_intervals_by_validation_rule = items_for_intervals_by_validation_rule
        self.intervals_by_ocr = intervals_by_ocr
        self.items_for_intervals_by_ocr = items_for_intervals_by_ocr
        self.processed_items_by_ocr = processed_items_by_ocr
        self.time_validation_rule = time_validation_rule
        self.time_download_images = time_download_images
        self.time_ocr = time_ocr
        self.time_generate_video = time_generate_video
        self.time_insert_intervals = time_insert_intervals
        self.time_insert_history_csv_processed = time_insert_history_csv_processed
        self.time_delete_local_files = time_delete_local_files
        self.time_delete_remote_files = time_delete_remote_files
        self.time_total_process = time_total_process
        self.successful_process = int(successful_process)
        self.type_of_error = type_of_error
        self.error_description = error_description
        self._avg_detection_score_items_validation_rule = round(float(avg_detection_score_items_validation_rule),
                                                                self.SQL_DECIMAL_PRECISION)
        self.detections_below_avg_score_validation_rule = detections_below_avg_score_validation_rule
        self._avg_detection_score_items_ocr = round(float(avg_detection_score_items_ocr),
                                                    self.SQL_DECIMAL_PRECISION)
        self.detections_below_avg_score_ocr = detections_below_avg_score_ocr

    def __repr__(self):
        return self.TEMPLATE__REPR__.format(
            self.__class__, self.date, self.country_id, self.country, self.server_process_id, self.server_process,
            self.csv, self.server_detection_id, self.server_detection, self.vehicle_id,
            self.vehicle, self.source_signal_id, self.date_emission,
            self.time_reading_csv, self.items_per_csv, self.interval_validation_rule_id,
            self.intervals_by_validation_rule, self.items_for_intervals_by_validation_rule,
            self.intervals_by_ocr, self.items_for_intervals_by_ocr,
            self.processed_items_by_ocr, self.time_validation_rule,
            self.time_download_images, self.time_ocr,
            self.time_generate_video, self.time_insert_intervals,
            self.time_insert_history_csv_processed, self.time_delete_local_files,
            self.time_delete_remote_files, self.time_total_process,
            self.successful_process, self.type_of_error, self.error_description,
            self.avg_detection_score_items_validation_rule, self.detections_below_avg_score_validation_rule,
            self.avg_detection_score_items_ocr, self.detections_below_avg_score_ocr
        )

    def to_values_insert_sql(self):
        date = self.date.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        date_emission = self.date_emission.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        return self.TEMPLATE_TO_VALUES_INSERT_SQL.format(
            date, self.country_id, self.country, self.server_process_id, self.server_process,
            self.csv, self.server_detection_id, self.server_detection, self.vehicle_id,
            self.vehicle, self.source_signal_id, date_emission,
            self.time_reading_csv, self.items_per_csv, self.interval_validation_rule_id,
            self.intervals_by_validation_rule, self.items_for_intervals_by_validation_rule,
            self.intervals_by_ocr, self.items_for_intervals_by_ocr,
            self.processed_items_by_ocr, self.time_validation_rule,
            self.time_download_images, self.time_ocr,
            self.time_generate_video, self.time_insert_intervals,
            self.time_insert_history_csv_processed, self.time_delete_local_files,
            self.time_delete_remote_files, self.time_total_process,
            self.successful_process, self.type_of_error, self.error_description,
            self.avg_detection_score_items_validation_rule, self.detections_below_avg_score_validation_rule,
            self.avg_detection_score_items_ocr, self.detections_below_avg_score_ocr
        )

    @property
    def avg_detection_score_items_validation_rule(self):
        return self._avg_detection_score_items_validation_rule

    @avg_detection_score_items_validation_rule.setter
    def avg_detection_score_items_validation_rule(self, value):
        self._avg_detection_score_items_validation_rule = round(float(value), self.SQL_DECIMAL_PRECISION)

    @property
    def avg_detection_score_items_ocr(self):
        return self._avg_detection_score_items_ocr

    @avg_detection_score_items_ocr.setter
    def avg_detection_score_items_ocr(self, value):
        self._avg_detection_score_items_ocr = round(float(value), self.SQL_DECIMAL_PRECISION)

