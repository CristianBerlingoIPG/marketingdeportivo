from datetime import datetime, time


class Vehicle:
    TEMPLATE__REPR__ = '{} id:{}, name:{}, sub_medium_id:{}, plaza_id:{}, imtwister_id:{}, ibope_id:{}, active:{}, ' \
                       'audit_start_time:{}, audit_end_time:{}'
    SQL_SERVER_FORMAT_TIME = '%H:%M:%S'

    def __init__(self, name, sub_medium_id, plaza_id, imtwister_id, ibope_id, active, audit_start_time: time,
                 audit_end_time: time, id=0):
        self.id = int(id)
        self.name = name
        self.sub_medium_id = int(sub_medium_id)
        self.plaza_id = int(plaza_id)
        self.imtwister_id = int(imtwister_id)
        self.ibope_id = int(ibope_id)
        self.active = int(active)
        self.audit_start_time: time = audit_start_time
        self.audit_end_time: time = audit_end_time

    def __repr__(self):
        return self.TEMPLATE__REPR__.format(self.__class__, self.id, self.name, self.sub_medium_id, self.plaza_id,
                                            self.imtwister_id, self.ibope_id, self.active, self.audit_start_time,
                                            self.audit_end_time)
