
class Client:
    TEMPLATE__REPR__ = '{} id:{}, name:{}, active:{}, created_by:{}, updated_by:{}'

    def __init__(self, id, name, active, created_by, updated_by):
        self.id = id
        self.name = name
        self.active = active
        self.created_by = created_by
        self.updated_by = updated_by

    def __repr__(self):
        return Client.TEMPLATE__REPR__.format(self.__class__, self.id, self.name, self.active, self.created_by,
                                              self.updated_by)
