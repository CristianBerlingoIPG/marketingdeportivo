
class Logo:
    TEMPLATE__REPR__ = '{} id:{}, name:{}, observations:{}, path:{}, product_id:{}, active:{}, display_name:{}'

    def __init__(self, name, observations, path, product_id, active, display_name, id=0):
        self.id = id
        self.name = name
        self.observations = observations
        self.path = path
        self.product_id = product_id
        self.active = active
        self.display_name = display_name

    def __repr__(self):
        return Logo.TEMPLATE__REPR__.format(self.__class__, self.id, self.name, self.observations, self.path,
                                            self.product_id, self.active, self.display_name)

    def get_valids_values_for_name(self, min_quantity_chars_by_value=3):
        dict_percent_match_ocr = {}
        len_display_name = len(self.display_name)
        for end_str in range(min_quantity_chars_by_value, len_display_name + 1):
            percent_match_ocr = 100 / len_display_name * end_str
            dict_percent_match_ocr[percent_match_ocr] = []
            for i in range(0, len_display_name - (end_str - 1)):
                dict_percent_match_ocr[percent_match_ocr].append(self.display_name[i:i + end_str].lower())

        return dict_percent_match_ocr
