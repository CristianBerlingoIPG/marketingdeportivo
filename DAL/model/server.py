from DAL.model.source_signal import SourceSignal

from typing import List


class Server:
    TEMPLATE__REPR__ = '{} id:{}, hostname:{}, ip:{}, country_id:{}, data_detection_path:{}'

    def __init__(self, id, hostname, ip, country_id, data_detection_path, source_signals: List[SourceSignal] = []):
        self.id = int(id)
        self.hostname = hostname
        self.ip = ip
        self.country_id = int(country_id)
        self.source_signals = source_signals
        self.data_detection_path = data_detection_path

    def __repr__(self):
        return Server.TEMPLATE__REPR__.format(self.__class__, self.id, self.hostname, self.ip, self.country_id,
                                              self.data_detection_path)

    def is_the_owner_of_the_source_signal(self, source_signal_id: int):
        is_the_owner_of_the_source_signal = any([source_signal_id == int(ss.id) for ss in self.source_signals])

        return is_the_owner_of_the_source_signal

    def get_source_signal_by_id(self, source_signal_id: int) -> SourceSignal:
        source_signal = [ss for ss in self.source_signals if ss.id == source_signal_id]
        source_signal = source_signal[0] if len(source_signal) > 0 else None

        return source_signal


class ServerType:
    DETECTION = 'DETECTION'
    INTERMEDIATE = 'INTERMEDIATE'
