class HistoryCSVProcessed:
    TEMPLATE__REPR__ = '{} id:{}, date:{}, path:{}, client_id:{}, vehicle_id:{}, source_signal_id:{}, server_id: {}, ' \
                       'process_interval:{}, process_ocr:{}, process_video_intervals:{}'
    TEMPLATE_TO_CSV = "'{}','{}',{},{},{},{},{},{},{}"
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3

    def __init__(self, date, path, client_id, vehicle_id, source_signal_id, server_id, process_interval=0,
                 process_ocr=0, process_video_intervals=0, id=0):
        self.id = id
        self.date = date
        self.path = path
        self.client_id = client_id
        self.vehicle_id = vehicle_id
        self.source_signal_id = source_signal_id
        self.server_id = int(server_id)
        self.process_interval = process_interval
        self.process_ocr = process_ocr
        self.process_video_intervals = process_video_intervals

    def __repr__(self):
        return HistoryCSVProcessed.TEMPLATE__REPR__.format(self.__class__, self.id, self.date, self.path,
                                                           self.client_id, self.vehicle_id, self.source_signal_id,
                                                           self.server_id, self.process_interval, self.process_ocr,
                                                           self.process_video_intervals)

    def to_csv(self, template=None, format_date=None, datetime_to_sql=True):
        template = template if template is not None else HistoryCSVProcessed.TEMPLATE_TO_CSV
        subtract_characters_datetime = HistoryCSVProcessed.SUBTRACT_CHARACTER_DATETIME_SQL if datetime_to_sql else None
        format_date = format_date if format_date is not None else HistoryCSVProcessed.FORMAT_DATE
        to_csv = template.format(self.date.strftime(format_date)[:subtract_characters_datetime], self.path,
                                 self.client_id, self.vehicle_id, self.source_signal_id, int(self.server_id),
                                 int(self.process_interval), int(self.process_ocr), int(self.process_video_intervals))

        return to_csv
