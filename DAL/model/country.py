class Country:
    TEMPLATE__REPR__ = '{} id:{}, name:{}, active:{}'

    def __init__(self, id, name, active):
        self.id = int(id)
        self.name = name
        self.active = active

    def __repr__(self):
        return Country.TEMPLATE__REPR__.format(self.__class__, self.id, self.name, self.active)
