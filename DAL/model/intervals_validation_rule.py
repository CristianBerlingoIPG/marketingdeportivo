import math

from DAL.model.interval import Interval
from DAL.model.item_real_time import ItemRealTime
from DAL.model.source_signal import SourceSignal
from DAL.model.logo import Logo

from datetime import timedelta
from typing import List, Union, Tuple


class IntervalsValidationRule:
    TEMPLATE__REPR__ = '{} id:{}, client_id:{}, country_id:{}, since:{}, minimum_exposition_time:{}, ' \
                       'minimum_percent_frames:{}, minimum_percent_match_detection:{}, created:{}'

    def __init__(self, id, client_id, country_id, since, minimum_exposition_time, minimum_percent_frames,
                 minimum_percent_match_detection, created):
        self.id = id
        self.client_id = client_id
        self.country_id = country_id
        self.since = since
        self.minimum_exposition_time = minimum_exposition_time
        self.minimum_percent_frames = minimum_percent_frames
        self.minimum_percent_match_detection = minimum_percent_match_detection
        self.created = created

    def __repr__(self):
        return IntervalsValidationRule.TEMPLATE__REPR__.format(self.__class__, self.id, self.client_id, self.country_id,
                                                               self.since, self.minimum_exposition_time,
                                                               self.minimum_percent_frames,
                                                               self.minimum_percent_match_detection,
                                                               self.created)

    @staticmethod
    def get_difference_between_intervals(minuend_interval, subtrahend_interval):
        difference = [minuend_frame for minuend_frame in minuend_interval
                      if minuend_frame.date > subtrahend_interval[-1].date]

        return difference

    @staticmethod
    def get_minimum_frames_to_interval(interval, frames_per_second, minimum_percent):
        length_interval = int((interval[-1].date - interval[0].date).total_seconds())
        total_interval_frames = length_interval * frames_per_second

        minimum_frames_to_interval = int(total_interval_frames * minimum_percent)

        return minimum_frames_to_interval

    @staticmethod
    def get_interval_total_seconds(interval: List[ItemRealTime]):
        return (interval[-1].date - interval[0].date).total_seconds()

    def is_valid_interval(self, interval, frames_per_second, minimum_percent):
        return len(interval) >= self.get_minimum_frames_to_interval(interval, frames_per_second, minimum_percent) \
               and self.minimum_exposition_time <= self.get_interval_total_seconds(interval)

    def join_exposition_intervals(self, overlapped_interval, new_interval, frames_per_second, minimum_percent):
        difference = self.get_difference_between_intervals(new_interval, overlapped_interval)
        join_interval = [*overlapped_interval, *difference]
        if not self.is_valid_interval(join_interval, frames_per_second, minimum_percent):
            join_interval = None

        return join_interval

    def get_default_minimum_nframes_for_exposition(self, fps, at_least_one_frame_per_second=True):
        dmnf = int((self.minimum_exposition_time * fps) * self.minimum_percent_frames)
        if self.minimum_exposition_time * fps > dmnf and at_least_one_frame_per_second:
            dmnf = math.ceil(self.minimum_exposition_time)

        return dmnf

    @staticmethod
    def intervals_are_equal(interval1: Interval, interval2: Interval):
        intervals_are_equal = interval1.start_time == interval2.start_time \
                              and interval1.end_time == interval2.end_time \
                              and interval1.vehicle_id == interval2.vehicle_id \
                              and interval1.source_signal_id == interval2.source_signal_id \
                              and interval1.logo_id == interval2.logo_id

        return intervals_are_equal

    def already_exists_interval(self, new_interval: Interval, existing_intervals: List[Interval]):
        already_exists_interval = any([self.intervals_are_equal(ei, new_interval) for ei in existing_intervals])

        return already_exists_interval

    def get_valid_intervals(self, matched_frames: List[ItemRealTime], source_signals: List[SourceSignal],
                            logos: List[Logo], path_file_raw_data: str, return_items_real_time=False,
                            server_host_ip='') -> Union[List[Interval], List[Tuple[Interval, List[ItemRealTime]]]]:
        valid_intervals = []
        valid_frames_intervals = []

        for source_signal in source_signals:
            default_minimum_nframes_for_exposition = self.get_default_minimum_nframes_for_exposition(source_signal.fps)
            for logo in logos:
                matched_frames_by_source_signal_and_logo = [mf for mf in matched_frames
                                                            if mf.logo_id == logo.id
                                                            and mf.source_signal_id == source_signal.id
                                                            and mf.score >= self.minimum_percent_match_detection]

                matched_frames_by_source_signal_and_logo = sorted(matched_frames_by_source_signal_and_logo,
                                                                  key=lambda k: k.date)

                for mf in matched_frames_by_source_signal_and_logo:
                    max_time_interval = mf.date + timedelta(seconds=float(self.minimum_exposition_time))
                    frames_interval = [f for f in matched_frames_by_source_signal_and_logo
                                       if mf.date <= f.date <= max_time_interval]
                    lenght_frames_interval = (frames_interval[-1].date - frames_interval[0].date).total_seconds()

                    if len(frames_interval) >= default_minimum_nframes_for_exposition \
                            and float(self.minimum_exposition_time - 1) <= lenght_frames_interval:
                        overlapped_interval = None
                        for valid_interval in valid_frames_intervals:
                            # if any(frames_interval[0].date == f.date for f in valid_interval):

                            # al ya no subirse los items real time a la BD antes de generarse los intervalos todos los
                            # items tienen el ID = 0, por esa razon siempre considera que es un intervalo superpuesto
                            # y ningun archivo CSV generaba mas de un intervalo
                            # if any(frames_interval[0].id == f.id
                            #        and frames_interval[0].logo_id == f.logo_id
                            #        and frames_interval[0].source_signal_id == source_signal.id
                            #        for f in valid_interval):
                            #     overlapped_interval = valid_interval

                            # para resolver el problema del anterior if se cambio id por date
                            if any(frames_interval[0].date == f.date
                                   and frames_interval[0].logo_id == f.logo_id
                                   and frames_interval[0].source_signal_id == source_signal.id
                                   for f in valid_interval):
                                overlapped_interval = valid_interval
                        if overlapped_interval is None:
                            valid_frames_intervals.append(frames_interval)
                        else:
                            frames_interval = self.join_exposition_intervals(overlapped_interval,
                                                                             frames_interval,
                                                                             source_signal.fps,
                                                                             self.minimum_percent_frames)
                            if frames_interval is not None:
                                if overlapped_interval[0].id != frames_interval[0].id \
                                        or overlapped_interval[-1].id != frames_interval[-1].id:
                                    valid_frames_intervals.remove(overlapped_interval)
                                    valid_frames_intervals.append(frames_interval)

        for valid_frames_interval in valid_frames_intervals:
            interval = Interval(intervals_validation_rule_id=self.id,
                                logo_id=valid_frames_interval[0].logo_id,
                                vehicle_id=valid_frames_interval[0].vehicle_id,
                                source_signal_id=valid_frames_interval[0].source_signal_id,
                                start_time=valid_frames_interval[0].date,
                                end_time=valid_frames_interval[-1].date,
                                start_item_real_time_id=valid_frames_interval[0].id,
                                end_item_real_time_id=valid_frames_interval[-1].id,
                                grps=0,
                                gross_investment=0,
                                net_investment=0,
                                path_file_raw_data=path_file_raw_data,
                                server_host_ip=server_host_ip,
                                average_percentage_logo_area=Interval.get_average_percentage_logo_area(
                                    valid_frames_interval,
                                    source_signal.height,
                                    source_signal.width)
                                )

            if return_items_real_time:
                if not self.already_exists_interval(interval, [vi for vi, list_irt in valid_intervals]):
                    valid_intervals.append((interval, valid_frames_interval))
            else:
                if not self.already_exists_interval(interval, valid_intervals):
                    valid_intervals.append(interval)

        return valid_intervals
