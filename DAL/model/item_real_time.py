import os

from datetime import datetime


class ItemRealTime:
    TEMPLATE_TO_CSV = '{},{},{},{},{},{},{},{},{},{},{},{}'
    TEMPLATE_TO_VALUES_INSERT_SQL = "{},{},{},'{}','{}',{},{},{},{},{},'{}','{}'"
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3
    TEMPLATE__REPR__ = '{} id:{}, vehicle_id:{}, source_signal_id:{}, logo_id:{}, date:{}, path:{}, score:{}, ' \
                       'box_y_min:{}, box_x_min:{}, box_y_max:{}, box_x_max:{}, path_file_raw_data:{}, ' \
                       'recognized_text:{}'
    DEFAULT_VALUE_RECOGNIZED_TEXT = 'SIN PROCESAMIENTO OCR'
    TEMPLATE_URL = 'http://{}:{}{}'
    SQL_DECIMAL_PRECISION_SCORE = 7
    SQL_DECIMAL_PRECISION_BOXES = 9

    def __init__(self, vehicle_id, source_signal_id, logo_id, date, path, score,
                 box_y_min, box_x_min, box_y_max, box_x_max, path_file_raw_data, id=0, recognized_text=None):
        self.vehicle_id = int(vehicle_id)
        self.source_signal_id = int(source_signal_id)
        self.logo_id = int(logo_id)

        type_date = type(date)
        if not type_date == datetime:
            if type_date == str:
                len_date = len(date)
                if len_date == 26 or len_date == 23 or len_date == 19:
                    date += '.000000' if len_date == 19 else ''
                    date += '000' if len_date == 23 else ''
                    date = datetime.strptime(date, ItemRealTime.FORMAT_DATE)

                else:
                    raise ValueError('La extension del parametro date: {}  no es valido, '
                                     'el parametro debe tener una extension de 26 caracteres y con el formato {}'
                                     .format(type_date, ItemRealTime.FORMAT_DATE))
            else:
                raise ValueError('Tipo del parametro date: {}  no es valido, '
                                 'el parametro debe ser de tipo datetime o un string convertible'.format(type_date))
        self.date = date
        self.path = path
        self.score = round(float(score), ItemRealTime.SQL_DECIMAL_PRECISION_SCORE)
        self.box_y_min = round(float(box_y_min), ItemRealTime.SQL_DECIMAL_PRECISION_BOXES)
        self.box_x_min = round(float(box_x_min), ItemRealTime.SQL_DECIMAL_PRECISION_BOXES)
        self.box_y_max = round(float(box_y_max), ItemRealTime.SQL_DECIMAL_PRECISION_BOXES)
        self.box_x_max = round(float(box_x_max), ItemRealTime.SQL_DECIMAL_PRECISION_BOXES)
        self.path_file_raw_data = path_file_raw_data
        self.id = int(id)
        self.recognized_text = recognized_text if recognized_text is not None \
            else ItemRealTime.DEFAULT_VALUE_RECOGNIZED_TEXT

    def __repr__(self):
        return ItemRealTime.TEMPLATE__REPR__.format(self.__class__, self.id, self.vehicle_id, self.source_signal_id,
                                                    self.logo_id, self.date, self.path, self.score, self.box_y_min,
                                                    self.box_x_min, self.box_y_max, self.box_x_max,
                                                    self.path_file_raw_data, self.recognized_text)

    def to_csv(self, template=None, format_date=None, datetime_to_sql=True):
        template = template if template is not None else ItemRealTime.TEMPLATE_TO_CSV
        subtract_characters_datetime = ItemRealTime.SUBTRACT_CHARACTER_DATETIME_SQL if datetime_to_sql else None
        format_date = format_date if format_date is not None else ItemRealTime.FORMAT_DATE
        to_csv = template.format(self.vehicle_id, self.source_signal_id, self.logo_id,
                                 self.date.strftime(format_date)[:subtract_characters_datetime], self.path,
                                 self.score, self.box_y_min, self.box_x_min, self.box_y_max, self.box_x_max,
                                 self.path_file_raw_data, self.recognized_text)

        return to_csv

    def to_values_insert_sql(self):

        return self.to_csv(template=self.TEMPLATE_TO_VALUES_INSERT_SQL)

    def get_url(self, server_ip, port=8000):
        return ItemRealTime.TEMPLATE_URL.format(server_ip, port, self.path.replace(os.environ['HOME'], ''))

    def get_percent_relative_area(self, height, width) -> float:
        percent_relative_area = (self.get_area_logo(height, width) / (width * height)) * 100

        return percent_relative_area

    def get_absolute_x_max(self, height) -> float:
        absolute_x_max = float(height * self.box_x_max)

        return absolute_x_max

    def get_absolute_x_min(self, height) -> float:
        absolute_x_min = float(height * self.box_x_min)

        return absolute_x_min

    def get_absolute_y_max(self, width) -> float:
        absolute_y_max = float(width * self.box_y_max)

        return absolute_y_max

    def get_absolute_y_min(self, width) -> float:
        absolute_y_min = float(width * self.box_y_min)

        return absolute_y_min

    def get_area_logo(self, image_height, image_width):
        height = self.get_absolute_x_max(image_height) - self.get_absolute_x_min(image_height)
        width = self.get_absolute_y_max(image_width) - self.get_absolute_y_min(image_width)
        area = width * height

        return area
