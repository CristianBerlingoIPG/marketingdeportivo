
class SourceSignal:
    TEMPLATE__REPR__ = '{} id:{}, name:{}, observations:{}, active:{}, path:{}, fps:{}, width:{}, height:{}'

    def __init__(self, id, name, observations, active, path, fps, width, height, server_id):
        self.id = int(id)
        self.name = name
        self.observations = observations
        self.active = active
        self.path = path
        self.fps = int(fps)
        self.width = int(width)
        self.height = int(height)
        self.server_id = int(server_id)

    def __repr__(self):
        return SourceSignal.TEMPLATE__REPR__.format(self.__class__, self.id, self.name, self.observations, self.active,
                                                    self.path, self.fps, self.width, self.height)
