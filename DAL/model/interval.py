import os
import re
import logging
import subprocess

from typing import List, Tuple, Union
from datetime import datetime, timedelta

from DAL.model.item_real_time import ItemRealTime


# ID	IntervalsValidationRuleID	LogoID	VehicleID	SourceSignalID	Starttime	Endtime	StartItemRealTimeID
# EndItemRealTimeID	GRPS	GrossInvestment	NetInvestment	PathVideo	FrameMatchOCRID	PercentMatchOCR	Monitored
# OperatorFilter	CategoryID	AuspiceID PathFileRawData Created RecognizedText Reviewed
class Interval:
    TEMPLATE_TO_CSV = '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'
    TEMPLATE_TO_VALUES_INSERT_SQL = "{},{},{},{},{},'{}','{}',{},{},{},{},{},'{}',{},{},{},{},{},{},'{}','{}','{}'," \
                                    "{},'{}',{}"
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3
    TEMPLATE__REPR__ = '{} id:{}, intervals_validation_rule_id:{}, logo_id:{}, vehicle_id:{}, ' \
                       'source_signal_id:{}, start_time:{}, end_time:{}, start_item_real_time_id:{}, ' \
                       'end_item_real_time_id:{}, grps:{}, gross_investment:{}, ' \
                       'net_investment:{}, path_file_raw_data:{}, path_video:{}, frame_match_ocr_id:{}, ' \
                       'percent_match_ocr:{}, monitored:{}, operator_filter:{}, category_id:{}, auspice_id:{}, ' \
                       'created:{}, recognized_text:{}, reviewed:{}, server_host_id:{}, ' \
                       'average_percentage_logo_area: {}'
    TEMPLATE_VIDEO_NAME = 'id{}_rid{}_lid{}_vid{}_sid{}_st{}_et{}_sirt{}_eirt{}.mp4'
    QUERY_SP_GETITEMSREALTIMEFORINTERVAL = "EXEC Detection.GetItemsRealTimeForInterval {}, {}, '{}', '{}', {}, {}, {}"
    SQL_DECIMAL_PRECISION = 2
    TEMPLATE_FILE_NAME_LIST_FRAMES = 'list_frames_interval_{}.txt'

    def __init__(self, intervals_validation_rule_id, logo_id, vehicle_id, source_signal_id,
                 start_time, end_time, start_item_real_time_id, end_item_real_time_id, grps, gross_investment,
                 net_investment, path_file_raw_data, id=0, path_video='', frame_match_ocr_id=None, percent_match_ocr=0,
                 monitored=0, operator_filter=0, category_id=None, auspice_id=None, created=None,
                 recognized_text='', reviewed=0, server_host_ip='', average_percentage_logo_area=0):
        self.id = id
        self.intervals_validation_rule_id = intervals_validation_rule_id
        self.logo_id = logo_id
        self.vehicle_id = vehicle_id
        self.source_signal_id = source_signal_id
        self.start_time = start_time
        self.end_time = end_time
        self.start_item_real_time_id = start_item_real_time_id
        self.end_item_real_time_id = end_item_real_time_id
        self.grps = grps
        self.gross_investment = gross_investment
        self.net_investment = net_investment
        self.path_video = path_video
        self.frame_match_ocr_id = int(frame_match_ocr_id) if frame_match_ocr_id is not None else None
        self.percent_match_ocr = round(float(percent_match_ocr), Interval.SQL_DECIMAL_PRECISION)
        self.monitored = monitored
        self.operator_filter = operator_filter
        self.category_id = int(category_id) if category_id is not None else None
        self.auspice_id = int(auspice_id) if auspice_id is not None else None
        self.path_file_raw_data = path_file_raw_data
        self.created = created if created is not None else datetime.now()
        self.recognized_text = recognized_text
        self.reviewed = reviewed
        self.server_host_ip = server_host_ip
        self.average_percentage_logo_area = round(float(average_percentage_logo_area), Interval.SQL_DECIMAL_PRECISION)

    def __repr__(self):
        return self.TEMPLATE__REPR__.format(self.__class__, self.id, self.intervals_validation_rule_id,
                                            self.logo_id, self.vehicle_id, self.source_signal_id, self.start_time,
                                            self.end_time, self.start_item_real_time_id, self.end_item_real_time_id,
                                            self.grps, self.gross_investment, self.net_investment,
                                            self.path_file_raw_data, self.path_video, self.frame_match_ocr_id,
                                            self.percent_match_ocr, self.monitored, self.operator_filter,
                                            self.category_id, self.auspice_id, self.created, self.recognized_text,
                                            self.reviewed, self.server_host_ip, self.average_percentage_logo_area)

    def to_csv(self, template=None, format_date=None, to_sql=True, with_id=False):
        template = template if template is not None else Interval.TEMPLATE_TO_CSV
        subtract_characters_datetime = Interval.SUBTRACT_CHARACTER_DATETIME_SQL if to_sql else None
        format_date = format_date if format_date is not None else Interval.FORMAT_DATE
        frame_match_ocr_id = 'NULL' if self.frame_match_ocr_id is None and to_sql else self.frame_match_ocr_id
        category_id = 'NULL' if self.category_id is None and to_sql else self.category_id
        auspice_id = 'NULL' if self.auspice_id is None and to_sql else self.auspice_id
        to_csv = template.format(self.id, self.intervals_validation_rule_id, self.logo_id, self.vehicle_id,
                                 self.source_signal_id,
                                 self.start_time.strftime(format_date)[:subtract_characters_datetime],
                                 self.end_time.strftime(format_date)[:subtract_characters_datetime],
                                 self.start_item_real_time_id, self.end_item_real_time_id, self.grps,
                                 self.gross_investment, self.net_investment, self.path_video, frame_match_ocr_id,
                                 self.percent_match_ocr, int(self.monitored), int(self.operator_filter), category_id,
                                 auspice_id, self.path_file_raw_data,
                                 self.created.strftime(format_date)[:subtract_characters_datetime],
                                 self.recognized_text.replace("'", ''), int(self.reviewed), self.server_host_ip,
                                 self.average_percentage_logo_area)
        if not with_id:
            length_str_id = len(str(self.id) + ',')
            to_csv = to_csv[length_str_id:]

        return to_csv

    def to_values_insert_sql(self, with_id=False):
        return self.to_csv(template=self.TEMPLATE_TO_VALUES_INSERT_SQL, with_id=with_id)

    def get_length_time(self) -> timedelta:
        return self.end_time - self.start_time

    def get_length_time_in_seconds(self) -> float:
        return self.get_length_time().total_seconds()

    # EXEC Detection.GetItemsRealTimeForInterval 145, 768, 1, 2, 2
    def get_items_real_time_for_interval(self, engine) -> List[ItemRealTime]:
        sql_command = Interval.QUERY_SP_GETITEMSREALTIMEFORINTERVAL \
            .format(self.start_item_real_time_id,
                    self.end_item_real_time_id,
                    self.start_time.strftime(Interval.FORMAT_DATE)[:Interval.SUBTRACT_CHARACTER_DATETIME_SQL],
                    self.end_time.strftime(Interval.FORMAT_DATE)[:Interval.SUBTRACT_CHARACTER_DATETIME_SQL],
                    self.logo_id,
                    self.vehicle_id,
                    self.source_signal_id)

        results = engine.execute(sql_command).fetchall()

        return [ItemRealTime(r.VehicleID,
                             r.SourceSignalID,
                             r.LogoID,
                             r.Date,
                             r.Path,
                             r.Score,
                             r.BoxYMin,
                             r.BoxXMin,
                             r.BoxYMax,
                             r.BoxXMax,
                             r.PathFileRawData,
                             id=r.ID) for r in results]

    def get_video_name(self):
        return Interval.TEMPLATE_VIDEO_NAME.format(self.id,
                                                   self.intervals_validation_rule_id,
                                                   self.logo_id,
                                                   self.vehicle_id,
                                                   self.source_signal_id,
                                                   self.start_time.strftime('%Y%m%dh%Hm%Ms%S'),
                                                   self.end_time.strftime('%Y%m%dh%Hm%Ms%S'),
                                                   self.start_item_real_time_id,
                                                   self.end_item_real_time_id)

    # TEMPLATE_VIDEO_NAME = 'id{}_rid{}_lid{}_vid{}_sid{}_st{}_et{}_sirt{}_eirt{}.mp4'
    # '%Y%m%d_h%Hm%Ms%Sf%f'
    @staticmethod
    def get_patterns_for_numbers(numbers, zfill=None):
        zfill = zfill if zfill is not None else 0
        str_numbers = [str(number).zfill(zfill) for number in numbers]
        max_length = max([len(sn) for sn in str_numbers])
        patterns_for_numbers = [[] for _ in range(0, max_length)]
        for i in range(0, max_length):
            numbers_with_n_digits = [sn for sn in str_numbers if len(sn) >= i + 1]
            for number_with_n_digits in numbers_with_n_digits:
                if number_with_n_digits[i] not in patterns_for_numbers[i]:
                    patterns_for_numbers[i].append(number_with_n_digits[i])

        str_patterns_for_numbers = ''
        for pattern_for_numbers in patterns_for_numbers:
            str_patterns_for_numbers += '[{}]'.format(','.join(sorted(pattern_for_numbers)))

        return str_patterns_for_numbers

    @staticmethod
    def get_available_times_digits(start_digit, end_digit, limit_representation):
        if start_digit <= end_digit:
            available_times_digits = list(range(start_digit, end_digit + 1))
        else:
            available_times_digits = list(range(0, end_digit + 1))
            available_times_digits.extend(list(range(start_digit, limit_representation)))

        return available_times_digits

    def generate_glob_pattern(self, prefix_files=''):
        years = list(range(self.start_time.year, self.end_time.year + 1))
        months = [self.start_time.month, self.end_time.month] if self.start_time.month != self.end_time.month \
            else [self.start_time.month]
        days = [self.start_time.day, self.end_time.day] if self.start_time.day != self.end_time.day \
            else [self.start_time.day]

        hours = self.get_available_times_digits(self.start_time.hour, self.end_time.hour, 24)
        minutes = self.get_available_times_digits(self.start_time.minute, self.end_time.minute, 60)
        seconds = self.get_available_times_digits(self.start_time.second, self.end_time.second, 60)

        glob_pattern = '{}{}{}{}_h{}m{}s{}*.jpg'.format(
            prefix_files,
            self.get_patterns_for_numbers(years),
            self.get_patterns_for_numbers(months, zfill=2),
            self.get_patterns_for_numbers(days, zfill=2),
            self.get_patterns_for_numbers(hours, zfill=2),
            self.get_patterns_for_numbers(minutes, zfill=2),
            self.get_patterns_for_numbers(seconds, zfill=2)
        )

        return glob_pattern

    def get_args_to_ffmpeg(self, frame_rate, path_output, prefix_input_files, str_join=True):
        ffmpeg_args = ['/snap/bin/ffmpeg',
                       '-framerate',
                       str(frame_rate),
                       '-pattern_type',
                       'glob',
                       '-i',
                       "'{}'".format(self.generate_glob_pattern(prefix_files=prefix_input_files)),
                       '-c:v',
                       'libx264',
                       '-pix_fmt',
                       'yuv420p',
                       path_output]
        if str_join:
            ffmpeg_args = [' '.join([str(arg) for arg in ffmpeg_args])]

        return ffmpeg_args

    # ffmpeg -framerate 15 -pattern_type glob -i '*20190408_h11m34s1[4-8]*.jpg' -c:v libx264 -pix_fmt yuv420p out.mp4
    def generate_video(self, path_input, path_output, frame_rate=5, prefix_input_files='DO_', overwrite=True):
        path_output_video = os.path.join(path_output, self.get_video_name())

        if os.path.isfile(path_output_video):
            if overwrite:
                os.remove(path_output_video)
            else:
                logging.info('el archivo {} ya existia'.format(path_output_video))

        if not os.path.isfile(path_output_video):
            ffmpeg_args = self.get_args_to_ffmpeg(frame_rate, path_output_video, prefix_input_files)
            logging.info('Ejecutando subprocess: {}'.format(' '.join([str(arg) for arg in ffmpeg_args])))
            ffmpeg_process = subprocess.run(args=ffmpeg_args,
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE,
                                            cwd=path_input,
                                            shell=True)

            if ffmpeg_process.returncode != 0:
                ffmpeg_process_stderr = ffmpeg_process.stderr.decode('utf-8')
                logging.error('Ocurrio un error durante la ejecucion de :{}'
                              '\nMensaje:'
                              '\n{}'
                              '\nIntentando ahora con otra configuracion'
                              .format(' '.join([str(arg) for arg in ffmpeg_args]),
                                      ffmpeg_process_stderr))
                ffmpeg_args = self.get_args_to_ffmpeg(frame_rate, path_output_video, prefix_input_files, str_join=False)
                logging.info('Ejecutando subprocess (segundo intento) con lista de args y sin shell=True: {}'
                             .format(' '.join([str(arg) for arg in ffmpeg_args])))
                ffmpeg_process = subprocess.run(args=ffmpeg_args,
                                                stdout=subprocess.PIPE,
                                                stderr=subprocess.PIPE,
                                                cwd=path_input)
                if ffmpeg_process.returncode != 0:
                    ffmpeg_process_stderr = ffmpeg_process.stderr.decode('utf-8')
                    logging.error('Ocurrio un error durante la segunda ejecucion de :{}'
                                  '\nMensaje:'
                                  '\n{}'
                                  .format(' '.join([str(arg) for arg in ffmpeg_args]),
                                          ffmpeg_process_stderr))
                    # raise ValueError('Ocurrio una excepcion al crear el video {}:\n{}'.format(path_output_video,
                    #                                                                           ffmpeg_process_stderr))
                else:
                    logging.info('Funciono con la segunda configuracion\n' + ffmpeg_process.stdout.decode('utf-8'))
            else:
                logging.info(ffmpeg_process.stdout.decode('utf-8'))

        return path_output_video

    @staticmethod
    def get_average_logo_area(items: List[ItemRealTime], height, width) -> float:
        qty_items = len(items)
        sum__area_items = sum([item.get_area_logo(height, width) for item in items])
        logo_area_average = sum__area_items / qty_items

        return logo_area_average

    @staticmethod
    def get_average_percentage_logo_area(items: List[ItemRealTime], height, width) -> float:
        logo_area_average = Interval.get_average_logo_area(items, height, width)
        average_percentage_logo_area = (logo_area_average / (height * width)) * 100

        return average_percentage_logo_area

    def get_file_name_list_frames(self):
        return Interval.TEMPLATE_FILE_NAME_LIST_FRAMES.format(self.id)

    @staticmethod
    def get_values_by_pattern(pattern, text):
        return re.search(pattern, text)

    @staticmethod
    def get_first_value_by_pattern_from_file_name_video(pattern, file_name):
        values = Interval.get_values_by_pattern(pattern, file_name)
        first_value = values[1] if values is not None else None

        if first_value is None:
            raise ValueError('El nombre de archivo de video {} no cumple con el patron {}, '
                             'por lo que no es un nombre valido de video de intervalo'.format(file_name,
                                                                                              pattern))

        return first_value

    @staticmethod
    def get_id_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('id(.+?)_', file_name)

    @staticmethod
    def get_interval_validation_rule_id_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('rid(.+?)_', file_name)

    @staticmethod
    def get_logo_id_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('rid(.+?)_', file_name)

    @staticmethod
    def get_vehicle_id_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('vid(.+?)_', file_name)

    @staticmethod
    def get_source_signal_id_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('sid(.+?)_', file_name)

    @staticmethod
    def get_start_time_str_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('st(.+?)_', file_name)

    @staticmethod
    def get_end_time_str_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('et(.+?)_', file_name)

    @staticmethod
    def get_start_time_from_file_name_video(file_name) -> datetime:
        start_time_str = Interval.get_start_time_str_from_file_name_video(file_name)
        start_time = datetime.strptime(start_time_str, '%Y%m%dh%Hm%Ms%S')

        return start_time

    @staticmethod
    def get_end_time_from_file_name_video(file_name) -> datetime:
        end_time_str = Interval.get_end_time_str_from_file_name_video(file_name)
        end_time = datetime.strptime(end_time_str, '%Y%m%dh%Hm%Ms%S')

        return end_time

    @staticmethod
    def get_start_item_real_time_id_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video('sirt(.+?)_', file_name)

    @staticmethod
    def get_end_item_real_time_id_from_file_name_video(file_name):
        return Interval.get_first_value_by_pattern_from_file_name_video(
            'eirt(.+?).{}'.format(Interval.get_valid_extension_for_video()),
            file_name
        )

    @staticmethod
    def get_valid_extension_for_video():
        return Interval.TEMPLATE_VIDEO_NAME.split('.')[-1]

    @staticmethod
    def get_avg_detection_score_items_and_detections_below(items: Union[List[ItemRealTime], List[float]]) \
            -> (float, int):

        def get_score(element):
            if type(element) == ItemRealTime:
                return element.score
            elif type(element) in [float, int]:
                return element

        qty_items = len(items)
        items_scores = [get_score(item) for item in items]
        avg_detection_score_items = sum(isc for isc in items_scores) / qty_items if qty_items != 0 else 0
        detections_below = sum([1 if isc < avg_detection_score_items else 0 for isc in items_scores])

        return avg_detection_score_items, detections_below
