from datetime import datetime

from DAL.dal import DAL
from DAL.model.interval import Interval
from DAL.model.report.stats_process_detections_to_intervals import StatsProcessDetectionsToIntervals
from DAL import sqlserverport

CONNECTION_STRING = 'DRIVER={};SERVER={};DATABASE={};Trusted_Connection=yes;'
CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'

driver = '{ODBC Driver 17 for SQL Server}'
server = '10.240.14.213'
instance = 'APP'
database = 'MarketingDeportivo'
uid = 'BUEMBW-LNXAPP06'
pwd = 'Mematas01'

port = sqlserverport.lookup(server, instance)

connection_string = CONNECTION_STRING_SQL_LOGIN.format(driver, server + ',' + str(port), database, uid, pwd)
dal = DAL(connection_string)

# dal.insert_intervals([Interval(intervals_validation_rule_id=1,
#                                logo_id=1,
#                                vehicle_id=1,
#                                source_signal_id=21,
#                                start_time=datetime.now(),
#                                end_time=datetime.now(),
#                                start_item_real_time_id=0,
#                                end_item_real_time_id=0,
#                                grps=0,
#                                gross_investment=0,
#                                net_investment=0,
#                                path_file_raw_data='/home/enzoscocco/Documents/object_detection/data_detection/vh14'
#                                                   '/2019/04/29/15/2019042915__34__14__.csv',
#                                recognized_text='TEST BORRAR')],
#                      verbose=True)

# interval = dal.get_interval_by_id(6959)
#
# items = dal.get_items_real_time_by_interval_id(interval.id, verbose=True)
#
# print(interval)
#
# interval.average_percentage_logo_area = Interval.get_average_percentage_logo_area(items, 480, 640)
#
# print(interval)
# print('\nto_csv:\n{}'.format(interval.to_csv()))
# print('\nto_values_insert_sql:\n{}'.format(interval.to_values_insert_sql()))
#
# interval.recognized_text = 'TEST BORRAR'
# dal.insert_intervals([interval], verbose=True)
#
# print('Termino')


def insert_stats_process_detections_to_intervals():
    stats = StatsProcessDetectionsToIntervals(datetime.now(),
                                              2,
                                              'ARGENTINA',
                                              6,
                                              'BUEMBW-LNXAPP06',
                                              'CSV_TEST',
                                              1,
                                              'BUEMBW-LNXAPP01',
                                              1,
                                              'C5N',
                                              21,
                                              datetime.now(),
                                              detections_below_avg_score_validation_rule=23,
                                              detections_below_avg_score_ocr=12,
                                              avg_detection_score_items_ocr=0.4)
    print('stats:', stats)

    dal.insert_stat_process_detections(stats, verbose=True)


if __name__ == '__main__':
    insert_stats_process_detections_to_intervals()

    print('Termino')
