from DAL.model.country import Country
from DAL.model.logo import Logo
from DAL.model.server import Server
from DAL.model.client import Client
from DAL.model.vehicle import Vehicle
from DAL.model.interval import Interval
from DAL.model.source_signal import SourceSignal
from DAL.model.item_real_time import ItemRealTime
from DAL.model.history_csv_processed import HistoryCSVProcessed
from DAL.model.report.csv_process_advance import CSVProcessAdvance
from DAL.model.intervals_validation_rule import IntervalsValidationRule

import os
import csv
import logging
import sqlalchemy
import subprocess
import configparser

from typing import List, Union
from datetime import datetime
from urllib.parse import quote_plus

import utils_cris as uc

from logo_detection_persistence import LogoDetectionPersistence as ldp


class DAL:
    ENGINE_CONNECTION_STRING = 'mssql+pyodbc:///?odbc_connect={}'
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3
    DAL_CONFIG_FILE = 'dal_config.ini'
    QUERY_SP_GetItemsRealTimeForIntervalWithLogo = 'EXEC [{}].GetItemsRealTimeForIntervalWithLogo {}, {}, {}, {}, {}'
    QUERY_SP_GetItemsRealTimeForIntervalWithIndistinctLogo \
        = "EXEC [{}].[GetItemsRealTimeForIntervalWithIndistinctLogo] {}, {}, '{}', '{}', {}, {}"
    QUERY_GET_HISTORY_UPDATES = 'SELECT * FROM [{}].HistoryUpdates'
    QUERY_GET_CLIENTS = 'SELECT * FROM [{}].Client WHERE Active = 1'
    QUERY_GET_SERVERS_BY_COUNTRY_WITH_SOURCESIGNALS = \
        'SELECT sv.*, ss.ID SourceSignalID, ss.[Name], ss.Observations, ss.Active,' \
        '\n\t   ss.[Path], ss.FPS, ss.Width, ss.Height, ss.ServerID ' \
        '\n\tFROM [{}].[Server] sv' \
        '\n\t\tINNER JOIN [{}].SourceSignal ss' \
        '\n\t\t\tON sv.ID = ss.ServerID' \
        '\n\tWHERE CountryID = {}'
    QUERY_GET_SERVERS_BY_TYPE_AND_COUNTRY = "SELECT sv.*" \
                                            "\n\tFROM [{}].[Server] sv" \
                                            "\n\tWHERE sv.[Type] = '{}'" \
                                            "\n\t\t  AND sv.CountryID = {}"
    QUERY_GET_CSV_WITHOUT_INTERVAL_PROCESS_BY_CLIENT = 'SELECT * ' \
                                                       '\n\tFROM [{}].HistoryCSVProcessed ' \
                                                       '\n\tWHERE ClientID = {} ' \
                                                       '\n\t\t  AND ProcessInterval = 0' \
                                                       '\n\t\t  AND ID % {} = {}'
    QUERY_GET_CSV_WITHOUT_INTERVAL_OR_OCR_PROCESS_BY_CLIENT = 'SELECT * ' \
                                                              '\n\tFROM [{}].HistoryCSVProcessed ' \
                                                              '\n\tWHERE ClientID = {} ' \
                                                              '\n\t\t  AND (ProcessInterval = 0 OR ProcessOCR = 0)' \
                                                              '\n\t\t  AND ID % {} = {}'
    QUERY_GET_HISTORY_CSV_PROCESSED = 'SELECT * ' \
                                      '\n\tFROM [{}].HistoryCSVProcessed '
    QUERY_INSERT_HISTORY_CSV_PROCESSED = 'INSERT INTO [{}].[HistoryCSVProcessed] VALUES ({})'
    QUERY_UPDATE_RECOGNIZED_TEXT_ITEMS_REAL_TIME = "UPDATE [{}].[ItemRealTime]" \
                                                   "\n\tSET RecognizedText = '{}'" \
                                                   "\n\tWHERE ID = {}"
    QUERY_GET_INTERVALS = "SELECT * " \
                          "\n\tFROM [{}].[Interval]" \
                          "\n\tWHERE Starttime >= '{}'" \
                          "\n\t\t  AND Endtime <= '{}'"
    QUERY_GET_INTERVALS_WITHOUT_VIDEO = "SELECT * " \
                                        "\n\tFROM [{}].[Interval]" \
                                        "\n\tWHERE Starttime >= '{}'" \
                                        "\n\t\t  AND Endtime <= '{}'" \
                                        "\n\t\t  AND PathVideo = ''"
    # QUERY_GET_INTERVALS_WITHOUT_VIDEO_BY_MOD = "SELECT i.* " \
    #                                            "\n\tFROM [{}].[Interval] i " \
    #                                            "\n\t\tINNER JOIN [{}].[ItemRealTime] irt" \
    #                                            "\n\t\t\tON i.StartItemRealTimeID = irt.ID" \
    #                                            "\n\t\tINNER JOIN [{}].[HistoryCSVProcessed] hcp" \
    #                                            "\n\t\t\tON irt.PathFileRawData = hcp.[Path]" \
    #                                            "\n\tWHERE i.Starttime >= '{}'" \
    #                                            "\n\t\t  AND i.Endtime <= '{}'" \
    #                                            "\n\t\t  AND PathVideo = ''" \
    #                                            "\n\t\t  AND hcp.ID % {} IN ({})"
    QUERY_GET_INTERVALS_WITHOUT_VIDEO_BY_MOD = "SELECT i.* " \
                                               "\n\tFROM [{}].[Interval] i " \
                                               "\n\t\tINNER JOIN [{}].[HistoryCSVProcessed] hcp" \
                                               "\n\t\t\tON i.PathFileRawData = hcp.[Path]" \
                                               "\n\tWHERE i.Starttime >= '{}'" \
                                               "\n\t\t  AND i.Endtime <= '{}'" \
                                               "\n\t\t  AND hcp.ProcessVideoIntervals = 0" \
                                               "\n\t\t  AND hcp.ID % {} IN ({})"
    QUERY_UPDATE_INTERVAL_PATH_VIDEO = "UPDATE [{}].[Interval]" \
                                       "\n\tSET PathVideo = '{}'" \
                                       "WHERE ID = {}"
    QUERY_INSERT_ITEM_REAL_TIME = 'INSERT INTO [{}].[ItemRealTime] VALUES ({})'
    QUERY_DELETE_ITEM_REAL_TIME_BY_CSV_PATH = "DELETE FROM [{}].[ItemRealTime] WHERE PathFileRawData = '{}'"
    QUERY_SP_GetItemsRealTimePathsByIntervalID = "EXEC [{}].[GetItemsRealTimePathsByIntervalID] {}"
    QUERY_SP_GetItemsRealTimePathsByIntervalsIDs = "EXEC [{}].[GetItemsRealTimePathsByIntervalsIDs] {}"
    QUERY_GET_ITEMS_REAL_TIME_PATHS_BY_INTERVALS_IDS = 'SELECT DISTINCT irt.[Path]' \
                                                       '\n\tFROM [{}].ItemRealTime irt' \
                                                       '\n\t\tINNER JOIN [{}].Interval i' \
                                                       '\n\t\t\tON irt.ID >= i.StartItemRealTimeID' \
                                                       '\n\t\t\t   AND irt.ID <= i.EndItemRealTimeID' \
                                                       '\n\t\t\t   AND irt.LogoID = i.LogoID' \
                                                       '\n\t\t\t   AND irt.VehicleID = i.VehicleID' \
                                                       '\n\t\t\t   AND irt.SourceSignalID = i.SourceSignalID' \
                                                       '\n\tWHERE i.ID IN ({})'
    QUERY_GET_ITEMS_REAL_TIME_BY_INTERVALS_ID = 'SELECT irt.*' \
                                                '\n\tFROM [{}].ItemRealTime irt' \
                                                '\n\t\tINNER JOIN [{}].Interval i' \
                                                '\n\t\t\tON irt.ID >= i.StartItemRealTimeID' \
                                                '\n\t\t\t   AND irt.ID <= i.EndItemRealTimeID' \
                                                '\n\t\t\t   AND irt.LogoID = i.LogoID' \
                                                '\n\t\t\t   AND irt.VehicleID = i.VehicleID' \
                                                '\n\t\t\t   AND irt.SourceSignalID = i.SourceSignalID' \
                                                '\n\tWHERE i.ID = {}'
    QUERY_GET_SERVER_BY_INTERVAL_ID = 'SELECT * ' \
                                      '\n\tFROM [{}].[Server] sv' \
                                      '\n\t\tINNER JOIN [{}].[SourceSignal] ss' \
                                      '\n\t\t\tON sv.ID = ss.ServerID' \
                                      '\n\t\tINNER JOIN [{}].[Interval] i' \
                                      '\n\t\t\tON ss.ID = i.SourceSignalID' \
                                      '\n\tWHERE i.ID = {}'
    QUERY_SP_StagingItemRealTimeToProduction = "EXEC [{}].[StagingItemRealTimeToProduction] '{}'"
    QUERY_INSERT_ITEMS_REAL_TIME_FROM_STAGING = "INSERT INTO [{}].[ItemRealTime]" \
                                                "\n\tSELECT * " \
                                                "\n\t\tFROM [{}].[ItemRealTime] sirt" \
                                                "\n\t\tWHERE sirt.PathFileRawData = '{}'"
    QUERY_DELETE_ITEMS_REAL_TIME_FROM_STAGING = "DELETE FROM [{}].[ItemRealTime]" \
                                                "\n\tWHERE PathFileRawData = '{}'"
    QUERY_GET_VEHICLES = 'SELECT *' \
                         '\n\tFROM [{}].[Vehicle]'
    QUERY_GET_VEHICLES_ASSOCIATED_TO_SOURCE_SIGNALS = 'SELECT v.*' \
                                                      '\n\tFROM [{}].[Vehicle] v' \
                                                      '\n\t\tINNER JOIN [{}].[SourceSignalVehicle] ssv' \
                                                      '\n\t\t\tON v.ID = ssv.VehicleID'
    QUERY_GET_COUNTRY_BY_ID = 'SELECT *' \
                              '\n\tFROM [{}].Country cty' \
                              '\n\tWHERE cty.ID = {}'
    QUERY_SP_GetVehicleByServerID = 'EXEC [{}].GetVehicleByServerID {}'
    QUERY_INSERT_REPORT_CSV_PROCESS_ADVANCE = 'INSERT INTO [{}].[CSVProcessAdvance] {}'
    QUERY_IS_CSV_ALREADY_PROCESSED = "SELECT COUNT(1) IsCSVAlreadyProcessed" \
                                     "\n\tFROM [{}].[HistoryCSVProcessed] hcp" \
                                     "\n\tWHERE hcp.[Path] = '{}'" \
                                     "\n\t\t  AND hcp.ProcessVideoIntervals = 1"
    QUERY_IS_CSV_ALREADY_PROCESSED_OR_BEING_PROCESSING = \
        "SELECT COUNT(1) IsCSVAlreadyProcessed" \
        "\n\tFROM [{}].[HistoryCSVProcessed] hcp" \
        "\n\tWHERE hcp.[Path] = '{}'" \
        "\n\t\t  AND (     " \
        "\n\t\t\t        (hcp.ProcessVideoIntervals = 1 AND hcp.BeingProcessing = 0)" \
        "\n\t\t\t      OR " \
        "\n\t\t\t        (hcp.ProcessVideoIntervals = 0 AND hcp.BeingProcessing = 1)" \
        "\n\t\t\t)"
    QUERY_SP_GetIntervalsValidationRule = "EXECUTE [{}].[GetIntervalsValidationRule] '{}', '{}', {}, {}"
    QUERY_GET_INTERVALS_VALIDATION_RULE_BY_ID = 'SELECT * ' \
                                                '\n\tFROM [{}].[IntervalsValidationRule]' \
                                                '\n\tWHERE ID = {}'
    QUERY_INSERT_INTERVALS = "INSERT INTO [{}].[Interval] {}"
    QUERY_CSV_ALREADY_PROCESSED = 'SELECT hcp.*' \
                                  '\n\tFROM [{}].[HistoryCSVProcessed] hcp' \
                                  '\n\tWHERE hcp.ProcessVideoIntervals = 1'
    QUERY_CSV_ALREADY_PROCESSED_BY_SERVER_ID = QUERY_CSV_ALREADY_PROCESSED + '\n\t   AND ServerID = {}'
    QUERY_GET_SERVER_BY_IP = "SELECT * FROM [{}].[Server] WHERE IP = '{}'"
    QUERY_INSERT_STAT_PROCESS_DETECTIONS_TO_INTERVALS = "INSERT INTO [{}].[StatProcessDetectionToIntervals] {}"
    QUERY_DELETE_HISTORY_CSV_PROCESSED_BY_ERROR_PROCESS = "DELETE FROM [{}].[HistoryCSVProcessed] hcp" \
                                                          "\n\tWHERE hcp.[Path] = {}" \
                                                          "\n\t\t  AND hcp.BeingProcessed = 0"
    QUERY_UPDATE_CSV_BEING_PROCESSED_BY_PATH_AND_SERVER = "UPDATE [{}].[HistoryCSVProcessed]" \
                                                          "\n\tSET BeingProcessing = 0" \
                                                          "\n\tWHERE [Path] = '{}'" \
                                                          "\n\t\t  AND ServerProcessID = {}"
    QUERY_SP_IsCSVAlreadyProcessedOrBeingProcessing = "EXEC [{}].IsCSVAlreadyProcessedOrBeingProcessing '{}'"
    QUERY_UPDATE_CSV_BEING_PROCESSED_BY_SERVER = "UPDATE [{}].CurrentCSVBeingProcessing" \
                                                 "\n\tSET CSV = '{}'," \
                                                 "\n\t\tSince = GETDATE()" \
                                                 "\n\tWHERE ServerProcessID = {}"
    QUERY_UPDATE_INTERVAL_REVIEWED_OPERATOR_FILTER_BY_VIDEO_NAME = \
        "UPDATE [{}].[Interval]" \
        "\n\tSET Reviewed = {}," \
        "\n\t\tOperatorFilter = {}" \
        "\n\tWHERE SUBSTRING(PathVideo, CHARINDEX('id', PathVideo), len(PathVideo)) = '{}'"

    def __init__(self, connection_string, dal_config_file=None):
        self.connection_string = connection_string
        self.engine_connection_string = DAL.ENGINE_CONNECTION_STRING.format(quote_plus(self.connection_string))
        self.engine: sqlalchemy.engine.base.Engine = sqlalchemy.create_engine(self.engine_connection_string)
        self.dal_config_file = dal_config_file if dal_config_file is not None \
            else os.path.join(os.path.dirname(os.path.realpath(__file__)), DAL.DAL_CONFIG_FILE)
        self.dal_config = configparser.ConfigParser()
        self.dal_config.read(self.dal_config_file)
        self.schema_detection = self.dal_config['DAL']['schema_detection']
        self.schema_media = self.dal_config['DAL']['schema_media']
        self.schema_staging = self.dal_config['DAL']['schema_staging']
        self.schema_report = self.dal_config['DAL']['schema_report']
        self.store_procedure_items_real_time = self.dal_config['DAL']['store_procedure_items_real_time']
        self.store_procedure_get_logos_by_client = self.dal_config['DAL']['store_procedure_get_logos_by_client']

        self.driver = self.get_driver_from_connection_string(self.connection_string)
        self.server = self.get_server_from_connection_string(self.connection_string)
        self.database = self.get_database_from_connection_string(self.connection_string)
        self.uid = self.get_uid_from_connection_string(self.connection_string)
        self.pwd = self.get_pwd_from_connection_string(self.connection_string)

    @staticmethod
    def get_driver_from_connection_string(connection_string):
        return DAL.get_value_from_connection_string(connection_string, 'DRIVER')

    @staticmethod
    def get_server_from_connection_string(connection_string):
        return DAL.get_value_from_connection_string(connection_string, 'SERVER')

    @staticmethod
    def get_database_from_connection_string(connection_string):
        return DAL.get_value_from_connection_string(connection_string, 'DATABASE')

    @staticmethod
    def get_uid_from_connection_string(connection_string):
        return DAL.get_value_from_connection_string(connection_string, 'UID')

    @staticmethod
    def get_pwd_from_connection_string(connection_string):
        return DAL.get_value_from_connection_string(connection_string, 'PWD')

    @staticmethod
    def get_value_from_connection_string(connection_string, key):
        value = ''
        start_index = connection_string.upper().find(key.upper() + '=')
        end_index = connection_string.upper().find(';', start_index)
        if start_index != -1:
            end_index = end_index if end_index != -1 else len(connection_string)
            value = connection_string[start_index + len(key + '='):end_index]

        return value

    def get_items_real_time(self, execute_since, execute_until, client_id, country_id) -> List[ItemRealTime]:
        sql_str_execute_since = execute_since.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_str_execute_until = execute_until.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_command = "EXECUTE [{}].[{}] '{}', '{}', {}, {}".format(self.schema_detection,
                                                                    self.store_procedure_items_real_time,
                                                                    sql_str_execute_since,
                                                                    sql_str_execute_until,
                                                                    client_id,
                                                                    country_id)

        items_real_time = [ItemRealTime(irt.VehicleID,
                                        irt.SourceSignalID,
                                        irt.LogoID,
                                        irt.Date,
                                        irt.Path,
                                        irt.Score,
                                        irt.BoxYMin,
                                        irt.BoxXMin,
                                        irt.BoxYMax,
                                        irt.BoxXMax,
                                        irt.PathFileRawData,
                                        id=irt.ID) for irt in self.engine.execute(sql_command).fetchall()]

        return items_real_time

    def get_logos_by_client(self, client_id):
        sql_command = 'EXECUTE [{}].[{}] {}'.format(self.schema_detection,
                                                    self.store_procedure_get_logos_by_client,
                                                    client_id)

        logos = [Logo(l.Name,
                      l.Observations,
                      l.Path,
                      l.ProductID,
                      l.Active,
                      l.DisplayName,
                      id=l.ID) for l in self.engine.execute(sql_command).fetchall()]

        return logos

    def get_logos(self):
        sql_command = 'SELECT * ' \
                      '\n\tFROM [{}].[Logo]'.format(self.schema_detection)

        logos = [Logo(l.Name,
                      l.Observations,
                      l.Path,
                      l.ProductID,
                      l.Active,
                      l.DisplayName,
                      id=l.ID) for l in self.engine.execute(sql_command).fetchall()]

        return logos

    def get_items_real_time_for_interval_with_logo(self, start_item_real_time_id, end_item_real_time_id, logo_id,
                                                   vehicle_id, source_signal_id):
        results = self.engine.execute(self.QUERY_SP_GetItemsRealTimeForIntervalWithLogo
                                      .format(start_item_real_time_id,
                                              end_item_real_time_id,
                                              logo_id,
                                              vehicle_id,
                                              source_signal_id)).fetchall()

        items_real_time = [ItemRealTime(irt.VehicleID,
                                        irt.SourceSignalID,
                                        irt.LogoID,
                                        irt.Date,
                                        irt.Path,
                                        irt.Score,
                                        irt.BoxYMin,
                                        irt.BoxXMin,
                                        irt.BoxYMax,
                                        irt.BoxXMax,
                                        irt.PathFileRawData,
                                        id=irt.ID) for irt in results]

        return items_real_time

    def get_history_updates(self):
        results = self.engine.execute(DAL.QUERY_GET_HISTORY_UPDATES.format(self.schema_detection)).fetchall()

        return results

    def get_clients(self) -> List[Client]:
        results = self.engine.execute(DAL.QUERY_GET_CLIENTS.format(self.schema_media)).fetchall()
        clients = [Client(r.ID, r.Name, r.Active, r.CreatedBy, r.UpdatedBy) for r in results]

        return clients

    def get_servers(self, country_id) -> dict:
        results = self.engine.execute(DAL.QUERY_GET_SERVERS_BY_COUNTRY_WITH_SOURCESIGNALS
                                      .format(self.schema_detection, self.schema_detection, country_id)).fetchall()

        servers = {}
        [servers.update({int(r.ID): self.sql_result_to_server(r)})
         for r in results if not any([server_id == int(r.ID) for server_id in servers.keys()])]
        for key, server in servers.items():
            server.source_signals = [SourceSignal(r.SourceSignalID,
                                                  r.Name,
                                                  r.Observations,
                                                  r.Active,
                                                  r.Path,
                                                  r.FPS,
                                                  r.Width,
                                                  r.Height,
                                                  r.ServerID) for r in results if int(r.ServerID) == key]

        return servers

    def get_csv_without_interval_process_by_client(self, client_id, mod=1, result_mod=0, verbose=True) \
            -> List[HistoryCSVProcessed]:
        sql_command = DAL.QUERY_GET_CSV_WITHOUT_INTERVAL_PROCESS_BY_CLIENT.format(self.schema_detection,
                                                                                  client_id,
                                                                                  mod,
                                                                                  result_mod)

        if verbose:
            uc.print_and_logging('get_csv_without_interval_process_by_client: {}'.format(sql_command))

        results = self.engine.execute(sql_command).fetchall()

        csv_without_intervals_process = self.sql_results_to_history_csv_processed(results)

        return csv_without_intervals_process

    def get_csv_without_interval_or_ocr_process_by_client(self, client_id, mod=1, result_mod=0, verbose=True) \
            -> List[HistoryCSVProcessed]:
        sql_command = DAL.QUERY_GET_CSV_WITHOUT_INTERVAL_OR_OCR_PROCESS_BY_CLIENT.format(self.schema_detection,
                                                                                         client_id,
                                                                                         mod,
                                                                                         result_mod)

        if verbose:
            uc.print_and_logging('get_csv_without_interval_or_ocr_process_by_client: {}'.format(sql_command))

        results = self.engine.execute(sql_command).fetchall()

        csv_without_intervals_process = self.sql_results_to_history_csv_processed(results)

        return csv_without_intervals_process

    def get_csv_without_interval_or_ocr_process_by_client_sort_by_date(self, client_id, mod=1, result_mod=0,
                                                                       reverse=False, verbose=True):

        csv_without_intervals_or_ocr_process = self.get_csv_without_interval_or_ocr_process_by_client(client_id,
                                                                                                      mod,
                                                                                                      result_mod,
                                                                                                      verbose)
        tuple_csv_without_intervals__or_ocr_process = [(hcp,
                                                        ldp.get_datetime_from_filename_csv(os.path.basename(hcp.path))
                                                        )
                                                       for hcp in csv_without_intervals_or_ocr_process]

        tuple_csv_without_intervals__or_ocr_process.sort(key=lambda thcp: thcp[1], reverse=reverse)

        csv_without_interval_process_or_ocr_by_client_sort_by_date = [thcp[0] for thcp
                                                                      in tuple_csv_without_intervals__or_ocr_process]

        return csv_without_interval_process_or_ocr_by_client_sort_by_date

    def get_csv_without_interval_process_by_client_sort_by_date(self, client_id, mod=1, result_mod=0, verbose=True,
                                                                reverse=False):

        csv_without_intervals_process = self.get_csv_without_interval_process_by_client(client_id,
                                                                                        mod,
                                                                                        result_mod,
                                                                                        verbose)
        tuple_csv_without_intervals_process = [(hcp, ldp.get_datetime_from_filename_csv(os.path.basename(hcp.path)))
                                               for hcp in csv_without_intervals_process]

        tuple_csv_without_intervals_process.sort(key=lambda thcp: thcp[1], reverse=reverse)

        csv_without_interval_process_by_client_sort_by_date = [thcp[0] for thcp in tuple_csv_without_intervals_process]

        return csv_without_interval_process_by_client_sort_by_date

    def get_history_csv_processed_by_id(self, hcp_id) -> List[HistoryCSVProcessed]:
        results = self.engine.execute('SELECT * '
                                      '\n\tFROM [{}].HistoryCSVProcessed '
                                      '\n\tWHERE ID = {} '
                                      .format(self.schema_detection, hcp_id)).fetchall()

        history_csv_processed = self.sql_results_to_history_csv_processed(results)

        return history_csv_processed

    def write_history_csv_processed(self, history_csv_processed: HistoryCSVProcessed):
        if history_csv_processed.id != 0:
            sql_command = 'UPDATE [{}].[HistoryCSVProcessed]' \
                          '\n\tSET ProcessInterval = {},' \
                          '\n\t\tProcessOCR = {}' \
                          '\n\tWHERE ID = {}'.format(self.schema_detection,
                                                     int(history_csv_processed.process_interval),
                                                     int(history_csv_processed.process_ocr),
                                                     int(history_csv_processed.id))
        else:
            sql_command = DAL.QUERY_INSERT_HISTORY_CSV_PROCESSED.format(self.schema_detection,
                                                                        history_csv_processed.to_csv())

        self.engine.execute(sql_command)

    def update_history_csv_processed_process_video_intervals_and_being_processed(self,
                                                                                 history_csv_processed_path,
                                                                                 process_video_intervals,
                                                                                 being_processed,
                                                                                 verbose=False):
        sql_query = "UPDATE [{}].[HistoryCSVProcessed]" \
                    "\n\tSET ProcessVideoIntervals = {}," \
                    "\n\t\t BeingProcessed = {}" \
                    "\n\tWHERE [Path] = '{}'".format(self.schema_detection,
                                                     process_video_intervals,
                                                     being_processed,
                                                     history_csv_processed_path)

        if verbose:
            uc.print_and_logging('ejecutando update_history_csv_processed_process_video_intervals_and_being_processed:'
                                 '\n{}'.format(sql_query))

        self.engine.execute(sql_query)

    # def update_history_csv_processed_being_processing_by_server_process_id(self, csv_path, server_process_id, verbose=True):
    #     sql_update = DAL.QUERY_UPDATE_CSV_BEING_PROCESSED_BY_PATH_AND_SERVER.format(self.schema_detection,
    #                                                                                 csv_path,
    #                                                                                 server_process_id)
    #
    #     if verbose:
    #         uc.print_and_logging('update_csv_being_processing_by_server_process_id:\n{}'.format(sql_update))
    #
    #     self.engine.execute(sql_update)

    def update_history_csv_processed_process_video_intervals(self, history_csv_processed_path, process_video_intervals,
                                                             verbose=False):
        sql_query = "UPDATE [{}].[HistoryCSVProcessed]" \
                    "\n\tSET ProcessVideoIntervals = {}" \
                    "\n\tWHERE [Path] = '{}'".format(self.schema_detection,
                                                     process_video_intervals,
                                                     history_csv_processed_path)

        if verbose:
            uc.print_and_logging('ejecutando update_'
                                 'history_csv_processed_procecess_video_intervals:\n{}'
                                 .format(sql_query))

        self.engine.execute(sql_query)

    def get_history_csv_processed(self) -> List[HistoryCSVProcessed]:
        results = self.engine.execute(DAL.QUERY_GET_HISTORY_CSV_PROCESSED.format(self.schema_detection))

        history_csv_processed = self.sql_results_to_history_csv_processed(results)

        return history_csv_processed

    # ID Date Path ClientID ProcessInterval ProcessOCR
    def write_blank_history_csv_processed_for_all_clients(self, path_csv, vehicle_id, source_signal_id, server_id,
                                                          clients=None):

        _datetime = datetime.now()
        clients = clients if clients is not None else self.get_clients()
        for client in clients:
            try:
                hcsvp = HistoryCSVProcessed(_datetime, path_csv, client.id, vehicle_id, source_signal_id, server_id)
                sql_command = DAL.QUERY_INSERT_HISTORY_CSV_PROCESSED.format(self.schema_detection,
                                                                            hcsvp.to_csv())
                self.engine.execute(sql_command)
            except Exception as e:
                raise ValueError('[write_blank_write_history_csv_processed_for_all_clients] Sucedio un error:{}'
                                 '\npath_csv: {}'
                                 '\nvehicle_id: {}'
                                 '\nsource_signal_id: {}'
                                 '\nserver_id: {}'
                                 '\nclient: {}'
                                 .format(e,
                                         path_csv,
                                         vehicle_id,
                                         source_signal_id,
                                         server_id,
                                         client))

    def update_recognized_text_items_real_time(self, items_real_time: List[ItemRealTime]):
        template_sql_command = DAL.QUERY_UPDATE_RECOGNIZED_TEXT_ITEMS_REAL_TIME.format(self.schema_detection,
                                                                                       '{}',
                                                                                       '{}')

        for irt in items_real_time:
            sql_command = template_sql_command.format(irt.recognized_text,
                                                      irt.id)
            self.engine.execute(sql_command)

    @staticmethod
    def sql_result_to_intervals_validation_rule(result) -> IntervalsValidationRule:
        intervals_validation_rule = IntervalsValidationRule(
            id=result.ID,
            client_id=result.ClientID,
            country_id=result.CountryID,
            since=result.Since,
            minimum_exposition_time=result.MinimumExpositionTime,
            minimum_percent_frames=result.MinimumPercentFrames,
            minimum_percent_match_detection=result.MinimumPercentMatchDetection,
            created=result.Created)

        return intervals_validation_rule

    @staticmethod
    def sql_result_to_item_real_time(result) -> ItemRealTime:
        item_real_time = ItemRealTime(id=result.ID,
                                      vehicle_id=result.VehicleID,
                                      source_signal_id=result.SourceSignalID,
                                      logo_id=result.LogoID,
                                      date=result.Date,
                                      path=result.Path,
                                      score=result.Score,
                                      box_y_min=result.BoxYMin,
                                      box_x_min=result.BoxXMin,
                                      box_y_max=result.BoxYMax,
                                      box_x_max=result.BoxXMax,
                                      path_file_raw_data=result.PathFileRawData,
                                      recognized_text=result.RecognizedText)

        return item_real_time
    
    @staticmethod
    def sql_result_to_server(result):
        server = Server(id=result.ID,
                        hostname=result.HostName,
                        ip=result.IP,
                        country_id=result.CountryID,
                        data_detection_path=result.DataDetectionPath)
        
        return server

    @staticmethod
    def sql_result_to_history_csv_processed(result):
        history_csv_processed = HistoryCSVProcessed(date=result.Date,
                                                    path=result.Path,
                                                    client_id=result.ClientID,
                                                    vehicle_id=result.VehicleID,
                                                    source_signal_id=result.SourceSignalID,
                                                    server_id=result.ServerID,
                                                    process_interval=result.ProcessInterval,
                                                    process_ocr=result.ProcessOCR,
                                                    process_video_intervals=result.ProcessVideoIntervals,
                                                    id=result.ID)

        return history_csv_processed

    @staticmethod
    def sql_results_to_item_real_time(results: List) -> List[ItemRealTime]:
        items_real_time = [DAL.sql_result_to_item_real_time(result) for result in results]

        return items_real_time

    @staticmethod
    def sql_results_to_countries(results):
        countries = [Country(id=r.ID,
                             name=r.Name,
                             active=r.Active) for r in results]

        return countries

    @staticmethod
    def sql_results_to_servers(results):
        servers = [DAL.sql_result_to_server(r) for r in results]

        return servers

    @staticmethod
    def sql_results_to_intervals(results):
        intervals = [Interval(intervals_validation_rule_id=r.IntervalsValidationRuleID,
                              logo_id=r.LogoID,
                              vehicle_id=r.VehicleID,
                              source_signal_id=r.SourceSignalID,
                              start_time=r.Starttime,
                              end_time=r.Endtime,
                              start_item_real_time_id=r.StartItemRealTimeID,
                              end_item_real_time_id=r.EndItemRealTimeID,
                              grps=r.GRPS,
                              gross_investment=r.GrossInvestment,
                              net_investment=r.NetInvestment,
                              id=r.ID,
                              path_video=r.PathVideo,
                              frame_match_ocr_id=r.FrameMatchOCRID,
                              percent_match_ocr=r.PercentMatchOCR,
                              monitored=r.Monitored,
                              operator_filter=r.OperatorFilter,
                              category_id=r.CategoryID,
                              auspice_id=r.AuspiceID,
                              path_file_raw_data=r.PathFileRawData,
                              created=r.Created,
                              recognized_text=r.RecognizedText,
                              reviewed=r.Reviewed,
                              server_host_ip=r.ServerHostIP,
                              average_percentage_logo_area=r.AveragePercentageLogoArea) for r in results]

        return intervals

    @staticmethod
    def sql_results_to_vehicles(results):
        vehicles = [Vehicle(name=r.Name,
                            sub_medium_id=r.SubMediumID,
                            plaza_id=r.PlazaID,
                            imtwister_id=r.IMTwisterID,
                            ibope_id=r.IbopeID,
                            active=r.Active,
                            audit_start_time=r.AuditStartTime,
                            audit_end_time=r.AuditEndTime,
                            id=r.ID) for r in results]

        return vehicles

    @staticmethod
    def sql_results_to_history_csv_processed(results):
        list_history_csv_processed = [DAL.sql_result_to_history_csv_processed(r) for r in results]

        return list_history_csv_processed

    def get_interval_by_id(self, interval_id):
        r = self.engine.execute('SELECT * FROM [{}].Interval '
                                '\n\tWHERE ID = {}'
                                .format(self.schema_detection, interval_id)).fetchall()[0]

        intervals = self.sql_results_to_intervals([r])
        interval = intervals[0] if len(intervals) > 0 else None

        return interval

    def get_intervals(self, since: datetime, until=datetime.now()) -> List[Interval]:
        results = self.engine.execute(DAL.QUERY_GET_INTERVALS
                                      .format(self.schema_detection,
                                              since.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL],
                                              until.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL])
                                      ).fetchall()

        return self.sql_results_to_intervals(results)

    def get_intervals_without_video(self, since: datetime, until=datetime.now()) -> List[Interval]:
        results = self.engine.execute(DAL.QUERY_GET_INTERVALS_WITHOUT_VIDEO
                                      .format(self.schema_detection,
                                              since.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL],
                                              until.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL])
                                      ).fetchall()

        return self.sql_results_to_intervals(results)

    def get_intervals_without_video_by_mods(self, since: datetime, until=datetime.now(), mod=1, results_mod=[0],
                                            verbose=False) -> List[Interval]:
        sql_query = DAL.QUERY_GET_INTERVALS_WITHOUT_VIDEO_BY_MOD \
            .format(self.schema_detection,
                    self.schema_detection,
                    since.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL],
                    until.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL],
                    mod,
                    ','.join([str(rm) for rm in results_mod]))

        if verbose:
            uc.print_and_logging('get_intervals_without_video_by_mod:\n{}'.format(sql_query))

        results = self.engine.execute(sql_query).fetchall()

        return self.sql_results_to_intervals(results)

    def update_interval_path_video(self, interval_id, path_video):
        sql_command = DAL.QUERY_UPDATE_INTERVAL_PATH_VIDEO.format(self.schema_detection,
                                                                  path_video,
                                                                  interval_id)
        self.engine.execute(sql_command)

    # QUERY_INSERT_ITEM_REAL_TIME = 'INSERT INTO [{}].[ItemRealTime] VALUES ({})'
    def inserts_item_reals_time_from_csv(self, path_csv, start_row=1) -> List[ItemRealTime]:
        items_inserted = []
        with open(path_csv, newline='') as csvfile:
            nrows = len(list(csv.reader(csvfile, delimiter=',', quotechar='|'))) - start_row
            if nrows > 0:
                csvfile.seek(0)
                rows = csv.reader(csvfile, delimiter=',', quotechar='|')
                for index, row in enumerate(rows):
                    if index > start_row:
                        item_real_time = ItemRealTime(*row[:-1], recognized_text=row[-1])
                        sql_command = DAL.QUERY_INSERT_ITEM_REAL_TIME.format(self.schema_detection,
                                                                             item_real_time.to_values_insert_sql())
                        logging.info('Insertando item real time {}/{}: {}'.format(index, nrows, item_real_time))
                        self.engine.execute(sql_command)
                        items_inserted.append(item_real_time)
                logging.info('Fin de insercion de items real time de {}\n'.format(path_csv))
            else:
                logging.info('El CSV {} estaba vacio\n')

        return items_inserted

    def delete_items_real_time_by_csv_path(self, csv_path):
        self.engine.execute(DAL.QUERY_DELETE_ITEM_REAL_TIME_BY_CSV_PATH.format(self.schema_detection,
                                                                               csv_path))

    def get_full_path_table(self, table, database=None, schema='dbo'):
        database = database if database is not None else self.database
        full_path_table = '[{}].[{}].[{}]'.format(database, schema, table)

        return full_path_table

    # args = ['bcp',
    # 'MarketingDeportivo.[Staging].[ItemRealTimeTestBCP_04]', 'in', '/home/enzoscocco/Desktop/test_bcp04.csv',
    # '-S', '10.240.14.213,59964',
    # '-U', 'BUEMBW-LNXAPP06',
    # '-P', 'Mematas01',
    # '-t', ',',
    # '-c',
    # '-F', '2']

    def get_args_to_bcp(self, csv_path, bcp_path='/opt/mssql-tools/bin/bcp'):
        args = [bcp_path,
                self.get_full_path_table('ItemRealTime', schema=self.schema_staging),
                'in', csv_path,
                '-S', self.server,
                '-U', self.uid,
                '-P', self.pwd,
                '-t', ',',
                '-c',
                '-F', '2']

        return args

    def load_items_real_time_through_bcp(self, csv_path, transform=None):
        if transform is not None:
            csv_path, n_irt = transform(csv_path)

        if n_irt > 0:
            bcp_args = self.get_args_to_bcp(csv_path)
            logging.info('{} items real time en archivo a subir por bcp'.format(n_irt))
            logging.info('Ejecutando subprocess: {}'.format(' '.join([str(arg) for arg in bcp_args])))
            bcp_process = subprocess.run(args=bcp_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if bcp_process.returncode != 0:
                logging.error(bcp_process.stderr.decode('utf-8'))
            else:
                logging.info(bcp_process.stdout.decode('utf-8'))
        else:
            logging.info('Sin items para subir en archivo')

        return n_irt

    def get_items_real_time_paths_by_interval_id(self, interval_id) -> List[str]:
        sql_command = DAL.QUERY_SP_GetItemsRealTimePathsByIntervalID.format(self.schema_detection,
                                                                            interval_id)
        items_real_time_paths_by_interval_id = [path for path in self.engine.execute(sql_command).fetchall()]

        return items_real_time_paths_by_interval_id

    def get_items_real_time_paths_by_intervals_ids(self, intervals_ids: List[int]) -> List[str]:
        sql_command = DAL.QUERY_GET_ITEMS_REAL_TIME_PATHS_BY_INTERVALS_IDS \
            .format(self.schema_detection,
                    self.schema_detection,
                    ', '.join([str(itv_id) for itv_id in intervals_ids]))

        print(sql_command)

        items_real_time_paths_by_interval_id = [path for path in self.engine.execute(sql_command).fetchall()]

        return items_real_time_paths_by_interval_id

    def get_items_real_time_for_interval_with_indistinct_logo(self, start_item_real_time_id, end_item_real_time_id,
                                                              start_time: datetime, end_time: datetime,
                                                              vehicle_id, source_signal_id, verbose=True):
        sql_command = DAL.QUERY_SP_GetItemsRealTimeForIntervalWithIndistinctLogo.format(
            self.schema_detection,
            start_item_real_time_id,
            end_item_real_time_id,
            start_time.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL],
            end_time.strftime(DAL.FORMAT_DATE)[:DAL.SUBTRACT_CHARACTER_DATETIME_SQL],
            vehicle_id,
            source_signal_id)

        if verbose:
            uc.print_and_logging('get_items_real_time_for_interval_with_indistinct_logo: \n{}'.format(sql_command))

        results = self.engine.execute(sql_command).fetchall()
        items_real_time = [ItemRealTime(irt.VehicleID,
                                        irt.SourceSignalID,
                                        irt.LogoID,
                                        irt.Date,
                                        irt.Path,
                                        irt.Score,
                                        irt.BoxYMin,
                                        irt.BoxXMin,
                                        irt.BoxYMax,
                                        irt.BoxXMax,
                                        irt.PathFileRawData,
                                        id=irt.ID) for irt in results]

        return items_real_time

    def get_server_by_interval_id(self, interval_id, verbose=True):
        sql_command = DAL.QUERY_GET_SERVER_BY_INTERVAL_ID.format(self.schema_detection,
                                                                 self.schema_detection,
                                                                 self.schema_detection,
                                                                 interval_id)
        if verbose:
            uc.print_and_logging('get_server_by_interval_id: {}'.format(sql_command))

        result = self.engine.execute(sql_command).fetchone()

        server = self.sql_result_to_server(result)

        return server

    def update_items_real_time_from_staging(self, path_file_raw_data, verbose=False):
        sql_query_insert_items = self.QUERY_INSERT_ITEMS_REAL_TIME_FROM_STAGING.format(self.schema_detection,
                                                                                       self.schema_staging,
                                                                                       path_file_raw_data)

        sql_query_delete_items = self.QUERY_DELETE_ITEMS_REAL_TIME_FROM_STAGING.format(self.schema_staging,
                                                                                       path_file_raw_data)

        connection = self.engine.connect()
        transaction = connection.begin()
        try:
            if verbose:
                uc.print_and_logging('ejecutando insercion de items:\n\t{}'.format(sql_query_insert_items))
            connection.execute(sql_query_insert_items)
            if verbose:
                uc.print_and_logging('ejecutando borrado de items de staging:\n\t{}'.format(sql_query_insert_items))
            connection.execute(sql_query_delete_items)

            if verbose:
                uc.print_and_logging(
                    'Ejecutando commit '
                    'execute_staging_item_real_time_to_production(path_file_raw_data={})'.format(path_file_raw_data)
                )
            transaction.commit()

        except Exception as e:
            transaction.rollback()
            raise ValueError('Ocurrio una excepcion produciendo un rollback durante '
                             'execute_staging_item_real_time_to_production(path_file_raw_data={}):\n{}'
                             .format(path_file_raw_data, e))
        finally:
            connection.close()

    def get_vehicles(self):
        sql_select = self.QUERY_GET_VEHICLES.format(self.schema_media)

        return self.sql_results_to_vehicles(self.engine.execute(sql_select).fetchall())

    def get_vehicles_associated_to_source_signals(self, verbose=False):
        sql_select = self.QUERY_GET_VEHICLES_ASSOCIATED_TO_SOURCE_SIGNALS.format(self.schema_media,
                                                                                 self.schema_detection)
        if verbose:
            uc.print_and_logging('get_vehicles_associated_to_source_signals:\n{}'.format(sql_select))

        return self.sql_results_to_vehicles(self.engine.execute(sql_select).fetchall())

    def get_servers_by_type_and_country_id(self, _type, country_id, verbose=False) -> List[Server]:
        sql_query = DAL.QUERY_GET_SERVERS_BY_TYPE_AND_COUNTRY.format(self.schema_detection,
                                                                     _type,
                                                                     country_id)

        if verbose:
            uc.print_and_logging('get_servers_by_type_and_country_id:\n{}'.format(sql_query))

        results = self.engine.execute(sql_query).fetchall()
        servers = self.sql_results_to_servers(results)

        return servers

    def get_country_by_id(self, country_id, verbose=False):
        sql_query = DAL.QUERY_GET_COUNTRY_BY_ID.format(self.schema_media,
                                                       country_id)

        if verbose:
            uc.print_and_logging('get_country_by_id:\n{}'.format(country_id))

        results = self.engine.execute(sql_query).fetchall()
        country = self.sql_results_to_countries(results)[0]

        return country

    def get_vehicles_by_server_id(self, server_id):
        sql_sp = DAL.QUERY_SP_GetVehicleByServerID.format(self.schema_detection, int(server_id))

        results = self.engine.execute(sql_sp).fetchall()
        vehicles = self.sql_results_to_vehicles(results)

        return vehicles

    @staticmethod
    def get_part_of_the_insertion_values(items):
        template_first_values = '\n\tVALUES ({}),'
        template_values = '\n\t\t   ({}),'
        part_of_the_insertion_values = ''
        if not isinstance(items, list):
            items = [items]

        for index, item in enumerate(items):
            if not hasattr(item, 'to_values_insert_sql') or not hasattr(item.to_values_insert_sql, '__call__'):
                raise ValueError('Todos los items pasado como parametro a get_part_of_the_insertion_values '
                                 'deben tener la funcion to_values_insert_sql.'
                                 '\nEl item {}:\n\t{}.\nNo la tiene'.format(index, item))
            part_of_the_insertion_values += template_first_values.format(item.to_values_insert_sql()) \
                if index == 0 else template_values.format(item.to_values_insert_sql())

        part_of_the_insertion_values = part_of_the_insertion_values[:-1]

        return part_of_the_insertion_values

    def insert_report_csv_process_advance(self, report: Union[CSVProcessAdvance, List[CSVProcessAdvance]],
                                          verbose=False):
        if len(report) > 0:
            sql_insert = DAL.QUERY_INSERT_REPORT_CSV_PROCESS_ADVANCE.format(
                self.schema_report,
                self.get_part_of_the_insertion_values(report)
            )

            if verbose:
                uc.print_and_logging('insert_report_csv_process_advance:\n{}'.format(sql_insert))

            self.engine.execute(sql_insert)

        else:
            uc.print_and_logging('No se realizo ninguna insercion. La lista esta vacia')

    def is_csv_already_processed(self, csv_path, verbose=False) -> bool:
        sql_query = DAL.QUERY_IS_CSV_ALREADY_PROCESSED.format(self.schema_detection,
                                                              csv_path)
        if verbose:
            uc.print_and_logging('is_csv_already_processed:\n{}'.format(sql_query))

        is_already_processed = bool(int(self.engine.execute(sql_query).fetchall()[0].IsCSVAlreadyProcessed))

        return is_already_processed

    # def is_csv_already_processed_or_being_processing(self, csv_path, verbose=False) -> bool:
    #     sql_query = DAL.QUERY_IS_CSV_ALREADY_PROCESSED_OR_BEING_PROCESSING.format(self.schema_detection, csv_path)
    #
    #     if verbose:
    #         uc.print_and_logging('is_csv_already_processed_or_being_processing:\n{}'.format(sql_query))
    #
    #     is_already_processed_or_being_processing = bool(
    #         int(self.engine.execute(sql_query).fetchall()[0].IsCSVAlreadyProcessed)
    #     )
    #
    #     return is_already_processed_or_being_processing

    def is_csv_already_processed_or_being_processing(self, csv_path, verbose=False) -> bool:
        sql_sp = DAL.QUERY_SP_IsCSVAlreadyProcessedOrBeingProcessing.format(self.schema_detection, csv_path)

        if verbose:
            uc.print_and_logging('is_csv_already_processed_or_being_processing:\n{}'.format(sql_sp))

        is_csv_already_processed_or_being_processing = bool(
            int(self.engine.execute(sql_sp).fetchall()[0].IsCSVAlreadyProcessedOrBeingProcessing)
        )

        return is_csv_already_processed_or_being_processing

    def get_intervals_validation_rule(self, since, client_id, country_id, verbose=False):
        sql_str_execute_since = since.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_sp_exec = DAL.QUERY_SP_GetIntervalsValidationRule.format(self.schema_detection,
                                                                     sql_str_execute_since,
                                                                     client_id,
                                                                     country_id)

        if verbose:
            uc.print_and_logging('get_intervals_validation_rule:\n{}'.format(sql_sp_exec))

        intervals_validation_rule = self.sql_result_to_intervals_validation_rule(
            self.engine.execute(sql_sp_exec).fetchone()
        )

        return intervals_validation_rule

    def get_intervals_validation_rule_by_id(self, id, verbose=False):
        sql_select = DAL.QUERY_GET_INTERVALS_VALIDATION_RULE_BY_ID.format(self.schema_detection,
                                                                          id)
        if verbose:
            uc.print_and_logging('get_intervals_validation_rule_by_id:\n{}'.format(sql_select))

        intervals_validation_rule = self.sql_result_to_intervals_validation_rule(
            self.engine.execute(sql_select).fetchone()
        )

        return intervals_validation_rule

    def insert_intervals(self, intervals, verbose=False):
        sql_insert = DAL.QUERY_INSERT_INTERVALS.format(self.schema_detection,
                                                       self.get_part_of_the_insertion_values(intervals))

        if verbose:
            uc.print_and_logging('insert_intervals:\n{}'.format(sql_insert))

        self.engine.execute(sql_insert)

    def insert_history_csv_processed(self, path, client_id, vehicle_id, source_signal_id, server_id,
                                     process_interval, process_ocr, process_video_intervals, verbose=False):

        _datetime = datetime.now()
        hcp = HistoryCSVProcessed(date=_datetime,
                                  path=path,
                                  client_id=client_id,
                                  vehicle_id=vehicle_id,
                                  source_signal_id=source_signal_id,
                                  server_id=server_id,
                                  process_interval=process_interval,
                                  process_ocr=process_ocr,
                                  process_video_intervals=process_video_intervals)

        sql_insert = DAL.QUERY_INSERT_HISTORY_CSV_PROCESSED.format(self.schema_detection, hcp.to_csv())
        if verbose:
            uc.print_and_logging('insert_history_csv_processed:\n{}'.format(sql_insert))

        try:
            self.engine.execute(sql_insert)
        except Exception as e:
            raise ValueError('[write_blank_write_history_csv_processed_for_all_clients] Sucedio un error:{}'
                             '\n\tpath_csv: {}'
                             '\n\tvehicle_id: {}'
                             '\n\tsource_signal_id: {}'
                             '\n\tserver_id: {}'
                             '\n\tclient_id: {}'.format(e,
                                                        path,
                                                        vehicle_id,
                                                        source_signal_id,
                                                        server_id,
                                                        client_id))

    # scp -r 10.228.69.113:/home/enzoscocco/Documents/object_detection/data_detection/vh14/2019/04/29/07
    # /home/enzoscocco/Documents/object_detection/data_detection/vh14/2019/04/29
    @staticmethod
    def get_files_scp_recursive(password, path_remote_files: str, path_output, create_path_output=False):

        if create_path_output:
            uc.create_folders(path_output, path_base='/')

        scp_args = ['sshpass', '-p', password, 'scp', '-r', path_remote_files, path_output]
        scp_process = subprocess.run(args=scp_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if scp_process.returncode != 0:
            scp_process_stderr = scp_process.stderr.decode('utf-8')
            logging.error('Ocurrio un error durante la ejecucion de :{}'
                          '\nMensaje:'
                          '\n{}'
                          '\nIntentando ahora con otra configuracion'
                          .format(' '.join([str(arg) for arg in scp_args]),
                                  scp_process_stderr))
        else:
            uc.print_and_logging('Obtencion recursiva de archivos se ejecuto correctamente:\n{}'
                                 .format(' '.join([str(arg) for arg in scp_args])))

    def get_history_csv_already_processed(self, verbose=False) -> List[HistoryCSVProcessed]:
        sql_select = DAL.QUERY_CSV_ALREADY_PROCESSED.format(self.schema_detection)

        if verbose:
            uc.print_and_logging('get_csv_already_processed:\n{}'.format(sql_select))

        results = self.engine.execute(sql_select).fetchall()
        history_csv_already_processed = self.sql_results_to_history_csv_processed(results)

        return history_csv_already_processed

    def get_history_csv_already_processed_by_server_id(self, server_id, verbose=False) -> List[HistoryCSVProcessed]:
        sql_select = DAL.QUERY_CSV_ALREADY_PROCESSED_BY_SERVER_ID.format(self.schema_detection, server_id)

        if verbose:
            uc.print_and_logging('get_history_csv_already_processed_by_server_id:\n{}'.format(sql_select))

        results = self.engine.execute(sql_select).fetchall()
        history_csv_already_processed = self.sql_results_to_history_csv_processed(results)

        return history_csv_already_processed

    def get_items_real_time_by_interval_id(self, interval_id, verbose=False):
        sql_select = DAL.QUERY_GET_ITEMS_REAL_TIME_BY_INTERVALS_ID.format(self.schema_detection,
                                                                          self.schema_detection,
                                                                          interval_id)

        if verbose:
            uc.print_and_logging('get_items_real_time_by_interval_id:\n{}'.format(sql_select))

        results = self.engine.execute(sql_select).fetchall()
        items_real_time = self.sql_results_to_item_real_time(results)

        return items_real_time
    
    def get_server_by_ip(self, ip, verbose=False):
        sql_select = DAL.QUERY_GET_SERVER_BY_IP.format(self.schema_detection, ip)
        
        if verbose:
            uc.print_and_logging('get_server_by_ip:\n{}'.format(sql_select))
            
        server = DAL.sql_result_to_server(self.engine.execute(sql_select).fetchone())

        return server

    def insert_stat_process_detections(self, stat_process, verbose=True):
        sql_insert = DAL.QUERY_INSERT_STAT_PROCESS_DETECTIONS_TO_INTERVALS.format(
            self.schema_report,
            self.get_part_of_the_insertion_values([stat_process])
        )

        if verbose:
            uc.print_and_logging('insert_stat_process_detections:\n{}'.format(sql_insert))

        self.engine.execute(sql_insert)

    def delete_history_csv_processed_by_error_process(self, csv_path, verbose):
        sql_delete = DAL.QUERY_DELETE_HISTORY_CSV_PROCESSED_BY_ERROR_PROCESS.format(self.schema_detection,
                                                                                    csv_path)

        if verbose:
            uc.print_and_logging('delete_history_csv_processed_by_error_process:\n{}'.format(sql_delete))

        self.engine.execute(sql_delete)

    def update_csv_being_processing_by_server(self, csv_path, server_id, verbose=True):
        sql_update = DAL.QUERY_UPDATE_CSV_BEING_PROCESSED_BY_SERVER.format(self.schema_detection, csv_path, server_id)

        if verbose:
            uc.print_and_logging('update_csv_being_processing_by_server:\n{}'.format(sql_update))

        self.engine.execute(sql_update)

    def clear_csv_being_processing_by_server(self, server_id, verbose=True):
        self.update_csv_being_processing_by_server('clear_at_server_id_{}'.format(server_id), server_id, verbose)

    def update_interval_reviewed_and_operator_filter_by_video_name(self, reviewed, operator_filter, path_video,
                                                                   verbose=False):
        query_update = DAL.QUERY_UPDATE_INTERVAL_REVIEWED_OPERATOR_FILTER_BY_VIDEO_NAME.format(
            self.schema_detection,
            reviewed,
            operator_filter,
            path_video
        )

        if verbose:
            uc.print_and_logging('update_interval_reviewed_and_operator_filter_by_path_video:\n{}'.format(query_update))

        self.engine.execute(query_update)
