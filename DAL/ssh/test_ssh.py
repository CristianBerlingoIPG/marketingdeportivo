from zipfile import ZipFile

from DAL.ssh.ssh_util import SshUtil, ZipValuesIfExists
import utils_cris as uc

server_id = '10.228.69.54'
username = 'enzoscocco'
password = 'Mematas01'

ssh = SshUtil(server_id, username, password)

# print(ssh.execute_command(['pwd && cd /home/enzoscocco/Documents/object_detection/data_detection && pwd']))

path_input = '/home/enzoscocco/Documents/object_detection/data_detection/vh17/2019/04/08/11/'
path_output = 'images.zip'


# primer intervalo
st_creation_zip = uc.print_start_time('Inicio de creacion de del primer intervalo: {}')
ssh.zip(path_input, path_output, '20190408_h11m29s[3][3,8]*.jpg', verbose=True, if_exists=ZipValuesIfExists.REMOVE)
st_end_zip = uc.print_end_elapsed_time(st_creation_zip,
                                       message='Finalizacion creacion de zip: End time: {} Elapsed time: {}')

st_download_zip = uc.print_start_time('Inicio de descarga de zip: {}')
ssh.get_file_scp(path_input + path_output, '/home/enzoscocco/Desktop/zips/01' + path_output)
uc.print_end_elapsed_time(st_download_zip, message='Finalizacion descarga de zip: End time: {} Elapsed time: {}\n\n')


# segundo intervalo 20190408_h11m32s2
st_creation_zip = uc.print_start_time('Inicio de creacion de zip del segundo intervalo: {}')
ssh.zip(path_input, path_output, '20190408_h11m32s[2,5][5,6,0]*.jpg', verbose=True)
uc.print_end_elapsed_time(st_creation_zip, message='Finalizacion creacion de zip: End time: {} Elapsed time: {}')

st_download_zip = uc.print_start_time('Inicio de descarga de zip del segundo intervalo: {}')
ssh.get_file_scp(path_input + path_output, '/home/enzoscocco/Desktop/zips/02' + path_output)
uc.print_end_elapsed_time(st_download_zip, message='Finalizacion descarga de zip: End time: {} Elapsed time: {}')

zip_ref = ZipFile('/home/enzoscocco/Desktop/zips/02' + path_output, 'r')
zip_ref.extractall('/home/enzoscocco/Desktop/zips')
zip_ref.close()
