import os
import re
import sys
import socket
import logging
import paramiko

from typing import List, Union, NamedTuple
from collections import namedtuple

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# from DAL.ssh import ssh_util_conf as conf_file
import DAL.ssh.scp as scp
import utils_cris as uc


class ZipValuesIfExists:
    ADDING = 'ADDING'
    REMOVE = 'REMOVE'


class SshUtil:
    """Class to connect to remote server"""

    def __init__(self, host, username, password, timeout=10.0, public_key=None, port=22):
        self.host = host
        self.username = username
        self.password = password
        self.timeout = timeout
        self.public_key = public_key
        self.port = port
        self.ssh_output = None
        self.ssh_error = None
        self.client = None
        self.client_scp: scp.SCPClient = None

    def connect(self):
        """Login to the remote server"""
        try:
            # Paramiko.SSHClient can be used to make connections to the remote server and transfer files
            self.client = paramiko.SSHClient()
            # Parsing an instance of the AutoAddPolicy to set_missing_host_key_policy() changes it to allow any host.
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            # Connect to the server
            if self.password == '':
                self.public_key = paramiko.RSAKey.from_private_key_file(self.public_key)
                self.client.connect(hostname=self.host, port=self.port, username=self.username, pkey=self.public_key,
                                    timeout=self.timeout, allow_agent=False, look_for_keys=False)
            else:
                self.client.connect(hostname=self.host, port=self.port, username=self.username, password=self.password,
                                    timeout=self.timeout, allow_agent=False, look_for_keys=False)
        except paramiko.AuthenticationException:
            print("Authentication failed, please verify your credentials")
            result_flag = False
        except paramiko.SSHException as sshException:
            print("Could not establish SSH connection: %s" % sshException)
            result_flag = False
        except socket.timeout as e:
            print("Connection timed out")
            result_flag = False
        except Exception as e:
            print('\nException in connecting to the server')
            print('PYTHON SAYS:', e)
            result_flag = False
            self.client.close()
        else:
            result_flag = True

        return result_flag

    def execute_command(self, commands, timeout=60, verbose=False):
        """Execute a command on the remote host.Return a tuple containing
        an integer status and a two strings, the first containing stdout
        and the second containing stderr from the command."""
        self.ssh_output = None
        self.ssh_error = None
        result_flag = True
        try:
            if self.connect():
                for command in commands:
                    # print("Executing command --> {}".format(command))
                    stdin, stdout, stderr = self.client.exec_command(command, timeout=timeout)
                    self.ssh_output = stdout.read() if self.ssh_output is None else self.ssh_output + stdout.read()
                    self.ssh_error = stderr.read() if self.ssh_error is None else self.ssh_error + stderr.read()
                    if self.ssh_error:
                        if verbose:
                            uc.print_and_logging("Problem occurred while running command:{} "
                                                 "The error is {}".format(command,
                                                                          self.ssh_error))
                        result_flag = False
                    # else:
                        # print("Command execution completed successfully", command)
                        # print('Output {}'.format(ssh.ssh_output))
                    # self.client.close()
                self.client.close()
            else:
                if verbose:
                    uc.print_and_logging("Could not establish SSH connection")
                result_flag = False
        except socket.timeout as e:
            if verbose:
                uc.print_and_logging("Command timed out.{}".format(command))
            self.client.close()
            result_flag = False
        except paramiko.SSHException:
            if verbose:
                uc.print_and_logging("Failed to execute the command!{}".format(command))
            self.client.close()
            result_flag = False

        return result_flag, commands, self.ssh_output, self.ssh_error

    def upload_file_sftp(self, uploadlocalfilepath, upload_remote_file_path):
        """This method uploads the file to remote server"""
        result_flag = True
        try:
            if self.connect():
                ftp_client = self.client.open_sftp()
                ftp_client.put(uploadlocalfilepath, upload_remote_file_path)
                ftp_client.close()
                self.client.close()
            else:
                print("Could not establish SSH connection")
                result_flag = False
        except Exception as e:
            print('\nUnable to upload the file to the remote server', upload_remote_file_path)
            print('PYTHON SAYS:', e)
            result_flag = False
            ftp_client.close()
            self.client.close()

        return result_flag

    def download_file_sftp(self, download_remote_file_path, download_local_file_path):
        """This method downloads the file from remote server"""
        result_flag = True
        try:
            if self.connect():
                ftp_client = self.client.open_sftp()
                ftp_client.get(download_remote_file_path, download_local_file_path)
                ftp_client.close()
                self.client.close()
            else:
                print("Could not establish SSH connection")
                result_flag = False
        except Exception as e:
            print('\nUnable to download the file from the remote server', download_remote_file_path)
            print('PYTHON SAYS:', e)
            result_flag = False
            ftp_client.close()
            self.client.close()

        return result_flag

    def is_client_connected(self):
        is_client_connected = False
        if self.client is not None:
            transport = self.client.get_transport()
            if transport is not None:
                is_client_connected = transport.is_active()

        return is_client_connected

    @staticmethod
    def create_dir(dir_name):
        _path_output = '/'
        folders = [f for f in dir_name.split('/') if f != '']
        for folder in folders:
            _path_output = os.path.join(_path_output, folder)
            if not os.path.isdir(_path_output):
                os.mkdir(_path_output)

    def create_client_scp(self):
        if not self.is_client_connected():
            self.connect()

        if self.client_scp is None:
            self.client_scp = scp.SCPClient(self.client.get_transport())

    def get_file_scp(self, path_remote_file, path_output, create_path_output=False):
        dir_name = os.path.dirname(path_output)
        if not os.path.isdir(dir_name):
            if create_path_output:
                self.create_dir(dir_name)
            else:
                raise ValueError('La ruta de salida no existe')

        if self.client_scp is None:
            self.create_client_scp()

        self.client_scp.get(path_remote_file, path_output)

    def get_files_scp_old(self, path_remote_files: List[str], path_output=os.getcwd(), create_path_output=False,
                      copy_folders_structure=False):
        n_files = len(path_remote_files)
        if not copy_folders_structure:
            uc.print_and_logging('Cantidad de archivos a descargar {} en la ruta {}'.format(n_files, path_output))
        else:
            uc.print_and_logging('Cantidad de archivos a descargar {} en en rutas equivalentes '
                                 'del sistema de archivos local'.format(n_files))
        for index, path_remote in enumerate(path_remote_files):
            if (index % 100 == 0 and index != 0) or index == n_files - 2:
                uc.print_and_logging('Descargados {}/{}'.format(index, n_files))
            if copy_folders_structure:
                self.get_file_scp(path_remote, path_remote, create_path_output=create_path_output)
            else:
                self.get_file_scp(path_remote, path_output, create_path_output=create_path_output)

    @staticmethod
    def get_dirs_remote_files(path_remote_files):
        dirs_remote_files = []
        for path in path_remote_files:
            dir_name = os.path.dirname(path)
            if dir_name not in dirs_remote_files:
                dirs_remote_files.append(dir_name)

        return dirs_remote_files

    def get_files_scp(self, path_remote_files: List[str], path_output=os.getcwd(), create_path_output=False,
                      copy_folders_structure=False):
        n_files = len(path_remote_files)
        # dirnames_remote_files = self.get_dirs_remote_files(path_remote_files)

        if not copy_folders_structure:
            uc.print_and_logging('Cantidad de archivos a descargar {} en la ruta {}'.format(n_files, path_output))
        else:
            uc.print_and_logging('Cantidad de archivos a descargar {} en en rutas equivalentes '
                                 'del sistema de archivos local'.format(n_files))
        for index, path_remote in enumerate(path_remote_files):
            if (index % 100 == 0 and index != 0) or index == n_files - 2:
                uc.print_and_logging('\tDescargados {}/{}'.format(index, n_files))
            if copy_folders_structure:
                self.get_file_scp(path_remote, path_remote, create_path_output=create_path_output)
            else:
                self.get_file_scp(path_remote, path_output, create_path_output=create_path_output)

    def get_cwd(self):
        command = 'pwd'
        output = self.execute_command([command])[2]

        return output.decode()

    def file_exists(self, path_file):
        file_exists = False
        ls = self.get_ls(path_file)
        if len(ls) > 0:
            file_exists = path_file == ls[0]

        return file_exists

    def rm_file(self, path):
        self.execute_command(['rm {}'.format(path)])

    def rm_folder(self, folder, verbose=False, check_if_exists=False):

        # if check_if_exists:
        #

        command = 'rm -r {}'.format(folder)
        result_status, _, ssh_output, ssh_error = self.execute_command([command])

        if result_status:
            if verbose:
                ssh_output_decode = ssh_output.decode() if ssh_output is not None else 'ssh no devolvio un output'
                logging.info(ssh_output_decode)
        else:
            if ssh_error is not None:
                ssh_error_decode = ssh_error.decode()
            else:
                ssh_error_decode = 'SSH no devolvio mensaje de error'
            exception_message = 'Ocurrio una execpcion durante rm_folder de SSH ({}):\n{}'.format(command,
                                                                                                  ssh_error_decode)
            logging.exception(exception_message)
            raise ValueError(exception_message)

    def zip(self, path_input, path_output, pattern_input, if_exists='ADDING', verbose=False):
        path_zip = None
        if if_exists == ZipValuesIfExists.REMOVE:
            path_output_to_remove = path_output if os.path.dirname(path_output) != '' \
                else os.path.join(path_input, path_output)
            if self.file_exists(path_output_to_remove):
                if verbose:
                    logging.info('Eliminado archivo {} del server {}'.format(path_output_to_remove, self.host))
                self.rm_file(path_output_to_remove)

        command = 'cd {} && zip {} {}'.format(path_input, path_output, pattern_input)
        result_status, _, ssh_output, ssh_error = self.execute_command([command])

        # if result_status or self.file_exists(os.path.join(path_input, path_output)):
        if result_status:
            path_zip = os.path.join(path_input, path_output)
            if verbose:
                logging.info(ssh_output.decode())
                print(ssh_output.decode())
        else:
            uc.print_and_logging('result_status {} file_exists {}'.format(
                result_status,
                self.file_exists(os.path.join(path_input, path_output)))
            )
            if ssh_error is not None:
                ssh_error_decode = ssh_error.decode()
            else:
                ssh_error_decode = 'SSH no devolvio mensaje de error'
            exception_message = 'Ocurrio una execpcion durante zip de SSH ({}):\n{}'.format(command, ssh_error_decode)
            logging.exception(exception_message)
            raise ValueError(exception_message)

        return path_zip

    # grep jpg to_zip.txt | zip archive.zip -@
    def zip_from_file_list(self, path_input, name_list, path_output, verbose=True):
        path_zip = None
        command = 'cd {} && grep jpg {} | zip {} -@'.format(path_input, name_list, path_output)
        path_output_to_remove = path_output if os.path.dirname(path_output) != '' \
            else os.path.join(path_input, path_output)
        if verbose:
            uc.print_and_logging('path_output_to_remove: {}'.format(path_output_to_remove))
        if self.file_exists(path_output_to_remove):
            if verbose:
                uc.print_and_logging('Eliminado archivo {} del server {}'.format(path_output_to_remove, self.host))
            self.rm_file(path_output_to_remove)
        else:
            if verbose:
                uc.print_and_logging('El archivo no existia previamente')

        if verbose:
            uc.print_and_logging('zip_from_file_list command: {}'.format(command))

        result_status, _, ssh_output, ssh_error = self.execute_command([command])
        if result_status:
            path_zip = os.path.join(path_input, path_output)
            if verbose:
                uc.print_and_logging(ssh_output.decode())
        else:
            uc.print_and_logging('result_status {} file_exists{}'.format(
                result_status,
                self.file_exists(os.path.join(path_input, path_output)))
            )
            if ssh_error is not None:
                ssh_error_decode = ssh_error.decode()
            else:
                ssh_error_decode = 'SSH no devolvio mensaje de error'
            exception_message = 'Ocurrio una excepcion ' \
                                'durante zip_from_file_list de SSH ({}):\n{}'.format(command, ssh_error_decode)
            uc.print_and_logging(exception_message, logging_type=uc.LoggingType.EXCEPTION)
            raise ValueError(exception_message)

        return path_zip

    def get_ls(self, path_remote) -> List[str]:
        command = 'ls {}'.format(path_remote)
        output = self.execute_command([command])[2]

        ls = [outp for outp in output.decode().split('\n') if outp != '']

        return ls

    def get_find(self, path_remote, iname) -> List[str]:
        find = []
        command = 'find {} -iname {}'.format(path_remote, iname)
        result_flag, _, ssh_output, ssh_error = self.execute_command([command])

        if result_flag:
            find = [outp for outp in ssh_output.decode().split('\n') if outp != '']
        else:
            uc.print_and_logging('No se pudo ejecutar el comando find via SSH al server {}'.format(self.host))

        return find

    def write_to_file(self, text, path_file, append=True, verbose=False):
        operator_to_file = '>>' if append else '>'
        command = 'echo "{}" {} {}'.format(text, operator_to_file, path_file)

        if verbose:
            uc.print_and_logging('command: {}'.format(command))

        result_status, _, ssh_output, ssh_error = self.execute_command([command])

        if result_status:
            if verbose:
                uc.print_and_logging(ssh_output.decode())
        else:
            uc.print_and_logging('result_status {} file_exists {}'.format(
                result_status,
                self.file_exists(path_file))
            )
            if ssh_error is not None:
                ssh_error_decode = ssh_error.decode()
            else:
                ssh_error_decode = 'SSH no devolvio mensaje de error'
            exception_message = 'Ocurrio una execpcion durante la escritura del archivo {} ' \
                                'via SSH ({}):\n{}'.format(path_file, command, ssh_error_decode)
            uc.print_and_logging(exception_message, logging_type=uc.LoggingType.EXCEPTION)
            raise ValueError(exception_message)

    def execute_df(self, options: str = '-h', mounted_on: Union[str, List[str]] = None, verbose=False) \
            -> List[namedtuple]:
        """df - report file system disk space usage"""
        command = 'df {}'.format(options)
        # command += '| grep -w {}'.format(mounted_on) if mounted_on is not None else ''

        if verbose:
            uc.print_and_logging('command: {}'.format(command))

        result_status, _, ssh_output, ssh_error = self.execute_command([command])

        df_output = ssh_output.decode().split('\n')

        attr_df_result = [h.strip().replace(' ', '_').replace('%', '').replace('\n', '').lower()
                          for h in re.findall('[A-Z][^A-Z]*', df_output[0])]
        # DfResult = namedtuple('DfResult', attr_df_result)
        DfResult = namedtuple('DfResult', attr_df_result)
        df_result = [DfResult(*[v for v in dfo.split(' ') if v != '']) for dfo in df_output[1:] if dfo != '']

        if mounted_on is not None:
            mounted_on = [mounted_on] if type(mounted_on) == str else mounted_on
            df_result = [dfr for dfr in df_result if dfr.mounted_on in mounted_on]

        if result_status:
            if verbose:
                uc.print_and_logging(df_output)
        else:
            if ssh_error is not None:
                ssh_error_decode = ssh_error.decode()
            else:
                ssh_error_decode = 'SSH no devolvio mensaje de error'
            exception_message = 'Ocurrio una execpcion durante la ejecucion de df ' \
                                'via SSH ({}):\n{}'.format(command, ssh_error_decode)
            uc.print_and_logging(exception_message, logging_type=uc.LoggingType.EXCEPTION)
            raise ValueError(exception_message)

        return df_result

# ---USAGE EXAMPLES
if __name__ == '__main__':
    print("Start of %s" % __file__)

    # Initialize the ssh object
    ssh = SshUtil('10.228.69.54', 'enzoscocco', 'Mematas01')

    # Sample code to execute commands
    # if ssh.execute_command(['hostname']) is True:
    #     print("Commands executed successfully\n")
    # else:
    #     print("Unable to execute the commands")

    # correct_execute, command, result = ssh.execute_command(['hostname'])
    # print(command, ':', result)

    # output = ssh.get_ls('/home/enzoscocco/Documents/object_detection/data_detection/vh17/2019/03/25')
    # output = ssh.get_find('/home/enzoscocco/Documents/object_detection/data_detection/vh17/2019/',
    #                       '20[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]__[0-9]*__[0-9]*__.csv')
    # print(len(output), sorted(output))

    # ssh.get_files_scp(output, copy_folders_structure=True, create_path_output=True)

    # download_remote_file_path = '/home/enzoscocco/Documents/object_detection/data_detection/vh17/2019/03/22/08/scp.csv'
    # upload_remote_file_path = '/home/enzoscocco/Desktop/sftp.csv'
    # if ssh.download_file(download_remote_file_path, upload_remote_file_path):
    #     print('El archivo se descargo correctamente')

    # ssh.download_file()

    df = ssh.execute_df(mounted_on=['/home', '/run', '/dev'])
    print('\n'.join([str(d) for d in df]))

    """
    #Sample code to upload a file to the server
    if ssh_obj.upload_file(ssh_obj.uploadlocalfilepath,ssh_obj.uploadremotefilepath) is True:
        print "File uploaded successfully", ssh_obj.uploadremotefilepath
    else:
        print  "Failed to upload the file"

    #Sample code to download a file from the server
    if ssh_obj.download_file(ssh_obj.downloadremotefilepath,ssh_obj.downloadlocalfilepath) is True:
        print "File downloaded successfully", ssh_obj.downloadlocalfilepath
    else:
        print  "Failed to download the file"
    """
