from DAL.model.logo import Logo
from DAL.model.source_signal import SourceSignal
from DAL.model.item_real_time import ItemRealTime
from DAL.model.intervals_validation_rule import IntervalsValidationRule

import sqlalchemy

from urllib.parse import quote_plus
from datetime import datetime, timedelta


class ImplementationIntervalsValidatonRule:
    intervals_validation_rule: IntervalsValidationRule
    ENGINE_CONNECTION_STRING = 'mssql+pyodbc:///?odbc_connect={}'
    FORMAT_DATE = '%Y-%m-%d %H:%M:%S.%f'
    SUBTRACT_CHARACTER_DATETIME_SQL = -3

    def __init__(self, client_id, country_id, connection_string,
                 execute_since=(datetime.now() - timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0),
                 execute_until=datetime.now().replace(hour=0, minute=0, second=0, microsecond=0),
                 schema='Detection', table_intervals='Interval',
                 store_procedure_intervals_validation_rule='GetIntervalsValidationRule',
                 schema_source_signal='Detection', table_source_signal='SourceSignal',
                 store_procedure_items_real_time='GetItemsRealTime', store_procedure_logos='GetLogosByClient',
                 schema_history='Detection', table_history='HistoryUpdates'):
        self.client_id = client_id
        self.country_id = country_id
        self.connection_string = connection_string
        self.execute_since = execute_since
        self.execute_until = execute_until
        if self.execute_until <= self.execute_since:
            raise ValueError('el parametro execute_until no puede ser igual o menor a execute_until')
        self.schema = schema
        self.table_intervals = table_intervals
        self.store_procedure_intervals_validation_rule = store_procedure_intervals_validation_rule
        self.store_procedure_items_real_time = store_procedure_items_real_time
        self.store_procedure_logos = store_procedure_logos
        self.schema_source_signal = schema_source_signal
        self.table_source_signal = table_source_signal
        self.engine_connection_string = ImplementationIntervalsValidatonRule \
            .ENGINE_CONNECTION_STRING.format(quote_plus(self.connection_string))
        self.engine = sqlalchemy.create_engine(self.engine_connection_string)
        self.schema_history = schema_history
        self.table_history = table_history
        self.intervals_validation_rule = None
        self.load_intervals_validation_rule()
        self.source_signals = []
        self.load_source_signals()
        self.items_real_time = []
        self.load_items_real_time()
        self.logos = []
        self.load_logos()

    def load_intervals_validation_rule(self):
        sql_str_execute_since = self.execute_since.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_command = "EXECUTE [{}].[{}] '{}', {}, {}".format(self.schema,
                                                              self.store_procedure_intervals_validation_rule,
                                                              sql_str_execute_since,
                                                              self.client_id,
                                                              self.country_id)

        ivr = self.engine.execute(sql_command).first()
        if ivr is not None:
            self.intervals_validation_rule = IntervalsValidationRule(ivr.ID,
                                                                     ivr.ClientID,
                                                                     ivr.CountryID,
                                                                     ivr.Since,
                                                                     ivr.MinimumExpositionTime,
                                                                     ivr.MinimumPercentFrames,
                                                                     ivr.MinimumPercentMatchDetection,
                                                                     ivr.Created)
        else:
            raise ValueError('No existe ninguna regla de validación de intervalos para estos parametros')

    def load_source_signals(self):
        sql_command = 'SELECT * FROM [{}].[{}]'.format(self.schema_source_signal, self.table_source_signal)

        self.source_signals = [SourceSignal(ss.ID,
                                            ss.Name,
                                            ss.Observations,
                                            ss.Active,
                                            ss.Path,
                                            ss.FPS,
                                            ss.Width,
                                            ss.Height)
                               for ss in self.engine.execute(sql_command).fetchall()]

    def load_items_real_time(self):
        sql_str_execute_since = self.execute_since.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_str_execute_until = self.execute_until.strftime(self.FORMAT_DATE)[:self.SUBTRACT_CHARACTER_DATETIME_SQL]
        sql_command = "EXECUTE [{}].[{}] '{}', '{}', {}, {}".format(self.schema,
                                                                    self.store_procedure_items_real_time,
                                                                    sql_str_execute_since,
                                                                    sql_str_execute_until,
                                                                    self.client_id,
                                                                    self.country_id)

        self.items_real_time = [ItemRealTime(irt.VehicleID,
                                             irt.SourceSignalID,
                                             irt.LogoID,
                                             irt.Date,
                                             irt.Path,
                                             irt.Score,
                                             irt.BoxYMin,
                                             irt.BoxXMin,
                                             irt.BoxYMax,
                                             irt.BoxXMax,
                                             irt.PathFileRawData,
                                             id=irt.ID) for irt in self.engine.execute(sql_command).fetchall()]

    def load_logos(self):
        sql_command = 'EXECUTE [{}].[{}] {}'.format(self.schema, self.store_procedure_logos, self.client_id)

        self.logos = [Logo(l.Name,
                           l.Observations,
                           l.Path,
                           l.ProductID,
                           l.Active,
                           l.DisplayName,
                           id=l.ID) for l in self.engine.execute(sql_command).fetchall()]

    def get_valid_intervals(self):
        valid_intervals = self.intervals_validation_rule.get_valid_intervals(self.items_real_time,
                                                                             self.source_signals,
                                                                             self.logos)

        return valid_intervals

    def write_valid_intervals_to_sql(self):
        sql_command_insert = 'INSERT INTO [{}].[{}] VALUES ({})'.format(self.schema, self.table_intervals, '{}')
        valid_intervals = self.get_valid_intervals()
        for interval in valid_intervals:
            print(sql_command_insert.format(interval.to_values_insert_sql(with_id=False)))
            self.engine.execute(sql_command_insert.format(interval.to_values_insert_sql(with_id=False)))
