#!/bin/bash

# para evitar imprimir por terminal scriptname >/dev/null 2>&1

cd /home/enzoscocco/Marketing_Deportivo/models/research/object_detection

source ~/anaconda3/etc/profile.d/conda.sh
conda activate tfgputest

echo "conda activated enviroment:"
conda info -e

echo "launching python etl_intervals.py -cty 2 -m $1 -rm $2"

python etl_intervals.py -cty 2 -m $1 -rm $2