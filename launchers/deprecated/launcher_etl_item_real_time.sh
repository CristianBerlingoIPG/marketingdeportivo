#!/bin/bash

# para evitar imprimir por terminal scriptname >/dev/null 2>&1

cd /home/enzoscocco/Marketing_Deportivo/models/research/object_detection

source ~/anaconda3/etc/profile.d/conda.sh
conda activate tfgputest

echo "conda activated enviroment:"
conda info -e

echo "etl_item_real_time_bcp_intermediate_server.py -cty 2"

python etl_item_real_time_bcp_intermediate_server.py -cty 2