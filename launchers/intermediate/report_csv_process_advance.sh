#!/bin/bash

# para evitar imprimir por terminal scriptname >/dev/null 2>&1

cd /home/enzoscocco/Marketing_Deportivo/models/research/object_detection/report_scripts

source ~/anaconda3/etc/profile.d/conda.sh
conda activate tfgputest

echo "conda activated enviroment:"
conda info -e

echo "launching python generate_report_csv_process_advance.py -cty 2"

python generate_report_csv_process_advance.py -cty 2