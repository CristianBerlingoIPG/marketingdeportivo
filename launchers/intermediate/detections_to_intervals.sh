#!/usr/bin/env bash

cd /home/enzoscocco/Marketing_Deportivo/models/research/object_detection

source ~/anaconda3/etc/profile.d/conda.sh
conda activate tfgputest

echo "conda activated environment:"
conda info -e

if [[ -z $1 ]]
then
    echo "launching detections_to_intervals.py -aocr False -fn True"
    python detections_to_intervals.py -aocr False -fn True
else
    echo "launching detections_to_intervals.py -csi $1 -aocr False -fn True"
    python detections_to_intervals.py -csi $1 -aocr False -fn True
fi
