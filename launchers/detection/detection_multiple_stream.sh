#!/bin/bash

cd /home/enzoscocco/Marketing_Deportivo/models/research/object_detection

echo "Mematas01" | sudo -S -k systemctl stop mssql-server
echo ""

source ~/anaconda3/etc/profile.d/conda.sh
conda activate tfgputest

echo "conda activated enviroment:"
conda info -e

echo "launching python detection_multiple_stream.py --model_path inference_graphs/20190704_directv/frozen_inference_graph.pb -mst 0.4 -plm data/20190704_directv/label_map.pbtxt -pdf /home/enzoscocco/Documents/object_detection/data_detection -cta 1"

pwd
python detection_multiple_stream.py --model_path inference_graphs/20190704_directv/frozen_inference_graph.pb -mst 0.4 -plm data/20190704_directv/label_map.pbtxt -pdf /home/enzoscocco/Documents/object_detection/data_detection -cta 1
