import sys

sys.path.append("..")

import logging

logging.getLogger("tensorflow").setLevel(logging.WARNING)

import os
import cv2
import ast
import argparse
import numpy as np
import tensorflow as tf

from utils import label_map_util
# from utils import visualization_utils as vis_util

import utils_cris as uc
from utils_cris import LoggingType as lt

from datetime import datetime
from streams_data import servers
from logo_detection_persistence import LogoDetectionPersistence

parser = argparse.ArgumentParser()
parser.add_argument('--model_path',
                    help='Ruta del modelo',
                    required=True)
parser.add_argument('--device',
                    help='Dispositivo sobre cual ejecutar el procesamiento grafico. '
                         'Ejemplo: /device:GPU:0 para utilizar el primer dispositivo gpu.'
                         '         /cpu:0 para utilizar el CPU.'
                         'Mas informacion: https://www.tensorflow.org/guide/using_gpu.'
                         'Default: /device:GPU:0',
                    default='/device:GPU:0')
parser.add_argument('-pdf',
                    '--path_detected_frames',
                    help='Path donde se guardaran los frames con detecciones.',
                    required=True)
parser.add_argument('--show_input',
                    help='Boolean que define si se mostrara en una ventana el input.'
                         'Default: False.',
                    default='False')
parser.add_argument('--show_output',
                    help='Boolean que define si se mostrara en una ventana el output.'
                         'Default: False.',
                    default='False'),
parser.add_argument('-mst',
                    '--min_score_thresh',
                    help='Decimal que establece el porcentaje minimo de coincidencia para que una deteccion sea '
                         'mostrada. Default: 0.5',
                    default=0.5,
                    type=float)
parser.add_argument('-plm',
                    '--path_labelmap',
                    help='Path del labelmap',
                    required=True)
parser.add_argument('-fps',
                    '--frames_per_second',
                    help='Cantidad de frames por segundo que se desean procesar',
                    default=15)
parser.add_argument('-v',
                    '--verbose',
                    type=bool,
                    help='Valor boolean que define si se debe imprimir por pantalla o no. Por defecto en false.',
                    default=False)
parser.add_argument('-cta',
                    '--classes_to_avoid',
                    help='Lista, separada por coma y sin espacios, de los ids de las clases del modelo que se deben '
                         'evitar. Por ejemplo 22,33,45. POr defecto vacia.',
                    type=lambda cta: [int(c) for c in cta.split(',')],
                    default=[])

MAX_FRAMES_NONE = 200

args = parser.parse_args()

verbose = args.verbose
print('verbose:{}'.format(verbose))
# if not verbose:
#     sys.stdout = open(os.devnull, 'w')
#     os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
#     tf.logging.set_verbosity(tf.logging.ERROR)
# else:
#     os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2 '

larg = []
[larg.extend(a) for a in args._get_kwargs()]
logging_message_configs = '\nParameters:' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}\n'.format(*larg)

hostname = os.uname()[1]
if hostname not in servers.keys():
    raise ValueError('El servidor {} no se encuentra dentro de la '
                     'lista de servidores con señales asociados'.format(hostname))

streams_data = servers[hostname]

logging_message_configs += '\nHostname: {}'.format(hostname)

logging_message_configs += '\nStreams:' \
                           '\n\t{}\n'.format('\n\t'
                                             .join(['stream: {}  '
                                                    'source_signal_id: {}  '
                                                    'vehicle_id: {}  '
                                                    'vehicle: {}'.format(st_data['stream'],
                                                                         st_data['source_signal_id'],
                                                                         st_data['vehicle_id'],
                                                                         st_data['vehicle'])
                                                    for st_data in streams_data]))

model_path = args.model_path
device = args.device
path_detected_frames = args.path_detected_frames
show_input = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_input == 'True' else False
show_output = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_output == 'True' else False
min_score_thresh = args.min_score_thresh
path_labelmap = args.path_labelmap
frames_per_second = args.frames_per_second
classes_to_avoid = args.classes_to_avoid

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
ip = uc.get_ip()
template_general_log_path = 'logs/detection_multiple_stream/detection_multiple_stream__ip{}__{}.log'.format(ip, '{}')
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('.log', '_ERROR.log')
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################


# NUM_CLASSES = 2
# PATH_TO_LABELS = os.path.join('data/ipgmediabrands', 'label_map.pbtxt')
# label_map = label_map_util.load_labelmap('data/20190201_summit_ipgmediabrands/label_map.pbtxt')
label_map = label_map_util.load_labelmap(path_labelmap)
num_clases = len(label_map.item)
categories = label_map_util.convert_label_map_to_categories(label_map,
                                                            max_num_classes=num_clases,
                                                            use_display_name=True)

logging_message_configs += '\nnum_clases: {}' \
                           '\ncategories: \n\t{}\n'.format(num_clases,
                                                           '\n\t'.join([str(category) for category in categories]))

category_index = label_map_util.create_category_index(categories)

with tf.device(device):
    detection_graph = tf.Graph()
# config = tf.ConfigProto(
#     device_count={'GPU': 0},
#     log_device_placement=True
# )
config = tf.ConfigProto(
    log_device_placement=True
)
# config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.5

with detection_graph.as_default():
    od_graph_def = tf.GraphDef()

    with tf.gfile.GFile(model_path, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    # sess = tf.Session(graph=detection_graph)
    sess = tf.Session(graph=detection_graph, config=config)

image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

num_detections = detection_graph.get_tensor_by_name('num_detections:0')


def get_video_captures_and_detections_persistence(_streams):
    for _stream in _streams:
        _stream['capture'] = cv2.VideoCapture(_stream['stream'])
        _stream['detection_persistence'] = LogoDetectionPersistence(_stream['vehicle_id'],
                                                                    _stream['source_signal_id'],
                                                                    path_detected_frames)

    return _streams


streams_data = get_video_captures_and_detections_persistence(streams_data)
len_streams = len(streams_data)
nframe = 0
nframe_none = 0

fps = streams_data[0]['capture'].get(cv2.CAP_PROP_FPS)
width = streams_data[0]['capture'].get(cv2.CAP_PROP_FRAME_WIDTH)
height = streams_data[0]['capture'].get(cv2.CAP_PROP_FRAME_HEIGHT)
mod_fps = round(fps / frames_per_second)

logging_message_configs += '\nfps: {}' \
                           '\nwidth: {}' \
                           '\nheight: {}' \
                           '\nmod_fps: {}\n'.format(fps, width, height, mod_fps)

uc.print_and_logging(logging_message_configs)
uc.print_and_logging('comenzando ciclo de deteccion')
while all(video['capture'].isOpened() for video in streams_data):
    try:
        now = datetime.now()
        if now.minute % 20 == 0 and now.second == 0:
            uc.print_and_logging('Deteccion ejecutandose')

        datetime_log = uc.restart_daily_logging_file(datetime_log,
                                                     template_general_log_path,
                                                     initial_message=logging_message_configs)

        # frames = [video.read()[1] for video in videos]
        frames_data_stream = [(sd['capture'].read()[1], sd)
                              for sd in streams_data]
        nframe += 1
        if nframe % mod_fps == 0:
            for frame, stream_data in frames_data_stream:
                if frame is not None:
                    nframe_none = 0
                    color_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    frame_expanded = np.expand_dims(color_frame, axis=0)
                    date_detection = datetime.now()
                    (boxes, scores, classes, num) = sess.run(
                        [detection_boxes, detection_scores, detection_classes, num_detections],
                        feed_dict={image_tensor: frame_expanded})

                    zip_bcs = zip(np.squeeze(boxes), np.squeeze(classes).astype(np.int32), np.squeeze(scores))
                    boxes_classes_scores = [[], [], []]
                    for _boxes, _class, _scores in zip_bcs:
                        if _class not in classes_to_avoid:
                            boxes_classes_scores[0].append(_boxes)
                            boxes_classes_scores[1].append(_class)
                            boxes_classes_scores[2].append(_scores)

                    frame, items_real_time = stream_data['detection_persistence'] \
                        .write_data_detection(cv2,
                                              color_frame,
                                              np.asarray(boxes_classes_scores[0]),
                                              np.asarray(boxes_classes_scores[1]),
                                              np.asarray(boxes_classes_scores[2]),
                                              date_detection,
                                              min_score_thresh)

                    # frame = cv2.cvtColor(color_frame, cv2.COLOR_RGB2BGR)
                    # if show_input:
                    #     cv2.imshow(stream_path, frame)

                    # if cv2.waitKey(1) == ord('q'):
                    #     break

                else:
                    nframe_none += 1

    except KeyboardInterrupt as ki:
        uc.print_and_logging('Saliendo por KeyboardInterrupt')
        for stream_data in streams_data:
            uc.print_and_logging('Cerrando archivo de items real time del stream: {}'.format(stream_data))
            stream_data['detection_persistence'].close_writer()
        sys.exit()

    except Exception as e:
        uc.print_and_logging('Ocurrio una excepcion:\n', logging_type=lt.EXCEPTION)
        for stream_data in streams_data:
            uc.print_and_logging('Cerrando archivo de items real time del stream: {}'.format(stream_data))
            stream_data['detection_persistence'].close_writer()
    if not all(video['capture'].isOpened() for video in streams_data) or nframe_none >= MAX_FRAMES_NONE:
        closed_streams = [sd for sd in streams_data if not sd['capture'].isOpened()]
        uc.print_and_logging('Terminando ejecución del proceso:'
                             '\n\tclosed_streams:{}'
                             '\n\tnframe_none: {}'.format(closed_streams, nframe_none),
                             logging_type=lt.EXCEPTION)
        for stream_data in streams_data:
            uc.print_and_logging('Cerrando archivo de items real time del stream: {}'.format(stream_data))
            stream_data['detection_persistence'].close_writer()
        sys.exit()

# if not verbose:
#     sys.stdout = sys.__stdout__
