import os
import argparse
import xml.etree.ElementTree as et

parser = argparse.ArgumentParser()
parser.add_argument('--path_input',
                    help='Ruta donde estan los archivos XML a actualizar',
                    required=True)
parser.add_argument('--label_to_replace',
                    help='Etiqueta a reemplazar',
                    required=True)
parser.add_argument('--new_label',
                    help='Nueva etiqueta',
                    required=True)

args = parser.parse_args()
path_input = args.path_input
label_to_replace = args.label_to_replace
new_label = args.new_label
absolut_path_input = os.path.abspath(path_input)
current_folder = absolut_path_input.split('/')[-1]

print(args, '\n')
xml_files = [picture_file for picture_file in os.listdir(path_input) if '.xml' in picture_file]
for index, xml_file in enumerate(xml_files):
    is_modified = False
    tree = et.parse(path_input + '/' + xml_file)
    root = tree.getroot()

    objects = root.findall('./object')
    for obj in objects:
        if obj.find('./name').text == label_to_replace:
            obj.find('./name').text = new_label
            is_modified = True

    if is_modified:
        print('Archivo {} numero {} editado'.format(xml_file, index))
        tree.write(path_input + '/' + xml_file)

print('Archivos actualizados: {}\nProceso terminado.'.format(index))
