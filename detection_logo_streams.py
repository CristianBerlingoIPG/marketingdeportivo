import sys
sys.path.append("..")

# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import argparse
import ast


# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

import logging
import utils_cris
from datetime import datetime, timedelta


parser = argparse.ArgumentParser()
parser.add_argument('--stream_paths',
                    nargs='+',
                    help='Lista de paths de streams',
                    required=True)
parser.add_argument('--model_path',
                    help='Ruta del modelo',
                    required=True)
parser.add_argument('--device',
                    help='Dispositivo sobre cual ejecutar el procesamiento grafico. '
                         'Ejemplo: /device:GPU:0 para utilizar el primer dispositivo gpu.'
                         '         /cpu:0 para utilizar el CPU.'
                         'Mas informacion: https://www.tensorflow.org/guide/using_gpu.'
                         'Default: /device:GPU:0',
                    default='/device:GPU:0')
parser.add_argument('--path_output_file',
                    help='Path donde se guardara el video con los objetos detectados.'
                         'Si no se configura un path no se guarda el archivo de salida.',
                    default=None)
parser.add_argument('--show_input',
                    help='Boolean que define si se mostrara en una ventana el input.'
                         'Default: False.',
                    default='False')
parser.add_argument('--show_output',
                    help='Boolean que define si se mostrara en una ventana el output.'
                         'Default: False.',
                    default='False')
parser.add_argument('--path_logging',
                    help='Path del archivo de log. Si no se configura no se loggea.'
                         'Default: Vacio.',
                    default=None)

args = parser.parse_args()

stream_paths = args.stream_paths
model_path = args.model_path
device = args.device
path_output_file = args.path_output_file
show_input = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_input == 'True' else False
show_output = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_output == 'True' else False
path_logging = args.path_logging
if path_logging is not None:
    path_logging = path_logging.split('.')[0] \
                   + '__{:%Y-%m-%d_%H_%M_%s}__'.format(datetime.now()) + path_logging.split('.')[1]
    log = True
else:
    log = False

logging.basicConfig(filename=path_logging,
                    level=logging.DEBUG,
                    format='%(asctime)s - [%(filename)s:%(lineno)d - %(exc_info)s - %(stack_info)s] - %(message)s',
                    filemode='w')


message_parameters = 'Parameters:'\
                     '\n\tstream_path: {}'\
                     '\n\tmodel_path: {}'\
                     '\n\tdevice: {}'\
                     '\n\tpath_output_file: {}'\
                     '\n\tshow_input: {}'\
                     '\n\tshow_output: {}'\
                     '\n\tpath_logging: {}'.format(stream_paths,
                                                   model_path,
                                                   device,
                                                   path_output_file,
                                                   show_input,
                                                   show_output,
                                                   path_logging)
logging.info(message_parameters)
print(message_parameters)

try:

    def get_video_captures(paths):
        video_captures = []
        for path in paths:
            video_captures.append(cv2.VideoCapture(path))

        return video_captures


    def join_frames(f):
        y = 0
        x = 0
        i = 0
        c = 2
        # columnas de la grilla de videos

        height = int(sum(image.shape[0] for image in f)/c)
        width = int(sum(image.shape[1] for image in f)/c)
        # height = sum(image.shape[0] for image in f[:int(len(f) / 2)])
        # width = sum(image.shape[1] for image in f[:int(len(f)/2)])

        output = np.zeros((height, width, 3), dtype=np.uint8)

        for image in f:
            h, w, d = image.shape
            # if i < c: f
            if i < c:
                output[y:y + h, 0:w] = image
                y += h
            else:
                output[x:x + h, w:w + w] = image
                x += h
            i += 1

        return output
    #
    #
    # def join_frames(f):
    #     y = 0
    #     x = 0
    #     i = 0
    #     c = 1
    #     # columnas de la grilla de videos
    #
    #     # height = int(sum(image.shape[0] for image in f)/c)
    #     # width = int(max(image.shape[1] for image in f))
    #     # height = sum(image.shape[0] for image in f[:int(len(f) / 2)])
    #     # width = sum(image.shape[1] for image in f[:int(len(f)/2)])
    #
    #     output = np.zeros((960, 1280, 3), dtype=np.uint8)
    #
    #     output[0:720, 0:1280] = f[0]
    #     # output[720:1440, 0:1280] = f[1]
    #
    #     # for image in f:
    #     #     h, w, d = image.shape
    #     #     # if i < c: f
    #     #     if i < c:
    #     #         output[y:y + h, 0:w] = image
    #     #         y += h
    #     #     else:
    #     #         output[x:x + h, w:w + w] = image
    #     #         x += h
    #     #     i += 1
    #
    #     return output

    st_process = utils_cris.print_start_time('Inicio del proceso: {}', to_log=log)

    # MODEL_NAME = 'qatar_airways_logo_graph'
    # CWD_PATH = os.getcwd()

    # Path to frozen detection graph .pb file, which contains the model that is used
    # for object detection.
    # PATH_TO_CKPT = os.path.join(CWD_PATH, MODEL_NAME, 'frozen_inference_graph.pb')

    # Path to label map file
    PATH_TO_LABELS = os.path.join('data', 'label_map.pbtxt')

    # Path to video
    # PATH_TO_VIDEO = os.path.join(CWD_PATH, VIDEO_NAME)
    PATH_TO_VIDEO = stream_paths

    # Number of classes the object detector can identify
    NUM_CLASSES = 2

    # Load the label map.
    # Label maps map indices to category names, so that when our convolution
    # network predicts `5`, we know that this corresponds to `king`.
    # Here we use internal utility functions, but anything that returns a
    # dictionary mapping integers to appropriate string labels would be fine
    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map,
                                                                max_num_classes=NUM_CLASSES,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    # Load the Tensorflow model into memory.
    # detection_graph = tf.Graph()
    # with detection_graph.as_default():
    #     od_graph_def = tf.GraphDef()
    #     with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    #         serialized_graph = fid.read()
    #         od_graph_def.ParseFromString(serialized_graph)
    #         tf.import_graph_def(od_graph_def, name='')
    #
    #     sess = tf.Session(graph=detection_graph)

    # with tf.device('/device:GPU:0'):
    with tf.device(device):
        detection_graph = tf.Graph()
    # config = tf.ConfigProto(
    #     device_count={'GPU': 0},
    #     log_device_placement=True
    # )
    config = tf.ConfigProto(
        log_device_placement=True
    )
    # config.gpu_options.allow_growth = True
    # config.gpu_options.per_process_gpu_memory_fraction = 0.5

    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()

        with tf.gfile.GFile(model_path, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        # sess = tf.Session(graph=detection_graph)
        sess = tf.Session(graph=detection_graph, config=config)

    # Define input and output tensors (i.e. data) for the object detection classifier

    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Output tensors are the detection boxes, scores, and classes
    # Each box represents a part of the image where a particular object was detected
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represents level of confidence for each of the objects.
    # The score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

    # Number of objects detected
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # Open video file
    # video = cv2.VideoCapture(PATH_TO_VIDEO)
    videos = get_video_captures(stream_paths)
    len_streams = len(videos)

    fps = int(videos[0].get(cv2.CAP_PROP_FPS))
    frame_size = int(videos[0].get(cv2.CAP_PROP_FRAME_WIDTH)), int(videos[0].get(cv2.CAP_PROP_FRAME_HEIGHT))

    # out = cv2.VideoWriter(filename='LOGO_DETECTION__' + VIDEO_NAME,
    if path_output_file is not None:
        out = cv2.VideoWriter(filename=path_output_file,
                              fourcc=cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
                              fps=fps,
                              frameSize=frame_size)

    nframe = 0
    nsecond = 0
    video_position = timedelta(seconds=nsecond)
    path_image_logo_finded = '/home/enzoscocco/tensorflow1/models/research/object_detection/images_qatar_logo_finded/'
    while all(video.isOpened() for video in videos):
        # if nframe % fps == 0:
        if nframe % (fps * len_streams) == 0:
            if nsecond != int(nframe / fps):
                message_second = 'Fin del procesamiento de la posicion {}: {}  tiempo transcurrido: {}' \
                                 '\n#################################################################################' \
                                 '###################################################################################' \
                                 '\n\n' \
                    .format(str_position, '{}', '{}')
                utils_cris.print_end_elapsed_time(st_process_second,
                                                  format_time='%Y-%m-%d %H:%M:%S.%f',
                                                  message=message_second,
                                                  to_log=log)
                nsecond = int(nframe / fps)
                video_position = timedelta(seconds=nsecond)

            total_seconds = int(video_position.total_seconds())
            hours, remainder = divmod(total_seconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            str_position = '{:02}:{:02}:{:02}'.format(hours, minutes, seconds)
            image_position = '{:02}_{:02}_{:02}_nframe{}'.format(hours, minutes, seconds, nframe)
            message_second = '\n####################################################################################' \
                             '####################################################################################\n' \
                             'Inicio de procesamiento de la posicion {}: {}'.format(str_position, '{}')
            st_process_second = utils_cris.print_start_time(message_second,
                                                            format_time='%Y-%m-%d %H:%M:%S.%f',
                                                            to_log=log)

        # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
        # i.e. a single-column array, where each item in the column has the pixel RGB value
        # ret, frame = video.read()
        frames = [video.read()[1] for video in videos]
        # frame = join_frames(frames)
        output_frames_processed = []
        nstream = 0
        for frame in frames:
            try:
                message_frame = '\nInicio de procesamiento del frame numero {}: {}'.format(nframe, '{}')
                st_process_frame = utils_cris.print_start_time(message=message_frame, to_log=log)

                # Recolor the frame. By default, OpenCV uses BGR color space.
                # This short blog post explains this better:
                # https://www.learnopencv.com/why-does-opencv-use-bgr-color-format/
                st_cvtcolor = utils_cris.print_start_time('Inicio de recolorizacion del frame {}')
                color_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                utils_cris.print_end_elapsed_time(st_cvtcolor,
                                                  message='Fin de recolorizacion del frame: {}  tiempo transcurrido: {}',
                                                  to_log=log)

                st_expand_dims = utils_cris.print_start_time('Inicio de np.expand_dims(color_frame, axis=0): {}',
                                                             to_log=log)
                frame_expanded = np.expand_dims(color_frame, axis=0)
                utils_cris.print_end_elapsed_time(st_expand_dims,
                                                  message='Fin de np.expand_dims(color_frame, axis=0): {}'
                                                          'tiempo transcurrido: {}',
                                                  to_log=log)

                # Perform the actual detection by running the model with the image as input
                st_detection = utils_cris.print_start_time(message='Inicio de deteccion {}', to_log=log)
                (boxes, scores, classes, num) = sess.run(
                    [detection_boxes, detection_scores, detection_classes, num_detections],
                    feed_dict={image_tensor: frame_expanded})
                utils_cris.print_end_elapsed_time(st_detection,
                                                  message='Fin de deteccion: {}  tiempo transcurrido: {}',
                                                  to_log=log)

                output_rgb = frame
                if len(boxes) > 0:
                    # Draw the results of the detection (aka 'visulaize the results')
                    st_drawbox = utils_cris.print_start_time('Inicio  de dibujado de caja\\s: {}')
                    # path_image_match = path_image_logo_finded + '/nStream{}_'.format(nstream) + image_position + '.jpg'
                    path_image_match = path_image_logo_finded + \
                                       '/nStream{}_'.format(nframe % len_streams) \
                                       + image_position + '.jpg'
                    has_boxes = vis_util.visualize_boxes_and_labels_on_image_array(
                        color_frame,
                        np.squeeze(boxes),
                        np.squeeze(classes).astype(np.int32),
                        np.squeeze(scores),
                        category_index,
                        use_normalized_coordinates=True,
                        line_thickness=8,
                        min_score_thresh=0.50,
                        nombre_image=path_image_match)[1]
                    if has_boxes:
                        utils_cris.print_end_elapsed_time(st_drawbox,
                                                          message='Fin de dibujado de caja\\s: {}  tiempo transcurrido: {}',
                                                          to_log=log)

                        # All the results have been drawn on the frame, so it's time to display it.
                        # cv2.imshow('Object detector', frame)
                        st_cvtcolor_back = utils_cris.print_start_time('Inicio de recoloreo para volver al original: {}',
                                                                       to_log=log)
                        output_rgb = cv2.cvtColor(color_frame, cv2.COLOR_RGB2BGR)

                        utils_cris.print_end_elapsed_time(st_cvtcolor_back,
                                                          message='Fin de recoloreo para volver al original: {}  '
                                                                  'tiempo transcurrido: {}',
                                                          to_log=log)

                        message_save_image = 'Inicio guardado imagen {} donde el logo se encontro: {}'\
                            .format('/nStream{}_'.format(nstream) + image_position + '.jpg', '{}')
                        st_save_image = utils_cris.print_start_time(message_save_image,
                                                                    to_log=log)
                        message_result_detection = 'boxes: {},\n scores: {},\n classes: {},\n num: {}\n'\
                            .format(boxes, scores, classes, num)
                        logging.info(message_result_detection)
                        cv2.imwrite(path_image_match, output_rgb)
                        utils_cris.print_end_elapsed_time(st_save_image,
                                                          message='Fin de guardado de imagen: {}  tiempo trancurrido: {}',
                                                          to_log=log)

                if show_input:
                    cv2.imshow('Object detector input', frame)
                # if show_output:
                #     cv2.imshow('Object detector output', output_rgb)

                output_frames_processed.append(output_rgb)

                if path_output_file is not None:
                    st_write = utils_cris.print_start_time('Inicio de write: {}')
                    out.write(output_rgb)
                    utils_cris.print_end_elapsed_time(st_write,
                                                      message='Fin de write: {}  tiempo transcurrido: {}',
                                                      to_log=log)

                # Press 'q' to quit
                if cv2.waitKey(1) == ord('q'):
                    break

                message_frame = 'Fin de procesamiento del frame numero {}: {}  tiempo transcurrido: {}\n'.format(nframe,
                                                                                                                 '{}',
                                                                                                                 '{}')
                utils_cris.print_end_elapsed_time(st_process_frame, message=message_frame, to_log=log)

                nframe += 1

            except Exception as e:
                total_seconds = int(video_position.total_seconds())
                hours, remainder = divmod(total_seconds, 3600)
                minutes, seconds = divmod(remainder, 60)
                message_error_frame = 'Error en el frame {} de la posicion del video {}'\
                    .format(nframe, '{:02}:{:02}:{:02}'.format(hours, minutes, seconds))
                logging.exception(message_error_frame)
                print(message_error_frame)
                if e is not None:
                    print('e:', e)

            nstream += 1

        joined_output = join_frames(output_frames_processed)
        cv2.imshow('Salida unida', joined_output)

    # Clean up
    for video in videos:
        video.release()
    cv2.destroyAllWindows()

    utils_cris.print_end_elapsed_time(st_process, message='Fin del proceso: {}  tiempo transcurrido: ', to_log=log)


except Exception as e:
    logging.exception('')
    if e is not None:
        print(e)
