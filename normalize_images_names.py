import os
import argparse
import xml.etree.ElementTree as et

parser = argparse.ArgumentParser()
parser.add_argument('--path_input',
                    help='Ruta donde estan los archivos XML a actualizar',
                    required=True)

args = parser.parse_args()
path_input = args.path_input
absolut_path_input = os.path.abspath(path_input)
current_folder = absolut_path_input.split('/')[-1]

print(args, '\n')
pictures_files = [picture_file for picture_file in os.listdir(path_input) if '.xml' not in picture_file]
xml_files = [picture_file for picture_file in os.listdir(path_input) if '.xml' in picture_file]
for index, xml_files in enumerate(xml_files):

