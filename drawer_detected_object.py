import os
import csv
import cv2
import numpy as np
import collections
import pytesseract
import PIL.Image as Image
import PIL.ImageColor as ImageColor
import PIL.ImageDraw as ImageDraw
import PIL.ImageFont as ImageFont

from typing import List
# from PIL import Image

from DAL.dal import DAL
from DAL.model.item_real_time import ItemRealTime
from DAL.model.logo import Logo


class DrawerDetectedObject:
    STANDARD_COLORS = [
        'AliceBlue', 'Chartreuse', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque',
        'BlanchedAlmond', 'BlueViolet', 'BurlyWood', 'CadetBlue', 'AntiqueWhite',
        'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson', 'Cyan',
        'DarkCyan', 'DarkGoldenRod', 'DarkGrey', 'DarkKhaki', 'DarkOrange',
        'DarkOrchid', 'DarkSalmon', 'DarkSeaGreen', 'DarkTurquoise', 'DarkViolet',
        'DeepPink', 'DeepSkyBlue', 'DodgerBlue', 'FireBrick', 'FloralWhite',
        'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'Gold', 'GoldenRod',
        'Salmon', 'Tan', 'HoneyDew', 'HotPink', 'IndianRed', 'Ivory', 'Khaki',
        'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon', 'LightBlue',
        'LightCoral', 'LightCyan', 'LightGoldenRodYellow', 'LightGray', 'LightGrey',
        'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue',
        'LightSlateGray', 'LightSlateGrey', 'LightSteelBlue', 'LightYellow', 'Lime',
        'LimeGreen', 'Linen', 'Magenta', 'MediumAquaMarine', 'MediumOrchid',
        'MediumPurple', 'MediumSeaGreen', 'MediumSlateBlue', 'MediumSpringGreen',
        'MediumTurquoise', 'MediumVioletRed', 'MintCream', 'MistyRose', 'Moccasin',
        'NavajoWhite', 'OldLace', 'Olive', 'OliveDrab', 'Orange', 'OrangeRed',
        'Orchid', 'PaleGoldenRod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed',
        'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum', 'PowderBlue', 'Purple',
        'Red', 'RosyBrown', 'RoyalBlue', 'SaddleBrown', 'Green', 'SandyBrown',
        'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue',
        'SlateGray', 'SlateGrey', 'Snow', 'SpringGreen', 'SteelBlue', 'GreenYellow',
        'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White',
        'WhiteSmoke', 'Yellow', 'YellowGreen'
    ]
    TEMPLATE_DISPLAY_STR_LIST = '{}: {}%'

    def __init__(self, client_id, country_id, since, until, connection_string, path_output,
                 items_real_time_csv_path=None):
        self.client_id = client_id
        self.country_id = country_id
        self.since = since
        self.until = until
        self.connection_string = connection_string
        self.path_output = path_output
        self.dal = DAL(self.connection_string)
        self.items_real_time_csv_path = items_real_time_csv_path
        self.items_real_time: List[ItemRealTime] = self.get_items_real_time() if self.items_real_time_csv_path is None \
            else self.get_items_real_time_from_csv()
        self.logos = self.get_logos()

    def get_items_real_time(self):
        return self.dal.get_items_real_time(self.since, self.until, self.client_id, self.country_id)

    def get_items_real_time_from_csv(self):
        with open(self.items_real_time_csv_path) as f:
            reader = csv.DictReader(f)

            items_real_time = [ItemRealTime(irt['VehicleID'],
                                            irt['SourceSignalID'],
                                            irt['LogoID'],
                                            irt['Date'],
                                            irt['Path'],
                                            irt['Score'],
                                            irt['BoxYMin'],
                                            irt['BoxXMin'],
                                            irt['BoxYMax'],
                                            irt['BoxXMax'],
                                            irt['PathFileRawData']) for irt in reader]

        return items_real_time

    def get_logos(self) -> List[Logo]:
        return self.dal.get_logos_by_client(self.client_id)

    def get_logo_by_id(self, logo_id):
        return self.static_get_logo_by_id(logo_id, self.logos)

    @staticmethod
    def static_get_logo_by_id(logo_id, logos):
        logo_id = int(logo_id)
        logo = None
        for lg in logos:
            if logo_id == lg.id:
                logo = lg
                break

        return logo

    @staticmethod
    def get_color_to_logo_box(logo_id):
        return DrawerDetectedObject.STANDARD_COLORS[int(logo_id) % len(DrawerDetectedObject.STANDARD_COLORS)]

    @staticmethod
    def static_write_images_with_detected_objects(items_real_time: List[ItemRealTime], logos: List[Logo],
                                                  prefix_filename_images='DO_', path_output=None):
        irt_paths = []
        irt_logos = []
        paths_images_with_detected_objects = []
        for irt in items_real_time:
            if irt.path not in irt_paths:
                irt_paths.append(irt.path)
            # if not any(irt_logo.id == irt.logo_id for irt_logo in irt_logos):
            if not any([int(irt.logo_id) == irt_logo.id for irt_logo in irt_logos]):
                irt_logos.append(DrawerDetectedObject.static_get_logo_by_id(int(irt.logo_id), logos))

        for irt_path in irt_paths:
            # por ahora, despues agregar en el algoritmo de persintecia que crea el CSV que se guarde la ruta completa
            # de la imagen
            # image = cv2.imread(os.path.join(os.path.dirname(self.items_real_time_csv_path), irt_path))
            if not os.path.isfile(irt_path):
                raise ValueError('No existe el archivo {}'.format(irt_path))
            image = cv2.imread(irt_path)
            items_to_image = [item for item in items_real_time if item.path == irt_path]
            for item_to_image in items_to_image:
                DrawerDetectedObject.draw_bounding_box_on_image_array(
                    image,
                    float(item_to_image.box_y_min),
                    float(item_to_image.box_x_min),
                    float(item_to_image.box_y_max),
                    float(item_to_image.box_x_max),
                    color=DrawerDetectedObject.get_color_to_logo_box(item_to_image.logo_id),
                    display_str_list=DrawerDetectedObject.TEMPLATE_DISPLAY_STR_LIST.format(
                        DrawerDetectedObject.static_get_logo_by_id(item_to_image.logo_id, logos).display_name,
                        item_to_image.score),
                    use_normalized_coordinates=True
                )
            path_output = path_output if path_output is not None else os.path.dirname(irt_path)
            path_image_with_detected_objects = os.path.join(path_output,
                                                            prefix_filename_images + os.path.basename(irt_path))
            cv2.imwrite(path_image_with_detected_objects,
                        image)
            paths_images_with_detected_objects.append(path_image_with_detected_objects)

        return paths_images_with_detected_objects

    def write_images_with_detected_objects(self, prefix_filename_images='DO_'):
        # has_boxes = vis_util.visualize_boxes_and_labels_on_image_array(
        #     frame,
        #     np.squeeze(boxes),
        #     np.squeeze(classes).astype(np.int32),
        #     np.squeeze(scores),
        #     category_index,
        #     use_normalized_coordinates=True,
        #     line_thickness=8,
        #     min_score_thresh=minimum_percent_match_detection)[1]
        irt_paths = []
        irt_logos = []
        for irt in self.items_real_time:
            if irt.path not in irt_paths:
                irt_paths.append(irt.path)
            # if not any(irt_logo.id == irt.logo_id for irt_logo in irt_logos):
            if not any([int(irt.logo_id) == irt_logo.id for irt_logo in irt_logos]):
                irt_logos.append(self.get_logo_by_id(int(irt.logo_id)))

        for irt_path in irt_paths:
            # por ahora, despues agregar en el algoritmo de persintecia que crea el CSV que se guarde la ruta completa
            # de la imagen
            image = cv2.imread(os.path.join(os.path.dirname(self.items_real_time_csv_path), irt_path))
            # image = cv2.imread(irt_path)
            items_to_image = [item for item in self.items_real_time if item.path == irt_path]
            for item_to_image in items_to_image:
                DrawerDetectedObject.draw_bounding_box_on_image_array(
                    image,
                    float(item_to_image.box_y_min),
                    float(item_to_image.box_x_min),
                    float(item_to_image.box_y_max),
                    float(item_to_image.box_x_max),
                    color=self.get_color_to_logo_box(item_to_image.logo_id),
                    display_str_list=self.TEMPLATE_DISPLAY_STR_LIST.format(
                        self.get_logo_by_id(item_to_image.logo_id).display_name,
                        item_to_image.score),
                    use_normalized_coordinates=True
                )
            cv2.imwrite(os.path.join(self.path_output, prefix_filename_images + os.path.basename(item_to_image.path)),
                        image)

    @staticmethod
    def draw_bounding_box_on_image(image,
                                   ymin,
                                   xmin,
                                   ymax,
                                   xmax,
                                   color='red',
                                   thickness=4,
                                   display_str_list=(),
                                   use_normalized_coordinates=True):
        """Adds a bounding box to an image.

      Bounding box coordinates can be specified in either absolute (pixel) or
      normalized coordinates by setting the use_normalized_coordinates argument.

      Each string in display_str_list is displayed on a separate line above the
      bounding box in black text on a rectangle filled with the input 'color'.
      If the top of the bounding box extends to the edge of the image, the strings
      are displayed below the bounding box.

      Args:
        image: a PIL.Image object.
        ymin: ymin of bounding box.
        xmin: xmin of bounding box.
        ymax: ymax of bounding box.
        xmax: xmax of bounding box.
        color: color to draw bounding box. Default is red.
        thickness: line thickness. Default value is 4.
        display_str_list: list of strings to display in box
                          (each to be shown on its own line).
        use_normalized_coordinates: If True (default), treat coordinates
          ymin, xmin, ymax, xmax as relative to the image.  Otherwise treat
          coordinates as absolute.
      """
        draw = ImageDraw.Draw(image)
        im_width, im_height = image.size
        if use_normalized_coordinates:
            (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                          ymin * im_height, ymax * im_height)
        else:
            (left, right, top, bottom) = (xmin, xmax, ymin, ymax)
        draw.line([(left, top), (left, bottom), (right, bottom),
                   (right, top), (left, top)], width=thickness, fill=color)
        try:
            font = ImageFont.truetype('arial.ttf', 24)
        except IOError:
            font = ImageFont.load_default()

        # If the total height of the display strings added to the top of the bounding
        # box exceeds the top of the image, stack the strings below the bounding box
        # instead of above.
        display_str_heights = [font.getsize(ds)[1] for ds in display_str_list]
        # Each display_str has a top and bottom margin of 0.05x.
        total_display_str_height = (1 + 2 * 0.05) * sum(display_str_heights)

        if top > total_display_str_height:
            text_bottom = top
        else:
            text_bottom = bottom + total_display_str_height
        # Reverse list and print from bottom to top.
        for display_str in display_str_list[::-1]:
            text_width, text_height = font.getsize(display_str)
            margin = np.ceil(0.05 * text_height)
            draw.rectangle(
                [(left, text_bottom - text_height - 2 * margin), (left + text_width,
                                                                  text_bottom)],
                fill=color)
            draw.text(
                (left + margin, text_bottom - text_height - margin),
                display_str,
                fill='black',
                font=font)
            text_bottom -= text_height - 2 * margin

    # def draw_bounding_box_on_image_array(self,
    @staticmethod
    def draw_bounding_box_on_image_array(image,
                                         ymin,
                                         xmin,
                                         ymax,
                                         xmax,
                                         color='red',
                                         thickness=4,
                                         display_str_list=(),
                                         use_normalized_coordinates=True):
        """Adds a bounding box to an image (numpy array).

      Bounding box coordinates can be specified in either absolute (pixel) or
      normalized coordinates by setting the use_normalized_coordinates argument.

      Args:
        image: a numpy array with shape [height, width, 3].
        ymin: ymin of bounding box.
        xmin: xmin of bounding box.
        ymax: ymax of bounding box.
        xmax: xmax of bounding box.
        color: color to draw bounding box. Default is red.
        thickness: line thickness. Default value is 4.
        display_str_list: list of strings to display in box
                          (each to be shown on its own line).
        use_normalized_coordinates: If True (default), treat coordinates
          ymin, xmin, ymax, xmax as relative to the image.  Otherwise treat
          coordinates as absolute.
      """
        image_pil = Image.fromarray(np.uint8(image)).convert('RGB')
        DrawerDetectedObject.draw_bounding_box_on_image(image_pil, ymin, xmin, ymax, xmax, color,
                                        thickness, display_str_list, use_normalized_coordinates)
        np.copyto(image, np.array(image_pil))

    @staticmethod
    def draw_mask_on_image_array(image, mask, color='red', alpha=0.4):
        """Draws mask on an image.

      Args:
        image: uint8 numpy array with shape (img_height, img_height, 3)
        mask: a uint8 numpy array of shape (img_height, img_height) with
          values between either 0 or 1.
        color: color to draw the keypoints with. Default is red.
        alpha: transparency value between 0 and 1. (default: 0.4)

      Raises:
        ValueError: On incorrect data type for image or masks.
      """
        if image.dtype != np.uint8:
            raise ValueError('`image` not of type np.uint8')
        if mask.dtype != np.uint8:
            raise ValueError('`mask` not of type np.uint8')
        if np.any(np.logical_and(mask != 1, mask != 0)):
            raise ValueError('`mask` elements should be in [0, 1]')
        if image.shape[:2] != mask.shape:
            raise ValueError('The image has spatial dimensions %s but the mask has '
                             'dimensions %s' % (image.shape[:2], mask.shape))
        rgb = ImageColor.getrgb(color)
        pil_image = Image.fromarray(image)

        solid_color = np.expand_dims(
            np.ones_like(mask), axis=2) * np.reshape(list(rgb), [1, 1, 3])
        pil_solid_color = Image.fromarray(np.uint8(solid_color)).convert('RGBA')
        pil_mask = Image.fromarray(np.uint8(255.0 * alpha * mask)).convert('L')
        pil_image = Image.composite(pil_solid_color, pil_image, pil_mask)
        np.copyto(image, np.array(pil_image.convert('RGB')))

    def visualize_boxes_and_labels_on_image_array(
            self,
            image,
            boxes,
            classes,
            scores,
            category_index,
            instance_masks=None,
            instance_boundaries=None,
            keypoints=None,
            use_normalized_coordinates=False,
            max_boxes_to_draw=20,
            min_score_thresh=.5,
            agnostic_mode=False,
            line_thickness=4,
            groundtruth_box_visualization_color='black',
            skip_scores=False,
            skip_labels=False,
            nombre_image=''):
        """Overlay labeled boxes on an image with formatted scores and label names.

      This function groups boxes that correspond to the same location
      and creates a display string for each detection and overlays these
      on the image. Note that this function modifies the image in place, and returns
      that same image.

      Args:
        image: uint8 numpy array with shape (img_height, img_width, 3)
        boxes: a numpy array of shape [N, 4]
        classes: a numpy array of shape [N]. Note that class indices are 1-based,
          and match the keys in the label map.
        scores: a numpy array of shape [N] or None.  If scores=None, then
          this function assumes that the boxes to be plotted are groundtruth
          boxes and plot all boxes as black with no classes or scores.
        category_index: a dict containing category dictionaries (each holding
          category index `id` and category name `name`) keyed by category indices.
        instance_masks: a numpy array of shape [N, image_height, image_width] with
          values ranging between 0 and 1, can be None.
        instance_boundaries: a numpy array of shape [N, image_height, image_width]
          with values ranging between 0 and 1, can be None.
        keypoints: a numpy array of shape [N, num_keypoints, 2], can
          be None
        use_normalized_coordinates: whether boxes is to be interpreted as
          normalized coordinates or not.
        max_boxes_to_draw: maximum number of boxes to visualize.  If None, draw
          all boxes.
        min_score_thresh: minimum score threshold for a box to be visualized
        agnostic_mode: boolean (default: False) controlling whether to evaluate in
          class-agnostic mode or not.  This mode will display scores but ignore
          classes.
        line_thickness: integer (default: 4) controlling line width of the boxes.
        groundtruth_box_visualization_color: box color for visualizing groundtruth
          boxes
        skip_scores: whether to skip score when drawing a single detection
        skip_labels: whether to skip label when drawing a single detection

      Returns:
        uint8 numpy array with shape (img_height, img_width, 3) with overlaid boxes.
      """
        # Create a display string (and color) for every box location, group any boxes
        # that correspond to the same location.
        box_to_display_str_map = collections.defaultdict(list)
        box_to_color_map = collections.defaultdict(str)
        box_to_instance_masks_map = {}
        box_to_instance_boundaries_map = {}
        box_to_keypoints_map = collections.defaultdict(list)
        if not max_boxes_to_draw:
            max_boxes_to_draw = boxes.shape[0]
        for i in range(min(max_boxes_to_draw, boxes.shape[0])):
            if scores is None or scores[i] > min_score_thresh:
                box = tuple(boxes[i].tolist())
                if instance_masks is not None:
                    box_to_instance_masks_map[box] = instance_masks[i]
                if instance_boundaries is not None:
                    box_to_instance_boundaries_map[box] = instance_boundaries[i]
                if keypoints is not None:
                    box_to_keypoints_map[box].extend(keypoints[i])
                if scores is None:
                    box_to_color_map[box] = groundtruth_box_visualization_color
                else:
                    display_str = ''
                    if not skip_labels:
                        if not agnostic_mode:
                            if classes[i] in category_index.keys():
                                class_name = category_index[classes[i]]['name']
                            else:
                                class_name = 'N/A'
                            display_str = str(class_name)
                    if not skip_scores:
                        if not display_str:
                            display_str = '{}%'.format(int(100 * scores[i]))
                        else:
                            display_str = '{}: {}%'.format(display_str, int(100 * scores[i]))
                    box_to_display_str_map[box].append(display_str)
                    if agnostic_mode:
                        box_to_color_map[box] = 'DarkOrange'
                    else:
                        box_to_color_map[box] = DrawerDetectedObject.STANDARD_COLORS[
                            classes[i] % len(DrawerDetectedObject.STANDARD_COLORS)]

        # Draw all boxes onto image.
        # for box, color in box_to_color_map.items():  original
        box_to_color_map_items = box_to_color_map.items()
        for box, color in box_to_color_map_items:
            # cv2.imshow('[Inicio] for box, color in box_to_color_map.items():', image)
            ymin, xmin, ymax, xmax = box
            if instance_masks is not None:
                self.draw_mask_on_image_array(
                    image,
                    box_to_instance_masks_map[box],
                    color=color
                )
                cv2.imshow('instance_masks is not None', image)
            if instance_boundaries is not None:
                self.draw_mask_on_image_array(
                    image,
                    box_to_instance_boundaries_map[box],
                    color='red',
                    alpha=1.0
                )
                cv2.imshow('instance_masks is not None', image)
            print(nombre_image)
            # logging.info(nombre_image)
            self.draw_bounding_box_on_image_array(
                image,
                ymin,
                xmin,
                ymax,
                xmax,
                color=color,
                thickness=line_thickness,
                display_str_list=box_to_display_str_map[box],
                use_normalized_coordinates=use_normalized_coordinates)
            # cv2.imshow('for box, color in box_to_color_map.items():', image)
            if keypoints is not None:
                self.draw_keypoints_on_image_array(
                    image,
                    box_to_keypoints_map[box],
                    color=color,
                    radius=line_thickness / 2,
                    use_normalized_coordinates=use_normalized_coordinates)
                cv2.imshow('if keypoints is not None:', image)

        return (image, box_to_color_map_items)

    def write_detected_objects(self, path_output_detected_objects):
        template_nobject = '__logoid{}_nobject{}'
        irt_paths = []
        for irt in self.items_real_time:
            if irt.path not in irt_paths:
                irt_paths.append(irt.path)

        for irt_path in irt_paths:
            # image = cv2.imread(os.path.join(os.path.dirname(self.items_real_time_csv_path), irt_path))
            image = Image.open(irt_path)
            items_to_image = [item for item in self.items_real_time if item.path == irt_path]
            for nobject, item_to_image in enumerate(items_to_image):
                # clone = image.clone()
                image_width, image_height = image.size
                # crop_clone = clone[int(item_to_image.box_y_min):int(item_to_image.box_y_max),
                #              int(item_to_image.box_x_min):int(item_to_image.box_x_max)]
                box = (float(item_to_image.box_x_min) * image_width,
                       float(item_to_image.box_y_min) * image_height,
                       float(item_to_image.box_x_max) * image_width,
                       float(item_to_image.box_y_max) * image_height)

                crop_image = image.crop(box)

                path_crop = os.path.join(path_output_detected_objects,
                                         os.path.basename(item_to_image.path).split('.')[0]
                                         + template_nobject.format(item_to_image.logo_id, nobject)
                                         + '.'
                                         + item_to_image.path.split('.')[-1])

                crop_image.save(path_crop)

    # image_black_and_white = cv2.threshold(image_grayscale, 230, 255, cv2.THRESH_BINARY)[1]
    @staticmethod
    def images_to_black_and_white(path_images, path_output, thresh=200, bilateral_filter=False):
        files = os.listdir(path_images)
        for file in files:
            try:
                image = cv2.imread(os.path.join(path_images, file))
                image_grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

                # Simple Threshold
                # image_black_and_white = cv2.threshold(image_grayscale, thresh, 255, cv2.THRESH_BINARY)[1]

                # Adaptative Threshold
                image_black_and_white = cv2.adaptiveThreshold(image_grayscale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                                              cv2.THRESH_BINARY, 31, 2)

                if bilateral_filter:
                    image_black_and_white = cv2.bilateralFilter(image_black_and_white, 9, 75, 75)

                cv2.imwrite(os.path.join(path_output, file), image_black_and_white)

            except Exception as e:
                print('Excepcion con el archivo {}: \n{}'.format(file, e))

    @staticmethod
    def images_to_text(path_images, write_image_with_recognized_text=False, bilateralFilter=False):
        path_images_recognized_text = []
        files = os.listdir(path_images)
        for file in files:
            image = Image.open(os.path.join(path_images, file))
            recognized_text = pytesseract.image_to_string(image).replace('\n', ' ')
            print('image',
                  file,
                  'recognized text',
                  recognized_text)
            path_images_recognized_text.append((os.path.join(path_images, file), image, recognized_text))

        if write_image_with_recognized_text:
            for path_image, image, recognized_text in path_images_recognized_text:
                font = ImageFont.truetype("arial.ttf", 12)
                border_image = Image.new(image.mode, (image.size[0], image.size[1] + font.size))
                border_image.paste(image, (0, 0))
                draw = ImageDraw.Draw(border_image)
                if recognized_text.replace(' ', '') != '':
                    draw.text((0, border_image.size[1] - font.size), recognized_text, fill=255, font=font)
                else:
                    draw.text((0, border_image.size[1] - font.size), '<without recognized text>', fill=255, font=font)
                border_image.save(path_image)

    # @staticmethod
    # def images_to_blur(path_images, path_output):
