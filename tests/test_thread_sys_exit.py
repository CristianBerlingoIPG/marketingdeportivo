import sys
import threading
import time


def loop1_10():
    for i in range(1, 11):
        time.sleep(1)
        print(i)


def loop1_20():
    for i in range(11, 21):
        time.sleep(1)
        print(i)


def launch_threads():
    print('Lanzando thread loop1_10')
    # threading.Thread(target=sys.exit(loop1_10())).start()
    threading.Thread(target=loop1_10()).start()
    print('\nLanzando thread loop1_20')
    threading.Thread(target=sys.exit(loop1_20())).start()


if __name__ == '__main__':
    launch_threads()
