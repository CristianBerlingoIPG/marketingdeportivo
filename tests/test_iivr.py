from datetime import datetime, timedelta
from logo_detection_persistence import ImplementationIntervalsValidatonRule


CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
cssl = CONNECTION_STRING_SQL_LOGIN.format('{ODBC Driver 17 for SQL Server}',
                                          '(local)',
                                          'MarketingDeportivo',
                                          'SA',
                                          'Mematas01')

execute_since = (datetime.now() - timedelta(days=6)).replace(hour=0, minute=0, second=0, microsecond=0)
execute_until = datetime.now()
iivr = ImplementationIntervalsValidatonRule(2,
                                            1,
                                            cssl,
                                            execute_since=execute_since,
                                            execute_until=execute_until)

# vi = iivr.get_valid_intervals()
iivr.write_valid_intervals_to_sql()

# for index, v in enumerate(vi):
#     print(index, v)


print('termino')
