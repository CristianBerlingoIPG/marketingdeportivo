import cv2

stream_path = 'http://10.10.1.46:8080'
video = cv2.VideoCapture(stream_path)
fps = video.get(cv2.CAP_PROP_FPS)
frame_count = video.get(cv2.CAP_PROP_FRAME_COUNT)
pos_frames = video .get(cv2.CAP_PROP_POS_FRAMES)
frame = ''
prev_pos_msec = 0
print('fps', fps, 'frame_count', frame_count, 'pos_frames', pos_frames)
while video.isOpened() and frame is not None:
    ret, frame = video.read()
    pos_msec = video.get(cv2.CAP_PROP_POS_MSEC)
    fps = video.get(cv2.CAP_PROP_FPS)
    frame_count = video.get(cv2.CAP_PROP_FRAME_COUNT)
    pos_frames = video.get(cv2.CAP_PROP_POS_FRAMES)
    cv2.imshow('video', frame)
    if cv2.waitKey(1) == ord('q'):
        break
    print('pos_msec', pos_msec, 'difference', pos_msec - prev_pos_msec, 'fps', fps,
          'frame_count', frame_count, 'pos_frames', pos_frames)
    prev_pos_msec = pos_msec
