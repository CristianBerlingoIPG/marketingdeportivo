import argparse
from datetime import datetime

from drawer_detected_object import DrawerDetectedObject


parser = argparse.ArgumentParser()
parser.add_argument('-cl',
                    '--client_id')
parser.add_argument('-cty',
                    '--country_id')
parser.add_argument('-s',
                    '--since')
parser.add_argument('-u',
                    '--until')
parser.add_argument('-o',
                    '--output')
parser.add_argument('-irt_csv',
                    '--items_real_time_csv_path',
                    default=None)


args = parser.parse_args()
client_id = args.client_id
country_id = args.country_id
since = datetime.strptime(args.since, '%Y-%m-%d')
until = datetime.strptime(args.until, '%Y-%m-%d')
output = args.output
items_real_time_csv_path = args.items_real_time_csv_path

CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
cssl = CONNECTION_STRING_SQL_LOGIN.format('{ODBC Driver 17 for SQL Server}',
                                          '(local)',
                                          'MarketingDeportivo',
                                          'SA',
                                          'Mematas01')

ddo = DrawerDetectedObject(client_id, country_id, since, until, cssl, output, items_real_time_csv_path)
# ddo.write_images_with_detected_objects()
# ddo.write_detected_objects('/home/enzoscocco/Documents/object_detection/data_detection/2019/03/02_crop')
# ddo.images_to_black_and_white('/home/enzoscocco/Documents/object_detection/data_detection/2019/03/02_crop',
#                               '/home/enzoscocco/Documents/object_detection/data_detection/2019/03/02_crop_to_bw',
#                               thresh=100,
#                               bilateral_filter=True)
# ddo.images_to_blur()
ddo.images_to_text('/home/enzoscocco/Documents/object_detection/data_detection/2019/03/02_crop_to_bw',
                   write_image_with_recognized_text=True)


print('Termino')
