import os
import logging
import argparse

from datetime import datetime, timedelta

import utils_cris as uc

from DAL.dal import DAL
from logo_detection_persistence import LogoDetectionPersistence as ldp, ImplementationIntervalsValidatonRule as iivr

parser = argparse.ArgumentParser()
parser.add_argument('-pf',
                    '--path_frames',
                    help='Ruta de los frames para procesar',
                    default='frames')
parser.add_argument('-iod',
                    '--is_object_detected',
                    type=lambda x: (str(x).lower() in ['true', '1', 'yes', 'y']),
                    help='Boolean que indica si las imagenes de path_frames son los objetos detectados ya estraidos de'
                         'los frames. Default True',
                    default=False)

args = parser.parse_args()
path_frames = args.path_frames
is_object_detected = args.is_object_detected

# logging configuration
datetime_log = datetime.now()
template_general_log_path = 'logs/{}/{}/info/detections_to_intervals__{}.log'
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

larg = []
[larg.extend(a) for a in args._get_kwargs()]
logging_message_configs = '\nParameters:' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}'.format(*larg)

uc.print_and_logging(logging_message_configs)

cs = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER=10.240.14.213,59964;' \
     'DATABASE=MarketingDeportivo;UID=BUEMBW-LNXAPP06;PWD=Mematas01'
dal = DAL(cs)
logos = dal.get_logos_by_client(2)

valid_values = {}
for logo in logos:
    min_quantity_chars_by_value = 3 if len(logo.display_name) > 3 else len(logo.display_name)
    valid_values[str(logo.id)] = logo.get_valids_values_for_name(min_quantity_chars_by_value)

files = [file for file in os.listdir(path_frames)]
frames = [frame for frame in files if '_DO' not in frame and '_BW' not in frame]
csv_files = [cf for cf in os.listdir() if '.csv' in cf.lower()]

# Borrando imagenes en blanco y negro u objetos detectados extraidos
[os.remove(os.path.join(os.getcwd(), path_frames, td)) for td in files if td not in frames]


def get_objects_detected_related(frame_path, _frames):
    return [
        _f for _f in _frames
        if _f == frame_path or _f.split('_')[:-1] == frame_path.replace('.' + _f.split('.')[-1], '').split('_')
    ]


qty_total_irt = 0
qty_total_irt_recognized = 0
total_sum_score = 0
total_max_score = 0
total_min_score = 100
total_qty_dropped_detections = 0

total_sum_et_ocr = timedelta()
total_max_et_ocr = timedelta()
total_min_et_ocr = timedelta(100)

qty_total_irt_invalids = 0
total_sum_score_invalids = 0

stats = []
for _csv in csv_files:
    uc.print_and_logging('\nProcesando archivo {}'.format(_csv))
    # st_csv = uc.print_and_logging_start_time()

    items_real_time = []
    invalids_irt = []
    if not is_object_detected:
        [
            items_real_time.append(irt) if os.path.basename(irt.path) in frames
            else invalids_irt.append(irt)
            for irt
            in ldp.get_items_real_from_csv(_csv)
        ]

    else:
        matching_frames_names = [f[:-6] + f[-4:] if '_' in f.split('__')[-1] else f for f in frames]
        [
            items_real_time.append(irt) if os.path.basename(irt.path) in matching_frames_names
            else invalids_irt.append(irt)
            for irt
            in ldp.get_items_real_from_csv(_csv)
        ]

    qty_irt = len(items_real_time)
    qty_total_irt += qty_irt
    qty_irt_recognized = 0

    sum_score = 0
    max_score = 0
    min_score = 100

    sum_et_ocr = timedelta()
    max_et_ocr = timedelta()
    min_et_ocr = timedelta(100)
    for n_irt, irt in enumerate(items_real_time):
        detected_objects = get_objects_detected_related(os.path.basename(irt.path), frames)
        for detected_object in detected_objects:
            try:
                irt.path = os.path.join(
                    os.getcwd(),
                    path_frames,
                    detected_object
                )

                st_ocr = datetime.now()
                irt.recognized_text = iivr.get_text_from_image(
                    irt,
                    process_all=True,
                    save_black_and_white_image=True,
                    save_detected_object_image=True,
                    include_text_in_file_name=True,
                    object_detected=None if not is_object_detected else irt.path
                )
                et_ocr = uc.print_end_elapsed_time(st_ocr, message=None)

                percent_match_ocr, value_coincident = iivr.get_percent_match_recognition_and_value_coincident(
                    irt.recognized_text,
                    valid_values[str(irt.logo_id)]
                )
                # uc.print_and_logging('Texto reconocido de la imagen [{}/{}] {}'.format(os.path.basename(irt.path), n_irt + 1, qty_irt))

                qty_irt_recognized += 1 if percent_match_ocr != 0 else 0

                sum_score += irt.score
                max_score = irt.score if irt.score > max_score else max_score
                min_score = irt.score if irt.score < min_score else min_score

                sum_et_ocr += et_ocr
                max_et_ocr = et_ocr if et_ocr > max_et_ocr else max_et_ocr
                min_et_ocr = et_ocr if et_ocr < min_et_ocr else min_et_ocr

                uc.print_and_logging(
                    '\tImagen [{}/{}] ocr_time: {} path: {} score: {} percent_match_ocr: {} recognized_text: {}'.format(
                        n_irt + 1,
                        qty_irt,
                        et_ocr,
                        os.path.basename(irt.path),
                        irt.score,
                        percent_match_ocr,
                        irt.recognized_text
                    )
                )

            except Exception as e:
                uc.print_and_logging('Ocurrio una excepcion durante el procesamiento de la '
                      'imagen [{}/{}] {}:\n{}'.format(os.path.basename(irt.path), n_irt + 1, qty_irt, e))

    # et_csv = uc.print_end_elapsed_time(st_csv, '\nFin procesamiento de CSV {} tiempo transcurrido {}')

    avg_score = sum_score / qty_irt if qty_irt != 0 else 0
    qty_total_irt_recognized += qty_irt_recognized
    total_sum_score += sum_score
    total_max_score = max_score if max_score > total_max_score else total_max_score
    total_min_score = min_score if min_score < total_min_score else total_min_score
    qty_dropped_detections = len([irt for irt in items_real_time if irt.score < avg_score])
    total_qty_dropped_detections += qty_dropped_detections

    avg_et_irt = sum_et_ocr / qty_irt if qty_irt != 0 else 0
    total_sum_et_ocr += sum_et_ocr
    total_max_et_ocr = max_et_ocr if max_et_ocr > total_max_et_ocr else total_max_et_ocr
    total_min_et_ocr = min_et_ocr if min_et_ocr else total_min_et_ocr

    qty_irt_invalids = len(invalids_irt)
    qty_total_irt_invalids += qty_irt_invalids
    sum_score_invalids = sum([iirt.score for iirt in invalids_irt])
    total_sum_score_invalids += sum_score_invalids
    avg_score_invalids = sum_score_invalids / qty_irt_invalids if qty_irt_invalids != 0 else 0
    
    stats.append([_csv, qty_irt, qty_irt_recognized, min_score, max_score, avg_score, qty_dropped_detections,
                  min_et_ocr, max_et_ocr, avg_et_irt, avg_score_invalids])


uc.print_and_logging(logging_message_configs)

for statistics in stats:
    uc.print_and_logging('\nEstadisticas para el archivo {} que tenia {} detecciones validas'.format(statistics[0], statistics[1]))
    uc.print_and_logging('\tCantidad de detecciones validas que pasaron el filtro OCR: {}'.format(statistics[2]))
    uc.print_and_logging('\tPorcentaje minimo de coincidencia Tensorflow de las detecciones validas: {}'.format(statistics[3]))
    uc.print_and_logging('\tPorcentaje maximo de coincidencia Tensorflow de las detecciones validas: {}'.format(statistics[4]))
    uc.print_and_logging('\tPorcentaje de coincidencia promedio : {}'.format(statistics[5]))
    uc.print_and_logging('\tCantidad de detecciones validas que quedan por debajo del promedio de '
          'coincidencia {}'.format(statistics[6]))
    uc.print_and_logging('\tTiempo minimo de OCR: {}'.format(statistics[7]))
    uc.print_and_logging('\tTiempo maximo de OCR: {}'.format(statistics[8]))
    uc.print_and_logging('\tTiempo promedio de OCR: {}'.format(statistics[9]))
    uc.print_and_logging('\tPorcentaje promedio de coincidencia Tensorflow de las detecciones invalidas: {}'.format(statistics[10]))


total_avg_score = total_sum_score / qty_total_irt if qty_total_irt != 0 else 0
total_avg_et_irt = total_sum_et_ocr / qty_total_irt if qty_total_irt != 0 else 0
total_avg_score_invalids = total_sum_score_invalids / qty_total_irt_invalids
uc.print_and_logging('\nEstadisticas totales para los '
      '{} CSV procesados que contenian '
      '{} detecciones validas'.format(len(csv_files),
                                      qty_total_irt))
uc.print_and_logging('\tCantidad de detecciones validas que pasaron el filtro OCR: {}'.format(qty_total_irt_recognized))
uc.print_and_logging('\tPorcentaje minimo de coincidencia Tensorflow: {}'.format(total_min_score))
uc.print_and_logging('\tPorcentaje maximo de coincidencia Tensorflow: {}'.format(total_max_score))
uc.print_and_logging('\tPorcentaje de coincidencia promedio : {} '.format(total_avg_score))
uc.print_and_logging('\tCantidad total de detecciones validas que quedan por debajo del promedio de '
      'coincidencia {}'.format(total_qty_dropped_detections))
uc.print_and_logging('\tTiempo minimo de OCR: {}'.format(total_min_et_ocr))
uc.print_and_logging('\tTiempo maximo de OCR: {}'.format(total_max_et_ocr))
uc.print_and_logging('\tTiempo promedio de OCR: {}'.format(total_avg_et_irt))
uc.print_and_logging('\tPorcentaje promedio de coincidencia Tensorflow de las '
      'detecciones invalidas: {}'.format(total_avg_score_invalids))


uc.print_and_logging('\nTermino')
