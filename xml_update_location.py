import os
import ast
import shutil
import argparse
import xml.etree.ElementTree as et

parser = argparse.ArgumentParser()
parser.add_argument('-pi',
                    '--path_input',
                    help='Ruta donde estan los archivos XML a actualizar',
                    required=True)
parser.add_argument('--normalize_filenames',
                    help='Boolean para decidir si se quiere normalizar los nombres de los archivos enumerandolos.'
                         'Default: False',
                    default='False')
parser.add_argument('-fxwj',
                    '--folder_xml_without_jpg',
                    help='Ruta donde guardar los XML sin JPG asociados. Por defecto carpeta xml_without_jpg dentro del '
                         'path_input',
                    default=None)

args = parser.parse_args()
path_input = args.path_input
normalize_filenames = ast.literal_eval(args.normalize_filenames) if args.normalize_filenames == 'False' \
                                                                    or args.normalize_filenames == 'True' else False
folder_xml_without_jpg = args.folder_xml_without_jpg if args.folder_xml_without_jpg is not None \
    else os.path.join(path_input, 'xml_without_jpg')

if not os.path.isdir(folder_xml_without_jpg):
    os.mkdir(folder_xml_without_jpg)

absolut_path_input = os.path.abspath(path_input)
current_folder = absolut_path_input.split('/')[-1]

print(args, '\n')
pictures_files = [picture_file for picture_file in os.listdir(path_input) if '.xml' not in picture_file]
xml_files = [picture_file for picture_file in os.listdir(path_input) if '.xml' in picture_file]
for index, xml_file in enumerate(xml_files):
    try:
        tree = et.parse(path_input + '/' + xml_file)
        root = tree.getroot()

        try:
            picture_filename = [picture_file for picture_file in pictures_files
                                if xml_file.split('.')[0] == picture_file.split('.')[0]][0]
        except Exception as ex_picture_filename:
            print('Exception: archivo XML {} '
                  'no tiene JPG asociado, sera movido a la carpeta {}'.format(xml_file, folder_xml_without_jpg))
            shutil.move(os.path.join(path_input, xml_file), folder_xml_without_jpg)

        new_picture_filename = '{:03d}.{}'.format(index, picture_filename.split('.')[-1]) \
            if normalize_filenames else picture_filename
        new_xml_filename = '{:03d}.xml'.format(index) if normalize_filenames else xml_file

        xml_filename = root.find('./filename')
        xml_filename.text = new_picture_filename

        folder = root.find('./folder')
        folder.text = current_folder

        path = root.find('./path')
        path.text = absolut_path_input + '/' + new_picture_filename

        print('Archivo {} numero {} editado'.format(xml_file, index))
        tree.write(path_input + '/' + xml_file)

        if normalize_filenames:
            os.rename(os.path.join(path_input, xml_file), new_xml_filename)
            os.rename(os.path.join(path_input, picture_filename), path_input + '/' + new_picture_filename)

    except Exception as e:
        print('Excepcion en el xml_file', xml_file)
        if e is not None:
            print('e', e)


print('Archivos actualizados: {}\nProceso terminado.'.format(index))
