import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import json
import argparse
from datetime import timedelta

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util


parser = argparse.ArgumentParser()
parser.add_argument('--path_video',
                    help='Ruta del video sobre el que se va a correr el proceso de deteccion',
                    required=True)
parser.add_argument('--path_model',
                    help='Ruta al archivo .pb de imagen del grafo de deteccion, '
                         'el cual contiene el modelo que es usado para la deteccion de objetos',
                    required=True)
parser.add_argument('--path_labelmap',
                    help='Ruta al archivo .pbtxt de mapeo de etiquetas',
                    required=True)
parser.add_argument('--minimum_exposition_time',
                    help='Tiempo minimo de exposicion para que un intervalo sea reconocido como valido en segundos',
                    type=int,
                    default=3)
parser.add_argument('--minimum_percent_frames',
                    help='Porcentaje minimo del total de frames con el que debe contar un intervalo que cumple el '
                         'tiempo minimo de exposicion. Ejemplo: 0.3',
                    type=float,
                    default=0.75)
parser.add_argument('--minimum_percent_match_detection',
                    help='Porcentaje minimo para considerar valido un match de la deteccion. Ejemplo: 0.7',
                    type=float,
                    default=0.2)
parser.add_argument('--show_matched_frames',
                    help='Mostrar los frames que hayan producido match',
                    type=bool,
                    default=True)

args = parser.parse_args()
path_video = args.path_video
path_model = args.path_model
path_labelmap = args.path_labelmap
minimum_exposition_time = args.minimum_exposition_time
minimum_percent_frames = args.minimum_percent_frames
minimum_percent_match_detection = args.minimum_percent_match_detection
show_matched_frames = args.show_matched_frames

larg = []
[larg.extend(a) for a in args._get_kwargs()]
print('\nArgumentos:'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}\n'.format(*larg))

# segundos
# minimum_exposition_time = 3
# porcentaje minimo del total de frames para el tiempo de exposicion establecido que se debe cumplir
# minimum_percent_frames = 0.75

# Name of the directory containing the object detection module we're using
# MODEL_NAME = 'graphs/directv'
# RELATIVE_PATH_VIDEO = 'test_videos/Colo_Colo_UEspanola2017__colocolo_2017.avi'

# Grab path to current working directory
CWD_PATH = os.getcwd()

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
# path_model = os.path.join(CWD_PATH, MODEL_NAME, 'frozen_inference_graph.pb')

# Path to label map file
# path_labelmap = os.path.join(CWD_PATH, 'data', 'label_map.pbtxt')

# Path to video
# path_video = os.path.join(CWD_PATH, RELATIVE_PATH_VIDEO)
video_name = os.path.splitext(os.path.basename(path_video))[0]

# Number of classes the object detector can identify
NUM_CLASSES = 2

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(path_labelmap)
categories = label_map_util.convert_label_map_to_categories(label_map,
                                                            max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(path_model, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)

# Define input and output tensors (i.e. data) for the object detection classifier

# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# Number of objects detected
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

# Open video file
video = cv2.VideoCapture(path_video)
fps = int(video.get(cv2.CAP_PROP_FPS))
frame_size = int(video.get(cv2.CAP_PROP_FRAME_WIDTH)), int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
nframes_match = 0
frame = 'first_time'
total_frames_video = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
valids_exposition_intervals = []
matched_frames = []
nframe = 0
template_path_frame = 'test_videos/matched_frames/{}/frame{}_frame_match{}_position{}.jpg'\
    .format(video_name, '{}', '{}', '{}')
if not os.path.isdir('test_videos'):
    os.mkdir('test_videos')
if not os.path.isdir('test_videos/matched_frames'):
    os.mkdir('test_videos/matched_frames')
if not os.path.isdir('test_videos/matched_frames/{}'.format(video_name)):
    os.mkdir('test_videos/matched_frames/{}'.format(video_name))
if not os.path.isdir('test_videos/valids_exposition_intervals'):
    os.mkdir('test_videos/valids_exposition_intervals')
if not os.path.isdir('test_videos/valids_exposition_intervals/{}'.format(video_name)):
    os.mkdir('test_videos/valids_exposition_intervals/{}'.format(video_name))

while video.isOpened() and frame is not None:

    # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
    ret, frame = video.read()
    if frame is not None:
        frame_expanded = np.expand_dims(frame, axis=0)

        # Perform the actual detection by running the model with the image as input
        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: frame_expanded})

        # Draw the results of the detection (aka 'visulaize the results')
        has_boxes = vis_util.visualize_boxes_and_labels_on_image_array(
                    frame,
                    np.squeeze(boxes),
                    np.squeeze(classes).astype(np.int32),
                    np.squeeze(scores),
                    category_index,
                    use_normalized_coordinates=True,
                    line_thickness=8,
                    min_score_thresh=minimum_percent_match_detection)[1]

        # All the results have been drawn on the frame, so it's time to display it.
        if has_boxes > 0:
            nframes_match += 1
            matched_frame = {
                'number_frame': nframe,
                'number_frame_match': nframes_match,
                'position': video.get(cv2.CAP_PROP_POS_MSEC)
            }
            path_frame = template_path_frame.format(*matched_frame.values())
            matched_frame['path'] = path_frame
            # if show_matched_frames:
            #     cv2.imshow('Object detector', frame)
            matched_frames.append(matched_frame)
            cv2.imwrite(path_frame, frame)
            cv2.imshow('Object detector', frame)
        # Press 'q' to quit
        if cv2.waitKey(1) == ord('q'):
            break
        nframe += 1

# Clean up
video.release()
cv2.destroyAllWindows()
print('frames_match', nframes_match)

# implementar regla de los 3 segundos
# se toma cada frame, y su busca sus siguentes que esten dentro del tiempo establecido de exposicion
# y se hace el calculo de procentaje de frames minimo


def get_difference_between_intervals(minuend_interval, subtrahend_interval):
    difference = [minuend_frame for minuend_frame in minuend_interval
                  if minuend_frame['position'] > subtrahend_interval[-1]['position']]

    return difference


def is_valid_interval(interval, frames_per_second, minimum_percent):
    return len(interval) >= get_minimum_frames_to_interval(interval, frames_per_second, minimum_percent)


def join_exposition_intervals(overlapped_interval, new_interval, frames_per_second, minimum_percent):
    difference = get_difference_between_intervals(new_interval, overlapped_interval)
    join_interval = [*overlapped_interval, *difference]
    if not is_valid_interval(join_interval, frames_per_second, minimum_percent):
        join_interval = None

    return join_interval


def get_minimum_frames_to_interval(interval, frames_per_second, minimum_percent):
    start_position = timedelta(milliseconds=interval[0]['position'])
    end_position = timedelta(milliseconds=interval[-1]['position'])
    length_interval = int((end_position - start_position).total_seconds())
    total_interval_frames = length_interval * frames_per_second

    minimum_frames_to_interval = int(total_interval_frames * minimum_percent)

    return minimum_frames_to_interval


default_minimum_nframes_for_exposition = int((minimum_exposition_time * fps) * minimum_percent_frames)
mf = {}
n_mf = 0
for mf in matched_frames:
    max_time_interval = mf['position'] + minimum_exposition_time * 1000
    frames_interval = [f for f in matched_frames
                       if mf['position'] <= f['position'] <= max_time_interval]
    if len(frames_interval) >= default_minimum_nframes_for_exposition:
        overlapped_interval = None
        for valid_interval in valids_exposition_intervals:
            if any(frames_interval[0]['position'] == f['position'] for f in valid_interval):
                overlapped_interval = valid_interval
        if overlapped_interval is None:
            valids_exposition_intervals.append(frames_interval)
        else:
            print("Antes frames_interval[0]['number_frame']:{}"
                  "\n\tframes_interval[-1]['number_frame']:{}"
                  "\n\toverlapped_interval[0]['number_frame']:{}"
                  "\n\toverlapped_interval[-1]['number_frame']:{}"
                  .format(frames_interval[0]['number_frame'],
                          frames_interval[-1]['number_frame'],
                          overlapped_interval[0]['number_frame'],
                          overlapped_interval[-1]['number_frame']))
            frames_interval = join_exposition_intervals(overlapped_interval,
                                                        frames_interval,
                                                        fps,
                                                        minimum_percent_frames)
            if frames_interval is not None:
                print(
                    "Despues frames_interval[0]['number_frame']:{}"
                    "\n\tframes_interval[-1]['number_frame']:{}"
                    "\n\toverlapped_interval[0]['number_frame']:{}"
                    "\n\toverlapped_interval[-1]['number_frame']:{}"
                    .format(frames_interval[0]['number_frame'],
                            frames_interval[-1]['number_frame'],
                            overlapped_interval[0]['number_frame'],
                            overlapped_interval[-1]['number_frame']))
                valids_exposition_intervals.remove(overlapped_interval)
                valids_exposition_intervals.append(frames_interval)

start_interval = {}
end_interval = {}
template_name_video_interval = 'test_videos/valids_exposition_intervals/{}/' \
                               '{}__interval{}__start{}_{}__end{}_{}__firstframe{}__lastframe{}.mp4'\
    .format(video_name, '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')
for index, valid_interval in enumerate(valids_exposition_intervals):
    start_interval['minute'], start_interval['second'] = divmod(int(valid_interval[0]['position'] / 1000), 60)
    end_interval['minute'], end_interval['second'] = divmod(int(valid_interval[-1]['position'] / 1000), 60)
    name_video_interval = template_name_video_interval\
        .format(os.path.splitext(os.path.basename(path_video))[0],
                index,
                start_interval['minute'],
                start_interval['second'],
                end_interval['minute'],
                end_interval['second'],
                valid_interval[0]['number_frame'],
                valid_interval[-1]['number_frame']
                )
    print('\nIntervalo valido numero', index)
    print('\tInicio intervalo: {}:{}  Fin intervalo: {}:{}'
          .format(*start_interval,
                  *end_interval)
          )
    print('Primer frame: {}  \nultimo frame: {}'.format(json.dumps(valid_interval[0], indent=1),
                                                        json.dumps(valid_interval[-1], indent=1)))
    out = cv2.VideoWriter(filename=name_video_interval,
                          fourcc=cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
                          fps=fps,
                          frameSize=frame_size)
    for f in valid_interval:
        out.write(cv2.imread(f['path']))
