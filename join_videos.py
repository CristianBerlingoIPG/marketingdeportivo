import cv2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--videos_to_join',
                    help='Lista de las rutas de los videos a unir. '
                         'Ejemplo: /home/username/videos/video01.mp4 path_relative/video02.mp4 video03.avi',
                    nargs='+',
                    required=True)
parser.add_argument('--path_output',
                    help='Ruta del video de salida.',
                    required=True)
parser.add_argument('--fps',
                    help='Frames por segundo del video de salida.',
                    type=int,
                    required=True)
parser.add_argument('--frame_size',
                    help='Dimensiones del video resultante en ancho y altura. Ejemplo: 1280 720',
                    nargs='+',
                    type=int,
                    required=True)

args = parser.parse_args()
videos_to_join = args.videos_to_join
path_output = args.path_output
fps = args.fps
frame_size = tuple(args.frame_size)

print('args:\n', args)

# Es necesario probar la conbinación de extension de archivo y codec de compresion ya que esto puede variar segun SO
# https://www.pyimagesearch.com/2016/02/22/writing-to-video-with-opencv/

# funciona pero el size del video es mucho mayor
# out = cv2.VideoWriter(filename=path_output,
#                       fourcc=cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
#                       fps=fps,
#                       frameSize=frame_size)

# mejor tasa de compresion que MJPG (aprox. la mitad de size), aunque posiblemente peor que H264\X264, que no
# estan soportados por opencv-python https://github.com/skvark/opencv-python/issues/100
out = cv2.VideoWriter(filename=path_output,
                      fourcc=cv2.VideoWriter_fourcc('X', 'V', 'I', 'D'),
                      fps=fps,
                      frameSize=frame_size)

for path_video in videos_to_join:
    print('Leyendo video', path_video)
    capture = cv2.VideoCapture(path_video)
    total_frames = capture.get(cv2.CAP_PROP_FRAME_COUNT)
    print('total_frames', total_frames)
    nframe = 0
    frame = 'first_time'
    while capture.isOpened() and frame is not None:
        ret, frame = capture.read()
        if frame is not None:
            if nframe % 500 == 0:
                print('path_video', path_video, 'nframe', nframe)
            out.write(frame)
        nframe += 1
    capture.release()

print('Proceso terminado')
