import os
import sys
import time
import logging
import argparse
import configparser

from datetime import datetime, timedelta
from zipfile import ZipFile, BadZipFile, LargeZipFile

import utils_cris as uc
from utils_cris import LoggingType as lt

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil, ZipValuesIfExists
from logo_detection_persistence import ImplementationIntervalsValidatonRule, LogoDetectionPersistence

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=15)
parser.add_argument('-cty',
                    '--country_id',
                    type=int,
                    help='Id del pais')
parser.add_argument('-m',
                    '--mod',
                    type=int,
                    help='Modulo por el cual se define de que porcion de los CSV sin procesar se encargara este '
                         'proceso. Por defecto 1, lo que significa que toma todos los CSV (en HistoryCSVProcessed) '
                         'cuyo ID % --mod = --result_mod. En caso de estar definido este parametro como 1 el '
                         'parametro --result_mod siempre valdra 0',
                    default=1)
parser.add_argument('-rm',
                    '--result_mod',
                    type=int,
                    help='Resultado que debe cumplir ID % --mod del CSV para ser procesado por este proceso. '
                         'En caso de ser => --mod el proceso se interrumpira lanzando una excepcion antes de '
                         'iniciar. Por defecto 0.',
                    default=0)

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
country_id = args.country_id
mod = args.mod
result_mod = args.result_mod if mod != 1 else 0

if mod <= 0:
    raise ValueError('El argumento --mod no puede ser < 1')
if result_mod >= mod:
    raise ValueError('El argumento --result_mod no puede ser >= a --mod')

########################################################################################################################
# logging configuration
datetime_log = datetime.now()

template_general_log_path = 'logs/etl_intervals/{}/{}/mod{}/{}/etl_intervals{}__{}.log' \
    .format('{}',
            '{}',
            str(result_mod).zfill(2),
            'info',
            str(result_mod).zfill(2),
            '{}')

# archivo de log general
log_general_path = template_general_log_path \
    .format(datetime_log.year,
            str(datetime_log.month).zfill(2),
            datetime_log.strftime('%Y%m%d_h%Hm%M'))

uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################

configs = configparser.ConfigParser()
configs.read(config_path)
database_config = configs['Database']
ssh_scp_configs = configs['SSH_SCP']

driver = database_config['driver']
server_sql = database_config['server']
database = database_config['database']
instance = database_config['instance'] if 'instance' in database_config else None
uid = database_config['uid'] if 'uid' in database_config else None
pwd = database_config['pwd'] if 'pwd' in database_config else None
schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
table_history = database_config['table_history'] if 'table_history' in database_config else None
sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

username = ssh_scp_configs['username']
password = ssh_scp_configs['password']

server_sql = server_sql if instance is None else '{},{}'.format(server_sql, sqlserverport.lookup(server_sql, instance))

CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                          server_sql,
                                          database,
                                          uid,
                                          pwd)

larg = []
[larg.extend(a) for a in args._get_kwargs()]
logging_message_configs = '\nParameters:' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}' \
                          '\n\t--{} {}'.format(*larg)

logging_message_configs += '\nConfigurations database:' \
                           '\n\tDriver: {}' \
                           '\n\tServer: {}' \
                           '\n\tDatabase: {}' \
                           '\n\tUID: {}' \
                           '\n\tSchema history: {}' \
                           '\n\tTable history: {}' \
                           '\n\tConnection string: {}'.format(driver,
                                                              server_sql,
                                                              database,
                                                              uid,
                                                              schema_history,
                                                              schema_history,
                                                              cssl.replace(pwd, '*******'))

logging_message_configs += '\nConfigurations SSH_SCP' \
                           '\n\tusername: {}\n'.format(username)

uc.print_and_logging(logging_message_configs)

dal = DAL(cssl)

while True:
    try:
        start_time = datetime.now()

        sleep_until = start_time + timedelta(minutes=waiting_time_iteraction)

        clients = dal.get_clients()
        servers = dal.get_servers(country_id)

        for client in clients:
            uc.print_and_logging('Cliente: {}'.format(client))
            # csvs_without_intervals_process = dal.get_csv_without_interval_process_by_client_sort_by_date(2,
            #                                                                                              mod,
            #                                                                                              result_mod)

            csvs_without_intervals_process = \
                dal.get_csv_without_interval_or_ocr_process_by_client_sort_by_date(2, mod, result_mod)

            # solo para prueba de validez de reconocimiento
            # csvs_without_intervals_process = [dal.get_history_csv_processed_by_id(11600)]

            uc.print_and_logging(
                'Cantidad de CSVs sin ejecutar proceso de intervalos: {}'.format(len(csvs_without_intervals_process)))
            for csv_wip in csvs_without_intervals_process:
                # checkeo para ver si es necesario crear nuevos archivos de log
                datetime_log = uc.restart_daily_logging_file(datetime_log,
                                                             template_general_log_path,
                                                             initial_message=logging_message_configs)

                try:
                    basename_csv_file = os.path.basename(csv_wip.path)
                    dir_name_csv_file = os.path.dirname(csv_wip.path)
                    dt_csv_wip = LogoDetectionPersistence.get_datetime_from_filename_csv(basename_csv_file)

                    iivr = ImplementationIntervalsValidatonRule(client.id,
                                                                country_id,
                                                                cssl,
                                                                execute_since=dt_csv_wip,
                                                                execute_until=dt_csv_wip + timedelta(hours=1))
                    uc.print_and_logging('\nObteniendo intervalidos validos para:'
                                         '\n\tarchivo: {}'
                                         '\n\tclient_id: {}'
                                         '\n\tcountry_id: {}'
                                         '\n\texecute_since: {}'
                                         '\n\texecute_until: {}'.format(csv_wip.path,
                                                                        client.id,
                                                                        country_id,
                                                                        dt_csv_wip,
                                                                        dt_csv_wip + timedelta(hours=1)))

                    intervals = iivr.get_valid_intervals(vehicle_id=csv_wip.vehicle_id,
                                                         source_signal_id=csv_wip.source_signal_id,
                                                         path_file_raw_data=csv_wip.path)

                    n_finded_intervals = len(intervals)
                    uc.print_and_logging('Cantidad de intervalos encontrados: {}'.format(n_finded_intervals))

                    csv_wip.process_interval = 1
                    csv_wip.process_ocr = 0 if len(intervals) > 0 else 1
                    uc.print_and_logging('Actualizando el valor de history_csv_processed: {}'.format(csv_wip))
                    dal.write_history_csv_processed(history_csv_processed=csv_wip)

                    if n_finded_intervals > 0:
                        uc.print_and_logging('Ejecutando filtro de OCR')
                        server_owner = [sv for sv in servers.values()
                                        if sv.is_the_owner_of_the_source_signal(csv_wip.source_signal_id)][0]
                        uc.print_and_logging('Inicializando SshUtil(server_owner.ip={}, username={}, password={})'
                                             .format(server_owner.ip, username, '******'))
                        ssh = SshUtil(server_owner.ip, username, password)

                        zip_path_file = csv_wip.path.replace('.csv', '.zip')
                        zip_max_attempts = 20
                        for n_interval, interval in enumerate(intervals):
                            uc.print_and_logging('Agregando al zip {} las imagenes del intervalo [{}/{}]'
                                                 .format(zip_path_file, n_interval + 1, n_finded_intervals))

                            for attempt in range(1, zip_max_attempts + 1):
                                uc.print_and_logging('\tIntento numero: {}'.format(attempt))
                                try:
                                    result_zip = ssh.zip(
                                        path_input=os.path.dirname(zip_path_file),
                                        path_output=os.path.basename(zip_path_file),
                                        pattern_input=interval.generate_glob_pattern(),
                                        if_exists=ZipValuesIfExists.REMOVE if n_interval == 0
                                        else ZipValuesIfExists.ADDING,
                                        verbose=False)
                                except ValueError as ve_zip:
                                    uc.print_and_logging('\tIntento [{}/{}] fallido:\n{}'.format(attempt,
                                                                                                 zip_max_attempts,
                                                                                                 ve_zip))
                                if result_zip == zip_path_file:
                                    uc.print_and_logging('\tIntento [{}/{}] exitoso'.format(attempt, zip_max_attempts))
                                    break
                                else:
                                    uc.print_and_logging('\tIntento [{}/{}] fallido'.format(attempt, zip_max_attempts))

                                if attempt == zip_max_attempts:
                                    max_attemps_reached = 'Se llego al limite de intentos para generar el zip {}' \
                                        .format(zip_path_file)
                                    uc.print_and_logging(max_attemps_reached)
                                    raise ValueError(max_attemps_reached)

                        uc.print_and_logging('Descargando zip {}'.format(zip_path_file))
                        ssh.get_file_scp(path_remote_file=zip_path_file,
                                         path_output=zip_path_file,
                                         create_path_output=True)

                        try:
                            uc.print_and_logging('Extrayendo imagenes en {}'.format(os.path.dirname(zip_path_file)))
                            zip_ref = ZipFile(zip_path_file, 'r')
                            zip_ref.extractall(os.path.dirname(zip_path_file))
                            zip_ref.close()
                        except (BadZipFile, LargeZipFile) as zip_exception:
                            raise ValueError('Ocurrio una excepcion durante la lectura del zip:\n{}'
                                             .format(zip_exception))

                        uc.print_and_logging('Ejecutando filtro OCR')
                        intervals_by_recognition = iivr.execute_filter_ocr(intervals)
                        uc.print_and_logging('Cantidad de intervalos que pasaron filtro OCR: {}'
                                             .format(len(intervals_by_recognition)))
                        if len(intervals_by_recognition) > 0:
                            iivr.write_valid_intervals_to_sql(intervals=intervals_by_recognition)
                        csv_wip.process_ocr = 1
                        uc.print_and_logging('actualizando el valor de history_csv_processed: {}'.format(csv_wip))
                        dal.write_history_csv_processed(history_csv_processed=csv_wip)
                except KeyboardInterrupt as ki:
                    uc.print_and_logging('Saliendo por KeyboardInterrupt')
                    sys.exit()
                except Exception as e:
                    uc.print_and_logging('Sucedio una exception con el HistoryCSVProcessed:\n{}\n{}'.format(csv_wip, e),
                                         logging_type=lt.EXCEPTION)

        uc.print_and_logging('Proceso de carga terminado')

        seconds_to_sleep = (sleep_until - datetime.now()).total_seconds()
        if seconds_to_sleep > 0:
            uc.print_and_logging('En sleep hasta: {}\n'.format(sleep_until.strftime('%H:%M:%S')))
            time.sleep(seconds_to_sleep)

    except KeyboardInterrupt as ki:
        uc.print_and_logging('Saliendo por KeyboardInterrupt')
        sys.exit()
    except Exception as e:
        uc.print_and_logging('Ocurrio una excepcion', logging_type=lt.EXCEPTION)
