import configparser

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil
from DAL.ssh import scp

configs = configparser.ConfigParser()
configs.read('etl_csv_files_to_intermediate_server.ini')
ssh_scp_configs = configs['SSH_SCP']
capture_servers = ssh_scp_configs['ssh_scp_configs'].split(',')
username = ssh_scp_configs['username']
password = ssh_scp_configs['password']

database_config = configs['Database']
driver = database_config['driver']
server = database_config['server']
database = database_config['database']
instance = database_config['instance'] if 'instance' in database_config else None
uid = database_config['uid'] if 'uid' in database_config else None
pwd = database_config['pwd'] if 'pwd' in database_config else None
schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
table_history = database_config['table_history'] if 'table_history' in database_config else None
sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False


server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                          server,
                                          database,
                                          uid,
                                          pwd)


dal = DAL(connection_string=cssl)
history_updates = dal.get_history_updates()
csvs_processed = [hu.PathFile for hu in history_updates]

for server in capture_servers:
    ssh = SshUtil(server, username, password)
    csvs_item_real_time = ssh.get_find('/home/enzoscocco/Documents/object_detection/data_detection/',
                                       '20[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]__[0-9]*__[0-9]*__.csv')
    csvs_not_processed = [csv for csv in csvs_item_real_time if csv not in csvs_processed]
    message_logging = 'Cantidad de nuevos archivos CSV sin procesar encontrados {}'.format(len(csvs_not_processed))
