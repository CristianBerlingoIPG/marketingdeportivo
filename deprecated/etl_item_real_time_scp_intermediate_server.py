import os
import time
import logging
import argparse
import configparser

from datetime import datetime, timedelta

from logo_detection_persistence import LogoDetectionPersistence

import utils_cris as uc

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil
from DAL.ssh import scp

import sys

# sys.path.append('/home/enzoscocco/projects/ETL_Excel_Generic')

# from LoaderExcel.LoaderExcel import LoaderExcel

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=15)
parser.add_argument('-cty',
                    '--country_id',
                    type=int,
                    help='Id del pais')

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
country_id = args.country_id

log_general_launcher = 'logs/etl_item_real_time__{}.log'.format(datetime.now().strftime('%Y-%m-%d'))
logging.basicConfig(filename=log_general_launcher, level=logging.INFO, format='%(asctime)s - %(message)s', filemode='w')

try:

    configs = configparser.ConfigParser()
    configs.read(config_path)

    config_data_detection = configs['data_detection']
    database_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    base_path_output = config_data_detection['base_path_output']
    template_foldersname_frames_detected_datetime = \
        config_data_detection['template_foldersname_frames_detected_datetime']
    template_foldersname_frames_detected = config_data_detection['template_foldersname_frames_detected']
    path_config_csv = config_data_detection['path_config_csv']
    key_configuration_file = config_data_detection['key_configuration_file']
    csv_pattern = config_data_detection['csv_pattern']

    driver = database_config['driver']
    server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None
    schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
    table_history = database_config['table_history'] if 'table_history' in database_config else None
    sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

    # capture_servers = ssh_scp_configs['capture_servers'].split(',')
    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              server,
                                              database,
                                              uid,
                                              pwd)

    uc.print_and_logging('\nConfigurations detection:'
                         '\n\tbase_path_output: {}'
                         '\n\tcsv_pattern: {}'.format(base_path_output,
                                                      csv_pattern))

    uc.print_and_logging('Configurations database:')
    uc.print_and_logging('    Driver:               ' + driver)
    uc.print_and_logging('    Server:               ' + server)
    uc.print_and_logging('    Database:             ' + database)
    uc.print_and_logging('    Uid:                  ' + str(uid))
    uc.print_and_logging('    Schema history:       ' + str(schema_history))
    uc.print_and_logging('    Table history:      \n' + str(table_history))

    uc.print_and_logging('\nConfigurations SSH_SCP'
                         '\n\tusername: {}'.format(username))

    dal = DAL(connection_string=cssl)

    while True:
        start_time = datetime.now()
        capture_servers = dal.get_servers(country_id).values()
        sleep_until = start_time + timedelta(minutes=waiting_time_iteraction)
        for server in capture_servers:
            try:
                uc.print_and_logging('\n\nServer: {}'.format(server))
                ssh = SshUtil(server.ip, username, password)

                # history_updates = dal.get_history_updates()
                history_csv_processed = dal.get_history_csv_processed()

                uc.print_and_logging('Ejecutando busqueda de archivos CSV.')
                csvs_processed = [hcp.path for hcp in history_csv_processed]

                csvs_item_real_time = sorted(
                    ssh.get_find(base_path_output,
                                 csv_pattern)
                )
                uc.print_and_logging('Se encontraron {} CSV  en el server {} ip {}'.format(len(csvs_item_real_time),
                                                                                           server.id,
                                                                                           server.ip))

                csvs_not_processed = [csv for csv in csvs_item_real_time if csv not in csvs_processed]

                message_logging = 'Cantidad de nuevos archivos CSV sin procesar encontrados {}' \
                    .format(len(csvs_not_processed))

                uc.print_and_logging(message_logging)

                clients = dal.get_clients()

                csv_start_time = start_time.replace(minute=0, second=0, microsecond=0)
                csvs_not_processed_concluded = [
                    csv_np for csv_np in csvs_not_processed
                    if LogoDetectionPersistence.get_datetime_from_filename_csv(
                        os.path.basename(csv_np)) != csv_start_time]

                ssh.get_files_scp(csvs_not_processed_concluded, copy_folders_structure=True, create_path_output=True)

                for csv_npc in csvs_not_processed_concluded:
                    basename_file = os.path.basename(csv_npc)
                    vehicle_id = LogoDetectionPersistence.get_vehicle_id_from_filename_csv(basename_file)
                    source_signal_id = LogoDetectionPersistence.get_source_signal_id_from_filename_csv(basename_file)

                    uc.print_and_logging('Subiendo archivo: {}'.format(csv_npc))

                    # loader_excel = LoaderExcel(os.path.abspath(csv_np),
                    #                            key_configuration_file,
                    #                            cssl,
                    #                            schema_history=schema_history,
                    #                            table_history=table_history,
                    #                            sql_server_linux=sql_server_linux,
                    #                            path_config_excels=path_config_csv)

                    # loader_excel.excel_to_sql(True)
                    uc.print_and_logging('Borrando items real time con PathFileRawData {}'.format(csv_npc))
                    dal.delete_items_real_time_by_csv_path(csv_npc)
                    inserted_irts = dal.inserts_item_reals_time_from_csv(csv_npc)
                    uc.print_and_logging('Se insertaron {} items real time'.format(len(inserted_irts)))
                    if len(inserted_irts) > 0:
                        uc.print_and_logging('Insertando HistoryCSVProcessed para todos los clientes:'
                                             '\n\tpath:{}'
                                             '\n\tvehicle_id:{}'
                                             '\n\tsource_signal_id:{}'.format(csv_npc,
                                                                              vehicle_id,
                                                                              source_signal_id))
                        dal.write_blank_history_csv_processed_for_all_clients(csv_npc, vehicle_id,
                                                                              source_signal_id, server.id)

                uc.print_and_logging('Proceso de carga terminado\n')
            except Exception as e:
                logging.exception('Ocurrio una excepcion:\n')
                print('Ocurrio una excepcion:\n')
                if e is not None:
                    print(e)

        seconds_to_sleep = (sleep_until - datetime.now()).total_seconds()
        if seconds_to_sleep > 0:
            uc.print_and_logging('En sleep hasta: {}'.format(sleep_until.strftime('%H:%M:%S')))
            time.sleep(seconds_to_sleep)

except Exception as e:
    logging.exception('Ocurrio una excepcion:\n')
    print('Ocurrio una excepcion:\n')
    if e is not None:
        print(e)
