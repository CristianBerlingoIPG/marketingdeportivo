import os
import sys
import csv
import time
import logging
import argparse
import configparser

from datetime import datetime, timedelta

import utils_cris as uc
from utils_cris import LoggingType as lt
from logo_detection_persistence import LogoDetectionPersistence as ldp

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil
from DAL.model.item_real_time import ItemRealTime


# sys.path.append('/home/enzoscocco/projects/ETL_Excel_Generic')

# from LoaderExcel.LoaderExcel import LoaderExcel

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=15)
parser.add_argument('-cty',
                    '--country_id',
                    type=int,
                    help='Id del pais')

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
country_id = args.country_id

PATH_TMP_CSVS = 'tmp/CSVs'

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
# template_general_log_path = 'logs/etl_video_intervals/{}/{}/info/etl_video_intervals__{}.log'
template_general_log_path = 'logs/etl_item_real_time/{}/{}/info/etl_item_real_time__{}.log'
# archivo de log general
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################


def transform_csv_float_precision(_csv_path, start_row=1):
    transformed_csv = None
    items_real_time = []
    with open(_csv_path, newline='') as csvfile:
        nrows = len(list(csv.reader(csvfile, delimiter=',', quotechar='|'))) - start_row
        if nrows > 0:
            csvfile.seek(0)
            rows = csv.reader(csvfile, delimiter=',', quotechar='|')
            transformed_csv = _csv_path.replace('.csv', 'for_bcp.csv')
            for index, row in enumerate(rows):
                if index > start_row:
                    try:
                        items_real_time.append(ItemRealTime(*row[:-1], recognized_text=row[-1]))
                    except Exception as transform_exception:
                        uc.print_and_logging('Ocurrio una excepcion en la funcion de transformado del CSV.'
                                             '\nrow {}: {}'.format(index, row),
                                             logging_type=lt.EXCEPTION)

    if len(items_real_time) > 0:
        header_csv = 'VehicleID,SourceSignalID,LogoID,Date,Path,Score,BoxYMin,BoxXMin,BoxYMax,BoxXMax,' \
                     'PathFileRawData,RecognizedText\n'
        with open(transformed_csv, 'w', newline='') as writer:
            writer.write(header_csv + '\n'.join([irt.to_csv() for irt in items_real_time]) + '\n')

    return transformed_csv, len(items_real_time)


try:
    configs = configparser.ConfigParser()
    configs.read(config_path)

    config_data_detection = configs['data_detection']
    database_config = configs['Database']
    database_local_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    base_path_output = config_data_detection['base_path_output']
    template_foldersname_frames_detected_datetime = \
        config_data_detection['template_foldersname_frames_detected_datetime']
    template_foldersname_frames_detected = config_data_detection['template_foldersname_frames_detected']
    path_config_csv = config_data_detection['path_config_csv']
    key_configuration_file = config_data_detection['key_configuration_file']
    csv_pattern = config_data_detection['csv_pattern']

    driver = database_config['driver']
    server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None
    schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
    table_history = database_config['table_history'] if 'table_history' in database_config else None
    sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

    # capture_servers = ssh_scp_configs['capture_servers'].split(',')
    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              server,
                                              database,
                                              uid,
                                              pwd)

    logging_message_configs = '\nConfigurations detection:' \
                              '\n\tbase_path_output: {}' \
                              '\n\tcsv_pattern: {}'.format(base_path_output,
                                                           csv_pattern)

    logging_message_configs += '\nConfigurations database:' \
                               '\n\tDriver: {}' \
                               '\n\tServer: {}' \
                               '\n\tDatabase: {}' \
                               '\n\tUID: {}' \
                               '\n\tSchema history: {}' \
                               '\n\tTable history: {}' \
                               '\n\tConnection string: {}'.format(driver,
                                                                  server,
                                                                  database,
                                                                  uid,
                                                                  schema_history,
                                                                  schema_history,
                                                                  cssl.replace(pwd, '*******'))

    logging_message_configs += '\nConfigurations SSH_SCP' \
                               '\n\tusername: {}\n'.format(username)

    uc.print_and_logging(logging_message_configs)

    dal = DAL(connection_string=cssl)

    while True:
        start_time = datetime.now()

        capture_servers = dal.get_servers(country_id).values()
        sleep_until = start_time + timedelta(minutes=waiting_time_iteraction)
        for server in capture_servers:
            try:
                uc.print_and_logging('\n\nServer: {}'.format(server))
                ssh = SshUtil(server.ip, username, password)

                # history_updates = dal.get_history_updates()
                history_csv_processed = dal.get_history_csv_processed()

                uc.print_and_logging('Ejecutando busqueda de archivos CSV.')
                csvs_processed = [hcp.path for hcp in history_csv_processed]

                csvs_item_real_time = sorted(
                    ssh.get_find(base_path_output,
                                 csv_pattern)
                )

                uc.print_and_logging('Se encontraron {} CSV  en el server {} ip {}'.format(len(csvs_item_real_time),
                                                                                           server.id,
                                                                                           server.ip))

                csvs_not_processed = [csv for csv in csvs_item_real_time if csv not in csvs_processed]

                clients = dal.get_clients()
                vehicles = dal.get_vehicles()
                vehicles_ids = [v.id for v in vehicles]

                csv_start_time = start_time.replace(minute=0, second=0, microsecond=0)

                csvs_not_processed_concluded = [
                    csv_np for csv_np in csvs_not_processed
                    if
                    ldp.get_datetime_from_filename_csv(os.path.basename(csv_np)) != csv_start_time]

                csvs_with_error_vehicle_id = [
                    _csv_without_existing_vehicle for _csv_without_existing_vehicle
                    in csvs_not_processed_concluded
                    if ldp.get_vehicle_id_from_filename_csv(_csv_without_existing_vehicle) not in vehicles_ids]

                uc.print_and_logging('Cantidad de CSV con vehicles_ids inexistentes: {}'.format(len(csvs_with_error_vehicle_id)))

                csvs_not_processed_concluded = [
                    _csv_with_existing_vehicle for _csv_with_existing_vehicle
                    in csvs_not_processed_concluded
                    if ldp.get_vehicle_id_from_filename_csv(_csv_with_existing_vehicle) in vehicles_ids]

                len_csvs_not_processed_concluded = len(csvs_not_processed_concluded)
                message_logging = 'Cantidad de nuevos archivos CSV sin procesar y conluidos encontrados {}' \
                    .format(len_csvs_not_processed_concluded)
                uc.print_and_logging(message_logging)

                # path_tmp_csvs_not_processed_concluded = '/tmp/csvs_not_processed_concluded_{}.txt'\
                #     .format(datetime.now().strftime('%Y%m%d'))
                # ssh.write_to_file('\n'.join(csvs_not_processed_concluded),
                #                   path_tmp_csvs_not_processed_concluded,
                #                   append=False)

                ssh.get_files_scp(csvs_not_processed_concluded, copy_folders_structure=True, create_path_output=True)

                for n_csv_npc, csv_npc in enumerate(csvs_not_processed_concluded):
                    # checkeo para ver si es necesario crear nuevos archivos de log
                    datetime_log = uc.restart_daily_logging_file(datetime_log,
                                                                 template_general_log_path,
                                                                 initial_message=logging_message_configs)

                    basename_file = os.path.basename(csv_npc)
                    vehicle_id = ldp.get_vehicle_id_from_filename_csv(basename_file)
                    source_signal_id = ldp.get_source_signal_id_from_filename_csv(basename_file)

                    uc.print_and_logging('\nSubiendo archivo [{}/{}] del server id {} ip {}: {}'
                                         .format(n_csv_npc + 1,
                                                 len_csvs_not_processed_concluded,
                                                 server.id,
                                                 server.ip,
                                                 csv_npc))

                    uc.print_and_logging('Borrando items real time con PathFileRawData {}'.format(csv_npc))
                    dal.delete_items_real_time_by_csv_path(csv_npc)

                    inserted_irts = dal.load_items_real_time_through_bcp(csv_npc,
                                                                         transform=transform_csv_float_precision)
                    if inserted_irts > 0:
                        uc.print_and_logging('Insertando HistoryCSVProcessed para todos los clientes:'
                                             '\n\tpath:{}'
                                             '\n\tvehicle_id:{}'
                                             '\n\tsource_signal_id:{}'
                                             '\n\tserver: {}'.format(csv_npc,
                                                                     vehicle_id,
                                                                     source_signal_id,
                                                                     server))
                        try:
                            dal.write_blank_history_csv_processed_for_all_clients(csv_npc,
                                                                                  vehicle_id,
                                                                                  source_signal_id,
                                                                                  server.id)
                            uc.print_and_logging('Actualizando items real time desde staging')
                            dal.update_items_real_time_from_staging(csv_npc)

                        except KeyboardInterrupt as ki:
                            uc.print_and_logging('Saliendo por KeyboardInterrupt')
                            sys.exit()
                        except Exception as e:
                            uc.print_and_logging('{}'.format(e), logging_type=lt.EXCEPTION)

                uc.print_and_logging('Proceso de carga terminado\n')
            except KeyboardInterrupt as ki:
                uc.print_and_logging('Saliendo por KeyboardInterrupt')
                sys.exit()
            except Exception as e:
                uc.print_and_logging('Ocurrio una exception con el server {}'.format(server),
                                     logging_type=lt.EXCEPTION)

        seconds_to_sleep = (sleep_until - datetime.now()).total_seconds()
        if seconds_to_sleep > 0:
            uc.print_and_logging('En sleep hasta: {}\n'.format(sleep_until.strftime('%H:%M:%S')))
            time.sleep(seconds_to_sleep)

except KeyboardInterrupt as ki:
    uc.print_and_logging('Saliendo por KeyboardInterrupt')
    sys.exit()
except Exception as e:
    uc.print_and_logging('Ocurrio una excepcion que no fue captura en los excepts anteriores y llego hasta el superior',
                         logging_type=lt.EXCEPTION)
