import os
import glob
import logging
import argparse
import configparser

from datetime import datetime

from DAL.dal import DAL

parser = argparse.ArgumentParser()
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteraction',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=15)
parser.add_argument('-cty',
                    '--country_id',
                    type=int,
                    help='Id del pais')
parser.add_argument('-s',
                    '--since',
                    help='Intervalos desde que fecha se procesaran')
parser.add_argument('-u',
                    '--until',
                    help='Intervalos hasta que fecha se procesaran')

args = parser.parse_args()
config_path = args.config_path
waiting_time_iteraction = args.waiting_time_iteraction
country_id = args.country_id
since = args.since
until = args.until

log_general_launcher = 'logs/etl_frames_detected__{}.log'.format(datetime.now().strftime('%Y-%m-%d'))
logging.basicConfig(filename=log_general_launcher, level=logging.INFO, format='%(asctime)s - %(message)s', filemode='w')

configs = configparser.ConfigParser()
configs.read(config_path)
database_config = configs['Database']

driver = database_config['driver']
server = database_config['server']
database = database_config['database']
uid = database_config['uid'] if 'uid' in database_config else None
pwd = database_config['pwd'] if 'pwd' in database_config else None
schema_history = database_config['schema_history'] if 'schema_history' in database_config else None
table_history = database_config['table_history'] if 'table_history' in database_config else None
sql_server_linux = database_config['sql_server_linux'] if 'sql_server_linux' in database_config else False

CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                          server,
                                          database,
                                          uid,
                                          pwd)

logging.info('Configurations database:')
logging.info('    Driver:               ' + driver)
logging.info('    Server:               ' + server)
logging.info('    Database:             ' + database)
logging.info('    Uid:                  ' + str(uid))
logging.info('    Schema history:       ' + str(schema_history))
logging.info('    Table history:        ' + str(table_history))

dal = DAL(cssl)

intervals = dal.get_intervals()


