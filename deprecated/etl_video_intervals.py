import os
import sys
import time
import logging
import argparse
import configparser

from datetime import datetime, timedelta
from zipfile import ZipFile, BadZipFile, LargeZipFile

from DAL.dal import DAL
from DAL import sqlserverport
from DAL.ssh.ssh_util import SshUtil
from utils_cris import LoggingType as lt
from drawer_detected_object import DrawerDetectedObject
from logo_detection_persistence import LogoDetectionPersistence as ldp

import utils_cris as uc

parser = argparse.ArgumentParser()
parser.add_argument('-pov',
                    '--path_output_video',
                    help='Ruta de salida de los videos generados.'
                         'Por defecto /home/enzoscocco/Documents/object_detection/data_detection/intervals',
                    default='/home/enzoscocco/Documents/object_detection/data_detection/intervals')
parser.add_argument('-s',
                    '--since',
                    help='Desde que fecha se buscaran intervalos. Por defecto no habra una fecha minima para traer '
                         'intervalos',
                    default=datetime(1900, 1, 1))
parser.add_argument('-u',
                    '--until',
                    help='Hasta que fecha se buscaran intervalos. Por defecto no habra una fecha maxima para traer'
                         'intervalos',
                    default=datetime.now().replace(year=datetime.now().year + 1))
parser.add_argument('-cp',
                    '--config_path',
                    help='Ruta del archivo de configuración',
                    default='etl_item_real_time.ini')
parser.add_argument('-wti',
                    '--waiting_time_iteration',
                    type=int,
                    help='Tiempo de espera entre cada iteraccion en minutos. Por defecto 15.',
                    default=15)
parser.add_argument('-fr',
                    '--frame_rate',
                    help='Tasa de frames de los videos generados para cada intervalo. Por defecto 5',
                    type=int,
                    default=5)
parser.add_argument('-m',
                    '--mod',
                    type=int,
                    help='Modulo por el cual se define de que porcion de los CSV sin procesar se encargara este '
                         'proceso. Por defecto 1, lo que significa que toma todos los CSV (en HistoryCSVProcessed) '
                         'cuyo ID % --mod = --result_mod. En caso de estar definido este parametro como 1 el '
                         'parametro --result_mod siempre valdra 0',
                    default=1)
parser.add_argument('-rm',
                    '--results_mod',
                    help='Alguno de los resultados que debe cumplir ID % --mod del CSV para ser procesado por este '
                         'proceso. En caso de ser => --mod el proceso se interrumpira lanzando una excepcion antes de '
                         'iniciar. Por ejemplo de encargarse el servidor de los resultados 0, 1 y 2 se debe pasar por '
                         'parametro a este script -rm 0,1,2 . Por default el valor es 0',
                    default='0')

args = parser.parse_args()
path_output_video = args.path_output_video
since = args.since
until = args.until
config_path = args.config_path
waiting_time_iteration = args.waiting_time_iteration
frame_rate = args.frame_rate
mod = args.mod
results_mod = [int(rm) for rm in args.results_mod.split(',')] if mod != 1 else [0]

if mod <= 0:
    raise ValueError('El argumento --mod no puede ser < 1')
if any([rm >= mod for rm in results_mod]):
    raise ValueError('El argumento --result_mod no puede tener ningun valor >= a --mod')

########################################################################################################################
# logging configuration
datetime_log = datetime.now()
# archivo de log general
template_general_log_path = 'logs/etl_video_intervals/{}/{}/info/etl_video_intervals__{}.log'
log_general_path = template_general_log_path.format(datetime_log.year,
                                                    str(datetime_log.month).zfill(2),
                                                    datetime_log.strftime('%Y%m%d_h%Hm%M'))

uc.create_folders(log_general_path)
logging.basicConfig(filename=log_general_path,
                    level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    filemode='w')

# archivo de log de errores
log_error_path = log_general_path.replace('info', 'error')
log_error_path = log_error_path.replace('.log', '_ERROR.log')
uc.create_folders(log_error_path)
file_handler_error = logging.FileHandler(log_error_path, 'w')
file_handler_error.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(message)s')
file_handler_error.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(file_handler_error)

########################################################################################################################
try:
    configs = configparser.ConfigParser()
    configs.read(config_path)
    database_config = configs['Database']
    ssh_scp_configs = configs['SSH_SCP']

    driver = database_config['driver']
    server = database_config['server']
    database = database_config['database']
    instance = database_config['instance'] if 'instance' in database_config else None
    uid = database_config['uid'] if 'uid' in database_config else None
    pwd = database_config['pwd'] if 'pwd' in database_config else None

    username = ssh_scp_configs['username']
    password = ssh_scp_configs['password']

    server = server if instance is None else '{},{}'.format(server, sqlserverport.lookup(server, instance))

    CONNECTION_STRING_SQL_LOGIN = 'DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'
    cssl = CONNECTION_STRING_SQL_LOGIN.format(driver,
                                              server,
                                              database,
                                              uid,
                                              pwd)

    larg = []
    [larg.extend(a) for a in args._get_kwargs()]
    logging_message_configs = '\nParameters:' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}' \
                              '\n\t--{} {}'.format(*larg)

    logging_message_configs += '\nConfigurations database:' \
                               '\n\tDriver: {}' \
                               '\n\tServer: {}' \
                               '\n\tDatabase: {}' \
                               '\n\tUID: {}' \
                               '\n\tConnection string: {}'.format(driver,
                                                                  server,
                                                                  database,
                                                                  uid,
                                                                  cssl.replace(pwd, '*******'))
    uc.print_and_logging(logging_message_configs)

    dal = DAL(cssl)

    while True:
        try:
            start_time = datetime.now()
            datetime_log = uc.restart_daily_logging_file(datetime_log,
                                                         template_general_log_path,
                                                         initial_message=logging_message_configs)

            sleep_until = start_time + timedelta(minutes=waiting_time_iteration)

            # intervals = dal.get_intervals_without_video(since, until)
            intervals = dal.get_intervals_without_video_by_mods(since, until, mod=mod, results_mod=results_mod)
            csv_list = []
            [csv_list.append(interval.path_file_raw_data) for interval in intervals
             if interval.path_file_raw_data not in csv_list]
            csv_list.sort(key=lambda _csv: ldp.get_datetime_from_filename_csv(os.path.basename(_csv)))
            qty_csv = len(csv_list)
            logos = dal.get_logos()
            uc.print_and_logging('cantidad de csv {}\ncantidad intervalos {}\ncantidad de logos encontrados {}'
                                 .format(qty_csv, len(intervals), len(logos)))

            for n_csv, csv in enumerate(csv_list):
                try:
                    interval_for_csv = [interval for interval in intervals if
                                        interval.path_file_raw_data == csv]
                    qty_interval_for_csv = len(interval_for_csv)
                    uc.print_and_logging('\n\nProcesando {} intervalos del csv[{}/{}] {}'
                                         .format(qty_interval_for_csv,
                                                 n_csv + 1,
                                                 qty_csv,
                                                 csv))

                    for n_interval, interval in enumerate(interval_for_csv):
                        error_csv = False
                        uc.print_and_logging('obteniendo items real time para el intervalo[{}/{}]:\n\t{}'
                                             .format(n_interval + 1,
                                                     qty_interval_for_csv,
                                                     interval))
                        # traia items de los cuales no se habian descargado sus imagenes
                        items_real_time = dal.get_items_real_time_for_interval_with_indistinct_logo(
                            interval.start_item_real_time_id,
                            interval.end_item_real_time_id,
                            interval.start_time,
                            interval.end_time,
                            interval.vehicle_id,
                            interval.source_signal_id
                        )

                        irts_without_downloaded_images = []
                        [irts_without_downloaded_images.append(wodi.path) for wodi in items_real_time
                         if not os.path.isfile(wodi.path)]

                        if len(irts_without_downloaded_images) > 0:
                            zip_list_names = '\n'.join([os.path.basename(frame_path) for frame_path
                                                        in irts_without_downloaded_images])
                            zip_path_input = os.path.dirname(irts_without_downloaded_images[0])
                            server_interval = dal.get_server_by_interval_id(interval.id)
                            ssh = SshUtil(server_interval.ip, username, password)
                            path_file_list_names = os.path.join(zip_path_input,
                                                                interval.get_file_name_list_frames())

                            uc.print_and_logging('escribiendo archivo con la lista de los frames faltantes '
                                                 'para el intervalo {}'
                                                 '\n\tzip_list_names: {}'
                                                 '\n\tpath_file: {}'.format(interval.id,
                                                                            zip_list_names,
                                                                            path_file_list_names))
                            ssh.write_to_file(text=zip_list_names,
                                              path_file=path_file_list_names,
                                              append=False)

                            zip_path_output = 'missing_frames_interval_{}.zip'.format(interval.id)
                            uc.print_and_logging('creando zip:'
                                                 '\n\tzip_path_input: {}'
                                                 '\n\tzip_list_names: {}'
                                                 '\n\tzip_path_output: {}'.format(zip_path_input,
                                                                                  zip_list_names,
                                                                                  zip_path_output))
                            zip_path_file = ssh.zip_from_file_list(path_input=zip_path_input,
                                                                   name_list=path_file_list_names,
                                                                   path_output=zip_path_output)

                            uc.print_and_logging('Descargando zip {} en carpeta equivalente'.format(zip_path_file))
                            ssh.get_file_scp(path_remote_file=zip_path_file,
                                             path_output=zip_path_file,
                                             create_path_output=True)

                            ssh.rm_file(zip_path_file)

                            try:
                                uc.print_and_logging('Extrayendo imagenes en {}'.format(os.path.dirname(zip_path_file)))
                                zip_ref = ZipFile(zip_path_file, 'r')
                                zip_ref.extractall(os.path.dirname(zip_path_file))
                                zip_ref.close()
                            except (BadZipFile, LargeZipFile) as zip_exception:
                                error_csv = True
                                raise ValueError('Ocurrio una excepcion durante la lectura del zip:\n{}'
                                                 .format(zip_exception))

                        uc.print_and_logging('cantidad de items encontrados: {}'.format(len(items_real_time)))
                        paths_images_with_detected_objects = DrawerDetectedObject \
                            .static_write_images_with_detected_objects(items_real_time,
                                                                       logos)

                        # path_video_interval = None
                        try:
                            path_input = os.path.dirname(items_real_time[0].path)
                            real_path_output_video = os.path.join(
                                path_output_video,
                                'generation_{}/emission_{}'.format(datetime.now().strftime('%Y%m%d'),
                                                                   items_real_time[0].date.strftime('%Y%m%d'))
                            )
                            uc.create_folders(os.path.join(real_path_output_video, interval.get_video_name()),
                                              path_base='/')
                            uc.print_and_logging('generate_video: path_input={}, path_output={}, frame_rate={}'
                                                 .format(path_input,
                                                         real_path_output_video,
                                                         frame_rate))
                            interval.path_video = interval.generate_video(path_input,
                                                                          real_path_output_video,
                                                                          frame_rate=frame_rate)

                            dal.update_interval_path_video(interval.id, interval.path_video)

                        except ValueError as ffmpeg_exc:
                            error_csv = True
                            uc.print_and_logging('Ocurrio una excepcion '
                                                 'durante la generacion del video del intervalo:\n',
                                                 logging_type=lt.EXCEPTION)

                    # cuando se terminan de generar todos los video si no ocurrio algun error se actualiza el campo
                    # process_video a true
                    if not error_csv:
                        dal.update_history_csv_processed_process_video_intervals_and_being_processed(csv, 1)

                except Exception as csv_for:
                    uc.print_and_logging('\n\nOcurrio una excepcion durante el procesamiento de los intervalos del '
                                         '\n\tcsv {} \n\tintervalo {}'.format(csv, interval))


                    # queda a resolver algun metodo para subir el video generado al server de la nube
                    # if path_video_interval is not None:
                    #     subir_video()

            seconds_to_sleep = (sleep_until - datetime.now()).total_seconds()
            if seconds_to_sleep > 0:
                uc.print_and_logging('En sleep hasta: {}\n'.format(sleep_until.strftime('%H:%M:%S')))
                time.sleep(seconds_to_sleep)

        except KeyboardInterrupt as ki:
            uc.print_and_logging('Saliendo por KeyboardInterrupt')
            sys.exit()
        except Exception as e:
            uc.print_and_logging('Ocurrio una excepcion', logging_type=lt.EXCEPTION)

except Exception as e:
    uc.print_and_logging('Ocurrio una excepcion durante la configuracion del proceso:\n', logging_type=lt.EXCEPTION)
