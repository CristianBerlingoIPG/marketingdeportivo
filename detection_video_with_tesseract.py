"""
Sections of this code were taken from:
https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb
"""
import numpy as np

import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import pytesseract

sys.path.append("..")

from utils import label_map_util

from utils import visualization_utils as vis_util

import cv2
import argparse
import ast

parser = argparse.ArgumentParser()
parser.add_argument('--video_path',
                    help='Ruta del video',
                    required=True)
parser.add_argument('--model_path',
                    help='Ruta del modelo',
                    required=True)
parser.add_argument('--labelmap_path',
                    help='Ruta del labelmap',
                    required=True)
parser.add_argument('--device',
                    help='Dispositivo sobre cual ejecutar el procesamiento grafico. '
                         'Ejemplo: /device:GPU:0 para utilizar el primer dispositivo gpu.'
                         '         /cpu:0 para utilizar el CPU.'
                         'Mas informacion: https://www.tensorflow.org/guide/using_gpu.'
                         'Default: /device:GPU:0',
                    default='/device:GPU:0')
parser.add_argument('--path_output_file',
                    help='Path donde se guardara el video con los objetos detectados.'
                         'Si no se configura un path no se guarda el archivo de salida.',
                    default=None)
parser.add_argument('--show_input',
                    help='Boolean que define si se mostrara en una ventana el input.'
                         'Default: False.',
                    default='False')
parser.add_argument('--show_output',
                    help='Boolean que define si se mostrara en una ventana el output.'
                         'Default: False.',
                    default='False')

args = parser.parse_args()

video_path = args.video_path
model_path = args.model_path
labelmap_path = args.labelmap_path
device = args.device
path_output_file = args.path_output_file
show_input = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_input == 'True' else False
show_output = ast.literal_eval(args.show_output) if args.show_output == 'False' or args.show_output == 'True' else False

larg = []
[larg.extend(a) for a in args._get_kwargs()]
print('\nArgumentos:'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}'
      '\n\t--{} {}\n'.format(*larg))



# Path to frozen detection graph. This is the actual model that is used
# for the object detection.

# MODEL_NAME = 'graphs/directv'
# PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
# PATH_TO_LABELS = os.path.join('../../../training', 'object-detection.pbtxt')
# PATH_TO_LABELS = os.path.join('data', 'label_map.pbtxt')

NUM_CLASSES = 1

# sys.path.append("..")


def detect_in_video():

    # VideoWriter is the responsible of creating a copy of the video
    # used for the detections but with the detections overlays. Keep in
    # mind the frame size has to be the same as original video.
    out = cv2.VideoWriter(path_output_file, cv2.VideoWriter_fourcc(
        'M', 'J', 'P', 'G'), 10, (1280, 720))

    with tf.device(device):
        detection_graph = tf.Graph()

    config = tf.ConfigProto(
        log_device_placement=True
    )
    # config.gpu_options.allow_growth = True
    config.gpu_options.per_process_gpu_memory_fraction = 0.3

    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(model_path, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    label_map = label_map_util.load_labelmap(labelmap_path)
    categories = label_map_util.convert_label_map_to_categories(
        label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            # Definite input and output Tensors for detection_graph
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            # Each box represents a part of the image where a particular object
            # was detected.
            detection_boxes = detection_graph.get_tensor_by_name(
                'detection_boxes:0')
            # Each score represent how level of confidence for each of the objects.
            # Score is shown on the result image, together with the class
            # label.
            detection_scores = detection_graph.get_tensor_by_name(
                'detection_scores:0')
            detection_classes = detection_graph.get_tensor_by_name(
                'detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name(
                'num_detections:0')
            cap = cv2.VideoCapture(video_path)

            while cap.isOpened():
                # Read the frame
                ret, frame = cap.read()

                # Recolor the frame. By default, OpenCV uses BGR color space.
                # This short blog post explains this better:
                # https://www.learnopencv.com/why-does-opencv-use-bgr-color-format/
                color_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                image_np_expanded = np.expand_dims(color_frame, axis=0)

                # Actual detection.
                (boxes, scores, classes, num) = sess.run(
                    [detection_boxes, detection_scores,
                        detection_classes, num_detections],
                    feed_dict={image_tensor: image_np_expanded})

                # Visualization of the results of a detection.
                # note: perform the detections using a higher threshold
                has_boxes = vis_util.visualize_boxes_and_labels_on_image_array(
                    color_frame,
                    np.squeeze(boxes),
                    np.squeeze(classes).astype(np.int32),
                    np.squeeze(scores),
                    category_index,
                    use_normalized_coordinates=True,
                    line_thickness=8,
                    min_score_thresh=.8)[1]

                # if has_boxes > 0:
                #     text_founded = pytesseract.image_to_string(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))
                # else:
                #     text_founded = 'None'
                if has_boxes > 1:
                    print('has_boxes', has_boxes)


                # cv2.imshow(video_path + text_founded, color_frame)
                cv2.imshow(video_path, color_frame)
                output_rgb = cv2.cvtColor(color_frame, cv2.COLOR_RGB2BGR)
                out.write(output_rgb)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

            out.release()
            cap.release()
            cv2.destroyAllWindows()


def main():
    detect_in_video()


if __name__ == '__main__':
    main()
