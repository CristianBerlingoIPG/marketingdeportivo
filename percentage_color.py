import numpy as np
import cv2

img = cv2.imread('images/ipgmediabrands_train/056.jpg')

reached_color = [255, 255, 255]  # RGB
diff = 30
boundaries = [([reached_color[2] - diff, reached_color[1] - diff, reached_color[0] - diff],
               [reached_color[2] + diff, reached_color[1] + diff, reached_color[0] + diff])]
# in order BGR as opencv represents images as numpy arrays in reverse order

for (lower, upper) in boundaries:
    lower = np.array(lower, dtype=np.uint8)
    upper = np.array(upper, dtype=np.uint8)
    mask = cv2.inRange(img, lower, upper)
    output = cv2.bitwise_and(img, img, mask=mask)

    ratio_reached_color = cv2.countNonZero(mask) / (img.size / 3)
    reached_color_pixel_percentage = np.round(ratio_reached_color * 100, 2)
    print('reached color pixel percentage:', reached_color_pixel_percentage)

    cv2.imshow('images reached color pixel percentage: {}'.format(reached_color_pixel_percentage),
               np.hstack([img, output]))
    # q
    cv2.waitKey(0)
