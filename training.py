from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import sys

sys.path.append("..")

import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET

########################################################################################################################
# IMPORTS GENERIC GENERATE TFRECORD
########################################################################################################################


import io
import tensorflow as tf
from PIL import Image

from utils import dataset_util
from collections import namedtuple

########################################################################################################################
# IMPORTS TRAIN
########################################################################################################################
import functools
import json

from builders import dataset_builder
from builders import graph_rewriter_builder
from builders import model_builder
from legacy import trainer
from utils import config_util

########################################################################################################################
# IMPORTS EXPORT INFERENCE GRAPH
########################################################################################################################
from google.protobuf import text_format
import exporter
from protos import pipeline_pb2


class Training:
    BASE_PATH_OUTPUT = os.path.join(os.getcwd(), 'data')
    SUFFIX_LABELS_CSV = '_labels.csv'
    LABEL_MAP = 'label_map.pbtxt'
    TRAIN_RECORD = 'train.record'
    TEST_RECORD = 'test.record'
    BASE_PATH_TRAINING = os.path.join(os.getcwd(), 'trainings')
    PIPELINE_CONFIG = 'ssd_mobilenet_v1_pets.config'
    BASE_TRAINED_CHECKPOINT_PREFIX = 'model.ckpt-'
    BASE_PATH_INFERENCE_GRAPHS = os.path.join(os.getcwd(), 'inference_graphs')
    PATH_TEMPLATE_PIPELINE_CONFIG = os.path.join(os.getcwd(), 'template_ssd_mobilenet_v1_pets.config')
    TEMPLATE_PIPELINE_CONFIG_REPLACE_NUM_CLASSES = 'REPLACE_NUM_CLASSES'
    TEMPLATE_PIPELINE_CONFIG_REPLACE_NUM_STEPS = 'REPLACE_NUM_STEPS'
    TEMPLATE_PIPELINE_CONFIG_REPLACE_TRAIN_INPUT_READER = 'REPLACE_TRAIN_INPUT_READER'
    TEMPLATE_PIPELINE_CONFIG_REPLACE_TRAIN_LABEL_MAP_PATH = 'REPLACE_TRAIN_LABEL_MAP_PATH'
    TEMPLATE_PIPELINE_CONFIG_REPLACE_EVAL_INPUT_READER = 'REPLACE_EVAL_INPUT_READER'
    TEMPLATE_PIPELINE_CONFIG_REPLACE_EVAL_LABEL_MAP_PATH = 'REPLACE_EVAL_LABEL_MAP_PATH'

    class ConfigTrain:

        TRAIN_SAVE_SUMMARIES_SECS = 900

        def __init__(self, master='', task=0, num_clones=1, clone_on_cpu=False, worker_replicas=1, ps_tasks=0,
                     train_dir='', pipeline_config_path='', train_config_path='', input_config_path='',
                     model_config_path='', save_summaries_secs=TRAIN_SAVE_SUMMARIES_SECS):
            self.master = master
            self.task = task
            self.num_clones = num_clones
            self.clone_on_cpu = clone_on_cpu
            self.worker_replicas = worker_replicas
            self.ps_tasks = ps_tasks
            self.train_dir = train_dir
            self.pipeline_config_path = pipeline_config_path
            self.train_config_path = train_config_path
            self.input_config_path = input_config_path
            self.model_config_path = model_config_path
            self.save_summaries_secs = save_summaries_secs

    class ConfigExportInferenceGraph:
        def __init__(self, pipeline_config_path, trained_checkpoint_prefix, output_directory, input_type='image_tensor',
                     input_shape=None, config_override='', write_inference_graph=False):
            self.pipeline_config_path = pipeline_config_path
            self.trained_checkpoint_prefix = trained_checkpoint_prefix
            self.output_directory = output_directory
            self.input_type = input_type
            self.input_shape = input_shape
            self.config_override = config_override
            self.write_inference_graph = write_inference_graph

    def __init__(self, training_name, xml_directories, training_num_steps, path_output_data=None,
                 save_summaries_secs=ConfigTrain.TRAIN_SAVE_SUMMARIES_SECS):
        self.training_name = training_name
        self.xml_directories = xml_directories
        self.training_num_steps = training_num_steps
        self.path_output_data = path_output_data \
            if path_output_data is not None \
            else os.path.join(Training.BASE_PATH_OUTPUT, training_name)
        self.paths_labels_csv = []
        self.paths_tfrecords = []
        with open(os.path.join(self.path_output_data, Training.LABEL_MAP)) as f:
            self.label_map = self.parse_text_labelmap_to_dict(f.read())
        self.config_train = Training.ConfigTrain(train_dir=os.path.join(Training.BASE_PATH_TRAINING, training_name),
                                                 pipeline_config_path=os.path.join(Training.BASE_PATH_TRAINING,
                                                                                   training_name,
                                                                                   Training.PIPELINE_CONFIG),
                                                 save_summaries_secs=save_summaries_secs)
        self.config_export_inference_graph = Training.ConfigExportInferenceGraph(
            pipeline_config_path=self.config_train.pipeline_config_path,
            trained_checkpoint_prefix=os.path.join(self.config_train.train_dir,
                                                   Training.BASE_TRAINED_CHECKPOINT_PREFIX + str(training_num_steps)),
            output_directory=os.path.join(Training.BASE_PATH_INFERENCE_GRAPHS, training_name)
        )

    @staticmethod
    def xml_to_csv(path):
        xml_list = []
        for xml_file in glob.glob(path + '/*.xml'):
            tree = ET.parse(xml_file)
            root = tree.getroot()
            for member in root.findall('object'):
                value = (root.find('filename').text,
                         int(root.find('size')[0].text),
                         int(root.find('size')[1].text),
                         member[0].text,
                         int(member[4][0].text),
                         int(member[4][1].text),
                         int(member[4][2].text),
                         int(member[4][3].text)
                         )
                xml_list.append(value)
        column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
        xml_df = pd.DataFrame(xml_list, columns=column_name)
        return xml_df

    def xml_directories_to_csv(self):
        for xml_directory in self.xml_directories:
            xml_df = self.xml_to_csv(xml_directory)
            if not os.path.isdir(self.path_output_data):
                os.mkdir(self.path_output_data)
            path_label_csv = '{}/{}{}'.format(self.path_output_data,
                                              xml_directory.split('/')[-1],
                                              Training.SUFFIX_LABELS_CSV)
            self.paths_labels_csv.append(path_label_csv)
            xml_df.to_csv(path_label_csv,
                          index=None)
            print('Successfully converted xml to csv.')

    ####################################################################################################################
    # GENERIC GENERATE TFRECORD
    ####################################################################################################################

    # python generic_generate_tfrecord.py
    #       --csv_input data/20190122_summit_ipgmediabrands/20190122_summit_ipgmediabrands_test_labels.csv
    #       --image_dir images/20190122_summit_ipgmediabrands_test
    #       --output_path data/20190122_summit_ipgmediabrands/test.record
    #       --path_labelmap data/20190122_summit_ipgmediabrands/label_map.pbtx

    def get_num_classes(self):
        return len(self.label_map)

    def class_text_to_int(self, row_label, labelmap):
        return self.get_int_label(row_label, labelmap)

    @staticmethod
    def get_int_label(label, labelmap):
        try:
            int_label = int([lm for lm in labelmap if lm['name'] == label][0]['id'])
        except Exception as ex_int_label:
            raise ValueError('label: {}, labelmap:{}, exception: {}'.format(label, labelmap, ex_int_label))
        return int_label

    @staticmethod
    def rawitem_to_parseditem(raw_item):
        attributes_pairs = [i.strip().split(':') for i in raw_item.strip().split('\n') if '{' not in i and '}' not in i]
        item = {k.strip().replace('"', ''): v.strip().replace('"', '') for k, v in attributes_pairs}

        return item

    # hecho pensando un labelmap con esta estructura
    # item {
    #   id: 1
    #   name: "directv"
    #   display_name: "directv"
    # }
    #
    # item {
    #   id: 2
    #   name: "Qatar_airways_logo"
    #   display_name: "Qatar_airways_logo"
    # }
    def parse_text_labelmap_to_dict(self, text_labelmap):
        raw_items = [ri.strip() for ri in text_labelmap.split('item') if ri != '']
        parsed_items = [self.rawitem_to_parseditem(raw_item) for raw_item in raw_items]

        return parsed_items

    def split(self, df, group):
        data = namedtuple('data', ['filename', 'object'])
        gb = df.groupby(group)
        return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]

    def create_tf_example(self, group, path, labelmap):
        with tf.gfile.GFile(os.path.join(path, '{}'.format(group.filename)), 'rb') as fid:
            encoded_jpg = fid.read()
        encoded_jpg_io = io.BytesIO(encoded_jpg)
        image = Image.open(encoded_jpg_io)
        width, height = image.size

        filename = group.filename.encode('utf8')
        image_format = b'jpg'
        xmins = []
        xmaxs = []
        ymins = []
        ymaxs = []
        classes_text = []
        classes = []

        for index, row in group.object.iterrows():
            xmins.append(row['xmin'] / width)
            xmaxs.append(row['xmax'] / width)
            ymins.append(row['ymin'] / height)
            ymaxs.append(row['ymax'] / height)
            classes_text.append(row['class'].encode('utf8'))
            classes.append(self.class_text_to_int(row['class'], labelmap))

        tf_example = tf.train.Example(features=tf.train.Features(feature={
            'image/height': dataset_util.int64_feature(height),
            'image/width': dataset_util.int64_feature(width),
            'image/filename': dataset_util.bytes_feature(filename),
            'image/source_id': dataset_util.bytes_feature(filename),
            'image/encoded': dataset_util.bytes_feature(encoded_jpg),
            'image/format': dataset_util.bytes_feature(image_format),
            'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
            'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
            'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
            'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
            'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
            'image/object/class/label': dataset_util.int64_list_feature(classes),
        }))
        return tf_example

    def main_generate_tfrecords(self, _=None):
        if not os.path.isdir(self.path_output_data):
            os.mkdir(self.path_output_data)
        # with open(os.path.join(self.path_output_data, Training.LABEL_MAP)) as f:
        #     self.label_map = self.parse_text_labelmap_to_dict(f.read())
        for path_label_csv, image_directory in zip(self.paths_labels_csv, self.xml_directories):
            if '_test' not in image_directory and '_train' not in image_directory:
                raise ValueError('Los directorios de las images y XML deben tener el sufijo _test o _train.'
                                 'Por ejemplo nombre_carpeta_test o nombre_carpeta_train')
            else:
                name_tfrecord = 'test.record' if '_test' in image_directory else 'train.record'
                self.paths_tfrecords.append(os.path.join(self.path_output_data, name_tfrecord))
                writer = tf.python_io.TFRecordWriter(self.paths_tfrecords[-1])
                examples = pd.read_csv(path_label_csv)
                grouped = self.split(examples, 'filename')
                for group in grouped:
                    tf_example = self.create_tf_example(group,
                                                        os.path.join(os.getcwd(), image_directory),
                                                        self.label_map)
                    writer.write(tf_example.SerializeToString())

                writer.close()
                print('Successfully created the TFRecords: {}'.format(self.paths_tfrecords[-1]))

    def generate_tfrecords(self):
        self.main_generate_tfrecords()

    ####################################################################################################################
    # TRAIN
    ####################################################################################################################

    # python train.py --logtostderr
    #                 --train_dir=trainings/20190122_summit_ipgmediabrands/
    #                 --pipeline_config_path=trainings/20190122_summit_ipgmediabrands/ssd_mobilenet_v1_pets.config

    def generate_pipeline_config_from_template(self):
        with open(Training.PATH_TEMPLATE_PIPELINE_CONFIG) as f:
            txt_template_pipeline_config = f.read()

        txt_template_pipeline_config = txt_template_pipeline_config \
            .replace(Training.TEMPLATE_PIPELINE_CONFIG_REPLACE_NUM_CLASSES,
                     str(self.get_num_classes()))

        txt_template_pipeline_config = txt_template_pipeline_config \
            .replace(Training.TEMPLATE_PIPELINE_CONFIG_REPLACE_NUM_STEPS,
                     str(self.training_num_steps))

        txt_template_pipeline_config = txt_template_pipeline_config \
            .replace(Training.TEMPLATE_PIPELINE_CONFIG_REPLACE_TRAIN_INPUT_READER,
                     os.path.join(self.path_output_data, Training.TRAIN_RECORD))

        txt_template_pipeline_config = txt_template_pipeline_config \
            .replace(Training.TEMPLATE_PIPELINE_CONFIG_REPLACE_TRAIN_LABEL_MAP_PATH,
                     os.path.join(self.path_output_data, Training.LABEL_MAP))

        txt_template_pipeline_config = txt_template_pipeline_config \
            .replace(Training.TEMPLATE_PIPELINE_CONFIG_REPLACE_EVAL_INPUT_READER,
                     os.path.join(self.path_output_data, Training.TEST_RECORD))

        txt_template_pipeline_config = txt_template_pipeline_config \
            .replace(Training.TEMPLATE_PIPELINE_CONFIG_REPLACE_EVAL_LABEL_MAP_PATH,
                     os.path.join(self.path_output_data, Training.LABEL_MAP))

        if not os.path.isdir(Training.BASE_PATH_OUTPUT):
            os.mkdir(Training.BASE_PATH_OUTPUT)
        if not os.path.isdir(self.path_output_data):
            os.mkdir(self.path_output_data)
        with open(self.config_train.pipeline_config_path, 'w') as fw:
            fw.write(txt_template_pipeline_config)

    @tf.contrib.framework.deprecated(None, 'Use object_detection/model_main.py.')
    def main_train(self, _=None):
        tf.logging.set_verbosity(tf.logging.INFO)
        assert self.config_train.train_dir, '`train_dir` is missing.'
        if self.config_train.task == 0:
            tf.gfile.MakeDirs(self.config_train.train_dir)
        if self.config_train.pipeline_config_path:
            self.generate_pipeline_config_from_template()
            configs = config_util.get_configs_from_pipeline_file(
                self.config_train.pipeline_config_path)
            if self.config_train.task == 0:
                tf.gfile.Copy(self.config_train.pipeline_config_path,
                              os.path.join(self.config_train.train_dir, 'pipeline.config'),
                              overwrite=True)
        else:
            configs = config_util.get_configs_from_multiple_files(
                model_config_path=self.config_train.model_config_path,
                train_config_path=self.config_train.train_config_path,
                train_input_config_path=self.config_train.input_config_path)
            if self.config_train.task == 0:
                for name, config in [('model.config', self.config_train.model_config_path),
                                     ('train.config', self.config_train.train_config_path),
                                     ('input.config', self.config_train.input_config_path)]:
                    tf.gfile.Copy(config, os.path.join(self.config_train.train_dir, name),
                                  overwrite=True)

        model_config = configs['model']
        train_config = configs['train_config']
        input_config = configs['train_input_config']

        model_fn = functools.partial(
            model_builder.build,
            model_config=model_config,
            is_training=True)

        def get_next(config):
            return dataset_builder.make_initializable_iterator(
                dataset_builder.build(config)).get_next()

        create_input_dict_fn = functools.partial(get_next, input_config)

        env = json.loads(os.environ.get('TF_CONFIG', '{}'))
        cluster_data = env.get('cluster', None)
        cluster = tf.train.ClusterSpec(cluster_data) if cluster_data else None
        task_data = env.get('task', None) or {'type': 'master', 'index': 0}
        task_info = type('TaskSpec', (object,), task_data)

        # Parameters for a single worker.
        ps_tasks = 0
        worker_replicas = 1
        worker_job_name = 'lonely_worker'
        task = 0
        is_chief = True
        master = ''

        if cluster_data and 'worker' in cluster_data:
            # Number of total worker replicas include "worker"s and the "master".
            worker_replicas = len(cluster_data['worker']) + 1
        if cluster_data and 'ps' in cluster_data:
            ps_tasks = len(cluster_data['ps'])

        if worker_replicas > 1 and ps_tasks < 1:
            raise ValueError('At least 1 ps task is needed for distributed training.')

        if worker_replicas >= 1 and ps_tasks > 0:
            # Set up distributed training.
            server = tf.train.Server(tf.train.ClusterSpec(cluster), protocol='grpc',
                                     job_name=task_info.type,
                                     task_index=task_info.index)
            if task_info.type == 'ps':
                server.join()
                return

            worker_job_name = '%s/task:%d' % (task_info.type, task_info.index)
            task = task_info.index
            is_chief = (task_info.type == 'master')
            master = server.target

        graph_rewriter_fn = None
        if 'graph_rewriter_config' in configs:
            graph_rewriter_fn = graph_rewriter_builder.build(configs['graph_rewriter_config'],
                                                             is_training=True)

        trainer.train(create_input_dict_fn,
                      model_fn,
                      train_config,
                      master,
                      task,
                      self.config_train.num_clones,
                      worker_replicas,
                      self.config_train.clone_on_cpu,
                      ps_tasks,
                      worker_job_name,
                      is_chief,
                      self.config_train.train_dir,
                      graph_hook_fn=graph_rewriter_fn,
                      save_summaries_secs=self.config_train.save_summaries_secs)

    def train(self):
        self.main_train()

    ####################################################################################################################
    # EXPORT INFERENCE GRAPH
    ####################################################################################################################

    # python original_export_inference_graph.py \
    #     --input_type image_tensor \
    #     --pipeline_config_path trainings/20190122_summit_ipgmediabrands/ssd_mobilenet_v1_pets.config \
    #     --trained_checkpoint_prefix trainings/20190122_summit_ipgmediabrands/model.ckpt-8967 \
    #     --output_directory inference_graphs/20190122_summit_ipgmediabrands

    def export_inference_graph(self):
        pipeline_config = pipeline_pb2.TrainEvalPipelineConfig()
        if not os.path.isdir(os.path.dirname(self.config_export_inference_graph.output_directory)):
            os.mkdir(os.path.dirname(self.config_export_inference_graph.output_directory))
        if not os.path.isdir(self.config_export_inference_graph.output_directory):
            os.mkdir(self.config_export_inference_graph.output_directory)
        with tf.gfile.GFile(self.config_export_inference_graph.pipeline_config_path, 'r') as f:
            text_format.Merge(f.read(), pipeline_config)
        text_format.Merge(self.config_export_inference_graph.config_override, pipeline_config)
        if self.config_export_inference_graph.input_shape:
            input_shape = [int(dim) if dim != '-1' else None
                           for dim in self.config_export_inference_graph.input_shape.split(',')]
        else:
            input_shape = None
        exporter.export_inference_graph(self.config_export_inference_graph.input_type,
                                        pipeline_config,
                                        self.config_export_inference_graph.trained_checkpoint_prefix,
                                        self.config_export_inference_graph.output_directory,
                                        input_shape=input_shape,
                                        write_inference_graph=self.config_export_inference_graph.write_inference_graph)
