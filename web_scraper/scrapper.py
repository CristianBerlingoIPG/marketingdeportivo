import os
import re
import requests
import mimetypes
import urllib.parse

from bs4.element import Tag
from bs4 import BeautifulSoup
from requests.compat import urljoin


class Scraper:

    regex_is_valid_url = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$',
        re.IGNORECASE
    )

    def __init__(self, url):
        self.url = url
        self.scraper = self.get_scraper()

    def get_scraper(self, url=None):
        url = url if url is not None else self.url

        return BeautifulSoup(self.get_url_content(url), 'html.parser')

    def get_url_content(self, url=None):
        url = url if url is not None else self.url

        return requests.get(url).content.decode('utf-8')

    @staticmethod
    def get_file_name_from_url(file_url):
        return os.path.basename(urllib.parse.urlparse(file_url).path)

    @staticmethod
    def is_valid_url(_url):
        return Scraper.regex_is_valid_url.search(_url) is not None

    @staticmethod
    def download_file(path_output, file_url: str, file_name=None, alternative_base_url=None):
        if not Scraper.is_valid_url(file_url) and not file_url.startswith('/'):
            raise ValueError('La URL {} no es valida'.format(file_url))
        try:
            response = None
            if not file_url.startswith('/'):
                response = requests.get(file_url)
            else:
                if alternative_base_url is not None:
                    print('Tratando {} como ruta relativa de {}'.format(file_url, alternative_base_url))
                    response = requests.get(urljoin(alternative_base_url, file_url))

            path_file = os.path.join(path_output,
                                     file_name if file_name is not None
                                     else Scraper.get_file_name_from_url(file_url))

            if response is not None:
                with open(path_file, 'wb') as f:
                    f.write(response.content)
        except Exception as ex_request_get:
            print('Ocurrio una excepcion durante la descarga de {}:\n{}'.format(file_url, ex_request_get))

    @staticmethod
    def download_files(path_output, file_urls):
        qty_file_urls = len(file_urls)
        for n_file, file_url in enumerate(file_urls):
            try:
                Scraper.download_file(path_output, file_url)
            except Exception as ex_download_file:
                print('\nOcurrio una excepcion con la url [{}/{}]:'
                      '\n{}'.format(n_file + 1, qty_file_urls, ex_download_file))

    @staticmethod
    def default_images_filter(img: Tag):
        guessed_type = mimetypes.guess_type(img['src'])[0] if img.has_attr('src') else None
        is_image_type = guessed_type.split('/')[0] == 'image' if guessed_type is not None else False

        return is_image_type

    def find_all_images(self, _filter=None, use_default_filter=True):
        images = self.scraper.find_all('img')

        _filters = []
        if _filter:
            _filters.append(_filter)
        if use_default_filter:
            _filters.append(Scraper.default_images_filter)

        images = [img for img in images if all(_f(img) for _f in _filters)]

        return images

    def save_all_images(self, path_output, _filter=None):
        images = self.find_all_images(_filter=_filter)
        src_images = [img['src'] for img in images]
        self.download_files(path_output, src_images)


if __name__ == '__main__':
    test_url = 'https://www.ole.com.ar/estudiantes'
    sp = Scraper(test_url)
    sp.save_all_images('/home/enzoscocco/Pictures/save_images_scraper')
    print('Termino')
